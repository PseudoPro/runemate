package com.sudo.v2.antiban;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.base.AntiBan;
import com.sudo.v2.base.SudoBot;
import com.runemate.game.api.hybrid.input.Mouse;
import com.runemate.game.api.hybrid.local.hud.InteractablePoint;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;

/**
 * Created by SudoPro on 3/5/2016.
 */
public class MoveCameraMiddleMouse extends AntiBan {

    @Override
    public void execute(SudoBot bot) {
        player = Players.getLocal();

        if (player != null) {
            // Set mouse in center
            Methods.updateAntiBanTask("Turning camera with Middle-Mouse Button", bot);
            player.hover();

            // Move camera with mouse
            Mouse.press(Mouse.Button.WHEEL);
            Mouse.move(new InteractablePoint((int) (Mouse.getPosition().getX() - 400 + (Math.random() * 800)), (int) (Mouse.getPosition().getY() - 400 + (Math.random() * 800))));
            Execution.delayWhile(Mouse::isMoving, 200, 750);
            Mouse.release(Mouse.Button.WHEEL);
        }
    }
}
