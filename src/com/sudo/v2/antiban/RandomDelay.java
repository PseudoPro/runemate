package com.sudo.v2.antiban;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.spectre.common.helpers.SudoTimer;
import com.sudo.v2.base.AntiBan;
import com.sudo.v2.base.SudoBot;
import com.runemate.game.api.hybrid.region.Players;

/**
 * Created by SudoPro on 3/5/2016.
 */
public class RandomDelay extends AntiBan {
    @Override
    public void execute(SudoBot bot) {
        if(Players.getLocal() != null) {
            SudoTimer timer = new SudoTimer(3000, 15000);

            Methods.updateAntiBanTask("Performing a random Delay for " + timer.getRemainingTime() / 1000 + " seconds.", bot);
            Methods.updateCurrentTask("Performing a random Delay for " + timer.getRemainingTime() / 1000 + " seconds.", bot);
            timer.start();
            while(timer.getRemainingTime() > 0);


        }
    }
}
