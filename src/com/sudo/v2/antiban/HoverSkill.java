package com.sudo.v2.antiban;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.base.AntiBan;
import com.sudo.v2.base.SudoBot;
import com.runemate.game.api.hybrid.input.Mouse;
import com.runemate.game.api.hybrid.local.hud.interfaces.Interfaces;
import com.runemate.game.api.script.Execution;

/**
 * Created by SudoPro on 3/4/2016.
 *
 * Hover over the skill icon at the top of the screen
 */
public class HoverSkill extends AntiBan {
    @Override
    public void execute(SudoBot bot) {
        if(Interfaces.getAt(1213, 52) != null) {
            Methods.updateAntiBanTask("Hovering over Skill Interface", bot);
            Interfaces.getAt(1213, 52).hover();
            Execution.delayWhile(Mouse::isMoving, 400, 1500);
        }
    }
}
