package com.sudo.v2.antiban;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.base.AntiBan;
import com.sudo.v2.base.SudoBot;
import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.local.Camera;
import com.runemate.game.api.hybrid.local.hud.Menu;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.script.Execution;

/**
 * Created by SudoPro on 3/4/2016.
 */
public class HoverRandomNpc extends AntiBan {
    @Override
    public void execute(SudoBot bot) {
        if(bot._player != null) {
            //Area aroundPlayer = DivMethods.getSurroundingSquare(player.getPosition(), 10);
            Area aroundPlayer = new Area.Circular(bot._player.getPosition(), 10);
            Npc randomNpc = Npcs.newQuery().within(aroundPlayer).results().random();
            if (randomNpc != null) {
                Methods.updateAntiBanTask("Hovering over a NPC: " + randomNpc.getName(), bot);
                if (randomNpc.isVisible()) {
                    randomNpc.hover();
                    Menu.open();
                }
                else {
                    Camera.concurrentlyTurnTo(randomNpc);
                    Execution.delay(200, 400);
                    randomNpc.hover();
                    Menu.open();
                }
            }
        }
    }
}
