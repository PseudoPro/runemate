package com.sudo.v2.antiban;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.base.AntiBan;
import com.sudo.v2.base.SudoBot;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.Camera;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;

/**
 * Created by SudoPro on 3/4/2016.
 */
public class HoverRandomPlayer extends AntiBan {
    @Override
    public void execute(SudoBot bot) {
        player = Players.getLocal();

        if(player != null) {
            Area aroundPlayer = new Area.Circular(player.getPosition(), 10);

            Player rndPlayer = Players.newQuery().within(aroundPlayer).results().random();
            if (rndPlayer != null) {
                Methods.updateAntiBanTask("Hovering over random player", bot);
                if (rndPlayer.isVisible())
                    rndPlayer.hover();
                else {
                    Camera.concurrentlyTurnTo(rndPlayer);
                    Execution.delay(100);
                    rndPlayer.hover();
                }
            }
        }
    }
}
