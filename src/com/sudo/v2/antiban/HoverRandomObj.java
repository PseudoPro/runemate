package com.sudo.v2.antiban;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.base.AntiBan;
import com.sudo.v2.base.SudoBot;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.local.Camera;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.script.Execution;

/**
 * Created by SudoPro on 3/4/2016.
 */
public class HoverRandomObj extends AntiBan {

    @Override
    public void execute(SudoBot bot) {

        if(player != null) {
            Area aroundPlayer = new Area.Circular(player.getPosition(), 10);

            GameObject randomObj = GameObjects.newQuery().within(aroundPlayer).results().random();
            if (randomObj != null) {
                if(randomObj.getDefinition().getName() != null) {
                    Methods.updateAntiBanTask("Hoving over random Game Object: " + randomObj.getDefinition().getName(), bot);
                    if (randomObj.isVisible())
                        randomObj.hover();
                    else {
                        Camera.concurrentlyTurnTo(randomObj);
                        Execution.delay(100);
                        randomObj.hover();
                    }
                }
            }
        }
    }
}
