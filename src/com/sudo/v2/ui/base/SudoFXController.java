package com.sudo.v2.ui.base;

import com.sudo.v2.antiban.*;
import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.base.SudoBot;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Slider;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Java FX GUI Controller Abstract Class
 *
 * Other SudoBot's setting GUIs will extends this class
 */
public abstract class SudoFXController implements Initializable {
    private SudoBot bot;

    //<editor-fold desc="antiban Tab">
    @FXML
    public CheckBox AntiBan_CB, AntiBan_HoverRndObj_CB, AntiBan_HoverRndNpc_CB, AntiBan_HoverRndPlyr_CB, AntiBan_HoverSkill_CB,
            AntiBan_RndDelay_CB, AntiBan_RightClickRndNpc_CB, AntiBan_RightClickRndPlyr_CB, AntiBan_CameraMiddleMouse_CB;

    @FXML
    public Slider AntiBanMin_Slider, AntiBanMax_Slider;

    @FXML
    public Text AntiBanSlider_Text;
    //</editor-fold>

    //<editor-fold desc="Settings Tab">
    @FXML
    public CheckBox CameraMiddleMouse_CB, Anagogic_CB, run_CB;
    //</editor-fold>

    public SudoFXController(SudoBot bot){
        this.bot = bot;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        AntiBan_CB.setOnAction(getAntiBan_CBEvent());

        // Handle antiban Minimum value slider
        AntiBanMin_Slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.doubleValue() > AntiBanMax_Slider.getValue()) {
                AntiBanMax_Slider.setValue(newValue.doubleValue());
                bot._abHandler.setMaxTime(newValue.intValue());
            }
            AntiBanSlider_Text.setText(Integer.toString(newValue.intValue()) + " - " + Integer.toString(bot._abHandler.getMaxTime()) + " seconds");
            bot._abHandler.setMinTime(newValue.intValue());
        });

        // Handle antiban Maximum value slider
        AntiBanMax_Slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.doubleValue() < AntiBanMin_Slider.getValue()) {
                AntiBanMin_Slider.setValue(newValue.doubleValue());
                bot._abHandler.setMinTime(newValue.intValue());
            }
            AntiBanSlider_Text.setText(Integer.toString(bot._abHandler.getMinTime()) + " - " + Integer.toString(newValue.intValue()) + " seconds");
            bot._abHandler.setMaxTime(newValue.intValue());
        });
    }

    public void getStart_BTEvent() {
        // Set Camera to use middle mouse or arrow keys
        bot._useMiddleMouseCamera = CameraMiddleMouse_CB.isSelected();
        Methods.debug("UseMiddleMouse: " + bot._useMiddleMouseCamera);

        bot._checkAnagogic = Anagogic_CB.isSelected();

        // Configure AntiBans
        if(AntiBan_CB.isSelected()) {
            bot._useAntiBan = true;
            bot._abHandler.setMinTime((int)AntiBanMin_Slider.getValue() * 1000);
            bot._abHandler.setMaxTime((int)AntiBanMax_Slider.getValue() * 1000);
            bot._abHandler.startTimer();
            addAntiBan();
        }
        else {
            bot._antibans.clear();
            bot._useAntiBan = false;
        }

        AntiBanMin_Slider.setDisable(true);
        AntiBanMax_Slider.setDisable(true);

        Methods.debug("antiban: " + bot._useAntiBan);

        bot._guiWait = false;
        Methods.debug("GuiWait: " + bot._guiWait);

        bot._enableRun = run_CB.isSelected();
        Methods.debug("Run Enabled: " + bot._enableRun);

        bot.changeProperty(2);
    }

    // Enable or diable the start button based on if required settings are selected to start bot
    private EventHandler<ActionEvent> getAntiBan_CBEvent(){
        return event -> {
            if(AntiBan_CB.isSelected()) {
                AntiBan_HoverRndObj_CB.setDisable(false);
                AntiBan_HoverRndNpc_CB.setDisable(false);
                AntiBan_HoverRndPlyr_CB.setDisable(false);
                AntiBan_HoverSkill_CB.setDisable(false);
                AntiBan_RndDelay_CB.setDisable(false);
                AntiBan_RightClickRndNpc_CB.setDisable(false);
                AntiBan_RightClickRndPlyr_CB.setDisable(false);
                AntiBan_CameraMiddleMouse_CB.setDisable(false);
                AntiBanMin_Slider.setDisable(false);
                AntiBanMax_Slider.setDisable(false);
            }
            else {
                AntiBan_HoverRndObj_CB.setDisable(true);
                AntiBan_HoverRndNpc_CB.setDisable(true);
                AntiBan_HoverRndPlyr_CB.setDisable(true);
                AntiBan_HoverSkill_CB.setDisable(true);
                AntiBan_RndDelay_CB.setDisable(true);
                AntiBan_RightClickRndNpc_CB.setDisable(true);
                AntiBan_RightClickRndPlyr_CB.setDisable(true);
                AntiBan_CameraMiddleMouse_CB.setDisable(true);
                AntiBanMin_Slider.setDisable(true);
                AntiBanMax_Slider.setDisable(true);
            }
        };
    }

    public void addAntiBan(){
        bot._antibans.clear();
        if(AntiBan_HoverRndObj_CB.isSelected())
            bot._antibans.add(new HoverRandomObj());

        if(AntiBan_HoverRndNpc_CB.isSelected())
            bot._antibans.add(new HoverRandomNpc());

        if(AntiBan_HoverRndPlyr_CB.isSelected())
            bot._antibans.add(new HoverRandomPlayer());

        if(AntiBan_HoverSkill_CB.isSelected())
            bot._antibans.add(new HoverSkill());

        if(AntiBan_RndDelay_CB.isSelected())
            bot._antibans.add(new RandomDelay());

        if(AntiBan_RightClickRndNpc_CB.isSelected())
            bot._antibans.add(new RightClickRandomNpc());

        if(AntiBan_RightClickRndPlyr_CB.isSelected())
            bot._antibans.add(new RightClickRandomPlayer());

        if(AntiBan_CameraMiddleMouse_CB.isSelected())
            bot._antibans.add(new MoveCameraMiddleMouse());

        // Change the antiban interval from seconds to milliseconds
        bot._abHandler.setMinTime(bot._abHandler.getMinTime() * 1000);
        bot._abHandler.setMaxTime(bot._abHandler.getMaxTime() * 1000);
    }
}
