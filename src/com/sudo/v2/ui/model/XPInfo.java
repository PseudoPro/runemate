package com.sudo.v2.ui.model;

import com.runemate.game.api.hybrid.local.Skill;
import com.runemate.game.api.hybrid.util.StopWatch;
import com.runemate.game.api.hybrid.util.calculations.CommonMath;

import java.util.LinkedHashMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by SudoPro on 5/16/2016.
 */
public class XPInfo {
    private LinkedHashMap<String, String> map;

    public Skill skillType;
    public int startLvl;
    public int startXP;
    public int currentLvl;
    public int currentXP;

    public XPInfo(Skill skillType){
        this.skillType = skillType;
        startLvl = skillType.getCurrentLevel();
        startXP = skillType.getExperience();
        currentLvl = startLvl;
        currentXP = startXP;
    }

    public void update(){
        currentLvl = skillType.getCurrentLevel();
        currentXP = skillType.getExperience();
    }

    public int getLevelsGained(){
        return currentLvl - startLvl;
    }

    public int getGainedXP() {
        return currentXP - startXP;
    }

    public int getXPHour(StopWatch watch){
        return (int) CommonMath.rate(TimeUnit.HOURS, watch.getRuntime(), getGainedXP());
    }

    public LinkedHashMap<String, String> getMap(StopWatch stopWatch) {
        map = new LinkedHashMap<>();
        if (currentXP > startXP) {
            map.put(skillType.toString() + " Level: ", Integer.toString(currentLvl) + " (" + Integer.toString(getLevelsGained()) + " Gained)");
            map.put(skillType.toString() + " XP Gained: ", Integer.toString(getGainedXP()) + " (" + Integer.toString(getXPHour(stopWatch)) + " per hour)");
        }
        return map;
    }
}