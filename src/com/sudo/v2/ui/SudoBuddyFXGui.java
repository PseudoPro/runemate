package com.sudo.v2.ui;

import com.sudo.v2.base.SudoBot;
import com.runemate.game.api.hybrid.util.Resources;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Java FX GUI for SudoBuddy
 */
public class SudoBuddyFXGui extends GridPane implements Initializable {

    private SudoBot bot;

    //<editor-fold desc="FXML">
    @FXML
    private ImageView Logo_IV;

    @FXML
    private Hyperlink SudoBuddy_HL;

    @FXML
    private Button Login_BT, Cancel_BT;

    @FXML
    private TextField Username_TF;

    @FXML
    private PasswordField Password_PF;
    //</editor-fold>

    public SudoBuddyFXGui(SudoBot bot){
        this.bot = bot;

        // Load the fxml file using RuneMate's resources class.
        FXMLLoader loader = new FXMLLoader();

        Future<InputStream> stream = bot.getPlatform().invokeLater(() -> Resources.getAsStream("com/sudo/v2/ui/SudoBuddy.fxml"));

        // Set this class as root and controller of the fxml file
        loader.setController(this);
        loader.setRoot(this);

        try {
            loader.load(stream.get());
        } catch (IOException | InterruptedException | ExecutionException e) {
            System.err.println("Error loading SudoBuddy GUI");
            e.printStackTrace();
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Logo_IV.setImage(new Image(Resources.getAsStream("com/sudo/v2/ui/SudoBuddy_2.png")));

        Login_BT.setOnAction(getLogin_BTEvent());
        Cancel_BT.setOnAction(getCancel_BTEvent());

        setVisible(true);
    }

    private EventHandler<ActionEvent> getLogin_BTEvent(){
        return event -> {

        };
    }

    private EventHandler<ActionEvent> getCancel_BTEvent(){
        return event -> {
            Password_PF.clear();
            bot.changeProperty(2);  // Set GUI back to Info GUI
        };
    }

}
