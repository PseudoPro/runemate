package com.sudo.v2.spectre.common.tasks;

import com.sudo.v2.base.SudoTask;
import com.sudo.v2.interfaces.Task;

import java.util.function.Supplier;

/**
 * DynamicTask SudoTask
 */
public final class DynamicTask extends SudoTask {
    private final Task _loopFunction;
    private final Supplier<Boolean> _taskCompletedCheck;

    public DynamicTask(Task loopFunction, Supplier<Boolean> taskCompletedCheck) {
        _loopFunction = loopFunction;
        _taskCompletedCheck = taskCompletedCheck;
    }

    @Override
    public void Loop() {
        if (_taskCompletedCheck.get()) {
            Complete();
            return;
        }

        _loopFunction.invoke();
    }
}
