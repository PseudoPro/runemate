package com.sudo.v2.spectre.common.tasks;

import com.sudo.v2.spectre.common.helpers.SudoTravel;
import com.sudo.v2.base.SudoTask;
import com.sudo.v2.spectre.common.helpers.Common;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.location.Area;

/**
 * Generic SudoTask, TravelTask
 *
 * Traverse player to a location
 */
public class TravelTask extends SudoTask {

    private Area _area;

    public TravelTask(Area area) {
        super();

        _area = area;
    }

    @Override
    public void Loop() {

        Player player = Common.GetPlayer();

        if (player != null && _area.contains(player)) {
            Complete();
            return;
        }

        SudoTravel.goTowards(_area);
    }
}
