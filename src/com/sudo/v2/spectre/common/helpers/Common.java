package com.sudo.v2.spectre.common.helpers;

import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.Camera;
import com.runemate.game.api.hybrid.region.Players;

/**
 * common UpdateUI used by SudoBots
 */
public class Common {

    private static Player _player;

//    public static void goTowards(Player player, Area area) {
//        // Get random coordinate in the Area
//        Coordinate coordinate = area.getRandomCoordinate();
//
//        if(coordinate.isVisible() && coordinate.isReachable() && player.distanceTo(coordinate) < 12) {
//            Camera.turnTo(coordinate);
//            coordinate.interact("Walk here");
//        } else {
//            final RegionPath rp = RegionPath.buildTo(coordinate);
//            if(rp != null && rp.isValid() && coordinate.isReachable()) {
//                common.AngleCameraUp();
//                if (rp.step(true)) {
//                    System.out.println("Walking with Region Path");
//                    Execution.delayWhile(player::isMoving, 1000, 4000);
//                }
//            }else{
//                final Path p = Traversal.getDefaultWeb().getPathBuilder().buildTo(coordinate);
//                if (p != null) {
//                    if (p.step(true)) {
//                        System.out.println("Walking with Web Path");
//                        Execution.delayWhile(player::isMoving, 2000, 4000);
//                        Camera.concurrentlyTurnTo(coordinate);
//                    }
//                } else {
//                    final BresenhamPath bp = BresenhamPath.buildTo(coordinate);
//
//                    if (bp != null) {
//                        if (bp.step(true)) {
//                            System.out.println("Walking with Bresenham Path");
//                            Execution.delayWhile(player::isMoving, 1000, 4000);
//                        }
//                    }
//                }
//            }
//        }
//    }

    public static void AngleCameraUp() {
        double pitch = .56 + (Math.random() / 10);
        Camera.turnTo(pitch);
    }

    public static Player GetPlayer() {
        if (_player == null)
            _player = Players.getLocal();

        return _player;
    }
}
