package com.sudo.v2.spectre.common.helpers;

import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.cache.sprites.Sprite;
import com.runemate.game.api.hybrid.input.Keyboard;
import com.runemate.game.api.hybrid.input.Mouse;
import com.runemate.game.api.hybrid.local.hud.InteractableRectangle;
import com.runemate.game.api.hybrid.local.hud.Menu;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.hybrid.queries.results.SpriteItemQueryResults;
import com.runemate.game.api.script.Execution;

import static java.awt.event.KeyEvent.VK_SHIFT;

/**
 * Created by SudoPro on 7/10/2016.
 */
public class SudoInventory {

    public static boolean drop(String name) {
        SpriteItem item = Inventory.newQuery().names(name).actions("Drop").results().first();
        if (item != null) {
            if (!Menu.isOpen()) {
                if (item.hover()) {
                    if (Menu.open()) {
                        Execution.delayUntil(Menu::isOpen, 400, 600);
                        if (Menu.contains("Drop")) {
                            if (Menu.getItem("Drop").hover())
                                if(Menu.getItem("Drop").click())
                                    return true;
                                else return false;
                            else return false;
                        } else {
                            Menu.close();
                            return false;
                        }
                    } else return false;
                }
            } else {
                if (Menu.contains("Drop")) {
                    if (Menu.getItem("Drop").click()) {
                        return true;
                    } else return false;
                } else {
                    Menu.close();
                    return false;
                }
            }
        }
        return false;
    }

    public static boolean drop(SpriteItem item) {
        if (item != null) {
            if (Inventory.contains(item.getDefinition().getName())) {
                if (!Menu.isOpen()) {
                    if (item.hover()) {
                        if (Menu.open()) {
                            Execution.delayUntil(Menu::isOpen, 400, 600);
                            if (Menu.contains("Drop")) {
                                if (Menu.getItem("Drop").hover())
                                    if(Mouse.click(Mouse.Button.LEFT))
                                        return true;
                                    else return false;
                                else return false;
                            } else {
                                Menu.close();
                                return false;
                            }
                        } else return false;
                    } else return false;
                } else {
                    if (Menu.contains("Drop")) {
                        if (Menu.getItem("Drop").click()) {
                            return true;
                        } else return false;
                    } else {
                        Menu.close();
                        return false;
                    }
                }
            } else
                return false;
        } else
            return false;
    }

    public static boolean dropAll(String... name) {
        if (Inventory.containsAnyOf(name)) {
            SpriteItemQueryResults results = Inventory.newQuery().names(name).actions("Drop").results();

            for (int i = 0; i < results.size(); ) {
                if (drop(results.get(i))) {
                    i++;
                }
            }
            return true;
        } else
            return false;
    }

    public static boolean dropAllContainsLower(String name) {
        SudoTimer timer = new SudoTimer(30000, 60000);
        timer.start();
        SpriteItemQueryResults results; // = Inventory.newQuery().filter(i -> i.getDefinition().getName().toLowerCase().contains(name)).actions("Drop").results();

        do{
            results = Inventory.newQuery().filter(i -> i.getDefinition().getName().toLowerCase().contains(name)).actions("Drop").results();
            if (results != null) {
                for (int i = 0; i < results.size(); ) {
                    if (timer.getRemainingTime() > 0) {
                        if (drop(results.get(i))) {
                            i++;
                        }
                    } else {
                        Methods.debug("dropAllContainsLower: Returning false");
                        return false;
                    }
                }

//            for(SpriteItem item : results){
//                if(timer.getRemainingTime() > 0)
//                    drop(item);
//                else {
//                    return false;
//                }
//            }
            }
        } while (results.size() > 0 && timer.getRemainingTime() > 0);

        if(Inventory.newQuery().filter(i -> i.getDefinition().getName().toLowerCase().contains(name)).actions("Drop").results().size() > 0)
            return false;
        else
            return false;
    }
}