package com.sudo.v2.spectre.common.helpers;

import com.runemate.game.api.hybrid.entities.Actor;
import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;

import java.util.HashSet;

/**
 * SudoPlayer Class
 *
 * Abstract methods for additional information for the play
 */
public class SudoPlayer {
    public static boolean isInAnimation(HashSet<Integer> anims) {
        int anim = Players.getLocal().getAnimationId();

        if (anim == -1) {
            Execution.delayUntil(() -> anims.contains(Players.getLocal().getAnimationId()), 2000, 2500);
            anim = Players.getLocal().getAnimationId();
        }

        if (anims.contains(anim))
            return true;
        else
            return false;
    }

    public static boolean isInteractingWith(Npc npc, Player player){
        if(npc != null && player != null) {
            Actor target = player.getTarget();
            if(target != null){
                if(target.equals(npc))
                    return true;
                else
                    return false;
            }
            else
                return false;
        }
        else
            return false;
    }
}
