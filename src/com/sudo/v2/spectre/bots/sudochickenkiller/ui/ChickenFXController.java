package com.sudo.v2.spectre.bots.sudochickenkiller.ui;

import com.sudo.v2.spectre.bots.sudochickenkiller.SudoChickenKiller;
import com.sudo.v2.ui.base.SudoFXController;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Java FX Controller for the ChickenFXGui class
 */
public class ChickenFXController extends SudoFXController implements Initializable {

    //private ObjectProperty<Node> property;

    private SudoChickenKiller bot;

    //<editor-fold desc="ChickenKiller Tab">
    @FXML
    private CheckBox Feather_CB;

    @FXML
    private Button start_BT;

    @FXML
    private Slider Radius_Slider, DropsMin_Slider, DropsMax_Slider;

    @FXML
    private Text Radius_Text, DropsCount_Text;

    @FXML
    private Label Prompt_L;
    //</editor-fold>


    public ChickenFXController(SudoChickenKiller bot){
        super(bot);
        this.bot = bot;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Initialize Super
        super.initialize(location, resources);

        start_BT.setOnAction(getStartButtonAction());

        // Handle Radius to scan chicken
        Radius_Slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            Radius_Text.setText(Integer.toString(newValue.intValue()));
            bot.radius = newValue.intValue();
        });

        // Handle amount of Feather drop locations until bot will pick up feathers
        DropsMin_Slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.doubleValue() > DropsMax_Slider.getValue()) {
                DropsMax_Slider.setValue(newValue.doubleValue());
                //bot.maxFeatherLocationsCount = newValue.intValue();
            }
            DropsCount_Text.setText(Integer.toString(newValue.intValue()) + " - " + (int)DropsMax_Slider.getValue());
            //bot.minFeatherLocationsCount = newValue.intValue();
        });

        // Handle amount of Feather drop locations until bot will pick up feathers
        DropsMax_Slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.doubleValue() < DropsMin_Slider.getValue()) {
                DropsMin_Slider.setValue(newValue.doubleValue());
                //bot.minFeatherLocationsCount = newValue.intValue();
            }
            DropsCount_Text.setText((int)DropsMin_Slider.getValue() + " - " + Integer.toString(newValue.intValue()));
            //bot.maxFeatherLocationsCount = newValue.intValue();
        });
    }


    public EventHandler<ActionEvent> getStartButtonAction(){
        return event -> {

            try {
                // Set grab feathers boolean
                bot.grabFeathers = Feather_CB.isSelected();
                bot.radius = (int)Radius_Slider.getValue();

                bot.minFeatherLocationsCount = (int)DropsMin_Slider.getValue();
                bot.maxFeatherLocationsCount = (int)DropsMax_Slider.getValue();

                start_BT.textProperty().set("Resume");
                Prompt_L.textProperty().set(" ");

                super.getStart_BTEvent();

            }catch(Exception e){
                e.printStackTrace();
            }
        };
    }



}
