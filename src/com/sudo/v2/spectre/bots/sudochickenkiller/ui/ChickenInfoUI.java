package com.sudo.v2.spectre.bots.sudochickenkiller.ui;

import com.sudo.v2.spectre.bots.sudochickenkiller.SudoChickenKiller;
import com.sudo.v2.ui.base.InfoUIController;
import com.runemate.game.api.hybrid.util.Resources;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Info GUI for the SudoKFC Bot
 *
 * This will show various live stats on the bot
 */
public class ChickenInfoUI extends InfoUIController implements Initializable {

    @FXML
    ImageView Logo_IV, Title_IV;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        Logo_IV.setImage(new Image(Resources.getAsStream("com/sudo/v2/ui/SudoBuddy_Icon2.png")));
        Title_IV.setImage(new Image(Resources.getAsStream("com/sudo/v2/spectre/bots/sudochickenkiller/ui/resources/SudoKFC.png")));
    }

    public ChickenInfoUI(SudoChickenKiller bot) {
        super(bot);

        // Load the fxml file using RuneMate's resources class.
        FXMLLoader loader = new FXMLLoader();

        Future<InputStream> stream = bot.getPlatform().invokeLater(() -> Resources.getAsStream("com/sudo/v2/spectre/bots/sudochickenkiller/ui/ChickenInfo.fxml"));

        // Set this class as root and controller of the fxml file
        loader.setController(this);
        loader.setRoot(this);

        try {
            loader.load(stream.get());
        } catch (IOException | InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

    }
}