package com.sudo.v2.spectre.bots.sudochickenkiller;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.spectre.bots.sudochickenkiller.tasks.AttackChickenTask;
import com.sudo.v2.spectre.bots.sudochickenkiller.tasks.GoToChickenTask;
import com.sudo.v2.spectre.bots.sudochickenkiller.tasks.PickUpFeatherTask;
import com.sudo.v2.spectre.bots.sudochickenkiller.ui.ChickenFXGui;
import com.sudo.v2.spectre.bots.sudochickenkiller.ui.ChickenInfoUI;
import com.sudo.v2.ui.Info;
import com.sudo.v2.ui.SudoBuddyFXGui;
import com.sudo.v2.ui.model.XPInfo;
import com.sudo.v2.base.SudoBot;
import com.sudo.v2.interfaces.ISudoTask;
import com.runemate.game.api.hybrid.entities.definitions.ItemDefinition;
import com.runemate.game.api.hybrid.local.Skill;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.net.GrandExchange;
import com.runemate.game.api.hybrid.region.GroundItems;
import com.runemate.game.api.hybrid.util.calculations.CommonMath;
import com.runemate.game.api.script.framework.listeners.InventoryListener;
import com.runemate.game.api.script.framework.listeners.events.ItemEvent;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * sudochickenkiller Bot for RuneMate spectre
 */
public class SudoChickenKiller extends SudoBot implements InventoryListener {

    private ChickenFXGui configUI;
    private ChickenInfoUI infoUI;

    public Area chickenArea = new Area.Rectangular(new Coordinate(3225, 3301, 0), new Coordinate(3236, 3294, 0));
    public Area chickenGateArea = new Area.Rectangular(new Coordinate(3239, 3297, 0), new Coordinate(3236, 3294, 0));

    public String attack = "Attack", feather = "Feather", chicken = "Chicken";

    public int startFeatherCount = 0, featherCount = 0, featherPrice = 0, radius = 0, minFeatherLocationsCount = 0, maxFeatherLocationsCount = 0, featherLocationsCount = 0;

    public boolean grabFeathers = true;

    private int featherID = 314;

    @Override
    public ObjectProperty<Node> botInterfaceProperty() {
        if (_botInterfaceProperty == null) {
            _botInterfaceProperty = new SimpleObjectProperty<>(configUI = new ChickenFXGui(this));
            _GUI = new ArrayList<Node>();
            _GUI.add(_sbGUI = new SudoBuddyFXGui(this));
            _GUI.add(configUI);
            _GUI.add(infoUI = new ChickenInfoUI(this));
        }
        return _botInterfaceProperty;
    }

    public SudoChickenKiller() {
        super();
        setEmbeddableUI(this);
    }
    @Override
    public void onStart(String... args) {
        super.onStart();

        getEventDispatcher().addListener(this);

        if(Inventory.contains(feather))
            startFeatherCount = Inventory.getQuantity(feather);
        else
            startFeatherCount = 0;

        // Add Skills to track to the HashMap
        _XPInfoMap.put(Skill.ATTACK, new XPInfo(Skill.ATTACK));
        _XPInfoMap.put(Skill.STRENGTH, new XPInfo(Skill.STRENGTH));
        _XPInfoMap.put(Skill.DEFENCE, new XPInfo(Skill.DEFENCE));
        _XPInfoMap.put(Skill.CONSTITUTION, new XPInfo(Skill.CONSTITUTION));
        _XPInfoMap.put(Skill.MAGIC, new XPInfo(Skill.MAGIC));
        _XPInfoMap.put(Skill.RANGED, new XPInfo(Skill.RANGED));
        _XPInfoMap.put(Skill.PRAYER, new XPInfo(Skill.PRAYER));

        featherPrice = GrandExchange.lookup(featherID).getPrice();

        _currentTask = new GoToChickenTask(this);
        _currentTask.OnTaskCompleted((ISudoTask task) -> OnTaskComplete(task));
        _currentTask.Start();
    }

    @Override
    public void onLoop() {
        super.onLoop();

        _currentTask.Loop();    // Loop current task

        // After a certain amount of feather pools are dropped, start PickUpFeatherTask
        if(grabFeathers && !(_currentTask instanceof GoToChickenTask)){
            if(featherLocationsCount <= 0) {
                featherLocationsCount = minFeatherLocationsCount + (int) (Math.random() * (maxFeatherLocationsCount - minFeatherLocationsCount));
                Methods.updateCurrentTask("Feathers will be picked up after " + featherLocationsCount + " drop(s) discovered", this);
            }

            if(GroundItems.newQuery().names(feather).results().size() >= featherLocationsCount) {
                _currentTask = new PickUpFeatherTask(this);
                _currentTask.OnTaskCompleted((ISudoTask task) -> OnTaskComplete(task));
            }
        }
    }

    @Override
    public void OnTaskComplete(ISudoTask completedTask) {
        super.OnTaskComplete(completedTask);
        if (completedTask instanceof GoToChickenTask)
            _currentTask = new AttackChickenTask(this);
        else if (completedTask instanceof PickUpFeatherTask)
            _currentTask = new AttackChickenTask(this);
        else if (completedTask instanceof AttackChickenTask)
            _currentTask = new GoToChickenTask(this);

        _currentTask.OnTaskCompleted((ISudoTask task) -> OnTaskComplete(task));
        _currentTask.Start();
    }

    @Override
    public void onItemAdded(ItemEvent event) {
        super.onItemAdded(event);
        ItemDefinition definition = event.getItem().getDefinition();
        if (definition != null) {
            if (definition.getName().contains("Feather")) {
                featherCount++;
            }
            if(definition.getName().contains("Feather")){
                featherCount = Inventory.getQuantity(feather) - startFeatherCount;
            }
        }
    }

    @Override
    public void updateInfo() {

        // UpdateUI all current values in the XPInfo List
        _XPInfoMap.values().forEach((xpInfo) -> xpInfo.update());

        _DisplayInfoMap.put("Feather Count: ", Integer.toString(featherCount) +
                " (" + Integer.toString((int)CommonMath.rate(TimeUnit.HOURS, _STOPWATCH.getRuntime(), featherCount)) + "/Hour)");
        _DisplayInfoMap.put("GP: ",Integer.toString(_grossIncome) +
                " (" + Integer.toString((int)CommonMath.rate(TimeUnit.HOURS, _STOPWATCH.getRuntime(), _grossIncome)) + "/Hour)");

        try {
        _info = new Info(_XPInfoMap,                    // XP Info
                _DisplayInfoMap,                        // DisplayInfo
                _STOPWATCH.getRuntimeAsString(),        // Total Runtime
                _currentTaskString,                     // Current Task
                _abTaskString);                         // antiban Task

        }catch(Exception e){
            e.printStackTrace();
        }

        Platform.runLater(() -> infoUI.update());
    }
}
