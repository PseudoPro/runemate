package com.sudo.v2.spectre.bots.sudochickenkiller.tasks;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.spectre.common.helpers.SudoCamera;
import com.sudo.v2.spectre.common.helpers.SudoInteract;
import com.sudo.v2.spectre.common.helpers.SudoPlayer;
import com.sudo.v2.spectre.bots.sudochickenkiller.SudoChickenKiller;
import com.sudo.v2.base.SudoTask;
import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.local.Camera;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.queries.results.LocatableEntityQueryResults;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.script.Execution;

/**
 * Created by SudoPro on 5/15/2016.
 */
public class AttackChickenTask extends SudoTask {
    private SudoChickenKiller bot;
    private Npc chicken;
    private LocatableEntityQueryResults<Npc> chickenBuilder;

    public AttackChickenTask(SudoChickenKiller bot){
        this.bot = bot;
    }

    @Override
    public void Loop(){
        if(!bot.chickenArea.contains(bot._player)){
            Methods.updateCurrentTask("AttackChickenTask Complete. Outside of Chicken Area.", bot);
            Complete();
        }
        else{
            if(Camera.getPitch() < .25)
                SudoCamera.AngleCameraUp(bot._useMiddleMouseCamera);

            if(chicken == null || !chicken.isValid()){
                chickenBuilder = Npcs.newQuery().names(bot.chicken).within(new Area.Circular(bot._player.getPosition(), bot.radius)).filter(i -> i.getAnimationId() != 5389 && i.getTarget() == null).results();

                if(chickenBuilder.size() >= 1){
                    chicken = chickenBuilder.random();
                }
                else{
                    chicken = Npcs.newQuery().names(bot.chicken).results().nearest();
                }
            }else if(!SudoPlayer.isInteractingWith(chicken, bot._player)) {
                if(chicken.getAnimationId() == 5389)
                    chickenBuilder = Npcs.newQuery().names(bot.chicken).within(new Area.Circular(bot._player.getPosition(), bot.radius)).filter(i -> i.getAnimationId() != 5389).results();
                else {
                    Methods.updateCurrentTask("Attacking Chicken", bot);
                    SudoInteract.interactWith(chicken, bot.attack, bot._useMiddleMouseCamera);
                    Execution.delayWhile(() -> SudoPlayer.isInteractingWith(chicken, bot._player) || bot._player.isMoving(), 1000);
                }
            }
        }
    }
}
