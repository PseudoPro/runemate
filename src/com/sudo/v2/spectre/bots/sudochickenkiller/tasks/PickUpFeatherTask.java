package com.sudo.v2.spectre.bots.sudochickenkiller.tasks;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.spectre.common.helpers.SudoInteract;
import com.sudo.v2.spectre.bots.sudochickenkiller.SudoChickenKiller;
import com.sudo.v2.base.SudoTask;
import com.runemate.game.api.hybrid.entities.GroundItem;
import com.runemate.game.api.hybrid.region.GroundItems;
import com.runemate.game.api.script.Execution;

/**
 * Created by SudoPro on 5/15/2016.
 */
public class PickUpFeatherTask extends SudoTask {
    private SudoChickenKiller bot;

    public PickUpFeatherTask(SudoChickenKiller bot){
        this.bot = bot;
    }

    @Override
    public void Loop(){
        GroundItem feather = GroundItems.newQuery().names(bot.feather).results().nearest();

        if(!bot.chickenArea.contains(bot._player) || feather == null || !bot.grabFeathers){
            Methods.updateCurrentTask("PickUpFeatherTask Complete", bot);
            bot.featherLocationsCount = bot.minFeatherLocationsCount + (int)(Math.random() * (bot.maxFeatherLocationsCount - bot.minFeatherLocationsCount));
            Methods.updateCurrentTask("Feathers will be picked up after " + bot.featherLocationsCount + " drop(s) discovered", bot);
            Complete();
        }
        else{
            if(feather != null){
                Methods.updateCurrentTask(bot.featherLocationsCount + " feather locations detected... Picking up feathers", bot);
                SudoInteract.interactWith(feather, "Take", bot._useMiddleMouseCamera);
                Execution.delayWhile(bot._player::isMoving, 500);
            }
        }
    }
}
