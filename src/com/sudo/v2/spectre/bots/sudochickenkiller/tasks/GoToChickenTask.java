package com.sudo.v2.spectre.bots.sudochickenkiller.tasks;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.spectre.common.helpers.SudoInteract;
import com.sudo.v2.spectre.common.helpers.SudoTravel;
import com.sudo.v2.spectre.bots.sudochickenkiller.SudoChickenKiller;
import com.sudo.v2.base.SudoTask;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.GameObjects;

/**
 * Created by SudoPro on 5/15/2016.
 */
public class GoToChickenTask extends SudoTask {

    private SudoChickenKiller bot;

    public GoToChickenTask(SudoChickenKiller bot){
        this.bot = bot;
    }

    @Override
    public void Loop(){
        if(bot.chickenArea.contains(bot._player)) {
            Methods.updateCurrentTask("GoToChickenTask Complete", bot);
            Complete();
        }
        else{
            if(!bot.chickenGateArea.contains(bot._player)) {
                Methods.updateCurrentTask("Traveling to Chicken Gate", bot);
                SudoTravel.goTowards(bot.chickenGateArea);
            }
            else{
                GameObject gate = GameObjects.newQuery().names("Gate").within(new Area.Circular(bot._player.getPosition(), 4)).actions("Open").results().nearest();
                if(gate != null){
                    Methods.updateCurrentTask("Opening Gate", bot);
                    SudoInteract.interactWith(gate, "Open", bot._useMiddleMouseCamera);
                }
                else{
                    Methods.updateCurrentTask("Entering Chicken Area", bot);
                    SudoTravel.bresenhamTowards(bot.chickenArea);
                }
            }
        }
    }

}
