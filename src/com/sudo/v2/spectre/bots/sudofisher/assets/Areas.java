package com.sudo.v2.spectre.bots.sudofisher.assets;

import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;

/**
 * Areas used for sudofisher
 */
public class Areas {
    public final static Area CATH_RS3_BANK_AREA = new Area.Rectangular(new Coordinate(2797, 3442, 0), new Coordinate(2794, 3437, 0));
    public final static Area CATH_OSRS_BANK_AREA = new Area.Rectangular(new Coordinate(2795, 3437, 0), new Coordinate(2797, 3442, 0));
    public final static Area CATH_FISH_AREA = new Area.Rectangular(new Coordinate(2834, 3433, 0), new Coordinate(2861, 3423, 0));
    public final static Area DRAY_BANK_AREA = new Area.Rectangular(new Coordinate(3092, 3240, 0), new Coordinate(3097, 3246, 0));
    public final static Area DRAY_FISH_AREA = new Area.Rectangular(new Coordinate(3083, 3236, 0), new Coordinate(3091, 3222, 0));
    public final static Area LUMB_BANK_AREA = new Area.Rectangular(new Coordinate(3211, 3259, 0), new Coordinate(3214, 3254, 0));
    public final static Area LUMB_FISH_AREA = new Area.Rectangular(new Coordinate(3242, 3260, 0), new Coordinate(3239, 3238, 0));
    public final static Area LUMB_FISH_SPOT_AREA = new Area.Rectangular(new Coordinate(3244, 3262, 0), new Coordinate(3237, 3236, 0));
    public final static Area ARMIES_BANK_AREA = new Area.Rectangular(new Coordinate(2400, 2843, 0), new Coordinate(2405, 2840, 0));
    public final static Area ARMIES_FISH_AREA = new Area.Polygonal(new Coordinate(2431, 2894, 0), new Coordinate(2435, 2889, 0), new Coordinate(2442, 2890, 0), new Coordinate(2446, 2899, 0), new Coordinate(2451, 2904, 0), new Coordinate(2451, 2912, 0), new Coordinate(2460, 2918, 0), new Coordinate(2469, 2926, 0), new Coordinate(2473, 2938, 0), new Coordinate(2466, 2942, 0), new Coordinate(2453, 2935, 0), new Coordinate(2440, 2922, 0), new Coordinate(2432, 2907, 0));
    public final static Area GUILD_BANK_RS3_AREA = new Area.Rectangular(new Coordinate(2585, 3421, 0), new Coordinate(2588, 3423, 0));
    public final static Area GUILD_BANK_OSRS_AREA = new Area.Rectangular(new Coordinate(2585, 3421, 0), new Coordinate(2589, 3418, 0));
    public final static Area GUILD_FISH_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2599, 3426, 0), new Coordinate(2605, 3426, 0), new Coordinate(2605, 3420, 0), new Coordinate(2597, 3420, 0)});
    public final static Area GUILD_FISH_SPOT_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2598, 3427, 0), new Coordinate(2606, 3427, 0), new Coordinate(2606, 3419, 0), new Coordinate(2599, 3418, 0)});
    public final static Area BARB_FISH_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(3102, 3423, 0), new Coordinate(3104, 3423, 0), new Coordinate(3105, 3428, 0), new Coordinate(3111, 3433, 0), new Coordinate(3106, 3437, 0), new Coordinate(3101, 3429, 0), new Coordinate(3100, 3425, 0)});
    //public final static Area BARB_FISH_SPOT_AREA = new Area.Rectangular(new Coordinate(3102, 3423, 0), new Coordinate(3239, 3238, 0));
    public final static Area EDGE_BANK_AREA = new Area.Rectangular(new Coordinate(3092, 3488, 0), new Coordinate(3098, 3498, 0));
    //public final static Area GUILD_GATE_OUT_AREA = new Area.Rectangular(new Coordinate(2610, 3386, 0), new Coordinate(2617, 3384, 0));
    //public final static Area INSIDE_FISHING_GUILD_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2579,3429,0),new Coordinate(2579,3414,0),new Coordinate(2581,3412,0),new Coordinate(2581,3402,0),new Coordinate(2585,3398,0),new Coordinate(2590,3398,0),new Coordinate(2599,3389,0),new Coordinate(2610,3389,0),new Coordinate(2612,3387,0),new Coordinate(2615,3387,0),new Coordinate(2617,3389,0),new Coordinate(2620,3389,0),new Coordinate(2624,3393,0),new Coordinate(2624,3402,0),new Coordinate(2611,3415,0),new Coordinate(2605,342560),new Coordinate(2593,3429,0)});
    //final static Area TZHAAR_BANK_AREA = new Area.Rectangular(new Coordinate(4643, 5154, 0), new Coordinate(4648, 5144, 0));
    //final static Area MUSA_FISH_AREA = new Area.Rectangular(new Coordinate(2926, 3180, 0), new Coordinate(2920, 3173, 0));
    //final static Area MUSA_FISH_SPOT_AREA = new Area.Rectangular(new Coordinate(2928, 3182, 0), new Coordinate(2918, 3169, 0));
    //final static Area TZHAAR_ENTRANCE_AREA = new Area.Rectangular(new Coordinate(2846, 3170, 0), new Coordinate(2844, 3166, 0));
    //final static Area TZHAAR_EXIT_AREA = new Area.Rectangular(new Coordinate(4669, 5059, 0), new Coordinate(4665, 5061, 0));
}
