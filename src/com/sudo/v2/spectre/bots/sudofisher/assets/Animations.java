package com.sudo.v2.spectre.bots.sudofisher.assets;

import java.util.HashSet;

/**
 * Animations used for sudofisher
 */
public class Animations {
    public final static HashSet<Integer> FISHING_ANIM = new HashSet<Integer>(){{
        add(24878);
        add(24928);
        add(24929);
        add(24930);
        add(24959);
        add(26386);
        add(621);
        add(622);
        add(623);
    }};
}
