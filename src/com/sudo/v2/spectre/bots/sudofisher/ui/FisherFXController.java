package com.sudo.v2.spectre.bots.sudofisher.ui;

import com.sudo.v2.spectre.bots.sudofisher.assets.Areas;
import com.sudo.v2.spectre.bots.sudofisher.SudoFisher;
import com.sudo.v2.ui.base.SudoFXController;
import com.runemate.game.api.hybrid.Environment;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Java FX Controller for the FishFXGui class
 */
public class FisherFXController extends SudoFXController implements Initializable {

    private final SudoFisher bot;
    boolean rs3 = false;

    @FXML
    private ListView<String> locationsListView, fishListView;

    @FXML
    private Button startButton;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        // Add Locations to the ListView
        if(rs3 = Environment.isRS3())
            locationsListView.getItems().addAll("Draynor", "Lumbridge", "Barbarian Village", "Catherby", "Fishing Guild", "Mobilising Armies");
        else
            locationsListView.getItems().addAll("Draynor", "Catherby", "Fishing Guild");

        locationsListView.setOnMouseClicked(getLocationClickAction());

        fishListView.setOnMouseClicked(getfishListClickAction());

        startButton.setOnAction(getStartButtonAction());
    }

    public FisherFXController(SudoFisher bot){
        super(bot);
        this.bot = bot;
    }

    private EventHandler<MouseEvent> getLocationClickAction(){
        return event -> {
            fishListView.getItems().clear();

            startButton.setDisable(true);
            try {
                switch (locationsListView.getSelectionModel().getSelectedItem().toString()) {
                    case "Draynor":
                        fishListView.getItems().add("Shrimp/Anchovies");
                        fishListView.getItems().add("Sardines/Herring");
                        break;

                    case "Lumbridge":
                        fishListView.getItems().add("Trout/Salmon");
                        fishListView.getItems().add("Pike");
                        break;

                    case "Barbarian Village":
                        fishListView.getItems().add("Trout/Salmon");
                        fishListView.getItems().add("Pike");
                        break;

                    case "Catherby":
                        fishListView.getItems().add("Shrimp/Anchovies");
                        fishListView.getItems().add("Sardines/Herring");
                        fishListView.getItems().add("Mackerel/Cod/Bass");
                        fishListView.getItems().add("Lobster");
                        fishListView.getItems().add("Tuna/Swordfish");
                        fishListView.getItems().add("Shark");
                        break;

                    case "Fishing Guild":
                        fishListView.getItems().add("Mackerel/Cod/Bass");
                        fishListView.getItems().add("Lobster");
                        fishListView.getItems().add("Tuna/Swordfish");
                        fishListView.getItems().add("Shark");
                        break;

                    case "Bhurg De Rott":
                        fishListView.getItems().add("Sharks");
                        break;

                    case "Mobilising Armies":
                        fishListView.getItems().add("Shrimp/Anchovies");
                        fishListView.getItems().add("Sardines/Herring");
                        fishListView.getItems().add("Mackerel/Cod/Bass");
                        fishListView.getItems().add("Lobster");
                        fishListView.getItems().add("Tuna/Swordfish");
                        break;

                }
            }catch(Exception e){
                e.printStackTrace();
            }
        };
    }

    private EventHandler<MouseEvent> getfishListClickAction(){
        return event -> {
            startButton.setDisable(false);
        };
    }

    public EventHandler<ActionEvent> getStartButtonAction(){
        return event -> {

            String location = "", fish = "";

            try {
                location = locationsListView.getSelectionModel().getSelectedItem().toString();
                fish = fishListView.getSelectionModel().getSelectedItem().toString();
            }catch(Exception e){
                System.out.println("Make sure you have both Fish and Location selected.");
            }

            if (location != null && fish != null) {
                try {
                    bot.equipment = new ArrayList<>();
                    bot.equipmentAccessory = new ArrayList<>();
                    switch (fish) {
                        case "Shrimp/Anchovies":
                            bot.fishAction = "Net";
                            bot.fishSpotAction = "Net";
                            bot.equipment.add("Small fishing net");
                            break;
                        case "Sardines/Herring":
                            bot.fishAction = "Bait";
                            bot.fishSpotAction = "Bait";
                            bot.equipment.add("Fishing rod");
                            bot.equipmentAccessory.add("Fishing bait");
                            break;
                        case "Trout/Salmon":
                            bot.fishAction = "Lure";
                            bot.fishSpotAction = "Lure";
                            bot.equipment.add("Fly fishing rod");
                            bot.equipmentAccessory.add("Feather");
                            break;
                        case "Pike":
                            bot.fishAction = "Bait";
                            bot.fishSpotAction = "Lure";
                            bot.equipment.add("Fishing rod");
                            bot.equipmentAccessory.add("Fishing bait");
                            break;
                        case "Mackerel/Cod/Bass":
                            bot.fishAction = "Net";
                            bot.fishSpotAction = "Net";
                            bot.equipment.add("Big fishing net");
                            break;
                        case "Lobster":
                            bot.fishAction = "Cage";
                            bot.fishSpotAction = "Cage";
                            bot.equipment.add("Lobster pot");
                            break;
                        case "Tuna/Swordfish":
                            bot.fishAction = "Harpoon";
                            bot.fishSpotAction = "Cage";
                            bot.equipment.add("Harpoon");
                            break;
                        case "Shark":
                            bot.fishAction = "Harpoon";
                            bot.fishSpotAction = "Net";
                            bot.equipment.add("Harpoon");
                            break;
                    }

                    switch (location) {
                        case "Lumbridge":
                            bot.BANK_AREA = Areas.LUMB_BANK_AREA;
                            bot.FISH_AREA.add(Areas.LUMB_FISH_AREA);
                            bot.FISH_SPOT_AREA = Areas.LUMB_FISH_SPOT_AREA;
                            break;
                        case "Draynor":
                            bot.BANK_AREA = Areas.DRAY_BANK_AREA;
                            bot.FISH_AREA.add(Areas.DRAY_FISH_AREA);
                            bot.FISH_SPOT_AREA = Areas.DRAY_FISH_AREA;
                            break;
                        case "Barbarian Village":
                            bot.BANK_AREA = Areas.EDGE_BANK_AREA;
                            bot.FISH_AREA.add(Areas.BARB_FISH_AREA);
                            bot.FISH_SPOT_AREA = Areas.BARB_FISH_AREA;
                            break;
                        case "Catherby":
                            if(bot._isRS3)
                                bot.BANK_AREA = Areas.CATH_RS3_BANK_AREA;
                            else
                                bot.BANK_AREA = Areas.CATH_OSRS_BANK_AREA;
                            bot.FISH_AREA.add(Areas.CATH_FISH_AREA);
                            bot.FISH_SPOT_AREA = Areas.CATH_FISH_AREA;
                            break;
                        case "Fishing Guild":
                            if(rs3)
                                bot.BANK_AREA = Areas.GUILD_BANK_RS3_AREA;
                            else
                                bot.BANK_AREA = Areas.GUILD_BANK_OSRS_AREA;
                            bot.FISH_AREA.add(Areas.GUILD_FISH_AREA);
                            bot.FISH_SPOT_AREA = Areas.GUILD_FISH_SPOT_AREA;
                            break;
                        case "Mobilising Armies":
                            bot.BANK_AREA = Areas.ARMIES_BANK_AREA;
                            bot.FISH_AREA.add(Areas.ARMIES_FISH_AREA);
                            bot.FISH_SPOT_AREA = Areas.ARMIES_FISH_AREA;
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }

                super.getStart_BTEvent();
                // Create temp arraylist and combine equipment and equipmentAccessory lists
                List<String> temp = new ArrayList<>();
                temp.addAll(bot.equipment);
                temp.addAll(bot.equipmentAccessory);

                // Create String array so it can later be used in BankInventoryTask
                // These items will not be banked
                bot.allEquipment = temp.toArray(new String[temp.size()]);
                temp = null;

                startButton.textProperty().set("Resume");
            }
        };
    }
}
