package com.sudo.v2.spectre.bots.sudofisher.tasks;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.spectre.common.helpers.SudoTravel;
import com.sudo.v2.spectre.bots.sudofisher.SudoFisher;
import com.sudo.v2.base.SudoTask;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.region.Players;

/**
 * TravelToFishingTask SudoTask used by sudofisher
 *
 * Traverses player to Fishing Area
 */
public class TravelToFishingTask extends SudoTask {
    private SudoFisher bot;
    private Player player;
    private int index = 0;

    public TravelToFishingTask(SudoFisher bot){
        this.bot = bot;
    }

    @Override
    public void Loop(){
        this.player = Players.getLocal();
        if (player == null)
            return;

        if(bot.FISH_SPOT_AREA.contains(Players.getLocal()) || Inventory.isFull()) {
            Methods.updateCurrentTask("TravelToFishingTask Complete", bot);
            Complete();
        }else {
            if(Bank.isOpen()) {
                Bank.close();
            }
            else {
                Methods.updateCurrentTask("Attempting TravelToFishingTask", bot);
                SudoTravel.bresenhamTowards(bot.FISH_SPOT_AREA);
            }

//            if (bot.FISH_AREA.get(index).contains(player)) {
//                if (index == bot.FISH_AREA.size() - 1)
//                    index = 0;
//                else
//                    index++;
//            }
        }

    }

}
