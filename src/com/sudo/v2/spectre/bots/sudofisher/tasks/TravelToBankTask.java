package com.sudo.v2.spectre.bots.sudofisher.tasks;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.spectre.common.helpers.SudoTravel;
import com.sudo.v2.spectre.bots.sudofisher.SudoFisher;
import com.sudo.v2.base.SudoTask;
import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.region.Players;

/**
 * TravelToBankTask SudoTask used by sudofisher
 *
 * Traverses player to bank location
 */
public class TravelToBankTask extends SudoTask {
    SudoFisher bot;

    public TravelToBankTask(SudoFisher bot){
        this.bot = bot;
    }

    @Override
    public void Loop(){
        Player player = Players.getLocal();
        if (player == null)
            return;

        if(Environment.isOSRS() && !bot.hasEquipment()) {
            if(bot.BANK_AREA.distanceTo(bot._player) < 6 || bot.BANK_AREA.contains((bot._player))){
                Methods.updateCurrentTask("TravelToBankTask Complete", bot);
                Complete();
            }
            else {
                Methods.updateCurrentTask("Attempting TravelToBankTask", bot);
                SudoTravel.bresenhamTowards(bot.BANK_AREA);
            }
        }
        else if(bot.BANK_AREA.distanceTo(bot._player) < 6 || bot.BANK_AREA.contains((bot._player)) || !Inventory.isFull()) {
            Methods.updateCurrentTask("TravelToBankTask Complete", bot);
            Complete();
        }
        else {
            Methods.updateCurrentTask("Attempting TravelToBankTask", bot);
            SudoTravel.bresenhamTowards(bot.BANK_AREA);
        }
    }
}
