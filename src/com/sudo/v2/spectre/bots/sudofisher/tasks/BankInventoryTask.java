package com.sudo.v2.spectre.bots.sudofisher.tasks;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.spectre.common.helpers.SudoCamera;
import com.sudo.v2.spectre.common.helpers.SudoInteract;
import com.sudo.v2.spectre.bots.sudofisher.SudoFisher;
import com.sudo.v2.base.SudoTask;
import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.script.Execution;

import java.util.regex.Pattern;

/**
 * BankInventoryTask SudoTask used by sudofisher
 *
 * Banks the players inventory appropriately
 */
public class BankInventoryTask extends SudoTask {

    private SudoFisher bot;
    private Pattern pattern;
    private GameObject bank;

    private String itemsNoBank[] = new String[]{"Small fishing net", "Fishing rod", "Feather", "Fishing bait", "Fly fishing rod", "Harpoon", "Lobster pot"};

    public BankInventoryTask(SudoFisher bot){
        this.bot = bot;
        pattern = Pattern.compile("(?i)^(raw).*?");
    }

    @Override
    public void Loop(){
        if(bot.hasEquipment() && (Inventory.newQuery().names(pattern).results().size() <= 0 || bot.BANK_AREA.distanceTo(bot._player) >= 10)) {
            Methods.updateCurrentTask("BankInventoryTask Complete", bot);
            Complete();
        }else {

            if (!Bank.isOpen()) {
                Methods.updateCurrentTask("Opening Bank", bot);
                if ((bank = GameObjects.newQuery().actions("Bank").results().nearest()) != null) {
                    if (!bank.isVisible())
                        SudoCamera.turnTo(bank);
                    else {
                        if (SudoInteract.interactWith(bank, "Bank", bot._useMiddleMouseCamera))
                            Execution.delayUntil(Bank::isOpen, 500, 1000);
                    }
                    bank = null;
                }
            }
            else {
                if (!bot.hasEquipment()) {
                    String missingEquipment = bot.getMissingEquipment();
                    String missingAccessory = bot.getMissingAccessory();
                    if(!bot._isRS3 && !missingEquipment.equals("")){
                        if (Bank.containsAnyOf(missingEquipment)) {
                            Methods.updateCurrentTask("Withdrawing the equipment " + missingEquipment, bot);
                            Bank.withdraw(missingEquipment, 1);
                        } else {
                            Methods.updateCurrentTask("Bot stopping, couldn't find " + missingEquipment, bot);
                            bot.stop();
                        }
                    }
                    else if(!missingAccessory.equals("")) {
                        if (Bank.containsAnyOf(missingAccessory)) {
                            Methods.updateCurrentTask("Withdrawing the equipment accessory " + missingAccessory, bot);
                            Bank.withdraw(missingAccessory, 100 + (((int)(Math.random() * 18)) * 50));
                        } else {
                            Methods.updateCurrentTask("Bot stopping, couldn't find " + missingAccessory, bot);
                            bot.stop();
                        }
                    }
                } else if (!Inventory.isEmpty()) {
                    Methods.updateCurrentTask("Depositing items", bot);
                    if (Environment.isOSRS())
                        Bank.depositAllExcept(bot.allEquipment);
                    else {
                        if (bot.fishAction.equals("Bait"))
                            Bank.depositAllExcept("Fishing bait");
                        else if (bot.fishAction.equals("Lure"))
                            Bank.depositAllExcept("Feather");
                        else {
                            Bank.depositInventory();
                        }
                    }
                    Execution.delayUntil(Inventory::isEmpty, 250, 750);
                } else {
                    Methods.updateCurrentTask("Closing Bank", bot);
                    Bank.close();
                }

            }
        }
    }
}
