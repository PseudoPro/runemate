package com.sudo.v2.spectre.bots.sudofisher.tasks;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.spectre.common.helpers.SudoInteract;
import com.sudo.v2.spectre.common.helpers.SudoPlayer;
import com.sudo.v2.spectre.bots.sudofisher.SudoFisher;
import com.sudo.v2.base.SudoTask;
import com.sudo.v2.interfaces.ISudoTask;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.script.Execution;


/**
 * GoFishingTask SudoTask used by sudofisher
 *
 * Fishes in the closest appropriate fishing spot
 */
public class GoFishingTask extends SudoTask {

    private SudoFisher bot;

    public GoFishingTask(SudoFisher bot){
        this.bot = bot;
    }

    @Override
    public void Loop() {
        if (bot._player == null)
            return;

        //if(Environment.isOSRS()){
            // If OSRS
            if (Inventory.isFull() || bot.fishingSpot == null || !bot.hasEquipment()) {
                Methods.updateCurrentTask("GoFishingTask Complete", bot);
                Complete();
            } else {
                if (bot.fishingSpot != null && !SudoPlayer.isInteractingWith(bot.fishingSpot, bot._player)) {
                    Methods.updateCurrentTask("Attempting to interact with Fishing Spot", bot);

                    if (bot.fishAction.equals("Bait")) {
                        if (Inventory.contains("Fishing bait"))
                            if (SudoInteract.interactWith(bot.fishingSpot, bot.fishAction, bot._useMiddleMouseCamera))
                                Execution.delayWhile(bot._player::isMoving, 2000, 5000);
                            else {
                                Methods.updateCurrentTask("Out of fishing bait, traveling to bank", bot);
                                bot._currentTask = new TravelToBankTask(bot);
                                bot._currentTask.OnTaskCompleted((ISudoTask task) -> bot.OnTaskComplete(task));
                            }
                    } else if (bot.fishAction.equals("Lure")) {
                        if (Inventory.contains("Feather"))
                            if (SudoInteract.interactWith(bot.fishingSpot, bot.fishAction, bot._useMiddleMouseCamera))
                                Execution.delayWhile(bot._player::isMoving, 2000, 5000);
                            else {
                                Methods.updateCurrentTask("Out of feathers, traveling to bank", bot);
                                bot._currentTask = new TravelToBankTask(bot);
                                bot._currentTask.OnTaskCompleted((ISudoTask task) -> bot.OnTaskComplete(task));
                            }
                    } else if (SudoInteract.interactWith(bot.fishingSpot, bot.fishAction, bot._useMiddleMouseCamera))
                        Execution.delayWhile(bot._player::isMoving, 2000, 5000);
                }
            }
       // }
//        else {
//            // If RS3
//            if (Inventory.isFull() || bot.fishingSpot == null) {
//                UpdateUI.updateCurrentTask("GoFishingTask Complete", bot);
//                Complete();
//            } else {
//                if (bot.fishingSpot != null && !SudoPlayer.isInteractingWith(bot.fishingSpot, bot.player)) {
//                    UpdateUI.updateCurrentTask("Attempting to interact with Fishing Spot", bot);
//
//                    if (bot.fishAction.equals("Bait")) {
//                        if (Inventory.contains("Fishing bait"))
//                            SudoInteract.interactWith(bot.fishingSpot, bot.fishAction, bot.useMiddleMouseCamera);
//                        else {
//                            UpdateUI.updateCurrentTask("Bot Stopping. Out of Bait", bot);
//                            bot.stop();
//                        }
//                    } else if (bot.fishAction.equals("Lure")) {
//                        if (Inventory.contains("Feather"))
//                            SudoInteract.interactWith(bot.fishingSpot, bot.fishAction, bot.useMiddleMouseCamera);
//                        else {
//                            UpdateUI.updateCurrentTask("Bot Stopping. Out of Feathers", bot);
//                            bot.stop();
//                        }
//                    } else
//                        SudoInteract.interactWith(bot.fishingSpot, bot.fishAction, bot.useMiddleMouseCamera);
//                }
//            }
//        }
    }


}
