package com.sudo.v2.spectre.bots.sudofisher.tasks;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.spectre.common.helpers.SudoTravel;
import com.sudo.v2.spectre.bots.sudofisher.SudoFisher;
import com.sudo.v2.base.SudoTask;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.Players;

/**
 * Created by SudoPro on 4/3/2016.
 */
public class GoToFishingSpotTask extends SudoTask {
    private SudoFisher bot;

    public GoToFishingSpotTask(SudoFisher bot){
        super();
        this.bot = bot;
    }

    @Override
    public void Loop(){
        if(Players.getLocal().distanceTo(bot.fishingSpot) < 6 || Inventory.isFull()){
            Methods.updateCurrentTask("GoToFishingSpot Complete", bot);
            Complete();
        }
        else {
            Methods.updateCurrentTask("Attempting TravelToFishingSpotTask", bot);
            if(bot.fishingSpot != null)
                SudoTravel.bresenhamTowards(new Area.Circular(bot.fishingSpot.getPosition(), 6));
            else
                SudoTravel.bresenhamTowards(bot.FISH_SPOT_AREA);
        }
    }
}
