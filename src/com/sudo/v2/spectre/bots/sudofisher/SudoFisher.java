package com.sudo.v2.spectre.bots.sudofisher;

import com.sudo.v2.spectre.common.helpers.SudoPlayer;
import com.sudo.v2.spectre.bots.sudofisher.ui.FishInfoUI;
import com.sudo.v2.spectre.bots.sudofisher.ui.FisherFXGui;
import com.sudo.v2.spectre.bots.sudofisher.tasks.*;
import com.sudo.v2.spectre.bots.sudofisher.tasks.BankInventoryTask;
import com.sudo.v2.ui.Info;
import com.sudo.v2.ui.SudoBuddyFXGui;
import com.sudo.v2.ui.model.XPInfo;
import com.sudo.v2.base.SudoBot;
import com.sudo.v2.interfaces.ISudoTask;
import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.entities.definitions.ItemDefinition;
import com.runemate.game.api.hybrid.local.Skill;
import com.runemate.game.api.hybrid.local.hud.interfaces.ChatDialog;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.hybrid.util.calculations.CommonMath;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.listeners.InventoryListener;
import com.runemate.game.api.script.framework.listeners.events.ItemEvent;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * sudofisher bot for RuneMate spectre
 */
public class SudoFisher extends SudoBot implements InventoryListener {

    private FishInfoUI infoUI;
    private FisherFXGui configUI;

    public Area BANK_AREA, FISH_SPOT_AREA;
    public ArrayList<Area> FISH_AREA = new ArrayList<>();

    public String fishAction = "";
    public String fishSpotAction = "";
    public List<String> equipment = new ArrayList<>();
    public List<String> equipmentAccessory = new ArrayList<>();
    public String[] allEquipment;

    private int fishCount;

    public Npc fishingSpot;

    public SudoFisher(){
        super();

        setEmbeddableUI(this);
    }

    @Override
    public ObjectProperty<? extends Node> botInterfaceProperty() {
        if (_botInterfaceProperty == null) {
            _botInterfaceProperty = new SimpleObjectProperty<>(configUI = new FisherFXGui(this));
            _GUI = new ArrayList<Node>();
            _GUI.add(_sbGUI = new SudoBuddyFXGui(this));
            _GUI.add(configUI);
            _GUI.add(infoUI = new FishInfoUI(this));

        }
        return _botInterfaceProperty;
    }

    @Override
    public void updateInfo() {

        // UpdateUI all current values in the XPInfo List
        _XPInfoMap.values().forEach((xpInfo) -> xpInfo.update());

        _DisplayInfoMap.put("Fish Count: ", Integer.toString(fishCount) +
                " (" + Integer.toString((int)CommonMath.rate(TimeUnit.HOURS, _STOPWATCH.getRuntime(), fishCount)) + "/Hour)");
        _DisplayInfoMap.put("GP: ",Integer.toString(_grossIncome) +
                " (" + Integer.toString((int)CommonMath.rate(TimeUnit.HOURS, _STOPWATCH.getRuntime(), _grossIncome)) + "/Hour)");

        try {
            _info = new Info(_XPInfoMap,           // XP Info
                    _DisplayInfoMap,
                    _STOPWATCH.getRuntimeAsString(),        // Total Runtime
                    _currentTaskString,                     // Current Task
                    _abTaskString);                         // antiban Task

        }catch(Exception e){
            e.printStackTrace();
        }

        Platform.runLater(() -> infoUI.update());
    }

    @Override
    public void onStart(String... args){
        getEventDispatcher().addListener(this);

        _currentTask = new TravelToFishingTask(this);
        _currentTask.OnTaskCompleted((ISudoTask task) -> OnTaskComplete(task));
        _currentTask.Start();

        // Add Skills to track to the HashMap
        _XPInfoMap.put(Skill.FISHING, new XPInfo(Skill.FISHING));

        super.onStart();

    }

    @Override
    public void onStop() {
        _currentTask.Stop();
    }

    @Override
    public void onLoop() {
        super.onLoop();

        if(ChatDialog.getContinue() != null) {
            ChatDialog.getContinue().select();
            Execution.delayWhile(() -> ChatDialog.getContinue() != null, 500);
        }

        if(!hasEquipment()){
            if(!(_currentTask instanceof TravelToBankTask) && !(_currentTask instanceof BankInventoryTask)) {
                _currentTask = new TravelToBankTask(this);
                _currentTask.OnTaskCompleted((ISudoTask task) -> OnTaskComplete(task));
            }
        }

        if(!SudoPlayer.isInteractingWith(fishingSpot, _player))
            fishingSpot = findFishingSpot();

        _currentTask.Loop();
    }

    @Override
    public void OnTaskComplete(ISudoTask completedTask) {
        super.OnTaskComplete(completedTask);

        if (completedTask instanceof TravelToFishingTask)
            _currentTask = new GoToFishingSpotTask(this);
        else if (completedTask instanceof GoToFishingSpotTask)
            _currentTask = new GoFishingTask(this);
        else if (completedTask instanceof GoFishingTask)
            _currentTask = new TravelToBankTask(this);
        else if (completedTask instanceof TravelToBankTask)
            _currentTask = new BankInventoryTask(this);
        else if (completedTask instanceof BankInventoryTask)
            _currentTask = new TravelToFishingTask(this);

        _currentTask.OnTaskCompleted((ISudoTask task) -> OnTaskComplete(task));
        _currentTask.Start();
    }

    @Override
    public void onItemAdded(ItemEvent event) {
        if(!(_currentTask instanceof BankInventoryTask))
            super.onItemAdded(event);

        if(event != null) {
            ItemDefinition definition = event.getItem().getDefinition();

            if (definition != null && definition.getName().contains("Raw")) {
                fishCount++;
            }
        }
    }

    @Override
    public void onItemRemoved(ItemEvent event){
        if(!(_currentTask instanceof BankInventoryTask))
            super.onItemRemoved(event);
    }

    public Npc findFishingSpot(){
        ArrayList<Npc> s1 = new ArrayList<>(Arrays.asList(Npcs.newQuery().within(FISH_SPOT_AREA).names("Fishing spot").actions(fishAction).results().sortByDistance().toArray()));
        s1.retainAll(new ArrayList<>(Arrays.asList(Npcs.newQuery().within(FISH_SPOT_AREA).names("Fishing spot").actions(fishSpotAction).results().sortByDistance().toArray())));

        if(s1.size() == 0) {
            ArrayList<Npc> s2 = new ArrayList<>(Arrays.asList(Npcs.newQuery().names("Fishing spot").actions(fishAction).results().sortByDistance().toArray()));
            s2.retainAll(new ArrayList<>(Arrays.asList(Npcs.newQuery().names("Fishing spot").actions(fishSpotAction).results().sortByDistance().toArray())));
            if(s2.size() == 0)
                return null;
            else
                return s2.get(0);
        }
        else
            return s1.get(0);
    }

    public boolean hasEquipment(){
        // If equipment is not found in the inventory, return false
        if(!_isRS3 && Inventory.containsAllOf(allEquipment))
            return true;
        else if(_isRS3 && Inventory.containsAllOf(equipmentAccessory.toArray(new String[0])))
            return true;
        else
            return false;
    }

    public String getMissingEquipment(){
        for(String i : equipment){
            if(!Inventory.contains(i))
                return i;
        }

        return "";
    }

    public String getMissingAccessory(){

        for(String i : equipmentAccessory){
            if(!Inventory.contains(i))
                return i;
        }

        return "";
    }
}
