package com.sudo.v2.spectre.bots.sudodivination;

import com.sudo.v2.spectre.common.helpers.SudoTimer;
import com.sudo.v2.spectre.bots.sudodivination.ui.DivInfoUI;
import com.sudo.v2.spectre.bots.sudodivination.ui.DivinationFXGui;
import com.sudo.v2.spectre.bots.sudodivination.tasks.*;
import com.sudo.v2.ui.Info;
import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.ui.SudoBuddyFXGui;
import com.sudo.v2.ui.model.XPInfo;
import com.sudo.v2.base.SudoBot;
import com.sudo.v2.interfaces.ISudoTask;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.entities.definitions.ItemDefinition;
import com.runemate.game.api.hybrid.local.Skill;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.hybrid.util.calculations.CommonMath;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.listeners.ChatboxListener;
import com.runemate.game.api.script.framework.listeners.InventoryListener;
import com.runemate.game.api.script.framework.listeners.events.ItemEvent;
import com.runemate.game.api.script.framework.listeners.events.MessageEvent;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;

/**
 * sudodivination Bot for RuneMate spectre
 */
public class SudoDivination extends SudoBot implements InventoryListener, ChatboxListener {

    private DivinationFXGui configUI;
    private DivInfoUI infoUI;

    public Area riftArea;

    public String chronicle = "Chronicle fragment", wisp = "", spring = "",
            rift = "Energy rift", riftCache = "Energy Rift", chronicleAction = "Capture", wispAction = "Harvest", riftAction = "";

    public int radius = 7, chronicleCount = 0, invDepositNum = 25, memoryCount = 0, chronicleMinTimer = 5000,
            chronicleMaxTimer = 300000, startEnergyCount = 0, energyCount = 0, energyIndex = 0;

    public boolean grabChronicles = true, chronicleFull = false, harvestEnriched = true, playCache = false;

    public final static HashSet<Integer> DIV_ANIM = new HashSet<Integer>(){{
        add(21228);
        add(21229);
        add(21230);
        add(21231);
        add(21232);
        add(21233);
        add(21234);
        add(23793);
    }};

    public int[] energyID = new int[]{
            29313,  // Pale Energy
            29314,  // Flickering Energy
            29315,  // Bright Energy
            29316,  // Glowing Energy
            29317,  // Sparkling Energy
            29318,  // Gleaming Energy
            29319,  // Vibrant Energy
            29320,  // Lustrous Energy
            31312,  // Elder Energy
            29321,  // Brilliant Energy
            29322,  // Radiant Energy
            29323,  // Luminous Energy
            29324   // Incandescent Energy
    };

    public SudoTimer chronicleTimer = new SudoTimer(chronicleMinTimer, chronicleMaxTimer);

    @Override
    public ObjectProperty<Node> botInterfaceProperty() {
        if (_botInterfaceProperty == null) {
            _botInterfaceProperty = new SimpleObjectProperty<>(configUI = new DivinationFXGui(this));
            _GUI = new ArrayList<Node>();
            _GUI.add(_sbGUI = new SudoBuddyFXGui(this));
            _GUI.add(configUI);
            _GUI.add(infoUI = new DivInfoUI(this));
        }
        return _botInterfaceProperty;
    }

    public SudoDivination() {
        super();
        setEmbeddableUI(this);
    }
    @Override
    public void onStart(String... args) {
        super.onStart();

        getEventDispatcher().addListener(this);

        // Set bot to be ready to capture a chronicle as soon as bot is started
        chronicleTimer.setRemainingTime(0);
        chronicleTimer.start();

        if(Inventory.contains(chronicle))
            chronicleCount = Inventory.getQuantity(chronicle);
        else
            chronicleCount = 0;

        // Add Skills to track to the HashMap
        _XPInfoMap.put(Skill.DIVINATION, new XPInfo(Skill.DIVINATION));
        _XPInfoMap.put(Skill.HUNTER, new XPInfo(Skill.HUNTER));

        _currentTask = new HarvestTask(this);
        _currentTask.OnTaskCompleted((ISudoTask task) -> OnTaskComplete(task));
        _currentTask.Start();

        try{
            setRiftArea();
        }catch(Exception e){
            Methods.debug("Unable to set Rift Area");
        }

    }

    @Override
    public void onLoop() {
        super.onLoop();

        // If Enriched wisp/spring appears
        if(harvestEnriched && Npcs.newQuery().names("Enriched " + wisp.toLowerCase(), "Enriched " + spring.toLowerCase()).results().size() > 0){
            _currentTask = new EnrichedTask(this);
            _currentTask.OnTaskCompleted((ISudoTask task) -> OnTaskComplete(task));
        }

        // If chronicle fragment appears
        if(grabChronicles && !chronicleFull && !Inventory.isFull() && chronicleTimer.getRemainingTime() <= 0 && Npcs.newQuery().names(chronicle).results().nearest() != null){
            _currentTask = new ChronicleTask(this);
            _currentTask.OnTaskCompleted((ISudoTask task) -> OnTaskComplete(task));
        }

        // If Guthixian Cache appears
        if(playCache){
            if(GameObjects.newQuery().names(riftCache).results().nearest() != null) {
                _currentTask = new EnterCacheTask(this);
                _currentTask.OnTaskCompleted((ISudoTask task) -> OnTaskComplete(task));
            }else if(GameObjects.newQuery().names("Unstable rift").results().nearest() != null){
                _currentTask = new GuthixianCacheTask(this);
                _currentTask.OnTaskCompleted((ISudoTask task) -> OnTaskComplete(task));
            }
        }

        _currentTask.Loop();    // Loop current task

    }

    @Override
    public void OnTaskComplete(ISudoTask completedTask) {
        super.OnTaskComplete(completedTask);
        if (completedTask instanceof HarvestTask)
            _currentTask = new ConvertTask(this);
        else if (completedTask instanceof ConvertTask)
            _currentTask = new HarvestTask(this);
        else if (completedTask instanceof EnrichedTask)
            _currentTask = new HarvestTask(this);
        else if (completedTask instanceof ChronicleTask)
            _currentTask = new HarvestTask(this);
        else if (completedTask instanceof DropItemsTask)
            _currentTask = new HarvestTask(this);
        else if(completedTask instanceof EnterCacheTask)
            _currentTask = new GuthixianCacheTask(this);
        else if(completedTask instanceof GuthixianCacheTask)
            _currentTask = new HarvestTask(this);

        _currentTask.OnTaskCompleted((ISudoTask task) -> OnTaskComplete(task));
        _currentTask.Start();
    }

    @Override
    public void onItemAdded(ItemEvent event) {
        super.onItemAdded(event);
        ItemDefinition definition = event.getItem().getDefinition();
        if (definition != null) {
            if (definition.getName().contains("memory")) {
                memoryCount++;
            }

            // For misclicks in radiant wisp location
            if(definition.getName().matches("(?i)^(.*logs|wooden knot)")){
                _currentTask = new DropItemsTask(this);
                _currentTask.OnTaskCompleted((ISudoTask task) -> OnTaskComplete(task));
            }
            if(definition.getName().contains("energy")){
                energyCount = Inventory.getQuantity(energyID[energyIndex]) - startEnergyCount;
            }
        }
    }

    @Override
    public void onMessageReceived(MessageEvent event) {
        String message = event.getMessage();

        // If user allows to grab chronicles
        if (grabChronicles && chronicleTimer.getRemainingTime() <= 0) {
            if (!chronicleFull && message.contains("chronicle escapes")) {
                //DivMethods.handleChronicle(this);
                _currentTask = new ChronicleTask(this);
            }

            if (!chronicleFull && message.contains("capture the chronicle fragment")) {
                chronicleCount = Inventory.getQuantity(chronicle);
                chronicleTimer.reset();
                Methods.updateCurrentTask("New chronicle capture will be ready in " + (chronicleTimer.getRemainingTime() / 1000) + " seconds.", this);
            }

            if (!chronicleFull && message.contains("30 chronicle fragments")) {
                Methods.updateCurrentTask("Chronicle count has reached 30. Disabling Chronicle Capturing.", this);
                chronicleFull = true;
            }
        }
    }

    @Override
    public void updateInfo() {

        // UpdateUI all current values in the XPInfo List
        _XPInfoMap.values().forEach((xpInfo) -> xpInfo.update());

        _DisplayInfoMap.put("Energy Count: ", Integer.toString(energyCount) +
                " (" + Integer.toString((int)CommonMath.rate(TimeUnit.HOURS, _STOPWATCH.getRuntime(), energyCount)) + "/Hour)");
        _DisplayInfoMap.put("GP: ",Integer.toString(_grossIncome) +
                " (" + Integer.toString((int)CommonMath.rate(TimeUnit.HOURS, _STOPWATCH.getRuntime(), _grossIncome)) + "/Hour)");

        try {
            _info = new Info(_XPInfoMap,           // XP Info
                    _DisplayInfoMap,
                    _STOPWATCH.getRuntimeAsString(),        // Total Runtime
                    _currentTaskString,                     // Current Task
                    _abTaskString);                         // antiban Task

        }catch(Exception e){
            e.printStackTrace();
        }

        Platform.runLater(() -> infoUI.update());
    }

    public void setRiftArea(){
        riftArea = new Area.Circular(GameObjects.newQuery().names(rift, riftCache).results().first().getPosition(), 5);
        //riftArea = new Area.Circular(new Coordinate(500, 500, 0), 4);
        Execution.delay(500);
    }

    public boolean interactingWithWisp(){
        Player player = Players.getLocal();
        if(player != null)
            if(DIV_ANIM.contains(player.getAnimationId()))
                return true;
            else
                return false;
        else
            return false;
    }
}
