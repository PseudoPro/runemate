package com.sudo.v2.spectre.bots.sudodivination.tasks;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.spectre.common.helpers.SudoInteract;
import com.sudo.v2.spectre.common.helpers.SudoPlayer;
import com.sudo.v2.spectre.bots.sudodivination.SudoDivination;
import com.sudo.v2.base.SudoTask;
import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.region.Npcs;

/**
 * SudoTask to handle interaction with Enriched Wisp/Spring
 */
public class EnrichedTask extends SudoTask {
    private SudoDivination bot;
    private Npc wispNpc;

    public EnrichedTask(SudoDivination bot){
        super();
        this.bot = bot;
    }

    @Override
    public void Loop(){
        if(bot.harvestEnriched && (wispNpc = Npcs.newQuery().names("Enriched " + bot.wisp.toLowerCase(), "Enriched " + bot.spring.toLowerCase()).results().first())!= null){
            if(!SudoPlayer.isInAnimation(bot.DIV_ANIM)) {
                Methods.updateCurrentTask("Attempting to Interact with Enriched", bot);
                SudoInteract.interactWith(wispNpc, bot.wispAction, bot._useMiddleMouseCamera);
            }
        }else {
            Methods.updateCurrentTask("EnrichedTask Complete", bot);
            Complete();
        }
    }
}
