package com.sudo.v2.spectre.bots.sudodivination.tasks;

import com.sudo.v2.spectre.common.helpers.*;
import com.sudo.v2.spectre.bots.sudodivination.SudoDivination;
import com.sudo.v2.base.SudoTask;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;

import java.util.regex.Pattern;

/**
 * SudoTask to handle interaction with Energy Rift
 */
public class ConvertTask extends SudoTask {

    private SudoDivination bot;
    private Pattern pattern;

    public ConvertTask(SudoDivination bot){
        this.bot = bot;
        pattern = Pattern.compile("^.*memory*.$");
    }

    @Override
    public void Loop() {

        if ((Inventory.newQuery().names(pattern).results().size() <= 0)) {
            Methods.updateCurrentTask("ConvertTask Complete", bot);
            Complete();
        } else {
            if (!SudoPlayer.isInAnimation(bot.DIV_ANIM)) {
                GameObject riftObj = GameObjects.newQuery().names(bot.rift, bot.riftCache).results().nearest();
                if (riftObj != null) {
                    //UpdateUI.updateCurrentTask("Attempting ConvertTask", bot);
                    //Rift is too far away from player (12 - 20 spaces away)
                    if (Players.getLocal().distanceTo(riftObj) > (10 + (int) (Math.random() * 9))) {
                        Methods.updateCurrentTask("Rift Object is outside of renderable area. Attempting to travel to Rift.", bot);
                        SudoTravel.bresenhamTowards(bot.riftArea);
                    } else {
                        Methods.updateCurrentTask("Attempting to interact with Rift.", bot);
                        if(SudoInteract.interactWithClick(riftObj, bot._useMiddleMouseCamera, bot.riftAction))
                            Execution.delayUntil(() -> bot.DIV_ANIM.contains(Players.getLocal().getAnimationId()), 1000, 2500);
                    }
                } else {
                    Methods.updateCurrentTask("Rift Object is null. Attempting to travel to Rift.", bot);
                    SudoTravel.bresenhamTowards(bot.riftArea);
                }
            }
        }
    }
}
