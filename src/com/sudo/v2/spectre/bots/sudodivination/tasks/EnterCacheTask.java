package com.sudo.v2.spectre.bots.sudodivination.tasks;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.spectre.common.helpers.SudoInteract;
import com.sudo.v2.spectre.bots.sudodivination.SudoDivination;
import com.sudo.v2.base.SudoTask;
import com.runemate.game.api.hybrid.region.GameObjects;

/**
 * Created by SudoPro on 4/10/2016.
 */
public class EnterCacheTask extends SudoTask {
    private SudoDivination bot;

    public EnterCacheTask (SudoDivination bot){
        this.bot = bot;
    }

    @Override
    public void Loop(){
        if(GameObjects.newQuery().names("Unstable rift").results().nearest() != null){
            Methods.updateCurrentTask("EnterCacheTask Complete", bot);
            Complete();
        }
        else{
            Methods.updateCurrentTask("Entering Cache", bot);
            SudoInteract.interactWith(GameObjects.newQuery().actions("Enter cache").results().nearest(), "Enter cache", bot._useMiddleMouseCamera);
        }
    }

}
