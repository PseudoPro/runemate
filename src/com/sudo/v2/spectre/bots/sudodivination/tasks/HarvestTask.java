package com.sudo.v2.spectre.bots.sudodivination.tasks;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.spectre.common.helpers.SudoInteract;
import com.sudo.v2.spectre.common.helpers.SudoPlayer;
import com.sudo.v2.spectre.bots.sudodivination.SudoDivination;
import com.sudo.v2.base.SudoTask;
import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;

/**
 * SudoTask to handle interaction normal wisps/springs
 */
public class HarvestTask extends SudoTask {
    private SudoDivination bot;
    private Npc wispNpc;

    public HarvestTask(SudoDivination bot) {
        super();
        this.bot = bot;
    }

    @Override
    public void Loop(){

        if((Inventory.getItems().size() >= bot.invDepositNum)){
            Methods.updateCurrentTask("HarvestTask Complete", bot);
            Complete();
        }else {
            if(!bot.interactingWithWisp()) {
                Area aroundPlayer = new Area.Circular(Players.getLocal().getPosition(), bot.radius);

                if (wispNpc == null || !wispNpc.isValid()) {
                    if ((wispNpc = Npcs.newQuery().names(bot.wisp).within(aroundPlayer).results().random()) != null) {
                        Methods.updateCurrentTask("Targeting wisp", bot);
                        if(SudoInteract.interactWith(wispNpc, bot.wispAction, bot._useMiddleMouseCamera))
                            Execution.delayUntil(() -> SudoPlayer.isInAnimation(bot.DIV_ANIM), 2000, 3000);
                    }
                    // If there exists a wisp spring around the player
                    else if ((wispNpc = Npcs.newQuery().names(bot.spring).within(aroundPlayer).results().nearest()) != null) {
                        Methods.updateCurrentTask("Targeting spring", bot);
                        if(SudoInteract.interactWith(wispNpc, bot.wispAction, bot._useMiddleMouseCamera))
                            Execution.delayUntil(() -> SudoPlayer.isInAnimation(bot.DIV_ANIM), 2000, 3000);
                    }
                    // If nothing is around player, interact with closest wisp/spring
                    else {
                        try {
                            wispNpc = Npcs.newQuery().names(bot.wisp, bot.spring).results().nearest();
                            Methods.updateCurrentTask("No wisp or spring located within scan radius.\nTargeting with closest wisp/spring", bot);
                            if(SudoInteract.interactWith(wispNpc, bot.wispAction, bot._useMiddleMouseCamera))
                                Execution.delayUntil(() -> SudoPlayer.isInAnimation(bot.DIV_ANIM), 2000, 3000);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    Methods.updateCurrentTask("Interacting with wisp/spring", bot);
                    if(SudoInteract.interactWith(wispNpc, bot.wispAction, bot._useMiddleMouseCamera))
                        Execution.delayUntil(() -> SudoPlayer.isInAnimation(bot.DIV_ANIM), 2000, 3000);
                }
            }
            else{
                Execution.delay(200, 1000);
            }

            Execution.delayWhile(bot._player::isMoving, 1500, 2500);
        }
    }

}
