package com.sudo.v2.spectre.bots.sudodivination.tasks;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.spectre.common.helpers.SudoInteract;
import com.sudo.v2.spectre.bots.sudodivination.SudoDivination;
import com.sudo.v2.base.SudoTask;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.Skill;
import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;

import java.util.ArrayList;

/**
 * Created by SudoPro on 4/10/2016.
 */
public class GuthixianCacheTask extends SudoTask {
    private SudoDivination bot;
    private Player player;
    private GameObject unstableRift, memory;
    private boolean usingRetrieve = false;

    private ArrayList<String> memories = new ArrayList<String>(){
        {
            add("Tiny raw memory");
            add("Raw memory");
            add("Large memory");
            add("Tiny memory");
        }
    };

    public GuthixianCacheTask(SudoDivination bot){
        this.bot = bot;
    }

    @Override
    public void Loop(){

        player = Players.getLocal();

        if(bot.riftArea.getPosition().distanceTo(bot._player) < 100){
            Methods.updateCurrentTask("GuthixianCacheTask Complete", bot);
            Complete();
        }
        else {
            // Remove weapon (if equiped)
            if (Equipment.getItemIn(Equipment.Slot.WEAPON) != null) {
                if (Equipment.getItemIn(Equipment.Slot.WEAPON).getDefinition() != null) {
                    if (!memories.contains(Equipment.getItemIn(Equipment.Slot.WEAPON).getDefinition().getName())) {
                        Methods.updateCurrentTask("Unequipping Weapon", bot);
                        Equipment.getItemIn(Equipment.Slot.WEAPON).interact("Remove");
                        Execution.delayWhile(() -> Equipment.getItemIn(Equipment.Slot.WEAPON) != null, 2000);
                        Loop();
                    }
                }
            } else if (Equipment.getItemIn(Equipment.Slot.SHIELD) != null) { // Remove shield/off-hand (if equipped)
                Methods.updateCurrentTask("Unequipping Off-hand", bot);
                Equipment.getItemIn(Equipment.Slot.SHIELD).interact("Remove");
                Execution.delayWhile(() -> Equipment.getItemIn(Equipment.Slot.SHIELD) != null, 2000);
                Loop();
            }



            // Find unstable Rift
            unstableRift = GameObjects.newQuery().names("Unstable rift").results().nearest();

            // If player is holding a memory
            if (Equipment.getItemIn(Equipment.Slot.WEAPON) != null) {
                if (memories.contains(Equipment.getItemIn(Equipment.Slot.WEAPON).getDefinition().getName())) {
                    // Deposit memory
                    Methods.updateCurrentTask("Depositing memory", bot);
                    SudoInteract.interactWith(unstableRift, "Offer memory", bot._useMiddleMouseCamera);
                } else {
                    memory = findMemory();
                    Methods.updateCurrentTask("Grabbing memory", bot);
                    if(usingRetrieve)
                        SudoInteract.interactWith(memory, "Retrieve", bot._useMiddleMouseCamera);
                    else
                        SudoInteract.interactWith(memory, "Take memory", bot._useMiddleMouseCamera);
                }
            } else {
                memory = findMemory();
                Methods.updateCurrentTask("Grabbing memory", bot);
                if(usingRetrieve)
                    SudoInteract.interactWith(memory, "Retrieve", bot._useMiddleMouseCamera);
                else
                    SudoInteract.interactWith(memory, "Take memory", bot._useMiddleMouseCamera);
            }

        }
    }

    private GameObject findMemory(){
        GameObject obj;
        int lvl = Skill.DIVINATION.getCurrentLevel();

        if(lvl < 45) {
            obj = GameObjects.newQuery().names(memories.get(0)).results().nearest();
            usingRetrieve = false;
        }
        else if(lvl >= 45 && lvl < 85) {
            obj = GameObjects.newQuery().names(memories.get(1)).results().nearest();
            usingRetrieve = false;
        }
        else {
            obj = GameObjects.newQuery().names(memories.get(2)).results().nearest();
            usingRetrieve = true;
        }

        if(obj != null)
            return obj;
        else
            return null;
    }

}
