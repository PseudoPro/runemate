package com.sudo.v2.spectre.bots.sudodivination.tasks;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.spectre.bots.sudodivination.SudoDivination;
import com.sudo.v2.base.SudoTask;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.hybrid.queries.results.SpriteItemQueryResults;

import java.util.regex.Pattern;

/**
 * Created by SudoPro on 4/3/2016.
 */
public class DropItemsTask extends SudoTask {
    private SudoDivination bot;
    private Pattern pattern;

    public DropItemsTask(SudoDivination bot){
        super();
        this.bot = bot;
        pattern = Pattern.compile("(?i)^(.*logs|wooden knot)");
    }

    @Override
    public void Loop(){
        SpriteItemQueryResults inventoryResults = Inventory.newQuery().names(pattern).results();
        SpriteItem inventoryFirst = inventoryResults.first();

        if((inventoryResults).size() > 0){
            Methods.updateCurrentTask("Attemping DropItemsTask", bot);
            for(int i = 0; i < inventoryFirst.getDefinition().getInventoryActions().size(); i++)
                Methods.debug("Has Action: " + inventoryFirst.getDefinition().getInventoryActions().get(i));
            Methods.debug("Item is Visible: " + inventoryFirst.isVisible());
            Methods.debug("Item is Valid: " + inventoryFirst.isValid());
            inventoryFirst.interact("Drop");
        }
        else{
            Methods.updateCurrentTask("DropItemsTask Complete", bot);
            Complete();
        }
    }
}
