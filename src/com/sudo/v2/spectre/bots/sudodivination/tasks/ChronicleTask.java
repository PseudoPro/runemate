package com.sudo.v2.spectre.bots.sudodivination.tasks;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.spectre.common.helpers.SudoInteract;
import com.sudo.v2.spectre.bots.sudodivination.SudoDivination;
import com.sudo.v2.base.SudoTask;
import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;

/**
 * SudoTask to handle Chronicle Interaction
 */
public class ChronicleTask extends SudoTask {
    SudoDivination bot;
    Npc chronicleNpc;

    public ChronicleTask(SudoDivination bot){
        this.bot = bot;
    }

    @Override
    public void Loop(){
        if (!bot.chronicleFull && (chronicleNpc = Npcs.newQuery().names(bot.chronicle).results().nearest()) != null) {
                SudoInteract.interactWith(chronicleNpc, bot.chronicleAction, bot._useMiddleMouseCamera);
                Execution.delayWhile(Players.getLocal()::isMoving, 500, 1500);
        }
        else {
            if(Inventory.getQuantity(bot.chronicle) >= 30){
                Methods.updateCurrentTask("Chronicle count has reached 30. Disabling Chronicle Capturing.", bot);
                bot.chronicleFull = true;
            }
            Methods.updateCurrentTask("ChronicleTask Complete", bot);
            Complete();
        }
    }

}
