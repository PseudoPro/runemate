package com.sudo.v2.spectre.bots.sudodungeon;

import com.sudo.v2.spectre.bots.sudodungeon.dungeon.Dungeon;
import com.runemate.game.api.script.framework.LoopingScript;

/**
 * sudodungeon Bot for RuneMate spectre
 */
public class SudoDungeon extends LoopingScript {

    Dungeon dungeon;

    @Override
    public void onStart(String ... args) {
        dungeon = new Dungeon();
        dungeon.onStart();
    }

    @Override
    public void onLoop() {
        dungeon.onLoop();
    }

    @Override
    public void onStop() {
        dungeon.onStop();
    }
}
