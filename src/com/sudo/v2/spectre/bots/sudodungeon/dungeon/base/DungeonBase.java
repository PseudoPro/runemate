package com.sudo.v2.spectre.bots.sudodungeon.dungeon.base;

import com.sudo.v2.spectre.bots.sudodungeon.dungeon.rooms.base.RoomBase;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.region.Players;

import java.util.List;

/**
 * DungeonBase Abstract Class
 */
public abstract class DungeonBase {
    public List<RoomBase> Rooms;
    public Player CurrentPlayer;

    public DungeonBase() {
        CurrentPlayer = Players.getLocal();
    }

    /**
     * Returns closest object that matches the id passed in
     * @param id: id of the desired object
     * @return GameObject matching @id closest to the player's location
     */
    /*public GameObject Search(int id) {
        JButton button = new JButton();
        button.addActionListener((actionEvent) -> System.out.println());
        double shortestDistance = Double.MAX_VALUE;
        GameObject closestObject = null;
        for (RoomBase room : Rooms) {
            GameObject gameObject = room.Search(id);
            if (gameObject != null) {
                double tempDistance = CurrentPlayer.distanceTo(gameObject);
                if (shortestDistance > tempDistance) {
                    shortestDistance = tempDistance;
                    closestObject = gameObject;
                }
            }
        }

        return closestObject;
    }*/
}
