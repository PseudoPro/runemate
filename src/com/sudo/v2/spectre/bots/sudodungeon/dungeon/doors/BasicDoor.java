package com.sudo.v2.spectre.bots.sudodungeon.dungeon.doors;

import com.sudo.v2.spectre.bots.sudodungeon.dungeon.doors.base.DoorBase;
import com.sudo.v2.spectre.bots.sudodungeon.dungeon.doors.enums.DoorType;
import com.sudo.v2.spectre.bots.sudodungeon.dungeon.rooms.base.RoomBase;
import com.runemate.game.api.hybrid.entities.GameObject;

/**
 * Generic door found within Dungeoneering
 */
public final class BasicDoor extends DoorBase {

    private BasicDoor(DoorBuilder doorBuilder) {
        super(doorBuilder);
    }

    @Override
    public boolean Unlockable() {
        return true;
    }

    @Override
    public boolean Unlock() {
        return true;
    }

    @Override
    public DoorType GetDoorType() {
        return DoorType.Basic;
    }

    public static BasicDoorBuilder BuildNew(GameObject door, RoomBase room) {
        return new BasicDoorBuilder(door, room);
    }

    protected static final class BasicDoorBuilder extends DoorBuilder {

        public BasicDoorBuilder(GameObject door, RoomBase room) {
            super(door, room);
        }

        @Override
        public BasicDoor Build() {
            return new BasicDoor(this);
        }
    }
}
