package com.sudo.v2.spectre.bots.sudodungeon.dungeon;

import com.sudo.v2.spectre.bots.sudodungeon.dungeon.rooms.BasicRoom;
import com.runemate.game.api.hybrid.entities.Player;

/**
 * dungeon Class representing the dungeon the playing is in
 */
public class Dungeon {
    protected Player player;
    protected BasicRoom test_room;

    public Dungeon() {}

    public void onStart() {
        /*player = Players.getLocal();

        // pick up food/equipment
        GroundItems.newQuery().filter(i -> i.getDefinition().getInventoryActions().contains("Eat")).results().forEach(i -> {
            i.interact("Take", i.getDefinition().getName());
            Execution.delayUntil(() -> !i.isValid() || !i.isVisible(), 2000, 3000);
            Execution.delay(100, 500);
        });*/
        test_room = BasicRoom.BuildNew().Build();
    }

    public void onLoop() {
        if (test_room != null)
            test_room.onLoop();
    }

    public void onStop() {

    }

    public void onPause() {

    }

    public void onResume() {

    }
}
