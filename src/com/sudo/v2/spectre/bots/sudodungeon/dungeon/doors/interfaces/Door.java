package com.sudo.v2.spectre.bots.sudodungeon.dungeon.doors.interfaces;

import com.sudo.v2.interfaces.Entity;
import com.sudo.v2.spectre.bots.sudodungeon.dungeon.doors.enums.DoorType;

/**
 * Door Interface Class
 */
public interface Door extends Entity {
    boolean Unlockable();
    boolean Unlock();
    boolean Open();
    DoorType GetDoorType();
}
