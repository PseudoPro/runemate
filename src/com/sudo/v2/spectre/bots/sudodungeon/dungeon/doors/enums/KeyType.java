package com.sudo.v2.spectre.bots.sudodungeon.dungeon.doors.enums;

/**
 * KeyType enum
 *
 * Typed of keys that can be found in a dungeon
 */
public enum KeyType {
    PurpleCorner
}
