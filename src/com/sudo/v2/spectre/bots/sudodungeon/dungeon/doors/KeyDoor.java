package com.sudo.v2.spectre.bots.sudodungeon.dungeon.doors;

import com.sudo.v2.spectre.bots.sudodungeon.dungeon.doors.base.DoorBase;
import com.sudo.v2.spectre.bots.sudodungeon.dungeon.doors.enums.DoorType;
import com.sudo.v2.spectre.bots.sudodungeon.dungeon.rooms.base.RoomBase;
import com.runemate.game.api.hybrid.entities.GameObject;

/**
 * Key Door type found in dungeoneering
 */
public final class KeyDoor extends DoorBase {

    private final String _keyName;
    private KeyDoor(KeyDoorBuilder builder) {
        super(builder);

        this._keyName = builder._keyName;
    }

    @Override
    public boolean Unlockable() {
        // TODO
        return false;
    }

    @Override
    public boolean Unlock() {
        return true;
    }
    @Override
    public DoorType GetDoorType() {
        return DoorType.Key;
    }

    public static KeyDoorBuilder BuildNew(GameObject door, RoomBase room, String keyName) {
        return new KeyDoorBuilder(door, room, keyName);
    }

    protected static final class KeyDoorBuilder extends DoorBuilder {

        private final String _keyName;
        public KeyDoorBuilder(GameObject door, RoomBase room, String keyName) {
            super(door, room);

            this._keyName = keyName;
        }

        @Override
        public KeyDoor Build() {
            return new KeyDoor(this);
        }
    }
}
