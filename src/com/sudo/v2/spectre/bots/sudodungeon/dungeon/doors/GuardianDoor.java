package com.sudo.v2.spectre.bots.sudodungeon.dungeon.doors;

import com.sudo.v2.spectre.bots.sudodungeon.dungeon.doors.base.DoorBase;
import com.sudo.v2.spectre.bots.sudodungeon.dungeon.doors.enums.DoorType;
import com.sudo.v2.spectre.bots.sudodungeon.dungeon.rooms.base.RoomBase;
import com.runemate.game.api.hybrid.entities.GameObject;

/**
 * The Guardian Door type found within dungeoneering
 */
public final class GuardianDoor extends DoorBase {

    private GuardianDoor(DoorBuilder doorBuilder) {
        super(doorBuilder);
    }

    @Override
    public boolean Unlockable() {
        return false;
    }

    @Override
    public boolean Unlock() {
        return true;
    }
    @Override
    public DoorType GetDoorType() {
        return DoorType.Guardian;
    }

    public static GuardianDoorBuilder BuildNew(GameObject door, RoomBase room) {
        return new GuardianDoorBuilder(door, room);
    }


    protected static final class GuardianDoorBuilder extends DoorBuilder {

        public GuardianDoorBuilder(GameObject door, RoomBase room) {
            super(door, room);
        }

        @Override
        public GuardianDoor Build() {
            return new GuardianDoor(this);
        }
    }
}
