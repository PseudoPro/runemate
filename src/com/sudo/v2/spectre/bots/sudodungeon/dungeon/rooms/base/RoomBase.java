package com.sudo.v2.spectre.bots.sudodungeon.dungeon.rooms.base;

import com.sudo.v2.interfaces.Builder;
import com.sudo.v2.spectre.bots.sudodungeon.dungeon.rooms.interfaces.Room;
import com.runemate.game.api.hybrid.entities.GameObject;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;

/**
 * RoomBase Abstract Class
 */
public abstract class RoomBase implements Room {
    public List<Room> Rooms;

    protected RoomBase(RoomBuilder builder) {}

    public GameObject Search(int id) {
        throw new NotImplementedException();
    }
    

    public static abstract class RoomBuilder implements Builder<Room> {
        public RoomBuilder() {}
    }
}
