package com.sudo.v2.spectre.bots.sudodungeon.dungeon.rooms.interfaces;

import com.sudo.v2.spectre.bots.sudodungeon.dungeon.rooms.enums.RoomType;

/**
 * Room Interface Class
 */
public interface Room {
    RoomType GetRoomType();
}
