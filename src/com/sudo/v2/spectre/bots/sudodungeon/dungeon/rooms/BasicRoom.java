package com.sudo.v2.spectre.bots.sudodungeon.dungeon.rooms;

import com.sudo.v2.spectre.bots.sudodungeon.dungeon.rooms.base.RoomBase;
import com.sudo.v2.spectre.bots.sudodungeon.dungeon.rooms.enums.RoomType;
import com.runemate.game.api.hybrid.entities.GroundItem;
import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.queries.results.LocatableEntityQueryResults;
import com.runemate.game.api.hybrid.region.GroundItems;
import com.runemate.game.api.hybrid.region.Npcs;

import java.util.function.Supplier;

/**
 * BasicRoom type found in dungeoneering
 */
public final class BasicRoom extends RoomBase {

    private LocatableEntityQueryResults<Npc> _enemies;
    private LocatableEntityQueryResults<GroundItem> _groundItems;
    private Supplier<Boolean> _currentTaskCompleteCheck;

    private BasicRoom(BasicRoomBuilder builder) {
        super(builder);

        _groundItems = GroundItems.newQuery().filter(i -> i.getDefinition().getInventoryActions().contains("Eat")).results();
        _enemies = Npcs.getLoaded();
    }

    @Override
    public RoomType GetRoomType() {
        return RoomType.Basic;
    }

    public static BasicRoomBuilder BuildNew() {
        return new BasicRoomBuilder();
    }

    public void onLoop() {

        // see if we have something else going on
        if (_currentTaskCompleteCheck != null && !_currentTaskCompleteCheck.get()) {
            System.out.println("Still trying to pick that thing up");
            return;
        }

        // might be something good to lewt
        if (_groundItems.size() > 0 && !Inventory.isFull()) {
            GroundItem groundItemToPickUp = _groundItems.nearest();
            groundItemToPickUp.interact("Take", groundItemToPickUp.getDefinition().getName());
            System.out.println("picking up " + groundItemToPickUp.getDefinition().getName());

            _currentTaskCompleteCheck = () -> !groundItemToPickUp.isVisible() || !groundItemToPickUp.isValid();
        }
    }

    public static final class BasicRoomBuilder extends RoomBuilder {

        public BasicRoomBuilder() {}

        @Override
        public BasicRoom Build() {
            return new BasicRoom(this);
        }
    }
}
