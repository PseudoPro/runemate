package com.sudo.v2.spectre.bots.sudodungeon.dungeon.rooms;

import com.sudo.v2.spectre.bots.sudodungeon.dungeon.rooms.base.RoomBase;
import com.sudo.v2.spectre.bots.sudodungeon.dungeon.rooms.enums.RoomType;

/**
 * BossRoom type found in dungeoneering
 */
public final class BossRoom extends RoomBase {

    private BossRoom(BossRoomBuilder builder) {
        super(builder);
    }

    @Override
    public RoomType GetRoomType() {
        return RoomType.Boss;
    }

    public static BossRoomBuilder BuildNew() {
        return new BossRoomBuilder();
    }


    protected static final class BossRoomBuilder extends RoomBuilder {
        public BossRoomBuilder() {
        }

        @Override
        public BossRoom Build() {
            return new BossRoom(this);
        }
    }
}
