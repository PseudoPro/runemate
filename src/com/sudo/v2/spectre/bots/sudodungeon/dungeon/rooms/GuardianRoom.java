package com.sudo.v2.spectre.bots.sudodungeon.dungeon.rooms;

import com.sudo.v2.spectre.bots.sudodungeon.dungeon.rooms.base.RoomBase;
import com.sudo.v2.spectre.bots.sudodungeon.dungeon.rooms.enums.RoomType;

/**
 * GuardianRoom type found in dungeoneering
 */
public final class GuardianRoom extends RoomBase {

    private GuardianRoom(GuardianRoomBuilder builder) {
        super(builder);
    }

    @Override
    public RoomType GetRoomType() {
        return RoomType.Guardian;
    }

    public static GuardianRoomBuilder BuildNew() {
        return new GuardianRoomBuilder();
    }


    protected static final class GuardianRoomBuilder extends RoomBuilder {

        protected GuardianRoomBuilder() {}

        @Override
        public GuardianRoom Build() {
            return new GuardianRoom(this);
        }
    }
}
