package com.sudo.v2.spectre.bots.sudodungeon.dungeon.rooms.enums;

/**
 * RoomType enum
 *
 * Types of Rooms you can find in a dungeon
 */
public enum RoomType {
    Basic,
    Guardian,
    Puzzle,
    Boss
}
