package com.sudo.v2.spectre.bots.sudodungeon.dungeon.doors;

import com.sudo.v2.spectre.bots.sudodungeon.dungeon.doors.base.DoorBase;
import com.sudo.v2.spectre.bots.sudodungeon.dungeon.doors.enums.DoorType;
import com.sudo.v2.spectre.bots.sudodungeon.dungeon.rooms.base.RoomBase;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.local.Skill;

/**
 * SkillDoor type found in dungeoneering
 */
public final class SkillDoor extends DoorBase {
    private Skill _skillType;
    private int _skillRequired;

    private SkillDoor(SkillDoorBuilder builder) {
        super(builder);

        _skillType = builder._skillType;
        _skillRequired = builder._skillRequired;
    }

    @Override
    public boolean Unlockable() {
        return _skillType.getCurrentLevel() >= _skillRequired;
    }

    @Override
    public boolean Unlock() {
        return true;
    }

    @Override
    public DoorType GetDoorType() {
        return DoorType.Skill;
    }

    public static SkillDoorBuilder BuildNew(GameObject door, RoomBase room, Skill skillType) {
        return new SkillDoorBuilder(door, room, skillType);
    }


    protected static final class SkillDoorBuilder extends DoorBuilder {
        private final Skill _skillType;
        private int _skillRequired = 0;

        public SkillDoorBuilder(GameObject door, RoomBase room, Skill skillType) {
            super(door, room);

            _skillType = skillType;
        }
        public SkillDoorBuilder Skill(int skillRequired) {
            _skillRequired = skillRequired;
            return this;
        }

        @Override
        public SkillDoor Build() {
            return new SkillDoor(this);
        }
    }
}
