package com.sudo.v2.spectre.bots.sudodungeon.dungeon.doors.base;

import com.sudo.v2.interfaces.Builder;
import com.sudo.v2.spectre.bots.sudodungeon.dungeon.doors.interfaces.Door;
import com.sudo.v2.spectre.bots.sudodungeon.dungeon.rooms.base.RoomBase;
import com.runemate.game.api.hybrid.entities.GameObject;

/**
 * DoorBase Abstract Class
 */
public abstract class DoorBase implements Door {
    protected final GameObject _door;
    protected final RoomBase _room;
    private boolean _isUnlocked = false;

    protected DoorBase(DoorBuilder doorBuilder) {
        _door = doorBuilder._door;
        _room = doorBuilder._room;
    }

    public void WalkTo() {
    }

    public boolean IsUnlocked() {
        return _isUnlocked;
    }

    @Override
    public GameObject GetGameObject() {
        return _door;
    }

    @Override
    public boolean Open() {
        // TODO
        return true;
    }

    protected abstract static class DoorBuilder implements Builder<Door> {
        private final GameObject _door;
        private final RoomBase _room;

        public DoorBuilder(GameObject door, RoomBase room) {
            _door = door;
            _room = room;
        }
    }
}
