package com.sudo.v2.spectre.bots.sudodungeon.dungeon.rooms;

import com.sudo.v2.spectre.bots.sudodungeon.dungeon.rooms.base.RoomBase;
import com.sudo.v2.spectre.bots.sudodungeon.dungeon.rooms.enums.RoomType;

/**
 * PuzzleRoom type found in dungeoneering
 */
public final class PuzzleRoom extends RoomBase {

    private PuzzleRoom(PuzzleRoomBuilder builder) {
        super(builder);
    }

    @Override
    public RoomType GetRoomType() {
        return RoomType.Puzzle;
    }

    public static PuzzleRoomBuilder BuildNew() {
        return new PuzzleRoomBuilder();
    }

    protected static final class PuzzleRoomBuilder extends RoomBuilder {

        public PuzzleRoomBuilder() {}

        @Override
        public PuzzleRoom Build() {
            return new PuzzleRoom(this);
        }
    }
}
