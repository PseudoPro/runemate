package com.sudo.v2.spectre.bots.sudodungeon.dungeon.doors.enums;

/**
 * DoorType enum
 *
 * Types of Doors found within a dungeon
 */
public enum DoorType {
    Basic,
    Guardian,
    Skill,
    Key
}
