package com.sudo.v2.spectre.bots.sudowoodcutter.ui;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.spectre.bots.sudowoodcutter.assets.Areas;
import com.sudo.v2.spectre.bots.sudowoodcutter.SudoWoodcutter;
import com.sudo.v2.spectre.bots.sudowoodcutter.enums.DropType;
import com.sudo.v2.ui.base.SudoFXController;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Java FX Controller for the CookerFXGui class
 */
public class CutterFXController extends SudoFXController implements Initializable {

    private SudoWoodcutter bot;

    @FXML
    private ListView locationListView, treeListView, treeP_LV;

    @FXML
    private Button startB_BT, startP_BT;

    @FXML
    private TabPane tabPane;

    @FXML
    private Tab bank_Tab, powerChop_Tab;

    @FXML
    private ComboBox dropMethod_ComB;

    @FXML
    private CheckBox Nest_CB, powerChop_CB, bank_CB;

    @FXML
    private Label Prompt_L, PromptP_L;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Initialize Super
        super.initialize(location, resources);

        // Add Locations to the ListView
        treeListView.getItems().addAll("Evergreen", "Oak", "Willow", "Maple", "Yew", "Magic");
        treeP_LV.getItems().addAll("Evergreen", "Tree", "Oak", "Willow", "Maple", "Yew", "Magic");

        dropMethod_ComB.getItems().addAll("c1d1", "fullDrop", "random");

        treeListView.setOnMouseClicked(getTreeClickAction());
        treeP_LV.setOnMouseClicked((getTreeP_LVAction()));

        locationListView.setOnMouseClicked(getLocationClickAction());

        startB_BT.setOnAction(getStart_BTAction());
        startP_BT.setOnAction(getStart_BTAction());

        powerChop_CB.setOnAction(getPowerChop_CBAction());
        bank_CB.setOnAction(getBank_CB_CBAction());

        dropMethod_ComB.setOnAction(getDropMethod_ComBAction());

    }

    public CutterFXController(SudoWoodcutter bot) {
        super(bot);
        this.bot = bot;
    }

    private EventHandler<ActionEvent> getPowerChop_CBAction(){
        return event -> {
            if(treeP_LV.getSelectionModel().getSelectedItem() != null && dropMethod_ComB.getSelectionModel().getSelectedItem() != null && powerChop_CB.isSelected()) {
                startP_BT.setDisable(false);
                startB_BT.setDisable(true);
            }
            else if(locationListView.getSelectionModel().getSelectedItem() != null && bank_CB.isSelected()) {
                startP_BT.setDisable(true);
                startB_BT.setDisable(false);
            }
            else {
                startP_BT.setDisable(true);
                startB_BT.setDisable(true);
            }

            if(powerChop_CB.isSelected()) {
                tabPane.getSelectionModel().select(powerChop_Tab);
                bank_CB.setSelected(false);
                treeP_LV.setDisable(false);
                dropMethod_ComB.setDisable(false);
                locationListView.setDisable(true);
                treeListView.setDisable(true);
                Nest_CB.setDisable(true);
            }
            else{
                powerChop_CB.setSelected(false);
                bank_CB.setSelected(true);
                treeP_LV.setDisable(true);
                dropMethod_ComB.setDisable(true);
                locationListView.setDisable(false);
                treeListView.setDisable(false);
                Nest_CB.setDisable(false);
            }
        };
    }

    private EventHandler<ActionEvent> getBank_CB_CBAction(){
        return event -> {
            if(treeP_LV.getSelectionModel().getSelectedItem() != null && dropMethod_ComB.getSelectionModel().getSelectedItem() != null && powerChop_CB.isSelected()) {
                startP_BT.setDisable(false);
                startB_BT.setDisable(true);
            }
            else if(locationListView.getSelectionModel().getSelectedItem() != null && bank_CB.isSelected()) {
                startP_BT.setDisable(true);
                startB_BT.setDisable(false);
            }
            else {
                startP_BT.setDisable(true);
                startB_BT.setDisable(true);
            }

            if(bank_CB.isSelected()) {
                tabPane.getSelectionModel().select(bank_Tab);
                powerChop_CB.setSelected(false);
                treeP_LV.setDisable(true);
                dropMethod_ComB.setDisable(true);
                locationListView.setDisable(false);
                treeListView.setDisable(false);
                Nest_CB.setDisable(false);
            }
            else{
                powerChop_CB.setSelected(true);
                bank_CB.setSelected(false);
                treeP_LV.setDisable(false);
                dropMethod_ComB.setDisable(false);
                locationListView.setDisable(true);
                treeListView.setDisable(true);
                Nest_CB.setDisable(true);
            }
        };
    }

    private EventHandler<MouseEvent> getTreeP_LVAction(){
        return event -> {
            if(treeP_LV.getSelectionModel().getSelectedItem() != null && dropMethod_ComB.getSelectionModel().getSelectedItem() != null && powerChop_CB.isSelected()) {
                startP_BT.setDisable(false);
                startB_BT.setDisable(true);
            }
            else if(locationListView.getSelectionModel().getSelectedItem() != null && bank_CB.isSelected()) {
                startP_BT.setDisable(true);
                startB_BT.setDisable(false);
            }
            else {
                startP_BT.setDisable(true);
                startB_BT.setDisable(true);
            }
        };
    }

    private EventHandler<ActionEvent> getDropMethod_ComBAction() {
        return event -> {
            if (treeP_LV.getSelectionModel().getSelectedItem() != null && dropMethod_ComB.getSelectionModel().getSelectedItem() != null && powerChop_CB.isSelected()) {
                startP_BT.setDisable(false);
                startB_BT.setDisable(true);
            } else if (locationListView.getSelectionModel().getSelectedItem() != null && bank_CB.isSelected()) {
                startP_BT.setDisable(true);
                startB_BT.setDisable(false);
            } else {
                startP_BT.setDisable(true);
                startB_BT.setDisable(true);
            }
        };
    }

    private EventHandler<MouseEvent> getLocationClickAction() {
        return event -> {
            if(treeP_LV.getSelectionModel().getSelectedItem() != null && dropMethod_ComB.getSelectionModel().getSelectedItem() != null && powerChop_CB.isSelected()) {
                startP_BT.setDisable(false);
                startB_BT.setDisable(true);
            }
            else if(locationListView.getSelectionModel().getSelectedItem() != null && bank_CB.isSelected()) {
                startP_BT.setDisable(true);
                startB_BT.setDisable(false);
            }
            else {
                startP_BT.setDisable(true);
                startB_BT.setDisable(true);
            }
        };
    }


    private EventHandler<MouseEvent> getTreeClickAction() {
        return event -> {
            locationListView.getItems().clear();
            if(treeP_LV.getSelectionModel().getSelectedItem() != null && dropMethod_ComB.getSelectionModel().getSelectedItem() != null && powerChop_CB.isSelected()) {
                startP_BT.setDisable(false);
                startB_BT.setDisable(true);
            }
            else if(locationListView.getSelectionModel().getSelectedItem() != null && bank_CB.isSelected()) {
                startP_BT.setDisable(true);
                startB_BT.setDisable(false);
            }
            else {
                startP_BT.setDisable(true);
                startB_BT.setDisable(true);
            }

            try {
                switch (treeListView.getSelectionModel().getSelectedItem().toString()) {
                    case "Evergreen":
                        locationListView.getItems().addAll("Catherby");
                        break;

                    case "Oak":
                        locationListView.getItems().add("Catherby");
                        break;

                    case "Willow":
                        locationListView.getItems().add("Draynor");
                        locationListView.getItems().add("Catherby");
                        locationListView.getItems().add("Seers");
                        break;

                    case "Maple":
                        locationListView.getItems().add("Seers");
                        break;

                    case "Yew":
                        locationListView.getItems().add("G.E.");
                        locationListView.getItems().add("Catherby");
                        locationListView.getItems().add("Seers");
                        break;

                    case "Magic":
                        locationListView.getItems().add("Seers South-West");
                        locationListView.getItems().add("Seers South");
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
    }
    private EventHandler<ActionEvent> getStart_BTAction() {
        return event -> {
            String location = null, tree = null;

            if(bank_CB.isSelected()) {
                // Handle Banking Startup
                powerChop_CB.setDisable(true);
                powerChop_Tab.setDisable(true);
                bot.powerChop = false;

                try {
                    location = locationListView.getSelectionModel().getSelectedItem().toString();
                    tree = treeListView.getSelectionModel().getSelectedItem().toString();
                } catch (Exception e) {
                    Prompt_L.textProperty().set("Be sure to have location and tree both selected.");
                    Methods.debug("Be sure to have location and tree both selected.");
                }

                if (location != null && tree != null) {
                    bot.treeName = tree;
                    bot.pickUpNest = Nest_CB.isSelected();
                    try {
                        switch (tree) {
                            case "Evergreen":
                                switch (location) {
                                    case "Catherby":
                                        bot.chopArea = Areas.CATH_EVER_TREE_AREA;
                                        bot.treeArea = Areas.CATH_EVER_TREE_AREA;
                                        if(bot._isRS3)
                                            bot.bankArea = Areas.CATH_RS3_BANK_AREA;
                                        else
                                            bot.bankArea = Areas.CATH_OSRS_BANK_AREA;
                                        break;
                                }
                                break;
                            case "Oak":
                                switch (location) {
                                    case "Catherby":
                                        bot.chopArea = Areas.CATH_OAK_TREE_AREA;
                                        bot.treeArea = Areas.CATH_OAK_TREE_AREA;
                                        if(bot._isRS3)
                                            bot.bankArea = Areas.CATH_RS3_BANK_AREA;
                                        else
                                            bot.bankArea = Areas.CATH_OSRS_BANK_AREA;
                                        break;
                                }
                                break;
                            case "Willow":
                                switch (location) {
                                    case "Draynor":
                                        bot.chopArea = Areas.DRAY_WILL_TREE_AREA;
                                        bot.treeArea = Areas.DRAY_WILL_TREE_ACTION_AREA;
                                        bot.bankArea = Areas.DRAY_BANK_AREA;
                                        break;
                                    case "Catherby":
                                        bot.chopArea = Areas.CATH_WILL_TREE_AREA;
                                        bot.treeArea = Areas.CATH_WILL_TREE_ACTION_AREA;
                                        if(bot._isRS3)
                                            bot.bankArea = Areas.CATH_RS3_BANK_AREA;
                                        else
                                            bot.bankArea = Areas.CATH_OSRS_BANK_AREA;
                                        break;
                                    case "Seers":
                                        bot.chopArea = Areas.SEER_WILL_TREE_AREA;
                                        bot.treeArea = Areas.SEER_WILL_TREE_AREA;
                                        bot.bankArea = Areas.SEER_BANK_AREA;
                                        break;
                                }
                                break;
                            case "Maple":
                                if(bot._isRS3)
                                    bot.treeName = "Maple Tree";
                                else
                                    bot.treeName = "Maple tree";

                                switch (location) {
                                    case "Seers":
                                        bot.chopArea = Areas.SEER_MAPLE_TREE_AREA;
                                        bot.treeArea = Areas.SEER_MAPLE_TREE_AREA;
                                        bot.bankArea = Areas.SEER_BANK_AREA;
                                        break;
                                }
                                break;
                            case "Yew":
                                switch (location) {
                                    case "G.E.":
                                        bot.chopArea = Areas.GE_YEW_TREE_AREA;
                                        bot.treeArea = Areas.GE_YEW_TREE_AREA;
                                        bot.bankArea = Areas.GE_BANK_AREA;
                                        break;
                                    case "Catherby":
                                        bot.chopArea = Areas.CATH_YEW_TREE_AREA;
                                        bot.treeArea = Areas.CATH_YEW_TREE_ACTION_AREA;
                                        if(bot._isRS3)
                                            bot.bankArea = Areas.CATH_RS3_BANK_AREA;
                                        else
                                            bot.bankArea = Areas.CATH_OSRS_BANK_AREA;
                                        break;
                                    case "Seers":
                                        bot.chopArea = Areas.SEER_YEW_TREE_AREA;
                                        bot.treeArea = Areas.SEER_YEW_TREE_AREA;
                                        bot.bankArea = Areas.SEER_BANK_AREA;
                                        break;
                                }
                                break;
                            case "Magic":
                                bot.treeName = "Magic tree";
                                switch (location) {
                                    case "Seers South-West":
                                        bot.chopArea = Areas.SEER_MAGIC_SW_TREE_AREA;
                                        bot.treeArea = Areas.SEER_MAGIC_SW_TREE_AREA;
                                        bot.bankArea = Areas.SEER_BANK_AREA;
                                        break;
                                    case "Seers South":
                                        bot.chopArea = Areas.SEER_MAGIC_S_TREE_AREA;
                                        bot.treeArea = Areas.SEER_MAGIC_S_TREE_ACTION_AREA;
                                        bot.bankArea = Areas.SEER_BANK_AREA;
                                        break;
                                }
                                break;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }// Handle Power Chopping Startup
            else {
                bank_CB.setDisable(true);
                bank_Tab.setDisable(true);

                try {
                    bot.treeName = treeP_LV.getSelectionModel().getSelectedItem().toString();

                    switch (bot.treeName) {
                        case "Maple":
                            bot.treeName = "Maple Tree";
                            break;
                        case "Magic":
                            bot.treeName = "Magic tree";
                            break;
                    }

                    switch(dropMethod_ComB.getSelectionModel().getSelectedIndex()){
                        case 0:
                            bot.dropMethod = DropType.c1d1;
                            break;
                        case 1:
                            bot.dropMethod = DropType.fullDrop;
                            break;
                        case 2:
                            bot.dropMethod = DropType.random;
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                    PromptP_L.textProperty().set("Be sure to have tree and dropType both selected.");
                    Methods.debug("Be sure to have tree and dropType both selected.");
                }

                bot.powerChop = true;
            }
            startB_BT.textProperty().set("Resume");
            startP_BT.textProperty().set("Resume");
            Prompt_L.textProperty().set(" ");
            super.getStart_BTEvent();
        };
    }
}