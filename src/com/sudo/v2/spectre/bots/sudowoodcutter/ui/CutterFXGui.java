package com.sudo.v2.spectre.bots.sudowoodcutter.ui;

import com.sudo.v2.spectre.bots.sudowoodcutter.SudoWoodcutter;
import com.runemate.game.api.hybrid.util.Resources;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.GridPane;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Java FX Gui for configuring ApexChopper bot settings
 */
public class CutterFXGui extends GridPane implements Initializable {

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setVisible(true);
    }

    public CutterFXGui(SudoWoodcutter bot){

        FXMLLoader loader = new FXMLLoader();

        Future<InputStream> stream = bot.getPlatform().invokeLater(() -> Resources.getAsStream("com/sudo/v2/spectre/bots/sudowoodcutter/ui/CutterGUI.fxml"));

        // Set this class as root of the fxml file, and SudoBuddyFXGui as the controller.
        loader.setController(new CutterFXController(bot));
        loader.setRoot(this);

        try {
            loader.load(stream.get());
        } catch (IOException | InterruptedException | ExecutionException e) {
            System.err.println("Error loading GUI");
            e.printStackTrace();
        }
    }
}
