package com.sudo.v2.spectre.bots.sudowoodcutter.assets;

import java.util.HashSet;

/**
 * Created by joeyw on 4/15/2016.
 */
public class Animations {

    public final static HashSet<Integer> CHOPPING_ANIM = new HashSet<Integer>(){{
        add(21177);
        add(21186);
        add(2794);
        add(21191);
        add(21192);
        add(867);
        add(868);
        add(869);
        add(870);
        add(871);
        add(872);
        add(873);
        add(874);
        add(875);
        add(876);
        add(877);
        add(878);
        add(879);
        add(880);
        add(881);
        add(2846);
    }};

}
