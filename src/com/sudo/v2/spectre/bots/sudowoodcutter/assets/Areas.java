package com.sudo.v2.spectre.bots.sudowoodcutter.assets;

import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;

/**
 * Created by SudoPro on 4/16/2016.
 */
public class Areas {

    public final static Area CATH_RS3_BANK_AREA = new Area.Rectangular(new Coordinate(2797, 3442, 0), new Coordinate(2794, 3437, 0));
    public final static Area CATH_OSRS_BANK_AREA = new Area.Rectangular(new Coordinate(2795, 3437, 0), new Coordinate(2797, 3442, 0));
    public final static Area CATH_WILL_TREE_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2786, 3428, 0), new Coordinate(2787, 3430, 0), new Coordinate(2784, 3432, 0), new Coordinate(2778, 3432, 0), new Coordinate(2776, 3428, 0), new Coordinate(2780, 3426, 0), new Coordinate(2783, 3425, 0)});
    public final static Area CATH_WILL_TREE_ACTION_AREA =new Area.Polygonal(new Coordinate[]{new Coordinate(2770, 3431, 0), new Coordinate(2766, 3428, 0), new Coordinate(2766, 3423, 0), new Coordinate(2773, 3422, 0), new Coordinate(2779, 3421, 0), new Coordinate(2787, 3422, 0), new Coordinate(2790, 3427, 0), new Coordinate(2791, 3433, 0), new Coordinate(2785, 3435, 0)});
    public final static Area CATH_OAK_TREE_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2804, 3443, 0), new Coordinate(2804, 3450, 0), new Coordinate(2796, 3453, 0), new Coordinate(2787, 3457, 0), new Coordinate(2776, 3458, 0), new Coordinate(2768, 3456, 0), new Coordinate(2767, 3449, 0), new Coordinate(2767, 3442, 0), new Coordinate(2771, 3439, 0), new Coordinate(2778, 3437, 0), new Coordinate(2783, 3431, 0), new Coordinate(2793, 3430, 0)});
    public final static Area CATH_YEW_TREE_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2760, 3429, 0), new Coordinate(2767, 3431, 0), new Coordinate(2768, 3434, 0), new Coordinate(2757, 3434, 0), new Coordinate(2754, 3431, 0)});
    public final static Area CATH_YEW_TREE_ACTION_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2748, 3436, 0), new Coordinate(2748, 3432, 0), new Coordinate(2750, 3427, 0), new Coordinate(2753, 3424, 0), new Coordinate(2765, 3424, 0), new Coordinate(2772, 3428, 0), new Coordinate(2776, 3438, 0), new Coordinate(2772, 3443, 0), new Coordinate(2767, 3440, 0), new Coordinate(2764, 3437, 0)});
    public final static Area CATH_EVER_TREE_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2789, 3474, 0), new Coordinate(2797, 3470, 0), new Coordinate(2806, 3470, 0), new Coordinate(2810, 3474, 0), new Coordinate(2811, 3479, 0), new Coordinate(2804, 3483, 0), new Coordinate(2805, 3487, 0), new Coordinate(2802, 3489, 0), new Coordinate(2796, 3487, 0), new Coordinate(2790, 3492, 0), new Coordinate(2784, 3493, 0), new Coordinate(2776, 3492, 0), new Coordinate(2777, 3479, 0), new Coordinate(2783, 3474, 0)});
    public final static Area GE_BANK_AREA = new Area.Rectangular(new Coordinate(3176, 3500, 0), new Coordinate(3185, 3509, 0));
    public final static Area GE_YEW_TREE_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(3203, 3506, 0), new Coordinate(3200, 3505, 0), new Coordinate(3202, 3501, 0), new Coordinate(3205, 3500, 0), new Coordinate(3212, 3499, 0), new Coordinate(3225, 3499, 0), new Coordinate(3225, 3506, 0)});
    public final static Area DRAY_BANK_AREA = new Area.Rectangular(new Coordinate(3092, 3240, 0), new Coordinate(3097, 3246, 0));
    public final static Area DRAY_WILL_TREE_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(3089, 3231, 0), new Coordinate(3088, 3237, 0), new Coordinate(3083, 3240, 0), new Coordinate(3082, 3231, 0)});
    public final static Area DRAY_WILL_TREE_ACTION_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(3085, 3240, 0), new Coordinate(3082, 3242, 0), new Coordinate(3078, 3240, 0), new Coordinate(3081, 3232, 0), new Coordinate(3085, 3223, 0), new Coordinate(3090, 3224, 0), new Coordinate(3092, 3227, 0), new Coordinate(3089, 3237, 0)});
    //public final static Area DRAY_ELDER_TREE_AREA = new Area.Rectangular(new Coordinate(3100, 3220, 0), new Coordinate(3093, 3210, 0));
    //public final static Area DRAY_ELDER_TREE_ACTION_AREA = new Area.Rectangular(new Coordinate(3099, 3219, 0), new Coordinate(3094, 3211, 0));
    public final static Area SEER_BANK_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2724, 3488, 0), new Coordinate(2724, 3491, 0), new Coordinate(2721, 3491, 0), new Coordinate(2721, 3494, 0), new Coordinate(2730, 3494, 0), new Coordinate(2730, 3490, 0), new Coordinate(2727, 3491, 0), new Coordinate(2728, 3488, 0)});
    public final static Area SEER_WILL_TREE_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2704, 3515, 0), new Coordinate(2699, 3510, 0), new Coordinate(2700, 3502, 0), new Coordinate(2708, 3504, 0), new Coordinate(2715, 3504, 0), new Coordinate(2719, 3502, 0), new Coordinate(2722, 3505, 0), new Coordinate(2718, 3510, 0), new Coordinate(2714, 3514, 0), new Coordinate(2709, 3518, 0), new Coordinate(2706, 3517, 0)});
    public final static Area SEER_MAPLE_TREE_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2717, 3498, 0), new Coordinate(2717, 3505, 0), new Coordinate(2728, 3505, 0), new Coordinate(2734, 3505, 0), new Coordinate(2738, 3497, 0), new Coordinate(2734, 3490, 0), new Coordinate(2731, 3490, 0), new Coordinate(2731, 3498, 0)});
    public final static Area SEER_YEW_TREE_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2718, 3457, 0), new Coordinate(2719, 3462, 0), new Coordinate(2713, 3468, 0), new Coordinate(2704, 3467, 0), new Coordinate(2705, 3456, 0)});
    public final static Area SEER_MAGIC_SW_TREE_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2704, 3423, 0), new Coordinate(2703, 3431, 0), new Coordinate(2691, 3433, 0), new Coordinate(2686, 3429, 0), new Coordinate(2687, 3423, 0), new Coordinate(2691, 3419, 0), new Coordinate(2702, 3420, 0)});
    public final static Area SEER_MAGIC_S_TREE_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2701, 3400, 0), new Coordinate(2698, 3397, 0), new Coordinate(2698, 3396, 0), new Coordinate(2700, 3394, 0), new Coordinate(2704, 3394, 0), new Coordinate(2706, 3396, 0), new Coordinate(2706, 3397, 0), new Coordinate(2703, 3400, 0)});
    public final static Area SEER_MAGIC_S_TREE_ACTION_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2707, 3401, 0), new Coordinate(2698, 3401, 0), new Coordinate(2698, 3393, 0), new Coordinate(2707, 3394, 0)});


}
