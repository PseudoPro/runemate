package com.sudo.v2.spectre.bots.sudowoodcutter.enums;

/**
 * Created by SudoPro on 7/6/2016.
 */

public enum DropType {
    c1d1,
    fullDrop,
    random,
    noDrop
}
