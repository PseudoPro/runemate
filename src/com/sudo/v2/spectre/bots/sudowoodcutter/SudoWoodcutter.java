package com.sudo.v2.spectre.bots.sudowoodcutter;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.spectre.bots.sudowoodcutter.ui.CutterFXGui;
import com.sudo.v2.spectre.bots.sudowoodcutter.ui.CutterInfoUI;
import com.sudo.v2.spectre.bots.sudowoodcutter.enums.DropType;
import com.sudo.v2.spectre.bots.sudowoodcutter.tasks.*;
import com.sudo.v2.ui.Info;
import com.sudo.v2.ui.SudoBuddyFXGui;
import com.sudo.v2.ui.model.XPInfo;
import com.sudo.v2.base.SudoBot;
import com.sudo.v2.interfaces.ISudoTask;
import com.runemate.game.api.client.embeddable.EmbeddableUI;
import com.runemate.game.api.hybrid.RuneScape;
import com.runemate.game.api.hybrid.entities.definitions.ItemDefinition;
import com.runemate.game.api.hybrid.local.Skill;
import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.GroundItems;
import com.runemate.game.api.hybrid.util.calculations.CommonMath;
import com.runemate.game.api.script.framework.listeners.InventoryListener;
import com.runemate.game.api.script.framework.listeners.SkillListener;
import com.runemate.game.api.script.framework.listeners.events.ItemEvent;
import com.runemate.game.api.script.framework.listeners.events.SkillEvent;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by joeyw on 4/13/2016.
 */
public class SudoWoodcutter extends SudoBot implements EmbeddableUI, InventoryListener, SkillListener {
    public Area bankArea = new Area.Rectangular(new Coordinate(5, 5, 0), new Coordinate(5, 5, 0));
    public Area chopArea = new Area.Rectangular(new Coordinate(5, 5, 0), new Coordinate(5, 5, 0));
    public Area treeArea = new Area.Rectangular(new Coordinate(5, 5, 0), new Coordinate(5, 5, 0));

    public String treeName = "", hatchet = "";

    public String[] hatchetList = null;

    public boolean firstTimeLoggedIn = true, pickUpNest = true, powerChop = false, wieldHatchet = false;

    private CutterFXGui configUI;
    private CutterInfoUI infoUI;

    private int logCount = 0, logPrice = 0, dropCount = -1;

    public DropType dropMethod = DropType.noDrop;

    public SudoWoodcutter() {
        super();
        setEmbeddableUI(this);
    }

    @Override
    public ObjectProperty<? extends Node> botInterfaceProperty() {
        if (_botInterfaceProperty == null) {
            _botInterfaceProperty = new SimpleObjectProperty<>(configUI = new CutterFXGui(this));
            _GUI = new ArrayList<Node>();
            _GUI.add(_sbGUI = new SudoBuddyFXGui(this));
            _GUI.add(configUI);
            _GUI.add(infoUI = new CutterInfoUI(this));
        }
        return _botInterfaceProperty;
    }

    @Override
    public void onStart(String... args) {
        super.onStart();

        getEventDispatcher().addListener(this);

        // Add Skills to track to the HashMap
        _XPInfoMap.put(Skill.WOODCUTTING, new XPInfo(Skill.WOODCUTTING));
    }

    @Override
    public void onLoop() {
        super.onLoop();

        if (RuneScape.isLoggedIn()) {
            if(firstTimeLoggedIn){
                if(_isRS3){
                    if(powerChop) {
                        if(Inventory.isFull())
                            _currentTask = new PowerDropTask(this);
                        else
                            _currentTask = new PowerChopTask(this);
                    }
                    else
                        _currentTask = new ChopTask(this);
                }
                else{
                    if(!powerChop) {
                        if (hatchetList == null)
                            hatchetList = getHatchetList();
                        if (hatchet.equals(""))
                            hatchet = getCurrentHatchet();

                        if (!hatchet.equals(""))
                            Methods.updateCurrentTask("Current Hatchet set to \"" + hatchet + "\"", this);
                        else
                            Methods.updateCurrentTask("No Hatchet found on player, running to check bank.", this);

                        Methods.updateCurrentTask("Setting task to ChopTask", this);
                        _currentTask = new ChopTask(this);
                    }
                    else {
                        Methods.updateCurrentTask("Dropping method set to \"" + dropMethod + "\"", this);

                        if (Inventory.isFull()) {
                            Methods.updateCurrentTask("Inventory full, setting task to PowerDropTask", this);
                            _currentTask = new PowerDropTask(this);
                        }
                        else {
                            Methods.updateCurrentTask("Setting task to PowerChopTask", this);
                            _currentTask = new PowerChopTask(this);
                        }
                    }
                }

                _currentTask.OnTaskCompleted((ISudoTask task) -> OnTaskComplete(task));
                _currentTask.Start();

                firstTimeLoggedIn = false;
            }

            if(!_isRS3)
                if(wieldHatchet && Inventory.contains(hatchet))
                    Inventory.newQuery().names(hatchet).results().first().interact("Wield");

            // Keep this out of startup in case they change GUI
            if (powerChop && dropCount <= 0 && dropMethod == DropType.random) {
                dropCount = 1 + (int) (Math.random() * 27);
                Methods.updateCurrentTask("Random drop count has been set to: " + dropCount, this);
            }

            if (!powerChop && GroundItems.newQuery().names("Bird's nest", "Bird nest").within(new Area.Circular(_player.getPosition(), 4)).results().nearest() != null) {
                _currentTask = new PickUpNestTask(this);
                _currentTask.OnTaskCompleted((ISudoTask task) -> OnTaskComplete(task));
            }

            _currentTask.Loop();    // Loop current task
        }
    }

    @Override
    public void OnTaskComplete(ISudoTask completedTask) {
        if (completedTask instanceof ChopTask)
            _currentTask = new BankInventoryTask(this);
        else if (completedTask instanceof BankInventoryTask)
            _currentTask = new ChopTask(this);
        else if (completedTask instanceof PickUpNestTask)
            _currentTask = new ChopTask(this);
        if (completedTask instanceof PowerChopTask)
            _currentTask = new PowerDropTask(this);
        else if (completedTask instanceof PowerDropTask)
            _currentTask = new PowerChopTask(this);

        _currentTask.OnTaskCompleted((ISudoTask task) -> OnTaskComplete(task));
        _currentTask.Start();
    }

    @Override
    public void onItemAdded(ItemEvent event) {
        if (!(_currentTask instanceof BankInventoryTask))
            super.onItemAdded(event);
        ItemDefinition definition = event.getItem().getDefinition();
        if (definition != null) {
            if (definition.getName().toLowerCase().contains("log")) {
                logCount++;

                if(powerChop){
                    switch(dropMethod) {
                        case fullDrop:
                            if (Inventory.isFull()) {
                                _currentTask = new PowerDropTask(this);
                                _currentTask.OnTaskCompleted((ISudoTask task) -> OnTaskComplete(task));
                                _currentTask.Start();
                            }
                            break;
                        case random:
                            if(Inventory.getItems().size() > dropCount){
                                if(!(_currentTask instanceof PowerDropTask)) {
                                    dropCount = 1 + (int) (Math.random() * 27);
                                    Methods.updateCurrentTask("Random drop count has been set to: " + dropCount, this);
                                }
                                _currentTask = new PowerDropTask(this);
                                _currentTask.OnTaskCompleted((ISudoTask task) -> OnTaskComplete(task));
                                _currentTask.Start();
                            }else if(dropCount < 0){
                                dropCount = 1 + (int) (Math.random() * 27);
                                Methods.updateCurrentTask("Random drop count has been set to: " + dropCount, this);
                            }
                            break;
                        case c1d1:
                            _currentTask = new PowerDropTask(this);
                            _currentTask.OnTaskCompleted((ISudoTask task) -> OnTaskComplete(task));
                            _currentTask.Start();
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }

    @Override
    public void onItemRemoved(ItemEvent event) {
        if (!(_currentTask instanceof BankInventoryTask))
            super.onItemRemoved(event);
    }

    @Override
    public void onLevelUp(SkillEvent event){
        if(event != null){
            if(!_isRS3) {
                if (event.getSkill() == Skill.WOODCUTTING) {
                    int lvl = Skill.WOODCUTTING.getCurrentLevel();

                    switch (lvl) {
                        case 6:
                        case 11:
                        case 21:
                        case 31:
                        case 41:
                        case 61:
                            hatchet = "";
                            hatchetList = getHatchetList();
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }

    @Override
    public void updateInfo() {

        // UpdateUI all current values in the XPInfo List
        _XPInfoMap.values().forEach((xpInfo) -> xpInfo.update());

        _DisplayInfoMap.put("Log Count: ", Integer.toString(logCount) +
                " (" + Integer.toString((int) CommonMath.rate(TimeUnit.HOURS, _STOPWATCH.getRuntime(), logCount)) + "/Hour)");
        _DisplayInfoMap.put("GP: ", Integer.toString(_grossIncome) +
                " (" + Integer.toString((int) CommonMath.rate(TimeUnit.HOURS, _STOPWATCH.getRuntime(), _grossIncome)) + "/Hour)");

        try {
            _info = new Info(_XPInfoMap,           // XP Info
                    _DisplayInfoMap,
                    _STOPWATCH.getRuntimeAsString(),        // Total Runtime
                    _currentTaskString,                     // Current Task
                    _abTaskString);                         // antiban Task

        } catch (Exception e) {
            e.printStackTrace();
        }

        Platform.runLater(() -> infoUI.update());
    }

    public boolean hasHatchet() {
        if(!hatchet.equals("")) {
            if (Inventory.contains(hatchet))
                return true;
            else if (Equipment.newQuery().names(hatchet).results().size() > 0)
                return true;
            else
                return false;
        }
        else
            return false;
    }

    public String getCurrentHatchet() {
        if (Inventory.containsAnyOf(hatchetList))
            return Inventory.getItems(hatchetList).first().toString();
        else if (Equipment.newQuery().names(hatchetList).results().size() > 0)
            return Equipment.newQuery().names(hatchetList).results().first().toString();
        else
            return "";
    }

    public String[] getHatchetList(){
        int woodLvl = Skill.WOODCUTTING.getCurrentLevel();

        if(woodLvl <= Skill.ATTACK.getCurrentLevel())
            wieldHatchet = true;

        if(woodLvl > 60)
            return new String[]{"Infernal axe", "Dragon axe", "Rune axe", "Adamant axe", "Mithril axe",
                    "Black axe", "Steel axe", "Iron axe", "Bronze axe"};
        else if(woodLvl > 40)
            return new String[]{"Rune axe", "Adamant axe", "Mithril axe", "Black axe", "Steel axe", "Iron axe", "Bronze axe"};
        else if(woodLvl > 30)
            return new String[]{"Adamant axe", "Mithril axe", "Black axe", "Steel axe", "Iron axe", "Bronze axe"};
        else if(woodLvl > 20)
            return new String[]{"Mithril axe", "Black axe", "Steel axe", "Iron axe", "Bronze axe"};
        else if(woodLvl > 5)
            return new String[]{"Black axe", "Steel axe", "Iron axe", "Bronze axe"};
        else
            return new String[]{"Iron axe", "Bronze axe"};
    }
}
