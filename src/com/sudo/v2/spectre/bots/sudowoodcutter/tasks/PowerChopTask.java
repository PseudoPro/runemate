package com.sudo.v2.spectre.bots.sudowoodcutter.tasks;

import com.sudo.v2.spectre.common.helpers.*;
import com.sudo.v2.spectre.bots.sudowoodcutter.assets.Animations;
import com.sudo.v2.spectre.bots.sudowoodcutter.SudoWoodcutter;
import com.sudo.v2.base.SudoTask;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.queries.results.LocatableEntityQueryResults;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.script.Execution;

import java.util.ArrayList;

/**
 * Created by SudoPro on 7/6/2016.
 */
public class PowerChopTask extends SudoTask {
    private SudoWoodcutter bot;

    private ArrayList<Coordinate> objList = new ArrayList<>();
    private LocatableEntityQueryResults<GameObject> goResults;
    private GameObject obj;
    private SudoTimer timer = new SudoTimer(5000, 15000);


    public PowerChopTask(SudoWoodcutter bot){
        timer.start();
        this.bot = bot;
    }

    @Override
    public void Loop(){

        if(obj == null || !obj.isValid() || timer.getRemainingTime() <= 0) {
            obj = GameObjects.newQuery().names(bot.treeName).actions("Chop down").results().nearestTo(new Area.Circular(bot._player.getPosition(), 10));
            timer.reset();
        }

        if(obj != null) {
            if(objList.size() < 32)
                objList.add(obj.getPosition());

            if (!SudoPlayer.isInAnimation(Animations.CHOPPING_ANIM)) {
                if (SudoInteract.interactWithClick(obj, bot._useMiddleMouseCamera, "Chop down")) {
                    Methods.updateCurrentTask("Attempting to interact with a tree", bot);
                    Execution.delayUntil(() -> bot._player.distanceTo(obj) < 6, 2000, 5000);
                }
            }
        }
        else{
            if(objList.size() > 0) {
                Methods.updateCurrentTask("Attempting to travel to previous seen tree location.", bot);
                SudoTravel.bresenhamTowards(new Area.Circular(objList.get((int) (Math.random() * objList.size() - 1)), 4));
            }
        }
    }
}
