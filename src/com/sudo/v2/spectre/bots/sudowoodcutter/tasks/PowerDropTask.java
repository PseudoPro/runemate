package com.sudo.v2.spectre.bots.sudowoodcutter.tasks;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.spectre.common.helpers.SudoInventory;
import com.sudo.v2.spectre.bots.sudowoodcutter.SudoWoodcutter;
import com.sudo.v2.spectre.bots.sudowoodcutter.enums.DropType;
import com.sudo.v2.base.SudoTask;
import com.runemate.game.api.hybrid.queries.results.SpriteItemQueryResults;

/**
 * Created by SudoPro on 7/6/2016.
 */
public class PowerDropTask extends SudoTask {
    private SudoWoodcutter bot;

    public PowerDropTask(SudoWoodcutter bot) {
        this.bot = bot;
    }

    @Override
    public void Loop() {
        SpriteItemQueryResults itemResults = null;
        if (bot.dropMethod != DropType.noDrop) {
//            if (bot._isRS3) {
//                if ((item = Inventory.newQuery().filter(i -> i.getDefinition().getName().toLowerCase().contains("log")).actions("Drop").results()).size() > 0) {
//                    ActionBar.Slot logSlot = ActionBar.newQuery().filter(i -> i.getName().toLowerCase().contains("log")).results().first();
//                    if (logSlot != null) {
//                        logSlot.activate();
//                    } else {
//                        item.first().interact("Drop");
//                    }
//                } else {
//                    UpdateUI.updateCurrentTask("Logs have been dropped", bot);
//                    Complete();
//                }
//            } else {
            Methods.updateCurrentTask("Attempting to drop logs", bot);

//            if ((itemResults = Inventory.newQuery().filter(i -> i.getDefinition().getName().toLowerCase().contains("log")).actions("Drop").results()).size() > 0) {
//                SudoInventory.drop(itemResults.first());
//            } else {
//                UpdateUI.updateCurrentTask("Logs have been dropped", bot);
//                Complete();
//            }
            if (!SudoInventory.dropAllContainsLower("log")) {
                Methods.updateCurrentTask("Logs have been dropped", bot);
                Complete();
            //}
            }
        } else {
            Methods.updateCurrentTask("DropType is set to noDrop", bot);
            Complete();
        }
    }
}
