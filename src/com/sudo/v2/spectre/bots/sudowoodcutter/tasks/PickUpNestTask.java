package com.sudo.v2.spectre.bots.sudowoodcutter.tasks;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.spectre.common.helpers.SudoInteract;
import com.sudo.v2.spectre.bots.sudowoodcutter.SudoWoodcutter;
import com.sudo.v2.base.SudoTask;
import com.runemate.game.api.hybrid.entities.GroundItem;
import com.runemate.game.api.hybrid.region.GroundItems;
import com.runemate.game.api.hybrid.region.Players;

/**
 * Created by joeyw on 4/13/2016.
 */
public class PickUpNestTask extends SudoTask {
    private SudoWoodcutter bot;
    private GroundItem nest;

    public PickUpNestTask(SudoWoodcutter bot){
        this.bot = bot;
    }

    @Override
    public void Loop(){
        nest = GroundItems.newQuery().names("Bird's nest", "Bird nest").results().nearest();
        if(nest == null){
            Methods.updateCurrentTask("PickUpNestTask Complete", bot);
            Complete();
        }
        else{
            Methods.updateCurrentTask("Attempting PickUpNestTask", bot);
            if(!Players.getLocal().isMoving())
                SudoInteract.interactWith(nest, "Take", bot._useMiddleMouseCamera);
        }
    }
}
