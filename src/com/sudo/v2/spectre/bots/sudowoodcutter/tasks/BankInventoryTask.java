package com.sudo.v2.spectre.bots.sudowoodcutter.tasks;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.spectre.common.helpers.SudoTravel;
import com.sudo.v2.spectre.bots.sudowoodcutter.SudoWoodcutter;
import com.sudo.v2.base.SudoTask;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.script.Execution;

import java.util.function.Predicate;

/**
 * BankInventoryTask SudoTask for the ApexChopper bot
 */
public class BankInventoryTask extends SudoTask {
    private SudoWoodcutter bot;

    public BankInventoryTask(SudoWoodcutter bot){
        this.bot = bot;
    }

    final Predicate<SpriteItem> itemFilter = item -> {
        if (item != null) {
            final String name = item.getDefinition().getName();
            if(name != null && !name.isEmpty())
                return name.contains("hatchet");
            else
                return false;
        }
        return false;
    };

    @Override
    public void Loop(){
        // If Inventory is empty or only contains a hatchet
        if(Inventory.getItems().size() <= 1 && !Bank.isOpen() && (bot._isRS3 || (!bot._isRS3 && bot.hasHatchet()))){
            Methods.updateCurrentTask("BankInventoryTask Complete", bot);
            Complete();
        }
        else{
            if(bot.bankArea.distanceTo(bot._player) < 5 || bot.bankArea.contains(bot._player)) {
                if (((!bot._isRS3 && Inventory.getItems().size() > 1) || (bot._isRS3 && !Inventory.isEmpty())) || (!bot._isRS3 && !bot.hasHatchet())) {
                    if (Bank.isOpen()) {
                        Methods.updateCurrentTask("Depositing Items", bot);
                        if(Bank.depositAllExcept(bot.hatchet))
                            Execution.delayUntil(() -> Inventory.getItems().size() < 2, 750, 1250);

                        // Go through the HATCHET array, if the bank contains one, withdraw it
                        if(!bot._isRS3 && !bot.hasHatchet()) {
                            if (Bank.containsAnyOf(bot.hatchetList)) {
                                for (String hatchet : bot.hatchetList) {
                                    if(Inventory.contains(hatchet)){
                                        Methods.updateCurrentTask("(No upgrade found) - Highest level hatchet found in inventory: " + hatchet, bot);
                                        bot.hatchet = hatchet;
                                        break;
                                    }
                                    if (Bank.containsAnyOf(hatchet)) {
                                        Methods.updateCurrentTask("(Upgrading to new Hatchet) - Withdrawing highest level hatchet: " + hatchet, bot);
                                        Bank.withdraw(hatchet, 1);
                                        bot.hatchet = hatchet;
                                        break;
                                    }
                                }
                            } else {
                                Methods.updateCurrentTask("Stopping bot. No Hatchet found.", bot);
                                bot.stop();
                            }
                        }

                    }
                    else {
                        Methods.updateCurrentTask("Opening Bank", bot);
                        Bank.open();
                    }
                } else {
                    if (Bank.isOpen()) {
                        Methods.updateCurrentTask("Closing Bank", bot);
                        Bank.close();
                    }
                }
            }
            else{
                Methods.updateCurrentTask("Running towards bank", bot);
                SudoTravel.goTowards(bot.bankArea);
            }
        }
    }
}
