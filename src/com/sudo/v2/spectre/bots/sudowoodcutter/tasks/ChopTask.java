package com.sudo.v2.spectre.bots.sudowoodcutter.tasks;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.spectre.common.helpers.SudoInteract;
import com.sudo.v2.spectre.common.helpers.SudoPlayer;
import com.sudo.v2.spectre.common.helpers.SudoTravel;
import com.sudo.v2.spectre.bots.sudowoodcutter.assets.Animations;
import com.sudo.v2.spectre.bots.sudowoodcutter.SudoWoodcutter;
import com.sudo.v2.base.SudoTask;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.script.Execution;

/**
 * ChopTask SudoTask for the ApexChopper bot
 */
public class ChopTask extends SudoTask {
    private SudoWoodcutter bot;
    private GameObject tree;

    public ChopTask(SudoWoodcutter bot){
        this.bot = bot;
    }

    @Override
    public void Loop(){
        if(Inventory.isFull() || (!bot._isRS3 && !bot.hasHatchet())){
            Methods.updateCurrentTask("ChopTask Complete", bot);
            Complete();
        }
        else{
            if(bot.treeArea.distanceTo(bot._player) < 12){
                if(!SudoPlayer.isInAnimation(Animations.CHOPPING_ANIM)){
                    Methods.updateCurrentTask("Attempting to interact with tree \"" + bot.treeName + "\"", bot);
                    tree = GameObjects.newQuery().names(bot.treeName).actions("Chop down").results().nearest();
                    if(tree != null) {
                        if (SudoInteract.interactWith(tree, "Chop down", bot._useMiddleMouseCamera))
                            Execution.delayUntil(bot._player::isMoving, 3000, 5000);
                    }
                    else {
                        Execution.delayUntil(() -> (tree = GameObjects.newQuery().names(bot.treeName).actions("Chop down").results().nearest()) != null, 15000, 25000);
                    }
                }
            }
            else{
                Methods.updateCurrentTask("Going towards tree area", bot);
                SudoTravel.goTowards(bot.chopArea);
            }
        }
    }

}
