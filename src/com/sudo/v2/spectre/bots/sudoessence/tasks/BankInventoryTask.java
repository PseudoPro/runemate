package com.sudo.v2.spectre.bots.sudoessence.tasks;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.spectre.common.helpers.SudoCamera;
import com.sudo.v2.spectre.bots.sudoessence.assets.Areas;
import com.sudo.v2.spectre.bots.sudoessence.SudoEssence;
import com.sudo.v2.base.SudoTask;
import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.script.Execution;

/**
 * Created by SudoPro on 6/19/2016.
 */
public class BankInventoryTask extends SudoTask {
    private SudoEssence bot;
    private GameObject bank;

    public BankInventoryTask(SudoEssence bot){
        this.bot = bot;
    }

    @Override
    public void Loop(){

        if(!Areas.VAR_BANK_AREA.contains(bot._player) && !Bank.isOpen()){
            Methods.updateCurrentTask("Player is not in bankingEnabled area, continuing to next task", bot);
            Complete();
        }
        else if(!Inventory.containsAnyOf("Rune essence", "Pure essence") && (Environment.isRS3() || (Environment.isOSRS() && bot.hasEquipment()))){
            Methods.updateCurrentTask("Banking Inventory Task Complete", bot);
            Complete();
        }
        else{
            if (!Bank.isOpen()) {
                Methods.updateCurrentTask("Opening Bank", bot);
                if ((bank = GameObjects.newQuery().actions("Bank").results().nearest()) != null) {
                    if (!bank.isVisible())
                        SudoCamera.turnTo(bank);
                    else {
                        if (Bank.open())
                            Execution.delayUntil(Bank::isOpen, 500, 1000);
                    }
                    bank = null;
                }
            }
            else{
                if(!Inventory.containsOnly(bot.pickaxe)) {
                    Methods.updateCurrentTask("Depositing Inventory", bot);
                    Bank.depositAllExcept(bot.pickaxe);
                }
                else if(!bot.hasEquipment() && Environment.isOSRS()){
                    String missingEquipment = bot.getMissingEquipment();
                    if(!missingEquipment.equals("")){
                        if (Bank.containsAnyOf(missingEquipment)) {
                            Methods.updateCurrentTask("Withdrawing the equipment " + missingEquipment, bot);
                            Bank.withdraw(missingEquipment, 1);
                        } else {
                            Methods.updateCurrentTask("Bot stopping, couldn't find " + missingEquipment, bot);
                            bot.stop();
                        }
                    }
                }
                else{
                    Methods.updateCurrentTask("Closing Bank", bot);
                    Bank.close();
                }
            }
        }
    }

}
