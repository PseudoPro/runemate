package com.sudo.v2.spectre.bots.sudoessence.tasks;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.spectre.common.helpers.SudoInteract;
import com.sudo.v2.spectre.common.helpers.SudoTravel;
import com.sudo.v2.spectre.bots.sudoessence.assets.Areas;
import com.sudo.v2.spectre.bots.sudoessence.SudoEssence;
import com.sudo.v2.base.SudoTask;
import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.script.Execution;

/**
 * Created by SudoPro on 6/19/2016.
 */
public class TravelToMineTask extends SudoTask {
    private SudoEssence bot;
    GameObject obj;

    public TravelToMineTask(SudoEssence bot){
        this.bot = bot;
    }

    @Override
    public void Loop(){
        if(bot.isInMine() || Inventory.isFull()){
            Methods.updateCurrentTask("Traveling to Mine task Complete", bot);
            Complete();
        }
        else{
            if(Areas.AUBURY_AREA.contains(bot._player) || Areas.AUBURY_W_COORD.equals(bot._player.getPosition()) || Areas.AUBURY_N_COORD.equals(bot._player.getPosition())){
                Methods.updateCurrentTask("Interacting with Aubury", bot);
                if(SudoInteract.interactWithClick(Npcs.newQuery().names("Aubury").actions("Teleport").results().nearest(), bot._useMiddleMouseCamera, "Teleport"))
                    Execution.delayUntil(() -> bot.isInMine(), 2000, 5000);
            }
            else{
                if(Bank.isOpen()){
                    Methods.updateCurrentTask("Closing Bank", bot);
                    Bank.close();
                }
                else if((obj = GameObjects.newQuery().names("Door").actions("Open").within(Areas.AUBURY_DOOR_AREA).results().nearest()) != null &&
                        !(Areas.AUBURY_AREA.contains(bot._player) || Areas.AUBURY_W_COORD.equals(bot._player.getPosition()) || Areas.AUBURY_N_COORD.equals(bot._player.getPosition()))){
                    Methods.updateCurrentTask("Attempting to open door", bot);
                    SudoInteract.interactWith(obj, "Open", bot._useMiddleMouseCamera);
                    obj = null;
                }else if(bot._player.distanceTo(Areas.AUBURY_WALKTO_AREA) > 15 && Environment.isOSRS()){
                    Methods.updateCurrentTask("Traveling to Aubury", bot);
                    SudoTravel.bresenhamTowards(Areas.AUBURY_WALKTO_AREA);
                    Execution.delayWhile(bot._player::isMoving, 2000, 2500);
                } else {
                    Methods.updateCurrentTask("Traveling to Aubury", bot);
                    SudoTravel.bresenhamTowards(Areas.AUBURY_AREA);
                    Execution.delayWhile(bot._player::isMoving, 2000, 2500);
                }
            }
        }
    }
}
