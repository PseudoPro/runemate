package com.sudo.v2.spectre.bots.sudoessence.tasks;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.spectre.common.helpers.SudoInteract;
import com.sudo.v2.spectre.common.helpers.SudoPlayer;
import com.sudo.v2.spectre.common.helpers.SudoTravel;
import com.sudo.v2.spectre.bots.sudoessence.assets.Animations;
import com.sudo.v2.spectre.bots.sudoessence.SudoEssence;
import com.sudo.v2.base.SudoTask;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.script.Execution;

/**
 * Created by SudoPro on 6/19/2016.
 */
public class MineEssenseTask extends SudoTask {
    private SudoEssence bot;
    private GameObject essRock;
    private int direction = 0;

    public MineEssenseTask(SudoEssence bot){
        this.bot = bot;
    }

    @Override
    public void Loop(){
        if(Inventory.isFull() || !bot.isInMine()){
            Methods.updateCurrentTask("Mining Task Complete, Inventory is full", bot);
            Complete();
        }
        else{
            if(direction <= 0)
                direction = (int)(Math.random() * 4) + 1;

//            if(Environment.isOSRS()) {
//                essRock = GameObjects.newQuery().filter(i -> {
//                    if (i.getDefinition() != null)
//                        if (i.getDefinition().getLocalState() != null)
//                            return i.getDefinition().getActions().contains("Mine") || i.getDefinition().getLocalState().getActions().contains("Mine") || i.getId() == 11916 || i.getId() == 11917;
//                        else
//                            return i.getDefinition().getActions().contains("Mine") || i.getId() == 11916 || i.getId() == 11917;
//                    else
//                        return i.getId() == 11916 || i.getId() == 11917;
//                }).results().nearest();
//            }
//            else{
//                essRock = GameObjects.newQuery().actions("Mine").results().nearest();
//            }

            essRock = GameObjects.newQuery().actions("Mine").results().nearest();

            if(essRock != null && essRock.isValid()){
                if(!SudoPlayer.isInAnimation(Animations.MINING_ANIM)) {
                    Methods.updateCurrentTask("Attempting to mine Rune Essence", bot);
//                    if(Environment.isOSRS())
//                        SudoInteract.interactWith(essRock, "Mine", bot.useMiddleMouseCamera);
//                    else
//                        SudoInteract.interactWith(essRock, "Mine", bot.useMiddleMouseCamera);
                    SudoInteract.interactWith(essRock, "Mine", bot._useMiddleMouseCamera);
                    Execution.delayWhile(bot._player::isMoving, 250, 1000);
                }
                direction = 0;
            }
            else{
                Methods.updateCurrentTask("Traveling to closest rock", bot);
                SudoTravel.bresenhamTowards(chooseRock());
                Execution.delayWhile(bot._player::isMoving, 750, 1500);
            }
        }
    }

    private Area chooseRock(){
        Area area;

        if(direction >= 4)
            area = new Area.Circular(new Coordinate(bot._player.getPosition().getX() + 25, bot._player.getPosition().getY() + 25), 5);
        else if (direction >= 3)
            area = new Area.Circular(new Coordinate(bot._player.getPosition().getX() - 25, bot._player.getPosition().getY() + 25), 5);
        else if (direction >= 2)
            area = new Area.Circular(new Coordinate(bot._player.getPosition().getX() - 25, bot._player.getPosition().getY() - 25), 5);
        else
            area = new Area.Circular(new Coordinate(bot._player.getPosition().getX() - 25, bot._player.getPosition().getY() - 25), 5);

        return area;
    }

//    // Get closest rock area and return it
//    private Area getClosestRock(){
//        Area area;
//
//        if(Areas.MINE_AREA1.contains(bot._player)) {
//            area = Areas.ESSENCE_NE_AREA1;
//
//            if (bot._player.distanceTo(area.getCenter()) > bot._player.distanceTo(Areas.ESSENCE_NW_AREA1.getCenter())) {
//                area = Areas.ESSENCE_NW_AREA1;
//            }
//
//            if (bot._player.distanceTo(area.getCenter()) > bot._player.distanceTo(Areas.ESSENCE_SE_AREA1.getCenter())) {
//                area = Areas.ESSENCE_SE_AREA1;
//            }
//
//            if (bot._player.distanceTo(area.getCenter()) > bot._player.distanceTo(Areas.ESSENCE_SW_AREA1.getCenter())) {
//                area = Areas.ESSENCE_SW_AREA1;
//            }
//        }
//        else if(Areas.MINE_AREA2.contains(bot._player)){
//            area = Areas.ESSENCE_NE_AREA2;
//
//            if (bot._player.distanceTo(area.getCenter()) > bot._player.distanceTo(Areas.ESSENCE_NW_AREA2.getCenter())) {
//                area = Areas.ESSENCE_NW_AREA2;
//            }
//
//            if (bot._player.distanceTo(area.getCenter()) > bot._player.distanceTo(Areas.ESSENCE_SE_AREA2.getCenter())) {
//                area = Areas.ESSENCE_SE_AREA2;
//            }
//
//            if (bot._player.distanceTo(area.getCenter()) > bot._player.distanceTo(Areas.ESSENCE_SW_AREA2.getCenter())) {
//                area = Areas.ESSENCE_SW_AREA2;
//            }
//        }else {
//            area = Areas.ESSENCE_NE_AREA3;
//
//            if (bot._player.distanceTo(area.getCenter()) > bot._player.distanceTo(Areas.ESSENCE_NW_AREA3.getCenter())) {
//                area = Areas.ESSENCE_NW_AREA3;
//            }
//
//            if (bot._player.distanceTo(area.getCenter()) > bot._player.distanceTo(Areas.ESSENCE_SE_AREA3.getCenter())) {
//                area = Areas.ESSENCE_SE_AREA3;
//            }
//
//            if (bot._player.distanceTo(area.getCenter()) > bot._player.distanceTo(Areas.ESSENCE_SW_AREA3.getCenter())) {
//                area = Areas.ESSENCE_SW_AREA3;
//            }
//        }
//
//
//
//        return area;
//    }
}
