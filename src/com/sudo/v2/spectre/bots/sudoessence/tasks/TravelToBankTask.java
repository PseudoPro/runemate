package com.sudo.v2.spectre.bots.sudoessence.tasks;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.spectre.common.helpers.SudoInteract;
import com.sudo.v2.spectre.common.helpers.SudoTravel;
import com.sudo.v2.spectre.bots.sudoessence.assets.Areas;
import com.sudo.v2.spectre.bots.sudoessence.SudoEssence;
import com.sudo.v2.base.SudoTask;
import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.entities.definitions.GameObjectDefinition;
import com.runemate.game.api.hybrid.entities.definitions.NpcDefinition;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.script.Execution;

/**
 * Created by SudoPro on 6/19/2016.
 */
public class TravelToBankTask extends SudoTask {
    private SudoEssence bot;
    private GameObject obj, portalObj;
    private Npc portalNpc;

    public TravelToBankTask(SudoEssence bot){
        this.bot = bot;
    }

    @Override
    public void Loop() {
        if (Areas.VAR_BANK_AREA.contains(bot._player) || Bank.isOpen()) {
            Methods.updateCurrentTask("Traveling to Bank task Complete", bot);
            Complete();
        } else if (bot.isInMine()) {
            Methods.updateCurrentTask("Attempting to leave mine area", bot);

            if (Environment.isOSRS()) {
                try {
                    portalObj = GameObjects.newQuery()
                            .filter(i -> {
                                GameObjectDefinition def;
                                GameObjectDefinition loc;
                                if ((def = i.getDefinition()) != null)
                                    if ((loc = def.getLocalState()) != null)
                                        return def.getName().equals("Portal") ||
                                                def.getActions().contains("Exit") ||
                                                def.getActions().contains("Use") ||
                                                loc.getName().equals("Portal") ||
                                                loc.getActions().contains("Use") ||
                                                loc.getActions().contains("Exit");
                                    else
                                        return def.getName().equals("Portal") ||
                                                def.getActions().contains("Exit") ||
                                                def.getActions().contains("Use");
                                else
                                    return false;
                            })
                            .results().nearest();

                    portalNpc = Npcs.newQuery()
                            .filter(i -> {
                                NpcDefinition def;
                                NpcDefinition loc;
                                if ((def = i.getDefinition()) != null)
                                    if ((loc = def.getLocalState()) != null)
                                        return def.getName().equals("Portal") ||
                                                def.getActions().equals("Use") ||
                                                def.getActions().equals("Exit") ||
                                                loc.getActions().contains("Use") ||
                                                loc.getName().equals("Portal") ||
                                                loc.getActions().contains("Exit") ||
                                                i.getId() == 5904 ||
                                                i.getId() == 5896;
                                    else
                                        return def.getName().equals("Portal") ||
                                                def.getActions().equals("Exit") ||
                                                def.getActions().equals("Use") ||
                                                i.getId() == 5904 ||
                                                i.getId() == 5896;
                                else
                                    return false;
                            })
                            .results().nearest();
                    if (portalNpc != null)
                        SudoInteract.interactWithClick(portalNpc, bot._useMiddleMouseCamera, "Use", "Exit");
                    else if (portalObj != null)
                        SudoInteract.interactWithClick(portalObj, bot._useMiddleMouseCamera, "Use", "Exit");

                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                }
            } else {
                portalNpc = Npcs.newQuery().names("Portal").actions("Enter").results().nearest();

                if (portalNpc != null)
                    SudoInteract.interactWithClick(portalNpc, bot._useMiddleMouseCamera, "Enter");
            }

            portalObj = null;
            portalNpc = null;
            //SudoInteract.interactWithClick(GameObjects.newQuery().names("Portal").results().nearest(), bot.useMiddleMouseCamera);
            Execution.delay(750, 1250);
        } else {
            if ((Areas.AUBURY_AREA.contains(bot._player) || Areas.AUBURY_W_COORD.equals(bot._player.getPosition()) || Areas.AUBURY_N_COORD.equals(bot._player.getPosition())) && (obj = GameObjects.newQuery().names("Door").actions("Open").within(Areas.AUBURY_DOOR_AREA).results().nearest()) != null) {
                Methods.updateCurrentTask("Attempting to Open Door", bot);
                SudoInteract.interactWith(obj, "Open", bot._useMiddleMouseCamera);
            } else if(bot._player.distanceTo(Areas.VAR_BANK_WALKTO_AREA) > 15 && Environment.isOSRS()){
                Methods.updateCurrentTask("Traveling to Bank", bot);
                SudoTravel.bresenhamTowards(Areas.VAR_BANK_WALKTO_AREA);
                Execution.delayWhile(bot._player::isMoving, 2000, 2500);
            } else {
                Methods.updateCurrentTask("Traveling to Bank", bot);
                SudoTravel.bresenhamTowards(Areas.VAR_BANK_AREA);
                Execution.delayWhile(bot._player::isMoving, 2000, 2500);
            }
        }

    }
}
