package com.sudo.v2.spectre.bots.sudoessence.ui;

import com.sudo.v2.spectre.bots.sudoessence.SudoEssence;
import com.runemate.game.api.hybrid.util.Resources;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.GridPane;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Java FX Gui used to configure settings for sudoessence bot
 */
public class EssenceFXGui extends GridPane implements Initializable {

    public EssenceFXGui(SudoEssence bot){
            // Create the FXML Loader
            FXMLLoader loader = new FXMLLoader();

            // Load the FXML file
            Future<InputStream> stream = bot.getPlatform().invokeLater(() -> Resources.getAsStream("com/sudo/v2/spectre/bots/sudoessence/ui/EssenceGUI.fxml"));

            // Set the root
            loader.setRoot(this);

            // Set the controller
            loader.setController(new EssenceFXController(bot));

        try {
            loader.load(stream.get());
        } catch (IOException | InterruptedException | ExecutionException e) {
            System.err.println("Error loading GUI");
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setVisible(true);
    }
}
