package com.sudo.v2.spectre.bots.sudoessence.ui;

import com.sudo.v2.spectre.bots.sudoessence.SudoEssence;
import com.sudo.v2.ui.base.SudoFXController;
import com.runemate.game.api.hybrid.Environment;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Java FX Controller for the EssenceFXGui class
 */
public class EssenceFXController extends SudoFXController implements Initializable {

    private final SudoEssence bot;
    private boolean isRS3 = true;

    @FXML
    private CheckBox limit_CB, wield_CB;

    @FXML
    private ComboBox pickaxe_CB;

    @FXML
    private TextField mined_TF;

    @FXML
    private Text mined_T, prompt_T, pickaxe_T;

    @FXML
    private Button start_BT;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        if(Environment.isRS3()) {
            //pickaxe_CB.getItems().addAll("Dwarven army axe", "Bronze pickaxe", "Iron pickaxe", "Steel pickaxe", "Mithril pickaxe",
            //        "Adamant pickaxe", "Rune pickaxe", "Inferno adze", "Dragon pickaxe", "Crystal pickaxe", "Imcando pickaxe");

            isRS3 = true;

            pickaxe_CB.setDisable(true);
            pickaxe_CB.setVisible(false);

            pickaxe_T.setDisable(true);
            pickaxe_T.setVisible(false);

            wield_CB.setDisable(true);
            wield_CB.setVisible(false);

            start_BT.setDisable(false);
        }
        else {
            isRS3 = false;

            pickaxe_CB.getItems().addAll("Bronze pickaxe", "Iron pickaxe", "Steel pickaxe", "Black pickaxe", "Mithril pickaxe",
                    "Adamant pickaxe", "Rune pickaxe", "Dragon pickaxe", "Infernal pickaxe");
            wield_CB.setDisable(false);
            wield_CB.setVisible(true);
        }

        mined_TF.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String s, String s2) {
                if(mined_TF.getLength() > 0) {
                    try{
                        Integer.parseInt(mined_TF.getText());
                    }catch(Exception e){
                        mined_TF.setText(mined_TF.getText(0, mined_TF.getText().length() - 1));
                        prompt_T.setText("Enter only integers");
                    }
                }
                if(mined_TF.getLength() > 0){
                    if(!isRS3) {
                        if (pickaxe_CB.getSelectionModel().getSelectedItem() != null) {
                            if (!limit_CB.isSelected() || (limit_CB.isSelected() && mined_TF.getText().length() > 0))
                                start_BT.setDisable(false);
                        } else
                            start_BT.setDisable(true);
                    }else
                        start_BT.setDisable(false);
                }else
                    start_BT.setDisable(true);
            }
        });

        pickaxe_CB.setOnAction(getpickaxe_CBAction());

        limit_CB.setOnAction(getlimit_CBAction());

        start_BT.setOnAction(getStartButtonAction());
    }

    public EssenceFXController(SudoEssence bot){
        super(bot);
        this.bot = bot;
    }

    private EventHandler<ActionEvent> getpickaxe_CBAction(){
        return event -> {
            if(pickaxe_CB.getSelectionModel().getSelectedItem() != null){
                if(!limit_CB.isSelected() || (limit_CB.isSelected() && mined_TF.getText().length() > 0))
                    start_BT.setDisable(false);
            }
            else{
                start_BT.setDisable(true);
            }
        };
    }

    private EventHandler<ActionEvent> getlimit_CBAction(){
        return event -> {
            if(limit_CB.isSelected()){
                mined_TF.setDisable(false);
                mined_T.setDisable(false);
                start_BT.setDisable(true);
            }
            else {
                mined_TF.clear();
                mined_TF.setDisable(true);
                mined_T.setDisable(true);
                if(!isRS3) {
                    if (pickaxe_CB.getSelectionModel().getSelectedItem() != null)
                        start_BT.setDisable(false);
                }else{
                    start_BT.setDisable(false);
                }
            }
        };
    }

    public EventHandler<ActionEvent> getStartButtonAction(){
        return event -> {

            super.getStart_BTEvent();

            if(limit_CB.isSelected()){
                bot.limitQuantity = Integer.parseInt(mined_TF.getText());
            }
            if(!isRS3) {
                bot.pickaxe = pickaxe_CB.getSelectionModel().getSelectedItem().toString();
                bot.wieldPickaxe = wield_CB.isSelected();
                bot.wieldPickaxe = wield_CB.isSelected();
            }

            start_BT.textProperty().set("Resume");

        };
    }
}
