package com.sudo.v2.spectre.bots.sudoessence;

import com.sudo.v2.spectre.common.helpers.Methods;
import com.sudo.v2.spectre.common.helpers.SudoInteract;
import com.sudo.v2.spectre.bots.sudoessence.ui.EssenceFXGui;
import com.sudo.v2.spectre.bots.sudoessence.ui.EssenceInfoUI;
import com.sudo.v2.spectre.bots.sudoessence.tasks.BankInventoryTask;
import com.sudo.v2.spectre.bots.sudoessence.tasks.MineEssenseTask;
import com.sudo.v2.spectre.bots.sudoessence.tasks.TravelToBankTask;
import com.sudo.v2.spectre.bots.sudoessence.tasks.TravelToMineTask;
import com.sudo.v2.ui.Info;
import com.sudo.v2.ui.SudoBuddyFXGui;
import com.sudo.v2.ui.model.XPInfo;
import com.sudo.v2.base.SudoBot;
import com.sudo.v2.interfaces.ISudoTask;
import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.RuneScape;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.definitions.ItemDefinition;
import com.runemate.game.api.hybrid.local.Skill;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.ChatDialog;
import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.util.calculations.CommonMath;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.listeners.InventoryListener;
import com.runemate.game.api.script.framework.listeners.events.ItemEvent;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static com.sudo.v2.spectre.bots.sudoessence.assets.Areas.VAR_BANK_STAIRCASE;
import static com.runemate.game.api.hybrid.RuneScape.logout;

/**
 * sudoessence bot for RuneMate spectre
 */
public class SudoEssence extends SudoBot implements InventoryListener{

    private EssenceInfoUI infoUI;
    private EssenceFXGui configUI;

    public String pickaxe = "";
    public int limitQuantity = -1;
    public boolean wieldPickaxe = false;

    private int essenceCount;

    public SudoEssence(){
        super();
        setEmbeddableUI(this);
    }

    @Override
    public ObjectProperty<? extends Node> botInterfaceProperty() {
        if (_botInterfaceProperty == null) {
            _botInterfaceProperty = new SimpleObjectProperty<>(configUI = new EssenceFXGui(this));
            _GUI = new ArrayList<Node>();
            _GUI.add(_sbGUI = new SudoBuddyFXGui(this));
            _GUI.add(configUI);
            _GUI.add(infoUI = new EssenceInfoUI(this));

        }
        return _botInterfaceProperty;
    }

    @Override
    public void updateInfo() {

        // UpdateUI all current values in the XPInfo List
        _XPInfoMap.values().forEach((xpInfo) -> xpInfo.update());

        _DisplayInfoMap.put("Essence Count: ", Integer.toString(essenceCount) +
                " (" + Integer.toString((int)CommonMath.rate(TimeUnit.HOURS, _STOPWATCH.getRuntime(), essenceCount)) + "/Hour)");
        _DisplayInfoMap.put("GP: ",Integer.toString(_grossIncome) +
                " (" + Integer.toString((int)CommonMath.rate(TimeUnit.HOURS, _STOPWATCH.getRuntime(), _grossIncome)) + "/Hour)");

        try {
            _info = new Info(_XPInfoMap,           // XP Info
                    _DisplayInfoMap,
                    _STOPWATCH.getRuntimeAsString(),        // Total Runtime
                    _currentTaskString,                     // Current Task
                    _abTaskString);                         // antiban Task

        }catch(Exception e){
            e.printStackTrace();
        }

        Platform.runLater(() -> infoUI.update());
    }

    @Override
    public void onStart(String... args){
        getEventDispatcher().addListener(this);

        _currentTask = new TravelToMineTask(this);
        _currentTask.OnTaskCompleted((ISudoTask task) -> OnTaskComplete(task));
        _currentTask.Start();

        // Add Skills to track to the HashMap
        _XPInfoMap.put(Skill.MINING, new XPInfo(Skill.MINING));

        super.onStart();

    }

    @Override
    public void onStop() {
        _currentTask.Stop();
    }

    @Override
    public void onLoop() {
        super.onLoop();

        if(ChatDialog.getContinue() != null) {
            ChatDialog.getContinue().select();
            Execution.delayWhile(() -> ChatDialog.getContinue() != null, 500);
        }

        if(RuneScape.isLoggedIn())
        {
            if (Environment.isOSRS())
            {
                if (!hasEquipment())
                {
                    if (!(_currentTask instanceof TravelToBankTask) && !(_currentTask instanceof BankInventoryTask))
                    {
                        _currentTask = new TravelToBankTask(this);
                        _currentTask.OnTaskCompleted((ISudoTask task) -> OnTaskComplete(task));
                    }
                }

                if (wieldPickaxe && Equipment.getItemIn(Equipment.Slot.WEAPON) == null && Inventory.containsAnyOf(pickaxe))
                {
                    if (Bank.isOpen())
                        Bank.close();
                    else
                        Inventory.newQuery().names(pickaxe).actions("Wield").results().first().interact("Wield");
                }
            }
            GameObject staircase;
            if (_player != null)
                if (_player.getPosition().getPlane() == 1 && (staircase = GameObjects.newQuery().names("Staircase").actions("Climb-down").within(VAR_BANK_STAIRCASE).results().nearest()) != null)
                {
                    Methods.updateCurrentTask("Climbing down staircase", this);
                    SudoInteract.interactWith(staircase, "Climb-down", _useMiddleMouseCamera);
                    System.out.println(staircase);
                    staircase = null;
                }
        }

        if(limitQuantity > 0){
            if(essenceCount >= limitQuantity){
                Methods.updateCurrentTask("Quantity limit of " + limitQuantity + " has been reached, stopping bot", this);
                logout(true);
                if(!RuneScape.isLoggedIn())
                    stop();
            }
            else{
                _currentTask.Loop();
            }
        }
        else{
            _currentTask.Loop();
        }

    }

    @Override
    public void OnTaskComplete(ISudoTask completedTask) {
        super.OnTaskComplete(completedTask);

        if (completedTask instanceof TravelToMineTask)
            _currentTask = new MineEssenseTask(this);
        else if (completedTask instanceof MineEssenseTask)
            _currentTask = new TravelToBankTask(this);
        else if (completedTask instanceof TravelToBankTask)
            _currentTask = new BankInventoryTask(this);
        else if (completedTask instanceof BankInventoryTask)
            _currentTask = new TravelToMineTask(this);

        _currentTask.OnTaskCompleted((ISudoTask task) -> OnTaskComplete(task));
        _currentTask.Start();
    }

    @Override
    public void onItemAdded(ItemEvent event) {
        if(!(_currentTask instanceof BankInventoryTask))
            super.onItemAdded(event);

        if(event != null) {
            ItemDefinition definition = event.getItem().getDefinition();

            if (definition != null && definition.getName().contains("essence")) {
                essenceCount++;
            }
        }
    }

    @Override
    public void onItemRemoved(ItemEvent event){
        if(!(_currentTask instanceof BankInventoryTask))
            super.onItemRemoved(event);
    }

    public boolean isInMine(){
        if(GameObjects.newQuery().names("Rockslide", "Rune Essence").results().size() > 0)
            return true;
        else
            return false;
    }

    public boolean hasEquipment(){
        if(Inventory.newQuery().names(pickaxe).results().size() > 0 || Equipment.newQuery().names(pickaxe).results().size() > 0)
            return true;
        else
            return false;
    }

    public String getMissingEquipment(){
        if(!Inventory.containsAnyOf(pickaxe))
            return pickaxe;

        return "";
    }
}
