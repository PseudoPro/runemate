package com.sudo.v2.spectre.bots.sudoessence.assets;

import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;

/**
 * Created by SudoPro on 6/19/2016.
 */
public class Areas {

    public final static Area VAR_BANK_AREA = new Area.Rectangular(new Coordinate(3257, 3423, 0), new Coordinate(3250, 3420, 0));
    public final static Area VAR_BANK_WALKTO_AREA = new Area.Circular(new Coordinate(3262, 3424, 0), 4);
    public final static Area VAR_BANK_STAIRCASE = new Area.Rectangular(new Coordinate(3257, 3423, 1), new Coordinate(3253, 3419, 1));

    public final static Area AUBURY_AREA = new Area.Circular(new Coordinate(3253, 3401, 0), 2);
    public final static Area AUBURY_DOOR_AREA = new Area.Rectangular(new Coordinate(3251, 3400, 0), new Coordinate(3255, 3397, 0));
    public final static Area AUBURY_WALKTO_AREA = new Area.Rectangular(new Coordinate(3259, 3406, 0), new Coordinate(3256, 3400, 0));

    public final static Area MINE_AREA1 = new Area.Rectangular(new Coordinate(6920, 5710, 0), new Coordinate(6865, 5650, 0));
    public final static Area ESSENCE_NE_AREA1 = new Area.Rectangular(new Coordinate(6907, 5693, 0), new Coordinate(6915, 5701, 0));
    public final static Area ESSENCE_NW_AREA1 = new Area.Rectangular(new Coordinate(6881, 5691, 0), new Coordinate(6873, 5699, 0));
    public final static Area ESSENCE_SE_AREA1 = new Area.Rectangular(new Coordinate(6909, 5667, 0), new Coordinate(6917, 5659, 0));
    public final static Area ESSENCE_SW_AREA1 = new Area.Rectangular(new Coordinate(6882, 5665, 0), new Coordinate(6874, 5657, 0));

    public final static Area MINE_AREA2 = new Area.Rectangular(new Coordinate(11305, 12000, 0), new Coordinate(11355, 12060, 0));
    public final static Area ESSENCE_NE_AREA2 = new Area.Rectangular(new Coordinate(11339, 12045, 0), new Coordinate(11347, 12053, 0));
    public final static Area ESSENCE_NW_AREA2 = new Area.Rectangular(new Coordinate(11313, 12043, 0), new Coordinate(11305, 12051, 0));
    public final static Area ESSENCE_SE_AREA2 = new Area.Rectangular(new Coordinate(11341, 12019, 0), new Coordinate(11349, 12011, 0));
    public final static Area ESSENCE_SW_AREA2 = new Area.Rectangular(new Coordinate(11314, 12017, 0), new Coordinate(11306, 12009, 0));

    public final static Area MINE_AREA3 = new Area.Rectangular(new Coordinate(10537, 9697, 0), new Coordinate(10587, 9757, 0));
    public final static Area ESSENCE_NE_AREA3 = new Area.Rectangular(new Coordinate(10571, 9742, 0), new Coordinate(10579, 9750, 0));
    public final static Area ESSENCE_NW_AREA3 = new Area.Rectangular(new Coordinate(10545, 9740, 0), new Coordinate(10537, 9748, 0));
    public final static Area ESSENCE_SE_AREA3 = new Area.Rectangular(new Coordinate(10573, 9716, 0), new Coordinate(10581, 9708, 0));
    public final static Area ESSENCE_SW_AREA3 = new Area.Rectangular(new Coordinate(10546, 9714, 0), new Coordinate(10538, 9706, 0));

    public final static Coordinate AUBURY_N_COORD = new Coordinate(3252, 3404, 0);
    public final static Coordinate AUBURY_W_COORD = new Coordinate(3250, 3401, 0);

}
