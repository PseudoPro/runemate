package com.sudo.v2.interfaces;

/**
 * YES
 */
public interface Builder<T> {
    T Build();
}
