package com.sudo.v2.interfaces;

import com.runemate.game.api.hybrid.entities.GameObject;

/**
 * Entity Interface
 */
public interface Entity {
    GameObject GetGameObject();
}
