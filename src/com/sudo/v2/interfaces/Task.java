package com.sudo.v2.interfaces;

/**
 * Task Interface
 */
@FunctionalInterface
public interface Task {
    void invoke();
}
