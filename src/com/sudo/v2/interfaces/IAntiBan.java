package com.sudo.v2.interfaces;

import com.sudo.v2.base.SudoBot;

/**
 * antiban Interface
 */
public interface IAntiBan {

    void execute(SudoBot bot);

}
