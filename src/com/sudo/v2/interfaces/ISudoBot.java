package com.sudo.v2.interfaces;


/**
 * SudoBot Interface
 */
public interface ISudoBot {
    void updateInfo();
    void OnTaskComplete(ISudoTask completedTask);
}
