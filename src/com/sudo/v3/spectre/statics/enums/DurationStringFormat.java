package com.sudo.v3.spectre.statics.enums;

/**
 * Created by joeyw on 4/19/2017.
 */
public enum DurationStringFormat {
    CLOCK,
    DESCRIPTION
}
