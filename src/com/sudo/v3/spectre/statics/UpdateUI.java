package com.sudo.v3.spectre.statics;

import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.script.framework.logger.BotLogger;
import com.sudo.v3.base.SudoBot;
import com.sudo.v3.spectre.statics.enums.DurationStringFormat;

/**
 * UpdateUI class for debugging purposes
 */
public class UpdateUI {

    public static void debug(String debug) {
        Environment.getLogger().println(BotLogger.Level.DEBUG, debug);
        SudoBot.logsSinceLastUpdate = SudoBot.logsSinceLastUpdate + "\\" + "n" + debug;
    }

    public static void currentTask(String task, SudoBot bot) {
        bot.setCurrentTask(task);
        Environment.getLogger().println(BotLogger.Level.INFO, task);
        SudoBot.logsSinceLastUpdate = SudoBot.logsSinceLastUpdate + "\\" + "n" + task;
    }

    public static void antiBanTask(String task, SudoBot bot) {
        bot.setAntiBanTask(task);
        Environment.getLogger().println(BotLogger.Level.INFO, task);
        SudoBot.logsSinceLastUpdate = SudoBot.logsSinceLastUpdate + "\\" + "n" + task;
    }

    public static String getDurationString(long seconds, DurationStringFormat stringFormat) {

        long days = seconds / 86400;
        long hours = (seconds / 3600) % 24;
        long minutes = (seconds % 3600) / 60;
        seconds = seconds % 60;

        if (stringFormat == DurationStringFormat.CLOCK) {
            if (days > 0)
                return twoDigitString(days) + ":" + twoDigitString(hours) + ":" + twoDigitString(minutes) + ":" + twoDigitString(seconds);
            else
                return twoDigitString(hours) + ":" + twoDigitString(minutes) + ":" + twoDigitString(seconds);
        } else {
            if (days > 0)
                return twoDigitString(days) + "Day(s) " + twoDigitString(hours) + "Hour(s) " + twoDigitString(minutes) + "Min(s) " + twoDigitString(seconds) + "Sec(s)";
            else
                return twoDigitString(hours) + "Hour(s) " + twoDigitString(minutes) + "Min(s) " + twoDigitString(seconds) + "Sec(s)";
        }
    }

    private static String twoDigitString(long number) {

        if (number == 0) {
            return "00";
        }

        if (number / 10 == 0) {
            return "0" + number;
        }

        return String.valueOf(number);
    }
}