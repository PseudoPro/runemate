package com.sudo.v3.spectre.statics;

import java.util.HashSet;

/**
 * Created by Proxify on 9/12/2017.
 */
public class UserList {
    public static HashSet<String> blacklist = new HashSet<String>(){{
        add("uptherar");
        add("jeshoce");
    }};

    public static HashSet<String> whiteList = new HashSet<String>(){{
        add("Snufalufugus");
        add("Jux7apose");
        add("Eviriany");
    }};

    public static HashSet<String> trialAbusers = new HashSet<String>(){{
        add("tuls");
        add("alverjose");
        add("Treefitty");
        add("dad3322");
        add("Treefitty");
        add("luuukinahs2");
        add("JiggerJuggler");
        add("Ibzino");
        add("oBosnian");
        add("Ibzino");
        add("Alphtec");
    }};
}
