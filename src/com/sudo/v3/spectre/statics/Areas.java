package com.sudo.v3.spectre.statics;

import com.runemate.game.api.hybrid.entities.details.Locatable;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;

import java.util.List;

/**
 * Created by SudoPro on 11/23/2016.
 */
public class Areas {
    public final static Area CATH_RS3_BANK_AREA = new Area.Rectangular(new Coordinate(2797, 3442, 0), new Coordinate(2794, 3437, 0));
    public final static Area CATH_OSRS_BANK_AREA = new Area.Rectangular(new Coordinate(2812, 3442, 0), new Coordinate(2807, 3438, 0));
    public final static Area CATH_FISH_AREA = new Area.Rectangular(new Coordinate(2834, 3433, 0), new Coordinate(2864, 3423, 0));
    public final static Area CATH_OSRS_RANGE_AREA = new Area.Rectangular(new Coordinate(2815, 3444, 0), new Coordinate(2818, 3438, 0));
    public final static Area CATH_RS3_RANGE_AREA = new Area.Rectangular(new Coordinate(2795, 3450, 0), new Coordinate(2798, 3453, 0));
    public final static Area CATH_WILL_TREE_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2786, 3428, 0), new Coordinate(2787, 3430, 0), new Coordinate(2784, 3432, 0), new Coordinate(2778, 3432, 0), new Coordinate(2776, 3428, 0), new Coordinate(2780, 3426, 0), new Coordinate(2783, 3425, 0)});
    public final static Area CATH_WILL_TREE_ACTION_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2770, 3431, 0), new Coordinate(2766, 3428, 0), new Coordinate(2766, 3423, 0), new Coordinate(2773, 3422, 0), new Coordinate(2779, 3421, 0), new Coordinate(2787, 3422, 0), new Coordinate(2790, 3427, 0), new Coordinate(2791, 3433, 0), new Coordinate(2785, 3435, 0)});
    public final static Area CATH_OSRS_OAK_TREE_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2804, 3443, 0), new Coordinate(2804, 3450, 0), new Coordinate(2796, 3453, 0), new Coordinate(2787, 3457, 0), new Coordinate(2776, 3458, 0), new Coordinate(2768, 3456, 0), new Coordinate(2767, 3449, 0), new Coordinate(2767, 3442, 0), new Coordinate(2771, 3439, 0), new Coordinate(2778, 3437, 0), new Coordinate(2783, 3431, 0), new Coordinate(2793, 3430, 0)});
    public final static Area CATH_RS3_OAK_TREE_AREA = new Area.Rectangular(new Coordinate(2767, 3425, 0), new Coordinate(2790, 3446, 0));
    public final static Area CATH_YEW_TREE_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2760, 3429, 0), new Coordinate(2767, 3431, 0), new Coordinate(2768, 3434, 0), new Coordinate(2757, 3434, 0), new Coordinate(2754, 3431, 0)});
    public final static Area CATH_YEW_TREE_ACTION_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2748, 3436, 0), new Coordinate(2748, 3432, 0), new Coordinate(2750, 3427, 0), new Coordinate(2753, 3424, 0), new Coordinate(2765, 3424, 0), new Coordinate(2772, 3428, 0), new Coordinate(2776, 3438, 0), new Coordinate(2772, 3443, 0), new Coordinate(2767, 3440, 0), new Coordinate(2764, 3437, 0)});
    public final static Area CATH_EVER_TREE_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2789, 3474, 0), new Coordinate(2797, 3470, 0), new Coordinate(2806, 3470, 0), new Coordinate(2810, 3474, 0), new Coordinate(2811, 3479, 0), new Coordinate(2804, 3483, 0), new Coordinate(2805, 3487, 0), new Coordinate(2802, 3489, 0), new Coordinate(2796, 3487, 0), new Coordinate(2790, 3492, 0), new Coordinate(2784, 3493, 0), new Coordinate(2776, 3492, 0), new Coordinate(2777, 3479, 0), new Coordinate(2783, 3474, 0)});

    public final static Area BURTHORPE_RS3_BANK_AREA = new Area.Rectangular(new Coordinate( 2883, 3541, 0), new Coordinate( 2891, 3534, 0));

    public final static Area DRAY_BANK_AREA = new Area.Rectangular(new Coordinate(3092, 3240, 0), new Coordinate(3097, 3246, 0));
    public final static Area DRAY_FISH_AREA = new Area.Rectangular(new Coordinate(3083, 3236, 0), new Coordinate(3091, 3222, 0));
    public final static Area DRAY_WILL_TREE_AREA = new Area.Polygonal(new Coordinate(3079, 3238, 0), new Coordinate(3082, 3239, 0), new Coordinate(3085, 3239, 0), new Coordinate(3090, 3237, 0), new Coordinate(3092, 3235, 0), new Coordinate(3092, 3231, 0), new Coordinate(3092, 3226, 0), new Coordinate(3089, 3223, 0), new Coordinate(3084, 3224, 0), new Coordinate(3080, 3231, 0));
    public final static Area DRAY_WILL_TREE_ACTION_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(3085, 3240, 0), new Coordinate(3082, 3242, 0), new Coordinate(3078, 3240, 0), new Coordinate(3081, 3232, 0), new Coordinate(3085, 3223, 0), new Coordinate(3090, 3224, 0), new Coordinate(3092, 3227, 0), new Coordinate(3089, 3237, 0)});
    public final static Area DRAY_NORM_N_TREE_AREA = new Area.Rectangular(new Coordinate(3074, 3275, 0), new Coordinate(3088, 3262, 0));
    public final static Area DRAY_NORM_SE_TREE_AREA = new Area.Polygonal(new Coordinate(3098, 3254, 0), new Coordinate(3098, 3232, 0), new Coordinate(3111, 3212, 0), new Coordinate(3131, 3209, 0), new Coordinate(3146, 3219, 0), new Coordinate(3155, 3263, 0), new Coordinate(3117, 3264, 0), new Coordinate(3101, 3256, 0));

    public final static Area LUMB_BANK_AREA = new Area.Rectangular(new Coordinate(3211, 3259, 0), new Coordinate(3214, 3254, 0));
    public final static Area LUMB_FISH_AREA = new Area.Rectangular(new Coordinate(3242, 3260, 0), new Coordinate(3239, 3238, 0));
    public final static Area LUMB_FISH_SPOT_AREA = new Area.Rectangular(new Coordinate(3244, 3262, 0), new Coordinate(3237, 3236, 0));

    public final static Area FALA_E_BANK_AREA = new Area.Rectangular(new Coordinate(3009, 3358, 0), new Coordinate(3017, 3355, 0));
    public final static Area FALA_YEW_SE_TREE_AREA = new Area.Rectangular(new Coordinate(3022, 3358, 0), new Coordinate(3015, 3320, 0));
    public final static Area FALA_YEW_SW_TREE_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2994, 3309, 0), new Coordinate(2999, 3309, 0), new Coordinate(3001, 3313, 0), new Coordinate(3001, 3320, 0), new Coordinate(2994, 3314, 0)});
    public final static Area FALA_NORM_OAK_TREE_AREA = new Area.Polygonal(new Coordinate(3005, 3321, 0), new Coordinate(3001, 3321, 0), new Coordinate(2995, 3314, 0), new Coordinate(2987, 3306, 0), new Coordinate(2983, 3305, 0), new Coordinate(2977, 3298, 0), new Coordinate(2973, 3289, 0), new Coordinate(2977, 3282, 0), new Coordinate(2985, 3279, 0), new Coordinate(2993, 3284, 0), new Coordinate(2999, 3281, 0), new Coordinate(3012, 3289, 0), new Coordinate(3012, 3311, 0), new Coordinate(3020, 3312, 0), new Coordinate(3024, 3315, 0), new Coordinate(3042, 3314, 0), new Coordinate(3043, 3326, 0), new Coordinate(3025, 3326, 0), new Coordinate(3023, 3323, 0), new Coordinate(3016, 3324, 0), new Coordinate(3011, 3320, 0));

    public final static Area VARROCK_EAST_BANK_AREA = new Area.Rectangular(new Coordinate(3250, 3423, 0), new Coordinate(3257, 3419, 0));
    public final static Area VARROCK_RS3_WEST_BANK_AREA = new Area.Rectangular(new Coordinate(3180, 3433, 0), new Coordinate(3192, 3446, 0));

    public final static Area ARMIES_BANK_AREA = new Area.Rectangular(new Coordinate(2400, 2843, 0), new Coordinate(2405, 2840, 0));
    public final static Area ARMIES_FISH_AREA = new Area.Polygonal(new Coordinate(2431, 2894, 0), new Coordinate(2435, 2889, 0), new Coordinate(2442, 2890, 0), new Coordinate(2446, 2899, 0), new Coordinate(2451, 2904, 0), new Coordinate(2451, 2912, 0), new Coordinate(2460, 2918, 0), new Coordinate(2469, 2926, 0), new Coordinate(2473, 2938, 0), new Coordinate(2466, 2942, 0), new Coordinate(2453, 2935, 0), new Coordinate(2440, 2922, 0), new Coordinate(2432, 2907, 0));

    public final static Area GUILD_BANK_RS3_AREA = new Area.Rectangular(new Coordinate(2584, 3420, 0), new Coordinate(2588, 3424, 0));
    public final static Area GUILD_BANK_OSRS_AREA = new Area.Polygonal(new Coordinate(2583, 3422, 0), new Coordinate(2583, 3417, 0), new Coordinate(2586, 3417, 0), new Coordinate(2587, 3416, 0), new Coordinate(2587, 3414, 0), new Coordinate(2589, 3414, 0), new Coordinate(2589, 3421, 0), new Coordinate(2588, 3422, 0));
    public final static Area GUILD_DOCK1_FISH_AREA = new Area.Rectangular(new Coordinate(2597, 3419, 0), new Coordinate(2605, 3426, 0));
    public final static Area GUILD_DOCK1_FISH_SPOT_AREA = new Area.Rectangular(new Coordinate(2596, 3418, 0), new Coordinate(2606, 3427, 0));
    public final static Area GUILD_DOCK2_FISH_AREA = new Area.Rectangular(new Coordinate(2602, 3406, 0), new Coordinate(2612, 3417, 0));
    public final static Area GUILD_DOCK2_FISH_SPOT_AREA = new Area.Rectangular(new Coordinate(2601, 3405, 0), new Coordinate(2613, 3418, 0));
    public final static Area GUILD_MINNOW_DOCK_AREA = new Area.Rectangular(new Coordinate(2622, 3446, 0), new Coordinate(2607, 3440, 0));
    public final static Area GUILD_MINNOW_FISH_EAST_AREA = new Area.Rectangular(new Coordinate(2617, 3444, 0), new Coordinate(2620, 3442, 0));
    public final static Area GUILD_MINNOW_FISH_WEST_AREA = new Area.Rectangular(new Coordinate(2609, 3444, 0), new Coordinate(2612, 3442, 0));

    public final static Area EDGE_BANK_AREA = new Area.Rectangular(new Coordinate(3098, 3489, 0), new Coordinate(3091, 3498, 0));
    public final static Area BARB_FISH_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(3094, 3422, 0), new Coordinate(3094, 3426, 0), new Coordinate(3099, 3426, 0), new Coordinate(3100, 3427, 0), new Coordinate(3100, 3429, 0), new Coordinate(3099, 3430, 0), new Coordinate(3099, 3434, 0), new Coordinate(3097, 3438, 0), new Coordinate(3101, 3441, 0), new Coordinate(3105, 3441, 0), new Coordinate(3110, 3438, 0), new Coordinate(3112, 3434, 0), new Coordinate(3111, 3428, 0), new Coordinate(3106, 3422, 0)});
    public final static Area EDGE_YEW_TREE_AREA = new Area.Rectangular(new Coordinate(3089, 3468, 0), new Coordinate(3085, 3482, 0));

    public final static Area GE_RS3_BANK_AREA = new Area.Rectangular(new Coordinate(3162, 3481, 0), new Coordinate(3174, 3501, 0));
    public final static Area GE_OSRS_BANK_AREA = new Area.Rectangular(new Coordinate(3168, 3493, 0), new Coordinate(3161, 3486, 0));
    public final static Area GE_YEW_TREE_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(3203, 3506, 0), new Coordinate(3200, 3505, 0), new Coordinate(3202, 3501, 0), new Coordinate(3205, 3500, 0), new Coordinate(3212, 3499, 0), new Coordinate(3225, 3499, 0), new Coordinate(3225, 3506, 0)});

    public final static Area SEER_BANK_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2724, 3488, 0), new Coordinate(2724, 3491, 0), new Coordinate(2721, 3491, 0), new Coordinate(2721, 3494, 0), new Coordinate(2730, 3494, 0), new Coordinate(2730, 3490, 0), new Coordinate(2727, 3491, 0), new Coordinate(2728, 3488, 0)});
    public final static Area SEER_WILL_TREE_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2704, 3515, 0), new Coordinate(2699, 3510, 0), new Coordinate(2700, 3502, 0), new Coordinate(2708, 3504, 0), new Coordinate(2715, 3504, 0), new Coordinate(2719, 3502, 0), new Coordinate(2722, 3505, 0), new Coordinate(2718, 3510, 0), new Coordinate(2714, 3514, 0), new Coordinate(2709, 3518, 0), new Coordinate(2706, 3517, 0)});
    public final static Area SEER_MAPLE_TREE_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2717, 3498, 0), new Coordinate(2717, 3505, 0), new Coordinate(2728, 3505, 0), new Coordinate(2734, 3505, 0), new Coordinate(2738, 3497, 0), new Coordinate(2734, 3490, 0), new Coordinate(2731, 3490, 0), new Coordinate(2731, 3498, 0)});
    public final static Area SEER_YEW_TREE_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2718, 3457, 0), new Coordinate(2719, 3462, 0), new Coordinate(2713, 3468, 0), new Coordinate(2704, 3467, 0), new Coordinate(2705, 3456, 0)});
    public final static Area SEER_MAGIC_SW_TREE_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2704, 3423, 0), new Coordinate(2703, 3431, 0), new Coordinate(2691, 3433, 0), new Coordinate(2686, 3429, 0), new Coordinate(2687, 3423, 0), new Coordinate(2691, 3419, 0), new Coordinate(2702, 3420, 0)});
    public final static Area SEER_MAGIC_S_TREE_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2701, 3400, 0), new Coordinate(2698, 3397, 0), new Coordinate(2698, 3396, 0), new Coordinate(2700, 3394, 0), new Coordinate(2704, 3394, 0), new Coordinate(2706, 3396, 0), new Coordinate(2706, 3397, 0), new Coordinate(2703, 3400, 0)});
    public final static Area SEER_MAGIC_S_TREE_ACTION_AREA = new Area.Polygonal(new Coordinate[]{new Coordinate(2707, 3401, 0), new Coordinate(2698, 3401, 0), new Coordinate(2698, 3393, 0), new Coordinate(2707, 3394, 0)});

    public final static Area ROGUE_RS3_BANK_AREA = new Area.Rectangular(new Coordinate(3035, 4951, 0), new Coordinate(3023, 4964, 0));
    public final static Area ROGUE_RS3_FIRE_AREA = new Area.Rectangular(new Coordinate(3027, 4961, 0), new Coordinate(3031, 4957, 0));
    public final static Area ROGUE_OSRS_BANK_AREA = new Area.Rectangular(new Coordinate(3048, 4979, 1), new Coordinate(3037, 4955, 1));
    public final static Area ROGUE_OSRS_FIRE_AREA = new Area.Rectangular(new Coordinate(3041, 4971, 1), new Coordinate(3045, 4975, 1));

    public final static Area ALKHARID_RS3_BANK_AREA = new Area.Rectangular(new Coordinate(3268, 3168, 0), new Coordinate(3272, 3164, 0));
    public final static Area ALKHARID_OSRS_BANK_AREA = new Area.Rectangular(new Coordinate(3269, 3168, 0), new Coordinate(3272, 3162, 0));
    public final static Area ALKHARID_RS3_RANGE_AREA = new Area.Rectangular(new Coordinate(3269, 3184, 0), new Coordinate(3272, 3181, 0));
    public final static Area ALKHARID_OSRS_RANGE_AREA = new Area.Rectangular(new Coordinate(3271, 3182, 0), new Coordinate(3275, 3179, 0));
    public final static Area ALKHARID_RS3_FISH_AREA = new Area.Polygonal(new Coordinate(3240, 3151, 0), new Coordinate(3243, 3148, 0), new Coordinate(3249, 3151, 0), new Coordinate(3251, 3164, 0), new Coordinate(3257, 3152, 0), new Coordinate(3265, 3155, 0), new Coordinate(3264, 3177, 0), new Coordinate(3239, 3175, 0));
    public final static Area ALKHARID_OSRS_FISH_AREA = new Area.Polygonal(new Coordinate(3257, 3151, 0), new Coordinate(3274, 3151, 0), new Coordinate(3282, 3141, 0), new Coordinate(3284, 3134, 0), new Coordinate(3280, 3133, 0), new Coordinate(3267, 3140, 0));

    public final static Area ALKHARID_DUEL_ARENA_RS3_BANK_AREA = new Area.Rectangular(new Coordinate(3345, 3238, 0), new Coordinate(3349, 3240, 0));
    public final static Area ALKHARID_DUEL_ARENA_TENT_RS3_BANK_AREA = new Area.Rectangular(new Coordinate(3380, 3267, 0), new Coordinate(3384, 3272, 0));

    public final static Area CANIFIS_BANK_AREA = new Area.Polygonal(new Coordinate(3509, 3483, 0), new Coordinate(3508, 3482, 0), new Coordinate(3508, 3479, 0), new Coordinate(3509, 3478, 0), new Coordinate(3509, 3474, 0), new Coordinate(3513, 3474, 0), new Coordinate(3513, 3476, 0), new Coordinate(3517, 3476, 0), new Coordinate(3517, 3484, 0), new Coordinate(3509, 3484, 0));

    public final static Area YANILLE_RS3_BANK_AREA = new Area.Rectangular(new Coordinate(2609, 3097, 0), new Coordinate(2614, 3088, 0));
    public final static Area YANILLE_RS3_MINE_AREA = new Area.Polygonal(
            new Coordinate(2623, 3153, 0),
            new Coordinate(2623, 3123, 0),
            new Coordinate(2646, 3123, 0),
            new Coordinate(2644, 3137, 0),
            new Coordinate(2632, 3152, 0)
    );

    public final static Area PISCATORIS_BANK_AREA = new Area.Rectangular(new Coordinate(2327, 3693, 0), new Coordinate(2332, 3686, 0));
    public final static Area PISCATORIS_FISH_AREA = new Area.Rectangular(new Coordinate(2324, 3696, 0), new Coordinate(2353, 3706, 0));

    public final static Area SHILO_BANK_AREA = new Area.Circular(new Coordinate(2852, 2954, 0), 3);
    public final static Area SHILO_FISH_AREA = new Area.Polygonal(new Coordinate(2867, 2971, 0), new Coordinate(2875, 2977, 0), new Coordinate(2872, 2980, 0), new Coordinate(2856, 2983, 0), new Coordinate(2841, 2981, 0), new Coordinate(2820, 2977, 0), new Coordinate(2819, 2967, 0), new Coordinate(2821, 2964, 0), new Coordinate(2851, 2966, 0), new Coordinate(2853, 2969, 0), new Coordinate(2868, 2969, 0));

    public final static Area ZEAH_LAND_END_BANK_AREA = new Area.Rectangular(new Coordinate(1512, 3419, 0), new Coordinate(1509, 3423, 0));
    public final static Area ZEAH_LAND_END_MAGIC_TREE_AREA = new Area.Rectangular(new Coordinate(1605, 3448, 0), new Coordinate(1618, 3437, 0));

    public final static Area HOSIDIUS_BANK_AREA = new Area.Rectangular(new Coordinate(1652, 3614, 0), new Coordinate(1660, 3604, 0));
    public final static Area HOSIDIUS_OSRS_RANGE_AREA = HOSIDIUS_BANK_AREA;

    public final static Area MENAPHOS_PORT_DEPOSITBOX_AREA = new Area.Rectangular(new Coordinate(3216, 2624, 0), new Coordinate(3218, 2621, 0));
    public final static Area MENAPHOS_PORT_FISH_AREA = new Area.Rectangular(new Coordinate(3209, 2632, 0), new Coordinate(3215, 2624, 0));
    public final static Area MENAPHOS_VIP_BANK_AREA = new Area.Rectangular(new Coordinate(3180, 2740, 0), new Coordinate(3184, 2744, 0));
    public final static Area MENAPHOS_VIP_FISH_AREA = new Area.Rectangular(new Coordinate(3182, 2745, 0), new Coordinate(3190, 2755, 0));
    public final static Area MENAPHOS_VIP_TREE_AREA = new Area.Rectangular(new Coordinate(3179, 2744, 0), new Coordinate(3194, 2755, 0));

    public final static Area DEEP_SEA_SWARM_DEPOSITBOX_AREA = new Area.Rectangular(new Coordinate(2095, 7093, 0), new Coordinate(2101, 7088, 0));
    public final static Area DEEP_SEA_SWARM_FISH_AREA = new Area.Rectangular(new Coordinate(2091, 7084, 0), new Coordinate(2101, 7078, 0));

    public final static Area COOK_GUILD_RANGE_AREA = new Area.Rectangular(new Coordinate(3144, 3454, 0), new Coordinate(3147, 3452, 0));
    public final static Area COOK_GUILD_BANK_AREA = new Area.Rectangular(new Coordinate(3147, 3454, 0), new Coordinate(3149, 3448, 0));
    public final static Area COOK_GUILD_LOBBY_AREA = new Area.Polygonal(new Coordinate(3144, 3453, 0), new Coordinate(3144, 3450, 0), new Coordinate(3147, 3450, 0), new Coordinate(3147, 3446, 0), new Coordinate(3145, 3444, 0), new Coordinate(3142, 3444, 0), new Coordinate(3138, 3448, 0), new Coordinate(3138, 3451, 0), new Coordinate(3140, 3453, 0));

    public final static Area WOOD_GUILD_BANK_AREA = new Area.Rectangular(new Coordinate(1589, 3481, 0), new Coordinate(1593, 3474, 0));
    public final static Area WOOD_GUILD_MAGIC_TREE_AREA = new Area.Rectangular(new Coordinate(1579, 3494, 0), new Coordinate(1582, 3480, 0));
    public final static Area WOOD_GUILD_YEW_TREE_AREA = new Area.Polygonal(new Coordinate(1584, 3478, 0), new Coordinate(1588, 3478, 0), new Coordinate(1598, 3483, 0), new Coordinate(1598, 3497, 0), new Coordinate(1589, 3503, 0), new Coordinate(1584, 3503, 0), new Coordinate(1580, 3498, 0));
    public final static Area WOOD_GUILD_MAPLE_TREE_AREA = new Area.Rectangular(new Coordinate(1608, 3498, 0), new Coordinate(1627, 3487, 0));
    public final static Area WOOD_GUILD_WILLOW_TREE_AREA = new Area.Rectangular(new Coordinate(1626, 3503, 0), new Coordinate(1644, 3494, 0));
    public final static Area WOOD_GUILD_OAK_TREE_AREA = new Area.Rectangular(new Coordinate(1615, 3515, 0), new Coordinate(1629, 3507, 0));
    public final static Area WOOD_GUILD_NORMAL_TREE_AREA = new Area.Rectangular(new Coordinate(1626, 3516, 0), new Coordinate(1653, 3506, 0));
    public final static Area WOOD_GUILD_REDWOOD_LEVEL1_AREA = new Area.Rectangular(new Coordinate(1567, 3496, 1), new Coordinate(1574, 3479, 1));
    public final static Area WOOD_GUILD_REDWOOD_LEVEL2_AREA = new Area.Rectangular(new Coordinate(1567, 3496, 2), new Coordinate(1574, 3479, 2));
    public final static Area WOOD_GUILD_REDWOOD_LADDER_BASE_AREA = new Area.Rectangular(new Coordinate(1574, 3457, 0), new Coordinate(1576, 3482, 0));
    public final static Area WOOD_GUILD_REDWOOD_LADDER_LEVEL1_AREA = new Area.Rectangular(new Coordinate(1568, 3493, 1), new Coordinate(1575, 3472, 1));
    public final static Area WOOD_GUILD_REDWOOD_LADDER_LEVEL2_AREA = new Area.Rectangular(new Coordinate(1568, 3492, 2), new Coordinate(1573, 3473, 2));

    public final static Area BARB_ASSAULT_BANK_AREA = new Area.Rectangular(new Coordinate(2529, 3576, 0), new Coordinate(2538, 3567, 0));
    public final static Area BAXTORIAN_FISH_AREA = new Area.Rectangular(new Coordinate(2497, 3520, 0), new Coordinate(2510, 3493, 0));

    public final static Area PHASMATYS_BANK_AREA = new Area.Rectangular(new Coordinate(3691, 3471, 0), new Coordinate(3686, 3465, 0));

    public final static Area FOSSIL_ISLAND_BANK_AREA = new Area.Rectangular(new Coordinate(3742, 3805, 0), new Coordinate(3739, 3803, 0));

    public final static Area CITY_OF_VARROCK_AREA = new Area.Polygonal(new Coordinate(3142, 3516, 0), new Coordinate(3138, 3512, 0), new Coordinate(3139, 3494, 0), new Coordinate(3142, 3492, 0), new Coordinate(3142, 3484, 0), new Coordinate(3139, 3481, 0), new Coordinate(3139, 3473, 0), new Coordinate(3144, 3468, 0), new Coordinate(3145, 3464, 0), new Coordinate(3174, 3446, 0), new Coordinate(3173, 3425, 0), new Coordinate(3174, 3425, 0), new Coordinate(3174, 3399, 0), new Coordinate(3181, 3398, 0), new Coordinate(3182, 3382, 0), new Coordinate(3208, 3382, 0), new Coordinate(3208, 3374, 0), new Coordinate(3214, 3374, 0), new Coordinate(3214, 3382, 0), new Coordinate(3241, 3382, 0), new Coordinate(3241, 3386, 0), new Coordinate(3253, 3386, 0), new Coordinate(3253, 3380, 0), new Coordinate(3263, 3380, 0), new Coordinate(3264, 3380, 0), new Coordinate(3265, 3376, 0), new Coordinate(3273, 3376, 0), new Coordinate(3285, 3377, 0), new Coordinate(3288, 3380, 0), new Coordinate(3288, 3382, 0), new Coordinate(3287, 3383, 0), new Coordinate(3287, 3388, 0), new Coordinate(3283, 3393, 0), new Coordinate(3283, 3403, 0), new Coordinate(3280, 3406, 0), new Coordinate(3280, 3407, 0), new Coordinate(3275, 3406, 0), new Coordinate(3272, 3409, 0), new Coordinate(3272, 3425, 0), new Coordinate(3277, 3425, 0), new Coordinate(3276, 3432, 0), new Coordinate(3271, 3432, 0), new Coordinate(3271, 3435, 0), new Coordinate(3269, 3436, 0), new Coordinate(3269, 3462, 0), new Coordinate(3261, 3471, 0), new Coordinate(3261, 3491, 0), new Coordinate(3252, 3491, 0), new Coordinate(3250, 3494, 0), new Coordinate(3249, 3500, 0), new Coordinate(3235, 3499, 0), new Coordinate(3228, 3506, 0), new Coordinate(3201, 3506, 0), new Coordinate(3188, 3516, 0), new Coordinate(3171, 3516, 0), new Coordinate(3167, 3513, 0), new Coordinate(3160, 3513, 0), new Coordinate(3157, 3516, 0));
    public final static Area CITY_OF_PORT_PHASMATYS_AREA = new Area.Polygonal(new Coordinate(3661, 3507, 0), new Coordinate(3655, 3507, 0), new Coordinate(3653, 3505, 0), new Coordinate(3653, 3472, 0), new Coordinate(3650, 3469, 0), new Coordinate(3650, 3458, 0), new Coordinate(3652, 3457, 0), new Coordinate(3660, 3457, 0), new Coordinate(3662, 3455, 0), new Coordinate(3666, 3455, 0), new Coordinate(3667, 3454, 0), new Coordinate(3673, 3454, 0), new Coordinate(3675, 3456, 0), new Coordinate(3693, 3456, 0), new Coordinate(3695, 3454, 0), new Coordinate(3701, 3454, 0), new Coordinate(3702, 3454, 0), new Coordinate(3707, 3455, 0), new Coordinate(3710, 3457, 0), new Coordinate(3710, 3460, 0), new Coordinate(3707, 3463, 0), new Coordinate(3708, 3464, 0), new Coordinate(3702, 3470, 0), new Coordinate(3703, 3472, 0), new Coordinate(3718, 3497, 0), new Coordinate(3704, 3539, 0), new Coordinate(3685, 3531, 0), new Coordinate(3682, 3516, 0), new Coordinate(3677, 3516, 0), new Coordinate(3669, 3507, 0));
    
    public static boolean anyContainsCoordinate(List<Area> areaList, Coordinate coord) {
        for (int i = 0; i < areaList.size(); i++) {
            if (areaList.get(i).contains(coord))
                return true;
        }
        return false;
    }

    public static boolean containsAnyOfCoordinates(Area area, List<Coordinate> coordList) {
        for (int i = 0; i < coordList.size(); i++) {
            if (area.contains(coordList.get(i)))
                return true;
        }
        return false;
    }

    public static Area getAreaContaining(List<Area> areas, Locatable locatable){
        Area area = null;

        for(int i = 0; i < areas.size(); i++){
            if(areas.get(i).contains(locatable))
                area = areas.get(i);

            if(area != null)
                break;
        }

        return area;
    }
}