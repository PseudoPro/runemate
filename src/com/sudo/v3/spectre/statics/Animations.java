package com.sudo.v3.spectre.statics;

import java.util.HashSet;

/**
 * Created by joeyw on 4/15/2016.
 */
public class Animations {
    public final static HashSet<Integer> CHOPPING_ANIM = new HashSet<Integer>() {{
        add(21177);
        add(21186);
        add(21187);
        add(21188);
        add(21189);
        add(21190);
        add(21191);
        add(21192);
        add(21193);
        add(867);
        add(868);
        add(869);
        add(870);
        add(871);
        add(872);
        add(873);
        add(874);
        add(875);
        add(876);
        add(877);
        add(878);
        add(879);
        add(880);
        add(881);
        add(2794);
        add(2846);
        add(28375);
        add(28376);
    }};

    public final static HashSet<Integer> DIV_ANIM = new HashSet<Integer>() {{
        add(21228);
        add(21229);
        add(21230);
        add(21231);
        add(21232);
        add(21233);
        add(21234);
        add(23793);
        add(31055);
    }};

    public final static HashSet<Integer> COOK_ANIM = new HashSet<Integer>() {{
        add(25650);
        add(24895);
        add(899);
        add(898);
        add(897);
        add(896);
        add(895);
        add(7529);
    }};

    public final static HashSet<Integer> HUNT_ANIM = new HashSet<Integer>() {{
        add(24885);
        add(24886);
        add(24887);
        add(24888);
        add(24889);
        add(24890);
        add(24891);
        add(5215);
        add(5214);
        add(5213);
        add(5212);
        add(5211);
        add(5216);
        add(5217);
        add(5218);
        add(5219);
    }};
}
