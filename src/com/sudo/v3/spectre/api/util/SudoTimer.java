package com.sudo.v3.spectre.api.util;

import com.runemate.game.api.hybrid.util.Timer;

/**
 * SudoTimer
 *
 * Wrapper class for RuneMate's Timer Class
 */
public class SudoTimer {
    private Timer timer;
    private int minTime, maxTime;
    private boolean hasStarted = false;
    private int sessionMaxTime;

    public SudoTimer(int min, int max) {
        minTime = min;
        maxTime = max;
        sessionMaxTime = min + (int) (Math.random() * (max - min));
        timer = new Timer(sessionMaxTime);
    }

    public long getRemainingTime() {
        return timer.getRemainingTime();
    }

    public long getRemainingTimeInSeconds() {
        return timer.getRemainingTime() / 1000;
    }

    public long getRemainingTimeInMinutes() {
        return timer.getRemainingTime() / 60000;
    }

    public boolean hasExpired() {
        return timer.getRemainingTime() <= 0;
    }

    public void setRemainingTime(int time) {
        timer = new Timer(time);
    }

    public void reset() {
        timer.stop();
        sessionMaxTime = minTime + (int) (Math.random() * (maxTime - minTime));
        timer = new Timer(sessionMaxTime);
        timer.start();
    }

    public void restartSameTimer() {
        timer.reset();
    }

    public void start() {
        timer.start();
        hasStarted = true;
    }

    public void stop() {
        timer.stop();
    }

    public boolean hasStarted(){
        return hasStarted;
    }

    public int getSessionMaxTime(){
        return sessionMaxTime;
    }
}