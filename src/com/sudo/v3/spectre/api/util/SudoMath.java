package com.sudo.v3.spectre.api.util;

import com.runemate.game.api.hybrid.util.StopWatch;

import java.util.concurrent.TimeUnit;

public class SudoMath {

    public static int getItemPerHour(StopWatch stopwatch, int itemCount){
        if(itemCount == 0){
            return 0;
        } else {
            double seconds = stopwatch.getRuntime(TimeUnit.SECONDS);
            double hours = seconds / 3600.0;
            double itemPerHour = (double)itemCount / hours;
            return (int)(itemPerHour);
        }
    }

}