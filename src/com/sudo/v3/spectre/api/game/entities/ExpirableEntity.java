package com.sudo.v3.spectre.api.game.entities;

import com.runemate.game.api.hybrid.entities.details.Interactable;
import com.runemate.game.api.hybrid.entities.details.Locatable;
import com.sudo.v3.spectre.api.interfaces.Expirable;
import com.sudo.v3.spectre.api.util.SudoTimer;

public class ExpirableEntity <T extends Interactable & Locatable> implements Expirable {
    private T entity;
    private SudoTimer sudoTimer;

    public ExpirableEntity(T entity, SudoTimer sudoTimer){
        this.entity = entity;
        this.sudoTimer = sudoTimer;

        sudoTimer.start();
    }

    @Override
    public boolean hasExpired() {
        return sudoTimer.hasExpired();
    }

    public T getEntity() {
        return entity;
    }

    public Locatable getLocatable() {
        return entity;
    }

}
