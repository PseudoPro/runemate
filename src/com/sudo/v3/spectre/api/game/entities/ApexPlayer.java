package com.sudo.v3.spectre.api.game.entities;

import com.runemate.game.api.hybrid.entities.Actor;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.hud.interfaces.ChatDialog;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;
import com.sudo.v3.spectre.statics.Animations;
import com.sudo.v3.spectre.statics.UpdateUI;

import java.util.Arrays;
import java.util.HashSet;


/**
 * Created by SudoPro on 3/8/2017.
 */
public class ApexPlayer {

    public static boolean isTargetting(String... npcName){
        Player local = Players.getLocal();
        Actor target;
        String targetName;
        return local != null && (target = local.getTarget()) != null && (targetName = target.getName()) != null && Arrays.stream(npcName).anyMatch(targetName::equals) && ChatDialog.getContinue() == null;
    }

    public static boolean isTargettingNameContaining(String partial){
        partial = partial.toLowerCase();
        Player local = Players.getLocal();
        Actor target;
        String targetName;
        return local != null && (target = local.getTarget()) != null && (targetName = target.getName()) != null && targetName.toLowerCase().contains(partial) && ChatDialog.getContinue() == null;
    }

    public static boolean isInAnimation(HashSet<Integer> animations){
        try
        {
            Player local = Players.getLocal();

            if(local != null && Execution.delayUntil(() -> animations.contains(local.getAnimationId()) && ChatDialog.getContinue() == null, 1500))
                return true;
            else
                return false;

        }catch(Exception e){
            return false;
        }
    }
}