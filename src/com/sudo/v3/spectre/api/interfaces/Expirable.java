package com.sudo.v3.spectre.api.interfaces;

import com.runemate.game.api.hybrid.entities.details.Interactable;
import com.runemate.game.api.hybrid.entities.details.Locatable;

public interface Expirable {

    boolean hasExpired();
}
