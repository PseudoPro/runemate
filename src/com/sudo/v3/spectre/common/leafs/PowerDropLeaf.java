package com.sudo.v3.spectre.common.leafs;

import com.runemate.game.api.hybrid.input.Keyboard;
import com.runemate.game.api.hybrid.local.hud.interfaces.ChatDialog;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.queries.results.SpriteItemQueryResults;
import com.runemate.game.api.hybrid.util.Regex;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.osrs.local.hud.interfaces.OptionsTab;
import com.runemate.game.api.rs3.local.hud.interfaces.eoc.ActionBar;
import com.runemate.game.api.rs3.queries.ActionBarQueryBuilder;
import com.runemate.game.api.rs3.queries.results.ActionBarQueryResults;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.base.SudoBot;
import com.sudo.v3.spectre.api.game.hud.SudoInventory;
import com.sudo.v3.spectre.api.util.SudoTimer;
import com.sudo.v3.spectre.common.leafs.interfaces.Succeedable;
import com.sudo.v3.spectre.statics.UpdateUI;

import java.util.List;

import static java.awt.event.KeyEvent.VK_SHIFT;

public abstract class PowerDropLeaf extends LeafTask implements Succeedable {
    private SudoBot bot;
    private int minAmountToClick = 0, maxAmountToClick = 1;
    private String nameContainsString;
    private SudoTimer timer = new SudoTimer(15000, 30000);

    public PowerDropLeaf(SudoBot bot){
        this.bot = bot;
    }

    public PowerDropLeaf(SudoBot bot, String nameContainsString, int minAmountToClick, int maxAmountToClick ) {
        this.bot = bot;
        this.nameContainsString = nameContainsString;
        this.minAmountToClick = minAmountToClick;
        this.maxAmountToClick = maxAmountToClick;
    }

    public void setValues(String nameContainsString, int minAmountToClick, int maxAmountToClick){
        this.nameContainsString = nameContainsString;
        this.minAmountToClick = minAmountToClick;
        this.maxAmountToClick = maxAmountToClick;
    }

    public abstract void successAction();

    public void setContainsString(String string){
        this.nameContainsString = string;
    }

    @Override
    public void execute() {
        bot.isDropping = true;

        if(!timer.hasStarted())
            timer.start();
         else if(timer.hasExpired()) {
            bot.isDropping = false;
            timer.reset();

            if(Keyboard.isPressed(VK_SHIFT)){
                Keyboard.releaseKey(VK_SHIFT);
            }
        }

        if (Inventory.contains(Regex.getPatternForContainsString(nameContainsString))) {
            if (bot.isRS3) {
                UpdateUI.currentTask("Attempting to drop from action bar", bot);

                if (ChatDialog.getContinue() != null) {
                    ChatDialog.getContinue().select();
                    Execution.delayUntil(() -> ChatDialog.getContinue() == null, 1500, 3000);
                }

                int randomNum = Random.nextInt(minAmountToClick, maxAmountToClick);

                ActionBarQueryBuilder actionBarSlots = ActionBar.newQuery().filled(true).empty(false);
                actionBarSlots.results().forEach(i -> UpdateUI.debug("Name: " + i.getName() + " - Index: " + i.getIndex()));

                actionBarSlots = actionBarSlots.names(Regex.getPatternForContainsString(nameContainsString));
                ActionBarQueryResults actionBarResults = actionBarSlots.results();

                if (actionBarResults != null && !actionBarResults.isEmpty()) {
                    for (int i = 0; i < actionBarResults.size(); i++) {
                        ActionBar.Slot slot = actionBarResults.get(i);
                        if (slot != null && slot.isActivatable()) {
                            for (int j = 0; j < randomNum; j++) {
                                List<String> slotActions = slot.getActions();
                                if (slotActions != null && slotActions.size() > 0) {
                                    if (slotActions.get(0).toLowerCase().contains("drop"))
                                        slot.activate();
                                    else if (slotActions.contains("Drop"))
                                        slot.interact("Drop", slot.getName());
                                    else if (slotActions.get(0).toLowerCase().contains("release"))
                                        slot.activate();
                                    else if (slotActions.contains("Release"))
                                        slot.interact("Release", slot.getName());
                                }
                            }
                        }
                    }
                } else {
                    UpdateUI.currentTask("Item not found in action bar, dropping from inventory.", bot);
                    SudoInventory.dropAll(Regex.getPatternForContainsString(nameContainsString));
                }

//            for (int i = 0; i < 12; i++) {
//                final int curIndex = i;
//                if (!actionBarSlots.filter(slot -> slot.getIndex() == curIndex).results().isEmpty()) {
//                    for (int j = 0; j < randomNum; j++) {
//                        switch (curIndex) {
//                            case 0:
//                                pressKey(VK_1);
//                                break;
//                            case 1:
//                                pressKey(VK_2);
//                                break;
//                            case 2:
//                                pressKey(VK_3);
//                                break;
//                            case 3:
//                                pressKey(VK_4);
//                                break;
//                            case 4:
//                                pressKey(VK_5);
//                                break;
//                            case 5:
//                                pressKey(VK_6);
//                                break;
//                            case 6:
//                                pressKey(VK_7);
//                                break;
//                            case 7:
//                                pressKey(VK_8);
//                                break;
//                            case 8:
//                                pressKey(VK_9);
//                                break;
//                            case 9:
//                                pressKey(VK_0);
//                                break;
//                            case 10:
//                                pressKey(VK_MINUS);
//                                break;
//                            case 11:
//                                pressKey(VK_EQUALS);
//                                break;
//                        }
//                    }
//                } else {
//                    UpdateUI.debug("ActionBar Slot " + i + " is empty or does not contain fish, cannot drop using slot");
//                }
//            }
            } else {
                if (OptionsTab.isShiftDroppingEnabled()) {
                    SpriteItemQueryResults items = Inventory.newQuery().names(Regex.getPatternForContainsString(nameContainsString)).results();

                    if (items != null)
                        bot.fastClick.clickItems(items, true);
                    else {
                        UpdateUI.debug("Item not found in inventory, dropping successful.");
                        bot.isDropping = false;
                        successAction();
                    }
                } else
                    OptionsTab.setShiftDropping(true);
            }
        } else {
            UpdateUI.debug("Item not found in inventory, dropping successful.");
            bot.isDropping = false;
            timer.reset();
            if(Keyboard.isPressed(VK_SHIFT)){
                Keyboard.releaseKey(VK_SHIFT);
            }
            successAction();
        }
    }

    private void pressKey(int key) {
        Keyboard.pressKey(key);
        Execution.delay(10, 50);
        Keyboard.releaseKey(key);
        Execution.delay(40, 100);
    }

}