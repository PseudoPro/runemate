package com.sudo.v3.spectre.common.leafs.base;

import com.runemate.game.api.hybrid.entities.details.Interactable;
import com.runemate.game.api.hybrid.entities.details.Locatable;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.base.SudoBot;
import com.sudo.v3.spectre.api.game.hud.SudoCamera;
import com.sudo.v3.spectre.common.leafs.interfaces.Succeedable;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Proxify on 10/17/2017.
 */
public abstract class InteractionLeafTask <T extends Interactable & Locatable> extends LeafTask implements Succeedable {
    protected SudoBot bot;
    private String name, action;
    protected T object = null, hoverObject = null;
    protected int minMovementDelay = 250, maxMovementDelay = 1000;
    protected int minReqDelay = 100, maxReqDelay = 300;

    public InteractionLeafTask(SudoBot bot, T object, String name, String action)
    {
        this.bot = bot;
        this.object = object;
        this.name = name;
        this.action = action;
    }

    public InteractionLeafTask(SudoBot bot, T object, T hoverObject, String name, String action)
    {
        this.bot = bot;
        this.object = object;
        this.name = name;
        this.action = action;
        this.hoverObject = hoverObject;
    }

    public void setObjects(T object, T hoverObject){
        this.object = object;
        this.hoverObject = hoverObject;
    }

    public void setObject(T object){
        this.object = object;
    }

    public void setHoverObject(T hoverObject){ this.hoverObject = hoverObject; }

    public void setMovementDelay(int min, int max){
        this.minMovementDelay = min;
        this.maxMovementDelay = max;
    }

    public void setRequiredDelay(int min, int max){
        this.minReqDelay = min;
        this.maxReqDelay = max;
    }

    @Override
    public void execute()
    {
        if(object != null)
        {
            if(object.getVisibility() < .85 || (bot.player != null && object.distanceTo(bot.player) > 4))
                SudoCamera.ConcurrentlyTurnToWithYaw(object);

            UpdateUI.currentTask("Interacting with " + name + " with action \"" + action + "\".", bot);
            if(object.interact(action, name)) {
                successAction();
                Execution.delay(minReqDelay, maxReqDelay);
                Execution.delayWhile(bot.player::isMoving, minMovementDelay, maxMovementDelay);
            } else if (object.interact(action)) {
                successAction();
                Execution.delay(minReqDelay, maxReqDelay);
                Execution.delayWhile(bot.player::isMoving, minMovementDelay, maxMovementDelay);
            }
        }
    }

    public void setName(String name){
        this.name = name;
    }

    public void setAction(String action){
        this.action = action;
    }

    public String getName(){
        return name;
    }

    public String getAction(){
        return action;
    }

    public abstract void successAction();
}
