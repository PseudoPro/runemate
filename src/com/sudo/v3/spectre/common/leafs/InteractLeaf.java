package com.sudo.v3.spectre.common.leafs;

import com.runemate.game.api.hybrid.entities.details.Interactable;
import com.runemate.game.api.hybrid.entities.details.Locatable;
import com.sudo.v3.base.SudoBot;
import com.sudo.v3.spectre.api.util.SudoTimer;
import com.sudo.v3.spectre.common.leafs.base.InteractionLeafTask;

/**
 * Created by SudoPro on 11/26/2016.
 */
public class InteractLeaf <T extends Interactable & Locatable> extends InteractionLeafTask {
    private SudoTimer timer = new SudoTimer(1000, 3000);

    public InteractLeaf(SudoBot bot, T object, String name, String action) {
        super(bot, object, name, action);
    }

    public InteractLeaf(SudoBot bot, T object, T hoverObject, String name, String action) {
        super(bot, object, hoverObject, name, action);
    }

    @Override
    public void execute() {
        super.execute();
        timer.start();

        if (object != null && hoverObject != null) {
            timer.reset();
            while (object != null && hoverObject != null && !timer.hasExpired() && bot.player.isMoving())
                hoverObject.hover();
        }
    }

    @Override
    public void successAction() {

    }
}