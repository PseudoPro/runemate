package com.sudo.v3.spectre.common.leafs.interfaces;

public interface Succeedable {
    void successAction();
}
