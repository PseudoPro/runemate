package com.sudo.v3.spectre.common.navigation;

import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.entities.details.Locatable;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.location.navigation.Path;
import com.runemate.game.api.hybrid.location.navigation.basic.BresenhamPath;
import com.runemate.game.api.hybrid.location.navigation.basic.CoordinatePath;
import com.runemate.game.api.hybrid.location.navigation.basic.ViewportPath;
import com.runemate.game.api.hybrid.location.navigation.cognizant.RegionPath;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.hybrid.region.Region;
import com.runemate.game.api.script.Execution;
import com.sudo.v3.spectre.api.game.hud.SudoCamera;
import com.sudo.v3.spectre.statics.UpdateUI;

import java.util.ArrayList;

/**
 * Created by SudoPro on 11/23/2016.
 */
public class Traverse {

    public static boolean webPath(Navigator nav, Area toArea) {
        Path path = null;

        Player player = Players.getLocal();

        if (player != null && toArea != null) {

            if (nav.web != null) {
                path = getNearestPath(nav.paths, toArea);
            }

            if (path != null) {
                if (path.getNext() != null) {
                    System.out.println(path);
                    return path.step();
                } else {
                    return smartRegionPath(nav, toArea);
                }
            } else if (Region.getArea().contains(toArea.getRandomCoordinate())) {
                return smartRegionPath(nav, toArea);
            } else {
                path = nav.web.getPathBuilder().buildTo(toArea.getRandomCoordinate());
                if (path != null) {
                    if (path.getNext() != null) {
                        UpdateUI.debug("WebPath added: " + path);
                        nav.paths.add(path);
                        return path.step();
                    } else {
                        return smartRegionPath(nav, toArea);
                    }
                } else {
                    UpdateUI.debug("That bitch be null, walking with bresenham.");
                    bresenhamPath(toArea);
                }
            }
        }
        return false;
    }

    public static boolean smartRegionPath(Navigator nav, Area toArea) {
        Path path;

        Player player = Players.getLocal();

        if (player != null && toArea != null) {
            if (player.distanceTo(toArea) < 8) {
                if (toArea.getVisibility() < 0.8)
                    SudoCamera.ConcurrentlyTurnToWithYaw(toArea);

                Coordinate coord = toArea.getRandomCoordinate();
                return coord != null && coord.interact("Walk here");
            }

            path = getNearestPath(nav.paths, toArea);

            if (path != null) {
                if (path.getNext() != null) {
                    UpdateUI.debug(path.toString());
                    return path.step();
                } else {
                    return bresenhamPath(toArea);
                }
            } else {
                path = RegionPath.buildTo(toArea.getRandomCoordinate());
                if (path != null) {
                    if (path.getNext() != null) {
                        UpdateUI.debug("RegionPath added: " + path);
                        nav.paths.add(path);
                        return path.step();
                    } else {
                        return bresenhamPath(toArea);
                    }
                } else {
                    UpdateUI.debug("That bitch be null, walking with bresenham");
                    bresenhamPath(toArea);
                }
            }
        } else {
            UpdateUI.debug("SmartRegionPath: Player Null: " + (player == null));
            UpdateUI.debug("SmartRegionPath: Area Null: " + (toArea == null));
        }
        return false;
    }

    public static boolean bresenhamPath(Area area) {
        Player player = Players.getLocal();
        if (area != null) {
            try {
                Coordinate coord = area.getRandomCoordinate();

                if (player.distanceTo(coord) > 5) {
                    final BresenhamPath bp = BresenhamPath.buildTo(coord);

                    if (bp != null) {
                        if (bp.step(true)) {
                            Execution.delayWhile(player::isMoving, 250, 500);
                            return true;
                        }
                    }
                } else {
                    if (!area.isVisible() || player.distanceTo(coord) > 4)
                        SudoCamera.ConcurrentlyTurnToWithYaw(coord);

                    return coord.interact("Walk here");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else
            UpdateUI.debug("That area bitch be null");

        return false;
    }

    private static Path getNearestPath(ArrayList<Path> paths, Area toArea) {
        if (paths.size() > 0) {
            for (Path i : paths)
                for (Locatable loc : i.getVertices())
                    if (loc.distanceTo(Players.getLocal()) < 10)
                        if (toArea.contains(i.getVertices().get(i.getVertices().size() - 1)))
                            return i;
        }
        return null;
    }

    public static boolean viewportPath(Area area) {
        if (area != null) {
            CoordinatePath path = BresenhamPath.buildTo(area.getRandomCoordinate());
            if (path != null) {
                final ViewportPath vp = ViewportPath.convert(path);

                if (vp != null) {
                    return vp.step(true);
                }
            }
        } else
            UpdateUI.debug("That area bitch be null");

        return false;
    }
}