package com.sudo.v3.spectre.bots.apexhunter.branches;

import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.RuneScape;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.GroundItem;
import com.runemate.game.api.hybrid.input.Keyboard;
import com.runemate.game.api.hybrid.local.Camera;
import com.runemate.game.api.hybrid.local.Skill;
import com.runemate.game.api.hybrid.location.Coordinate;

import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoRootTask;
import com.sudo.v3.spectre.api.game.entities.ExpirableEntity;
import com.sudo.v3.spectre.bots.apexhunter.ApexHunter;
import com.sudo.v3.spectre.bots.apexhunter.enums.TrapType;
import com.sudo.v3.spectre.bots.apexhunter.enums.TraversalLocation;
import com.sudo.v3.spectre.api.game.entities.ApexPlayer;
import com.sudo.v3.spectre.api.game.hud.SudoCamera;
import com.sudo.v3.spectre.common.leafs.InteractLeaf;
import com.sudo.v3.spectre.common.leafs.PowerDropLeaf;
import com.sudo.v3.spectre.statics.Animations;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.sudo.v3.ui.model.XPInfo;

import java.util.Collections;
import java.util.Comparator;
import java.util.Optional;

import static java.awt.event.KeyEvent.VK_SHIFT;

public class Root extends SudoRootTask {
    private ApexHunter bot;

    public Root(ApexHunter bot) {
        super(bot);

        this.bot = bot;
    }

    private void removeCoordFromList() {
        Execution.delayWhile(bot.player::isMoving, 1000, 2000);
        Execution.delayWhile(() -> !ApexPlayer.isInAnimation(Animations.HUNT_ANIM), 750, 1000);

        if (bot.player.distanceTo(bot.traversalArea.getPosition()) <= 1 && bot.player.isFacing(bot.traversalArea.getPosition())) {
            Optional<ExpirableEntity> tempEntity = bot.placedTrapCoordList.stream().parallel().filter(i -> i.getLocatable().getPosition().equals(bot.traversalArea.getPosition())).findFirst();
            bot.placedTrapCoordList.remove(tempEntity);

            // If another player is not around and the layout style does not contain the existing trap coordinate
//            if (Players.newQuery().within(bot.trapArea).results().size() <= 1 && !bot.generateSpotsFromLayout().contains(bot.traversalArea.getPosition())) {
//                GameObjects.newQuery().within(bot.traversalArea).names(bot.trapType.getUnsuccessfulName(), bot.trapType.getSuccessfulName(), bot.trapType.getName()).results().isEmpty();
//            }
            Execution.delayWhile(() -> ApexPlayer.isInAnimation(Animations.HUNT_ANIM), 1750, 2250);
        }
    }

    private void osrsAdditionalDelay(){
        if (bot.trapType == TrapType.BOX) {
            Execution.delayWhile(() -> ApexPlayer.isInAnimation(Animations.HUNT_ANIM), 2000, 4000);
            Execution.delay(500, 750);
            Execution.delayWhile(() -> ApexPlayer.isInAnimation(Animations.HUNT_ANIM), 1500, 3000);
        }
    }

//    private void addCoordToList(){
//        if (bot.player.distanceTo(bot.traversalArea.getPosition()) <= 1) {
//            UpdateUI.currentTask("Successful planted trap at " + bot.traversalArea.getPosition().toString(), bot);
//            bot.placedTrapCoordList.add(bot.traversalArea.getPosition());
//        }
//    }

    @Override
    public TreeTask rootTask() {
        if (!bot.isPremiumBot || bot.getMetaData().getHourlyPrice().doubleValue() > 0 || bot.isValidSession() || Environment.isSDK()) {
            if (bot.firstLogin) {
                if (RuneScape.isLoggedIn()) {
                    // Add Skills to track to the HashMap
                    bot.XPInfoMap.put(Skill.HUNTER, new XPInfo(Skill.HUNTER));

                    bot.firstLogin = false;

                    if (bot.resetTrapLayout)
                        bot.resetTrapLayoutTimer.start();

                    UpdateUI.debug("Hunter Level: " + Skill.HUNTER.getBaseLevel());
                    UpdateUI.debug("Location: (" + bot.trapCenterCoord.getX() + ", " + bot.trapCenterCoord.getY() + ", " + bot.trapCenterCoord.getPlane() + ")");
                    UpdateUI.debug("Trap Type: " + bot.trapType.name());
                    UpdateUI.debug("World Hop: " + bot.worldHopEnabled);
                    UpdateUI.debug("Reset Trap Layout: " + bot.resetTrapLayout);
                    UpdateUI.debug("World Hop: " + bot.worldHopEnabled);
                    bot.updateSession(0.0);

                    bot.powerDropLeaf = new PowerDropLeaf(bot) {
                        @Override
                        public void successAction() {
                            bot.managingInventory = false;
                        }
                    };

                    bot.pickupGroundTrapLeaf = new InteractLeaf<>(bot, bot.trapGroundItem, bot.trapType.getName(), bot.trapType.getPickupAction());
                    bot.layGroundTrapLeaf = new InteractLeaf<GroundItem>(bot, bot.trapGroundItem, bot.trapType.getName(), bot.trapType.getLayAction()){
                        @Override
                        public void successAction() {
                            Execution.delay(250, 500);
                            Execution.delayWhile(bot.player::isMoving, 1000, 1500);
                            if(!bot.isRS3)
                                osrsAdditionalDelay();
                        }
                    };
                    bot.replaceUnsuccessfulTrapLeaf = new InteractLeaf<GameObject>(bot, bot.unsuccessTrapObj, bot.trapType.getName(), bot.trapType.getRebuildAction()){
                        @Override
                        public void successAction(){
                            Execution.delay(250, 500);
                            Execution.delayWhile(bot.player::isMoving, 1000, 1500);
                            if(!bot.isRS3)
                                osrsAdditionalDelay();
                        }
                    };
                    bot.pickupSuccessfulTrapLeaf = new InteractLeaf<GameObject>(bot, bot.successTrapObj, bot.trapType.getSuccessfulName(), !bot.isRS3 && bot.trapType == TrapType.BOX ? bot.trapType.getRebuildAction() : bot.trapType.getGrabSuccessAction()) {
                        @Override
                        public void successAction() {
                            if(bot.isRS3)
                                removeCoordFromList();
                            Execution.delay(250, 500);
                            Execution.delayWhile(bot.player::isMoving, 1000, 1500);
                            if(!bot.isRS3)
                                osrsAdditionalDelay();
                        }
                    };
                    bot.dismantleUnsuccessfulTrapLeaf = new InteractLeaf<GameObject>(bot, bot.unsuccessTrapObj, bot.trapType.getName(), bot.trapType.getDismantleAction()) {
                        @Override
                        public void successAction() {
                            removeCoordFromList();
                        }
                    };

                    bot.pickupEquipmentLeaf = new InteractLeaf<>(bot, null, bot.trapType.getEquipmentNames()[0], "Take");

                    bot.pickupGroundTrapLeaf.setRequiredDelay(200, 1250);
                    bot.pickupSuccessfulTrapLeaf.setRequiredDelay(200, 1250);
                    bot.replaceUnsuccessfulTrapLeaf.setRequiredDelay(400, 1250);

                    bot.pickupGroundTrapLeaf.setMovementDelay(650, 1500);
                    bot.pickupSuccessfulTrapLeaf.setMovementDelay(750, 2000);
                    bot.replaceUnsuccessfulTrapLeaf.setMovementDelay(750, 2000);
                }
            }

            if (bot.currentlyBreaking) {
                if (bot.readyForBreak()) {
                    UpdateUI.currentTask("Attempting to log off.", bot);
                    if (RuneScape.isLoggedIn())
                        RuneScape.logout();
                }
                return bot.emptyLeaf;
            }

            if ((Random.nextInt(0, 100) < 4 || Camera.getPitch() < 0.325) && bot.traversalArea != null) {
                SudoCamera.ConcurrentlyTurnToWithYaw(bot.traversalArea);
            }

            if (bot.createSpotCoordListBoolean)
                bot.createSpotCoordList();

            if (bot.spotCoordList.size() > 2) {
                Collections.sort(bot.spotCoordList, new Comparator<Coordinate>() {
                    public int compare(Coordinate c1, Coordinate c2) {
                        if (c1.distanceTo(bot.player) == c2.distanceTo(bot.player))
                            return 0;
                        return c1.distanceTo(bot.player) < c2.distanceTo(bot.player) ? -1 : 1;
                    }
                });
            }

            // Remove traps who's timers have expired
            for(int i = bot.placedTrapCoordList.size() - 1; i >= 0; i--){
                if(bot.placedTrapCoordList.get(i).hasExpired())
                    bot.placedTrapCoordList.remove(i);
            }

            if (bot.worldHopEnabled && bot.worldHopping) {
                if (bot.readyForBreak()) {
                    return bot.worldHopLeaf;
                }
            }

            if (bot.managingInventory) {
                UpdateUI.debug(bot.player.getName() + " -> RootBranch: ManagingInventory");
                return bot.manageInventoryLeaf;
            } else {
                if (Keyboard.isPressed(VK_SHIFT)) {
                    Keyboard.releaseKey(VK_SHIFT);
                }
            }

            if (bot.validateTrapPlacement) {
                UpdateUI.debug(bot.player.getName() + " -> RootBranch: validateTrapPlacement: " + bot.validateTrapPlacement);
                return bot.hasTrapBeenPlaced;
            }

            if (bot.maxTrapsFailsafe) {
                UpdateUI.debug(bot.player.getName() + " -> RootBranch: \"Max traps exceeded.\"");
                return bot.maxTrapsReachedLeaf;
            }

            if(bot.trapType == TrapType.SALAMANDER)
                return bot.salRoot;

            if (bot.resetTrapLayout && bot.resetTrapLayoutTimer.hasExpired()) {
                return bot.resetLayoutStyleLeaf;
            }

            validate = bot.trapCenterCoord != null && bot.player.distanceTo(bot.trapCenterCoord) < 15;
            UpdateUI.debug(bot.player.getName() + " -> RootBranch: PlayerInArea: " + validate);
            if (validate) {
                bot.isTrapObjAround.trapState = IsTrapObjAround.TrapState.FALLEN;
                return bot.isTrapObjAround;
            } else {
                if (bot.trapCenterCoord != null) {
                    bot.traversalLocation = TraversalLocation.TRAP_AREA;
                    return bot.traversalLeaf;
                } else {
                    UpdateUI.currentTask("Trap area has not been properly set. Please follow setup instructions.", bot);
                    bot.stop("Trap are has not been properly set. Please follow setup instructions.");
                    return bot.emptyLeaf;
                }
            }
        } else {
            return bot.liteVersionExpireLeaf;
        }
    }
}