package com.sudo.v3.spectre.bots.apexhunter.branches;

import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.region.GroundItems;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexhunter.ApexHunter;
import com.sudo.v3.spectre.bots.apexhunter.enums.LogicType;
import com.sudo.v3.spectre.bots.apexhunter.enums.TrapType;
import com.sudo.v3.spectre.statics.Animations;
import com.sudo.v3.spectre.statics.UpdateUI;

import java.util.Arrays;

public class IsTrapObjAround extends SudoBranchTask {
    private ApexHunter bot;
    public TrapState trapState = TrapState.UNDEFINED;

    public IsTrapObjAround(ApexHunter bot) {
        this.bot = bot;
    }

    public enum TrapState {
        FALLEN,
        SUCCESS,
        FAILED,
        UNDEFINED
    }

    @Override
    public TreeTask successTask() {
        validate = bot.trapType == TrapType.SNARE ? Inventory.getItems().size() > 25 : Inventory.isFull();
        UpdateUI.debug(bot.player.getName() + " -> SubBranch: IsTrapAround(InventoryIsFull): " + validate);
        if (validate) {
            return bot.manageInventoryLeaf;
        } else {
            if (trapState == TrapState.FALLEN) {
                if (!Animations.HUNT_ANIM.contains(bot.player.getAnimationId()))
                    return bot.isFallenTrapOnLayout;
                    //return bot.pickupGroundTrapLeaf;
                else
                    return bot.manageInventoryLeaf;
            } else if (trapState == TrapState.SUCCESS) {
                return bot.pickupSuccessfulTrapLeaf;
            } else { // FAILED
                UpdateUI.debug("Player Coordinate: " + bot.player.getPosition());
                UpdateUI.debug("Box Trap Coordinate: " + bot.unsuccessTrapObj.getPosition());
                UpdateUI.debug("Box Trap Actions: " + Arrays.toString(bot.unsuccessTrapObj.getDefinition().getActions().toArray()));
                UpdateUI.debug("RebuildAction: " + bot.trapType.getRebuildAction());
                return bot.replaceUnsuccessfulTrapLeaf;
            }
        }
    }

    @Override
    public boolean validate() {
        if (trapState == TrapState.SUCCESS) {
            bot.successTrapObj = GameObjects.newQuery().names(bot.trapType.getSuccessfulName()).actions(bot.trapType.getGrabSuccessAction()).filter(i -> {
                Coordinate tempCoord = i.getPosition();
                return tempCoord != null && bot.spotCoordList.contains(tempCoord);
            }).results().random();
            validate = bot.successTrapObj != null && bot.successTrapObj.isValid();
            bot.pickupSuccessfulTrapLeaf.setObject(bot.successTrapObj);
        } else if (trapState == TrapState.FAILED) {
            bot.unsuccessTrapObj = GameObjects.newQuery().names(bot.trapType.getUnsuccessfulName()).actions(bot.trapType.getRebuildAction()).filter(i -> {
                Coordinate tempCoord = i.getPosition();
                return tempCoord != null && bot.spotCoordList.contains(tempCoord) && !i.getDefinition().getActions().contains("Investigate");
            }).results().nearest();
            validate = bot.unsuccessTrapObj != null && bot.unsuccessTrapObj.isValid();
            bot.replaceUnsuccessfulTrapLeaf.setObject(bot.unsuccessTrapObj);
        } else { // FALLEN
            validate = (bot.trapGroundItem = GroundItems.newQuery().within(bot.trapArea).names(bot.trapType.getName()).results().nearest()) != null && bot.trapGroundItem.isValid();
            bot.pickupGroundTrapLeaf.setObject(bot.trapGroundItem);
        }
        //UpdateUI.debug("TraversalArea: " + bot.traversalArea != null && bot.traversalArea.getPosition() != null ? bot.traversalArea.getPosition().toString() : "null");

        if (bot.traversalArea != null && bot.traversalArea.getPosition() != null)
            UpdateUI.debug("TraversalArea: " + bot.traversalArea.getPosition());

        UpdateUI.debug(bot.player.getName() + " -> Branch: IsTrapAround(" + trapState.name() + "): " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        if (trapState == TrapState.FALLEN) {
            if (bot.logicType == LogicType.PRIORITIZE_SPOTS) {
                return bot.isTrapSpotOpen;
            } else {
                trapState = TrapState.SUCCESS;
                return bot.isTrapObjAround;
            }
        }
        if (trapState == TrapState.SUCCESS) {
            if (bot.logicType == LogicType.PRIORITIZE_SPOTS) {
                trapState = TrapState.FAILED;
                return bot.isTrapObjAround;
            } else {
                return bot.isTrapSpotOpen;
            }
        } else {
            return bot.manageInventoryLeaf;
        }
    }
}