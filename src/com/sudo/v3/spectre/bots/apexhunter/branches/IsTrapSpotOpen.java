package com.sudo.v3.spectre.bots.apexhunter.branches;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexhunter.ApexHunter;
import com.sudo.v3.spectre.statics.UpdateUI;

import java.util.ArrayList;
import java.util.Collections;

public class IsTrapSpotOpen extends SudoBranchTask {
    private ApexHunter bot;
    private boolean trapDetected = false;
    private ArrayList<Coordinate> layoutList;

    public IsTrapSpotOpen(ApexHunter bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        return bot.inventoryHasTrap;
    }

    @Override
    public boolean validate() {
        if(bot.spotCoordList.size() < (layoutList = bot.generateSpotsFromLayout()).size())
            updateSpotCoordList();

        trapDetected = false;
        validate = false;
        if (bot.spotCoordList == null || bot.spotCoordList.size() <= 0)
            bot.createSpotCoordList();

        bot.playersList = Players.newQuery().filter(i -> {
            String name = i.getName();
            return name != null && !name.equals(bot.player.getName());
        }).within(bot.trapArea).results().asList();
        bot.playerCoords = new ArrayList<>();
        if(bot.playersList.size() > 0){
            for(Player player : bot.playersList){
                if(player.getPosition() != null){
                    bot.playerCoords.add(player.getPosition());
                }
            }
        }

        bot.trapObjects = GameObjects.newQuery().names(bot.trapType.getName(), bot.trapType.getSuccessfulName(), bot.trapType.getUnsuccessfulName()).within(bot.trapArea).results().asList();
        bot.trapCoords = new ArrayList<>();
        if (bot.trapObjects.size() > 0) {
            for (GameObject obj : bot.trapObjects) {
                if (obj.getPosition() != null) {
                    bot.trapCoords.add(obj.getPosition());
                }
            }
        }

        if (bot.trapCoords.size() > 0) {
            for (int i = 0; i < bot.spotCoordList.size(); i++) {
                if (!bot.trapCoords.contains(bot.spotCoordList.get(i)) && !bot.playerCoords.contains(bot.spotCoordList.get(i))) {
                    bot.traversalArea = bot.spotCoordList.get(i).getArea();
                    validate = true;
                    break;
                }
            }
        } else {
            bot.traversalArea = bot.spotCoordList.get(0).getArea();
            validate = true;
        }

        UpdateUI.debug(bot.player.getName() + " -> Branch: IsTrapSpotOpen: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        return bot.isTrapPlacementValid;
    }

    public void updateSpotCoordList(){
        Collections.shuffle(layoutList);
        for(Coordinate coord : layoutList){
            if(!bot.spotCoordList.contains(coord))
                bot.spotCoordList.add(coord);
        }
    }
}
