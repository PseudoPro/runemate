package com.sudo.v3.spectre.bots.apexhunter.branches.salamander;

import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexhunter.ApexHunter;
import com.sudo.v3.spectre.statics.UpdateUI;

public class SalRoot extends SudoBranchTask {
    private ApexHunter bot;

    public SalRoot (ApexHunter bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        validate = Inventory.getItems().size() < 24;
        UpdateUI.debug(bot.player.getName() + " -> SalRoot: InventoryNotNearFull: " + validate);
        if (validate)
            return bot.isFallenEquipmentAround;
        else
            return bot.manageInventoryLeaf;
    }

    @Override
    public boolean validate() {
        validate = Inventory.containsAllOf(bot.trapType.getEquipmentNames());
        UpdateUI.debug(bot.player.getName() + " -> SalRoot: InventoryHasRope&Net: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        return bot.outOfTrapLeaf;
    }
}
