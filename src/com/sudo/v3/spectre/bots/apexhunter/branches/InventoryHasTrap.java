package com.sudo.v3.spectre.bots.apexhunter.branches;

import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexhunter.ApexHunter;
import com.sudo.v3.spectre.bots.apexhunter.enums.TraversalLocation;
import com.sudo.v3.spectre.statics.UpdateUI;

public class InventoryHasTrap extends SudoBranchTask {
    private ApexHunter bot;

    public InventoryHasTrap(ApexHunter bot) {
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        bot.inventoryTest = 0;
        validate = bot.traversalArea.contains(bot.player) && !bot.player.isMoving();
        UpdateUI.debug(bot.player.getName() + " -> SubBranch: InvHasTrap(PlayerOnCoord): " + validate);

        if (validate) {
            return bot.placeTrapLeaf;
        } else {
            bot.traversalLocation = TraversalLocation.TRAP_SPOT;
            return bot.traversalLeaf;
        }
    }

    @Override
    public boolean validate() {
        validate = (bot.trapSprite = Inventory.newQuery().names(bot.trapType.getName()).actions(bot.trapType.getLayAction()).results().first()) != null && bot.trapSprite.isValid();
        UpdateUI.debug(bot.player.getName() + " -> Branch: InventoryHasTrap(" + bot.inventoryTest + "): " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        if (bot.inventoryTest > 3)
            return bot.outOfTrapLeaf;
        else
            bot.inventoryTest++;

        return bot.emptyLeaf;
    }
}