package com.sudo.v3.spectre.bots.apexhunter.branches;

import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexhunter.ApexHunter;
import com.sudo.v3.spectre.bots.apexhunter.enums.LogicType;
import com.sudo.v3.spectre.statics.UpdateUI;

public class IsTrapPlacementValid extends SudoBranchTask {
    private ApexHunter bot;

    public IsTrapPlacementValid (ApexHunter bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        if(bot.logicType == LogicType.PRIORITIZE_SPOTS)
            bot.isTrapObjAround.trapState = IsTrapObjAround.TrapState.SUCCESS;
        else
            bot.isTrapObjAround.trapState = IsTrapObjAround.TrapState.FAILED;
        return bot.isTrapObjAround;
    }

    @Override
    public boolean validate() {
        UpdateUI.debug(bot.player.getName() + " -> trapCoords: " + bot.trapCoords.toString());
        UpdateUI.debug(bot.player.getName() + " -> placedTrapCoordList: " + bot.placedTrapCoordList.toString());

        validate = true;
        for(Coordinate coord : bot.spotCoordList){
            if(bot.trapCoords.contains(coord) && bot.placedTrapCoordList.stream().parallel().filter(i -> i.getLocatable().getPosition().equals(coord)) == null) {
                bot.badCoordinate = coord;
                validate = false;
                break;
            }
        }

        UpdateUI.debug(bot.player.getName() + " -> Branch: IsTrapPlacementValid: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        return bot.adjustSpotCoordLeaf;
    }
}
