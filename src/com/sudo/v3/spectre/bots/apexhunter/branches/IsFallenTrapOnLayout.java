package com.sudo.v3.spectre.bots.apexhunter.branches;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexhunter.ApexHunter;
import com.sudo.v3.spectre.statics.UpdateUI;

public class IsFallenTrapOnLayout extends SudoBranchTask {
    private ApexHunter bot;
    private Coordinate fallenTrapCoord, trapObjCoord;
    private GameObject trapObjOnFallen;

    public IsFallenTrapOnLayout(ApexHunter bot) {
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        trapObjOnFallen = GameObjects.newQuery().within(fallenTrapCoord.getArea()).names(bot.trapType.getName(), bot.trapType.getSuccessfulName(), bot.trapType.getUnsuccessfulName()).results().first();
        validate = trapObjOnFallen != null && trapObjOnFallen.isValid();
        UpdateUI.debug(bot.player.getName() + " -> SubBranch: IsPlacedTrapOnFallenTrap: " + validate);
        if(validate) {
            bot.pickupGroundTrapLeaf.setObject(bot.trapGroundItem);
            return bot.pickupGroundTrapLeaf;
        }
        else {
            bot.layGroundTrapLeaf.setObject(bot.trapGroundItem);
            return bot.layGroundTrapLeaf;
        }
    }

    @Override
    public boolean validate() {
        validate = (fallenTrapCoord = bot.trapGroundItem.getPosition()) != null && bot.spotCoordList.contains(fallenTrapCoord);
        UpdateUI.debug(bot.player.getName() + " -> Branch: IsFallenTrapOnLayout: " + validate);

        return validate;
    }

    @Override
    public TreeTask failureTask() {
        bot.pickupGroundTrapLeaf.setObject(bot.trapGroundItem);
        return bot.pickupGroundTrapLeaf;
    }
}