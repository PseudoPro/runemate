package com.sudo.v3.spectre.bots.apexhunter.branches.salamander;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.queries.results.LocatableEntityQueryResults;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexhunter.ApexHunter;
import com.sudo.v3.spectre.bots.apexhunter.enums.LogicType;
import com.sudo.v3.spectre.bots.apexhunter.enums.TraversalLocation;
import com.sudo.v3.spectre.statics.Areas;
import com.sudo.v3.spectre.statics.UpdateUI;

import java.util.Arrays;

public class IsSuccessfulTreeTrapAround extends SudoBranchTask {
    private ApexHunter bot;

    public IsSuccessfulTreeTrapAround(ApexHunter bot) {
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        if (bot.successTrapObj.distanceTo(bot.player) > 10) {
            bot.traversalArea = new Area.Circular(bot.successTrapObj.getPosition(), 3);
            bot.traversalLocation = TraversalLocation.TRAP_SPOT;
            return bot.traversalLeaf;
        } else {
            bot.traversalArea = bot.successTrapObj.getArea();
            bot.pickupSuccessfulTrapLeaf.setObject(bot.successTrapObj);
            return bot.pickupSuccessfulTrapLeaf;
        }
    }

    @Override
    public boolean validate() {
        UpdateUI.debug("spotCoordList: " + Arrays.toString(bot.spotCoordList.toArray(new Coordinate[bot.spotCoordList.size()])));

        LocatableEntityQueryResults<GameObject> results = GameObjects.newQuery().names(bot.trapType.getSuccessfulName()).actions(bot.trapType.getGrabSuccessAction()).results();
        UpdateUI.debug(bot.player.getName() + " -> resultsSize: " + results.size());

        for(int i = results.size() - 1; i >= 0; i--){
            Area tempArea = results.get(i).getArea();
            UpdateUI.debug("tempAreaObj: " + tempArea.toString());
            if(tempArea == null || !Areas.containsAnyOfCoordinates(tempArea, bot.spotCoordList) || results.get(i).getDefinition().getActions().contains("Investigate"))
                results.remove(results.get(i));
        }

        bot.successTrapObj = results.nearest();
//        bot.successTrapObj = GameObjects.newQuery().names(bot.trapType.getSuccessfulName()).actions(bot.trapType.getGrabSuccessAction()).filter(i -> {
//            Area tempArea = i.getArea();
//            UpdateUI.debug("tempAreaObj: " + tempArea.toString());
//            return tempArea != null && Areas.containsAnyOfCoordinates(tempArea, bot.spotCoordList) && !i.getDefinition().getActions().contains("Investigate");
//        }).results().nearest();
        validate = bot.successTrapObj != null && bot.successTrapObj.isValid();
        UpdateUI.debug(bot.player.getName() + " -> Branch: IsSuccessfulTreeTrapAround: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        if(bot.logicType == LogicType.PRIORITIZE_SPOTS)
            return bot.manageInventoryLeaf;
        else
            return bot.isTreeTrapOpen;
    }
}
