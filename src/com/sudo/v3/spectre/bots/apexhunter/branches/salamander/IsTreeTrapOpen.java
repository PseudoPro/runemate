package com.sudo.v3.spectre.bots.apexhunter.branches.salamander;

import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexhunter.ApexHunter;
import com.sudo.v3.spectre.bots.apexhunter.enums.LogicType;
import com.sudo.v3.spectre.bots.apexhunter.enums.TraversalLocation;
import com.sudo.v3.spectre.statics.Areas;
import com.sudo.v3.spectre.statics.UpdateUI;

public class IsTreeTrapOpen extends SudoBranchTask {
    private ApexHunter bot;

    public IsTreeTrapOpen (ApexHunter bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        if(bot.treeTrapObj.distanceTo(bot.player) > 10){
            bot.traversalArea = new Area.Circular(bot.treeTrapObj.getPosition(), 3);
            bot.traversalLocation = TraversalLocation.TRAP_SPOT;
            return bot.traversalLeaf;
        } else {
            bot.traversalArea = bot.treeTrapObj.getArea();
            return bot.placeTrapLeaf;
        }
    }

    @Override
    public boolean validate() {
        bot.treeTrapObj = GameObjects.newQuery().names(bot.trapType.getName()).actions(bot.trapType.getLayAction()).filter(i -> {
            Area tempArea = i.getArea();
            UpdateUI.debug("tempAreaObj: " + tempArea.toString());
            return tempArea != null && Areas.containsAnyOfCoordinates(tempArea, bot.spotCoordList) && !i.getDefinition().getActions().contains("Investigate");
        }).results().nearest();
        validate = bot.placedTrapCoordList.size() < bot.maxNumOfTraps && bot.treeTrapObj != null && bot.treeTrapObj.isValid();
        UpdateUI.debug(bot.player.getName() + " -> Branch: IsTreeTrapOpen: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        if(bot.logicType == LogicType.PRIORITIZE_SPOTS)
            return bot.isSuccessfulTreeTrapAround;
        else
            return bot.manageInventoryLeaf;
    }
}
