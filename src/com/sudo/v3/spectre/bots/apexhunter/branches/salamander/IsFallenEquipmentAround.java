package com.sudo.v3.spectre.bots.apexhunter.branches.salamander;

import com.runemate.game.api.hybrid.entities.GroundItem;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.GroundItems;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexhunter.ApexHunter;
import com.sudo.v3.spectre.bots.apexhunter.enums.LogicType;
import com.sudo.v3.spectre.bots.apexhunter.enums.TraversalLocation;
import com.sudo.v3.spectre.statics.UpdateUI;

public class IsFallenEquipmentAround extends SudoBranchTask {
    private ApexHunter bot;
    private GroundItem equipmentGroundItem;

    public IsFallenEquipmentAround (ApexHunter bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        if (equipmentGroundItem.distanceTo(bot.player) > 8) {
            bot.traversalArea = new Area.Circular(equipmentGroundItem.getPosition(), 3);
            bot.traversalLocation = TraversalLocation.TRAP_SPOT;
            return bot.traversalLeaf;
        } else {
            String name = equipmentGroundItem.getDefinition().getName();
            UpdateUI.debug(bot.player.getName() + " -> DEBUG: Equipment Name: " + name);
            UpdateUI.debug(bot.player.getName() + " -> DEBUG: Equipment Interact Action: " + bot.pickupEquipmentLeaf.getAction());
            bot.pickupEquipmentLeaf.setObject(equipmentGroundItem);
            bot.pickupEquipmentLeaf.setName(name);
            return bot.pickupEquipmentLeaf;
        }
    }

    @Override
    public boolean validate() {
        equipmentGroundItem = GroundItems.newQuery().names(bot.trapType.getEquipmentNames()).within(new Area.Circular(bot.trapCenterCoord, 9)).results().nearest();

        validate = equipmentGroundItem != null && equipmentGroundItem.isValid() && equipmentGroundItem.getPosition() != null;
        UpdateUI.debug(bot.player.getName() + " -> Branch: IsFallenEquipmentAround: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        if(bot.logicType == LogicType.PRIORITIZE_SPOTS)
            return bot.isTreeTrapOpen;
        else
            return bot.isSuccessfulTreeTrapAround;
    }
}
