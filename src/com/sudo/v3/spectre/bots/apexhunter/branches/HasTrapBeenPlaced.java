package com.sudo.v3.spectre.bots.apexhunter.branches;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexhunter.ApexHunter;
import com.sudo.v3.spectre.bots.apexhunter.enums.TrapType;
import com.sudo.v3.spectre.statics.UpdateUI;

public class HasTrapBeenPlaced extends SudoBranchTask {
    private ApexHunter bot;
    private GameObject trap;
    private int checkAttempt = 0;

    public HasTrapBeenPlaced(ApexHunter bot) {
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        checkAttempt = 0;
        return bot.addTrapPlacementLeaf;
    }

    @Override
    public boolean validate() {
        Execution.delayUntil(() -> bot.player.distanceTo(bot.traversalArea) <= 1, 750, 1500);

        if(bot.trapType != TrapType.SALAMANDER)
            trap = GameObjects.newQuery().names(bot.trapType.getName()).within(bot.traversalArea).results().nearest();
        else
            trap = GameObjects.newQuery().names(bot.trapType.getName()).actions(bot.trapType.getDismantleAction()).within(bot.traversalArea).results().nearest();

        validate = trap != null && trap.isValid() && trap.distanceTo(bot.player) <= 1;
        UpdateUI.debug(bot.player.getName() + " -> Branch: HasTrapBeenPlaced(" + checkAttempt + "): " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        if (checkAttempt < 5) {
            checkAttempt = checkAttempt + 1;
            Execution.delay(200, 300);
            return bot.hasTrapBeenPlaced;
        } else {
            checkAttempt = 0;
            //if(Inventory.getQuantity(bot.trapType.getName()) < bot.trapInventoryCountAtPlacement)
                return bot.invalidTrapPlacementLeaf;
//            else {
//                bot.badCoordinate = bot.traversalArea.getPosition();
//                bot.validateTrapPlacement = false;
//                return bot.adjustSpotCoordLeaf;
//            }
        }
    }
}