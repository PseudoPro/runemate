package com.sudo.v3.spectre.bots.apexhunter.enums;

public enum LogicType {
    PRIORITIZE_SPOTS,
    PRIORITIZE_TRAPS
}
