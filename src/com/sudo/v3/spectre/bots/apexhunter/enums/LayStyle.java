package com.sudo.v3.spectre.bots.apexhunter.enums;

public enum LayStyle {
    BOX,
    LINE_NS,
    LINE_EW,
    PLUS_SIGN,
    PENTAGON
}
