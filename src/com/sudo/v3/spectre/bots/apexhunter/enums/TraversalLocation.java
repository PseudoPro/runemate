package com.sudo.v3.spectre.bots.apexhunter.enums;

public enum TraversalLocation {
    TRAP_AREA,
    TRAP_SPOT,
    UNDEFINED
}
