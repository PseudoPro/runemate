package com.sudo.v3.spectre.bots.apexhunter.enums;

import com.runemate.game.api.hybrid.Environment;

public enum TrapType {
    SNARE,
    BOX,
    MARASAMAW,
    TORTLE,
    SALAMANDER;

    private String name, successfulName, unsuccessfulName, rebuildAction, rebuildActionOSRS, dismantleAction, grabSuccessAction, layAction, pickupAction;
    private String[] equipmentNames;

    static {
        SNARE.name = "Bird snare";
        SNARE.successfulName = "Bird snare";
        SNARE.unsuccessfulName = "Bird snare";
        SNARE.rebuildAction = "Rebuild";
        SNARE.rebuildActionOSRS = "Dismantle";
        SNARE.dismantleAction = "Dismantle";
        SNARE.grabSuccessAction = "Check";
        SNARE.layAction = "Lay";
        SNARE.pickupAction = "Take";
        SNARE.equipmentNames = new String[]{"Bird snare"};

        BOX.name = "Box trap";
        BOX.successfulName = "Shaking box";
        BOX.unsuccessfulName = "Box trap";
        BOX.rebuildAction = "Rebuild";
        BOX.rebuildActionOSRS = "Reset";
        BOX.dismantleAction = "Dismantle";
        BOX.grabSuccessAction = "Check";
        BOX.layAction = "Lay";
        BOX.pickupAction = "Take";
        BOX.equipmentNames = new String[]{"Box trap"};

        MARASAMAW.name = "Marasamaw plant";
        MARASAMAW.successfulName = "Shaking marasamaw plant";
        MARASAMAW.unsuccessfulName = "Wilted marasamaw plant";
        MARASAMAW.rebuildAction = "Rebuild";
        MARASAMAW.rebuildActionOSRS = "Rebuild";
        MARASAMAW.dismantleAction = "Pick";
        MARASAMAW.grabSuccessAction = "Check";
        MARASAMAW.layAction = "Lay";
        MARASAMAW.pickupAction = "Take";
        MARASAMAW.equipmentNames = new String[]{"Marasamaw plant"};

        TORTLE.name = "Tortle trap";
        TORTLE.successfulName = "Tortle trap";
        TORTLE.unsuccessfulName = "Tortle trap";
        TORTLE.rebuildAction = "Reset";
        TORTLE.rebuildActionOSRS = "Reset";
        TORTLE.dismantleAction = "Dismantle";
        TORTLE.grabSuccessAction = "Check";
        TORTLE.layAction = "Lay";
        TORTLE.pickupAction = "Take";
        TORTLE.equipmentNames = new String[]{"Tortle trap"};

        SALAMANDER.name = "Young tree";
        SALAMANDER.successfulName = "Net trap";
        SALAMANDER.unsuccessfulName = "Young tree";
        SALAMANDER.rebuildAction = "Check";
        SALAMANDER.rebuildActionOSRS = "Check";
        SALAMANDER.dismantleAction = "Dismantle";
        SALAMANDER.grabSuccessAction = "Check";
        SALAMANDER.layAction = "Set-trap";
        SALAMANDER.pickupAction = "Take";
        SALAMANDER.equipmentNames = new String[]{"Small fishing net", "Rope"};
    }

    public String getName(){
        return name;
    }

    public String getSuccessfulName(){
        return successfulName;
    }

    public String getUnsuccessfulName() { return unsuccessfulName; }

    public String getRebuildAction(){
        if(Environment.isRS3())
            return rebuildAction;
        else
            return rebuildActionOSRS;
    }

    public String getDismantleAction(){
        return dismantleAction;
    }

    public String getGrabSuccessAction(){
        return grabSuccessAction;
    }

    public String getLayAction() {
        return layAction;
    }

    public String getPickupAction(){
        return pickupAction;
    }

    public String[] getEquipmentNames() { return equipmentNames; }
}
