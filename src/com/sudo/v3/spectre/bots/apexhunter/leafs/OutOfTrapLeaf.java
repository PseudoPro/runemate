package com.sudo.v3.spectre.bots.apexhunter.leafs;

import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexhunter.ApexHunter;
import com.sudo.v3.spectre.statics.UpdateUI;

import java.util.Arrays;

public class OutOfTrapLeaf extends LeafTask {
    private ApexHunter bot;

    public OutOfTrapLeaf(ApexHunter bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        UpdateUI.currentTask("Inventory is out of " + Arrays.toString(bot.trapType.getEquipmentNames()) + ". Stopping bot.", bot);
        bot.stop("Inventory is out of " + Arrays.toString(bot.trapType.getEquipmentNames()) + ". Stopping bot.");
    }
}
