package com.sudo.v3.spectre.bots.apexhunter.leafs;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.api.game.entities.ExpirableEntity;
import com.sudo.v3.spectre.api.util.SudoTimer;
import com.sudo.v3.spectre.bots.apexhunter.ApexHunter;
import com.sudo.v3.spectre.statics.UpdateUI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MaxTrapsReachedLeaf extends LeafTask {
    private ApexHunter bot;

    public MaxTrapsReachedLeaf (ApexHunter bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        bot.trapObjects = GameObjects.newQuery().names(bot.trapType.getName(), bot.trapType.getSuccessfulName(), bot.trapType.getUnsuccessfulName()).within(bot.trapArea).results().asList();
        ArrayList<Coordinate> generatedCoordList = bot.generateSpotsFromLayout();
        UpdateUI.debug("Trap Objects: " + Arrays.toString(bot.trapObjects.toArray()));
        UpdateUI.debug("Generated Coordinate Layout: " + Arrays.toString(generatedCoordList.toArray()));

        if(bot.trapObjects.size() == generatedCoordList.size()){
            UpdateUI.currentTask("Failsafe initiated. Traps currently placed are determined as ours.", bot);

            bot.spotCoordList.clear();
            for(GameObject obj : bot.trapObjects){
                //if(obj != null && obj.getPosition() != null)
                    bot.spotCoordList.add(obj.getPosition());
                    bot.placedTrapCoordList.add(new ExpirableEntity(obj.getPosition(), new SudoTimer(0, 120000)));
            }
            bot.maxTrapsFailsafe = false;
        } else if (bot.trapObjects.size() > generatedCoordList.size()){
            if(bot.worldHopEnabled) {
                UpdateUI.currentTask("Multiple traps other than our own detected. Preparing to hop worlds.", bot);
                bot.worldHopping = true;
                bot.maxTrapsFailsafe = false;
            }else{
                UpdateUI.currentTask("Multiple traps other than our own detected. Advised to hop worlds..", bot);
                bot.maxTrapsFailsafe = false;
            }
        }else{
            bot.maxTrapsFailsafe = false;
        }
    }
}
