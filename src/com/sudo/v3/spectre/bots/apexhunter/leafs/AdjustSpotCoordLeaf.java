package com.sudo.v3.spectre.bots.apexhunter.leafs;

import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexhunter.ApexHunter;

public class AdjustSpotCoordLeaf extends LeafTask {
    private ApexHunter bot;

    public AdjustSpotCoordLeaf(ApexHunter bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        bot.adjustSpotCoordinates();
    }
}
