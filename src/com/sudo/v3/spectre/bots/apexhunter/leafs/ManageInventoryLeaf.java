package com.sudo.v3.spectre.bots.apexhunter.leafs;

import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.util.Regex;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexhunter.ApexHunter;

import java.util.ArrayList;

public class ManageInventoryLeaf extends LeafTask {
    private ApexHunter bot;

    public ManageInventoryLeaf(ApexHunter bot){
        this.bot = bot;
    }

    private ArrayList<String> dropList = new ArrayList<String>(){{
        add("bones");
        add("Bones");
        add("bird meat");
        add("lizard");
        add("salamander");
        add("skin(0)");
    }};

    @Override
    public void execute() {
        bot.managingInventory = false;
        dropList.forEach(i -> {
            if(Inventory.contains(Regex.getPatternForContainsString(i))){
                bot.managingInventory = true;

                bot.powerDropLeaf.setValues(i, 1, 5);
                bot.powerDropLeaf.execute();
            }
        });

        if(!bot.managingInventory)
            Execution.delay(500, 750);

//        SpriteItemQueryResults results = Inventory.newQuery().filter(i -> dropList.contains(i.getDefinition().getName().toLowerCase())).results();
//        if(results != null){
//            List<String> tempList = new ArrayList<>();
//            results.forEach(i -> {
//                if(i.getDefinition() != null && i.getDefinition().getName() != null)
//                    tempList.add(i.getDefinition().getName());
//            });
//            bot.itemsToDrop = tempList.toArray(new String[0]);
//        }
//
//        if(bot.itemsToDrop.length > 0) {
//            bot.managingInventory = true;
//            if (bot.isRS3) {
//                for (int i = 0; i < results.size(); ) {
//                    if (SudoInventory.drop(results.get(i)))
//                        i++;
//                    else if(SudoInventory.drop(results.get(i), "Release"))
//                        i++;
//                }
//            } else {
//                if (OptionsTab.isShiftDroppingEnabled()) {
//                    if (results != null)
//                        bot.fastClick.clickItems(results, true);
//                } else
//                    OptionsTab.setShiftDropping(true);
//            }
//        }

//        for(String pattern : dropList) {
//            if (Inventory.contains(Regex.getPatternForContainsString(pattern))) {
//                UpdateUI.currentTask("Attempting to drop item containing \"" + pattern + "\"", bot);
//                dropItem(pattern);
//                bot.managingInventory = true;
//            }
//        }
    }

//    public boolean dropItem(String pattern){
//        SpriteItem item = Inventory.newQuery().names(Regex.getPatternForContainsString(pattern)).results().first();
//        if(item != null && item.isValid()){
//            if(item.interact("Drop")) {
//                Execution.delay(100, 300);
//                return true;
//            }
//        }
//        return false;
//    }
}
