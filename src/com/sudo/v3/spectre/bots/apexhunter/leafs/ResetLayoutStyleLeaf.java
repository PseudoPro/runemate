package com.sudo.v3.spectre.bots.apexhunter.leafs;

import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexhunter.ApexHunter;
import com.sudo.v3.spectre.bots.apexhunter.branches.IsTrapObjAround;
import com.sudo.v3.spectre.bots.apexhunter.enums.TrapType;
import com.sudo.v3.spectre.statics.UpdateUI;

public class ResetLayoutStyleLeaf extends LeafTask {
    private ApexHunter bot;

    public ResetLayoutStyleLeaf (ApexHunter bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        if (bot.placedTrapCoordList.size() > 0 && (bot.trapType != TrapType.SNARE || Inventory.getItems().size() > 25)) {
            UpdateUI.currentTask("Refreshing layout style.", bot);
            UpdateUI.debug("PlacedTrapCoordList: " + bot.placedTrapCoordList.size() + " - " + bot.placedTrapCoordList.toString());

            bot.isTrapObjAround.trapState = IsTrapObjAround.TrapState.FALLEN;
            if (bot.isTrapObjAround.validate()) {
                bot.isTrapObjAround.successTask().execute();
            } else {
                bot.isTrapObjAround.trapState = IsTrapObjAround.TrapState.SUCCESS;
                if (bot.isTrapObjAround.validate() && (bot.trapType != TrapType.SNARE || Inventory.getItems().size() > 25)) {
                    bot.isTrapObjAround.successTask().execute();
                } else {
                    bot.isTrapObjAround.trapState = IsTrapObjAround.TrapState.FAILED;
                    if (bot.isTrapObjAround.validate()) {
                        bot.dismantleUnsuccessfulTrapLeaf.setObject(bot.unsuccessTrapObj);
                        bot.dismantleUnsuccessfulTrapLeaf.execute();
                    }else{
                        bot.placedTrapCoordList.clear();
                    }
                }
            }
        } else {
            bot.resetTrapLayoutTimer.reset();
        }
    }
}
