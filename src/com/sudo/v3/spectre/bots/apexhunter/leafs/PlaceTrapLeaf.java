package com.sudo.v3.spectre.bots.apexhunter.leafs;

import com.runemate.game.api.hybrid.local.hud.interfaces.ChatDialog;
import com.runemate.game.api.hybrid.local.hud.interfaces.InterfaceComponent;
import com.runemate.game.api.hybrid.local.hud.interfaces.Interfaces;
import com.runemate.game.api.hybrid.queries.InterfaceComponentQueryBuilder;
import com.runemate.game.api.hybrid.util.Regex;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.rs3.local.hud.interfaces.eoc.ActionBar;
import com.runemate.game.api.rs3.queries.ActionBarQueryBuilder;
import com.runemate.game.api.rs3.queries.results.ActionBarQueryResults;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.api.game.hud.SudoInventory;
import com.sudo.v3.spectre.bots.apexhunter.ApexHunter;
import com.sudo.v3.spectre.bots.apexhunter.enums.TrapType;
import com.sudo.v3.spectre.api.game.entities.ApexPlayer;
import com.sudo.v3.spectre.statics.Animations;
import com.sudo.v3.spectre.statics.UpdateUI;

import java.util.List;

public class PlaceTrapLeaf extends LeafTask {
    private ApexHunter bot;
    private InterfaceComponentQueryBuilder builder;
    private InterfaceComponent cannotPlaceTrapHereComponent;
    private InterfaceComponent tooManyTrapsComponent;

    public PlaceTrapLeaf(ApexHunter bot) {
        this.bot = bot;
    }

    @Override
    public void execute() {

        if(bot.isRS3 && bot.trapType != TrapType.SALAMANDER){
            UpdateUI.currentTask("Attempting to lay from action bar...", bot);

            if (ChatDialog.getContinue() != null) {
                ChatDialog.getContinue().select();
                Execution.delayUntil(() -> ChatDialog.getContinue() == null, 1500, 3000);
            }

            ActionBarQueryBuilder actionBarSlots = ActionBar.newQuery().filled(true).empty(false);
            actionBarSlots.results().forEach(i -> UpdateUI.debug("Name: " + i.getName() + " - Index: " + i.getIndex()));

            actionBarSlots = actionBarSlots.names(Regex.getPatternForContainsString(bot.trapType.getName()));
            ActionBarQueryResults actionBarResults = actionBarSlots.results();

            if (actionBarResults != null && !actionBarResults.isEmpty()) {
                for (int i = 0; i < actionBarResults.size(); i++) {
                    ActionBar.Slot slot = actionBarResults.get(i);
                    if (slot != null && slot.isActivatable()) {
                        List<String> slotActions = slot.getActions();
                        if (slotActions != null && slotActions.size() > 0) {
                            if (slotActions.get(0).toLowerCase().contains(bot.trapType.getLayAction()))
                                slot.activate();
                            else if (slotActions.contains(bot.trapType.getLayAction()))
                                slot.interact(bot.trapType.getLayAction(), slot.getName());
                        }
                    }
                }
            } else {
                UpdateUI.currentTask("\"" + bot.trapType.getName() + "\"" + " not found in action bar.", bot);
            }
        }

        UpdateUI.currentTask("Attempting to place trap from Inventory.", bot);
        if (bot.trapType != TrapType.SALAMANDER ? bot.trapSprite.interact(bot.trapType.getLayAction()) : bot.treeTrapObj.interact(bot.trapType.getLayAction())) {
            Execution.delay(250);
            builder = Interfaces.newQuery().containers(1477);
            cannotPlaceTrapHereComponent = builder.textContains("lay a trap").results().first();
            tooManyTrapsComponent = builder.textContains("require a Hunter").results().first();

            if (tooManyTrapsComponent != null && tooManyTrapsComponent.isValid()) {
                UpdateUI.debug(bot.player.getName() + " -> InterfaceFound: \"Max traps exceeded.\"");
                bot.maxTrapsFailsafe = true;
                bot.maxTrapsReachedLeaf.execute();
            } else if (bot.trapType != TrapType.SALAMANDER && cannotPlaceTrapHereComponent != null && cannotPlaceTrapHereComponent.isValid()) {
                bot.badCoordinate = bot.traversalArea.getPosition();
                bot.adjustSpotCoordinates();
            } else {
                bot.validateTrapPlacement = true;
                Execution.delayWhile(bot.player::isMoving, 1000, 1500);
                if (Execution.delayUntil(() -> ApexPlayer.isInAnimation(Animations.HUNT_ANIM), 600, 1000))
                    Execution.delayWhile(() -> ApexPlayer.isInAnimation(Animations.HUNT_ANIM), 1000, 1500);
            }
        }
    }
}