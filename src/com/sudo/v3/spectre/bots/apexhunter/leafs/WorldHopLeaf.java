package com.sudo.v3.spectre.bots.apexhunter.leafs;

import com.runemate.game.api.hybrid.local.Worlds;
import com.runemate.game.api.hybrid.local.hud.interfaces.WorldHop;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexhunter.ApexHunter;
import com.sudo.v3.spectre.statics.UpdateUI;

public class WorldHopLeaf extends LeafTask {
    private ApexHunter bot;

    public WorldHopLeaf(ApexHunter bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        if(bot.worldOverview == null){
            bot.worldOverview = Worlds.newQuery().member().results().random();
        }

        UpdateUI.currentTask("Attempting to hop to world " + bot.worldOverview.getId() + ".", bot);

        if(WorldHop.hopTo(bot.worldOverview)){
            bot.worldOverview = Worlds.newQuery().member().results().random();
            bot.worldHopping = false;
        }
    }
}
