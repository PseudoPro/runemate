package com.sudo.v3.spectre.bots.apexhunter.leafs;

import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexhunter.ApexHunter;
import com.sudo.v3.spectre.bots.apexhunter.enums.TraversalLocation;
import com.sudo.v3.spectre.common.navigation.Traverse;
import com.sudo.v3.spectre.statics.UpdateUI;

public class TraversalLeaf extends LeafTask {
    private ApexHunter bot;

    public TraversalLeaf(ApexHunter bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        if(bot.traversalLocation == TraversalLocation.TRAP_AREA)
            UpdateUI.currentTask("Traveling to hunting area.", bot);
        else if(bot.traversalLocation == TraversalLocation.TRAP_SPOT)
            UpdateUI.currentTask("Traveling to trap spot.", bot);

        if(Traverse.smartRegionPath(bot.nav, bot.traversalArea)){
            Execution.delay(400, 1000);
            Execution.delayWhile(() -> !bot.traversalArea.contains(bot.player), 750, 2000);
        }
    }
}
