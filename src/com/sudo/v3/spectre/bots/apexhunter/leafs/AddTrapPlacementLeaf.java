package com.sudo.v3.spectre.bots.apexhunter.leafs;

import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.api.game.entities.ExpirableEntity;
import com.sudo.v3.spectre.api.util.SudoTimer;
import com.sudo.v3.spectre.bots.apexhunter.ApexHunter;
import com.sudo.v3.spectre.statics.UpdateUI;

public class AddTrapPlacementLeaf extends LeafTask {
    private ApexHunter bot;

    public AddTrapPlacementLeaf (ApexHunter bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        UpdateUI.currentTask("Successfully placed and detected trap at " + bot.traversalArea.getPosition().toString() + ".", bot);
        if(bot.placedTrapCoordList.stream().parallel().filter(i -> i.getLocatable().getPosition().equals(bot.traversalArea.getPosition())) == null)
            bot.placedTrapCoordList.add(new ExpirableEntity(bot.traversalArea.getPosition(), new SudoTimer(0, 120000)));
        bot.validateTrapPlacement = false;
    }
}
