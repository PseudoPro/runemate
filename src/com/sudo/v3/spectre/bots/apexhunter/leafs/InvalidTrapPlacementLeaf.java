package com.sudo.v3.spectre.bots.apexhunter.leafs;

import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexhunter.ApexHunter;
import com.sudo.v3.spectre.statics.UpdateUI;

public class InvalidTrapPlacementLeaf extends LeafTask {
    private ApexHunter bot;

    public InvalidTrapPlacementLeaf (ApexHunter bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        UpdateUI.currentTask("Failed to detect trap placement. Resetting logic.", bot);
        bot.validateTrapPlacement = false;
    }
}