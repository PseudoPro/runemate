package com.sudo.v3.spectre.bots.apexhunter.ui;

import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.Players;
import com.sudo.v3.spectre.bots.apexhunter.ApexHunter;
import com.sudo.v3.spectre.bots.apexhunter.enums.LayStyle;
import com.sudo.v3.spectre.bots.apexhunter.enums.LogicType;
import com.sudo.v3.spectre.bots.apexhunter.enums.TrapType;
import com.sudo.v3.spectre.common.leafs.InteractLeaf;
import com.sudo.v3.ui.base.SudoFXController;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.util.ResourceBundle;

public class HunterFXController extends SudoFXController implements Initializable {
    private ApexHunter bot;

    public HunterFXController(ApexHunter bot) {
        super(bot);

        this.bot = bot;
    }

    @FXML
    private Button start_BT;

    @FXML
    private Label prompt_L;

    @FXML
    private TextField xTextField, yTextField, planeTextField;

    @FXML
    private ComboBox trapTypeComboBox, layStyleComboBox, logicTypeComboBox, maxTrapsComboBox;

    @FXML
    private CheckBox worldHopCheckbox, resetTrapLayoutCheckbox;

    @FXML
    private Button setLocationButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Initialize Super
        super.initialize(location, resources);

        if(Environment.isRS3())
            trapTypeComboBox.getItems().addAll("Snare Trap", "Box Trap", "Marasamaw", "Tortle", "Salamander");
        else
            trapTypeComboBox.getItems().addAll("Snare Trap", "Box Trap", "Salamander");
        layStyleComboBox.getItems().addAll("Square", "Line North/South", "Line East/West", "Plus-Sign", "Pentagon");

        logicTypeComboBox.getItems().addAll("Prioritize open spot over successful traps.", "Prioritize successful traps over open spots.");

        maxTrapsComboBox.getItems().addAll("Generate from skill level.", "1", "2", "3", "4", "5");

        setLocationButton.setOnAction(getSetLocationButtonAction());
        trapTypeComboBox.setOnAction(getComboBoxAction());
        layStyleComboBox.setOnAction(getComboBoxAction());
        start_BT.setOnAction(getStart_BTAction());

    }

    private EventHandler<ActionEvent> getSetLocationButtonAction() {
        return event -> {
            if (logicTypeComboBox.getSelectionModel().getSelectedItem() == null) {
                prompt_L.setText("Please select trap type before setting location.");
            } else {
                prompt_L.setText("");
                bot.getPlatform().invokeLater(() -> {
                    System.out.println("Attempting to get player position.");
                    Player player = Players.getLocal();
                    if (player != null) {
                        bot.trapCenterCoord = player.getPosition();
                        bot.trapArea = new Area.Circular(player.getPosition(), trapTypeComboBox.getSelectionModel().getSelectedItem().toString().equals("Salamander") ? 7 : 3);
                        bot.createSpotCoordListBoolean = true;

                        Platform.runLater(() -> {
                            xTextField.textProperty().set(Integer.toString(bot.trapCenterCoord.getX()));
                            yTextField.textProperty().set(Integer.toString(bot.trapCenterCoord.getY()));
                            planeTextField.textProperty().set(Integer.toString(bot.trapCenterCoord.getPlane()));
                            start_BT.setDisable(startButtonIsDisabled());
                        });
                    }
                });
            }
        };
    }

    private EventHandler<ActionEvent> getComboBoxAction() {
        return event -> {
            start_BT.setDisable(startButtonIsDisabled());

            layStyleComboBox.setDisable(trapTypeComboBox.getSelectionModel().getSelectedItem().toString().equals("Salamander"));
            resetTrapLayoutCheckbox.setDisable(trapTypeComboBox.getSelectionModel().getSelectedItem().toString().equals("Salamander"));
        };
    }

    private EventHandler<ActionEvent> getStart_BTAction() {
        return event -> {
            switch(trapTypeComboBox.getSelectionModel().getSelectedItem().toString()){
                case "Snare Trap":
                    bot.trapType = TrapType.SNARE;
                    break;
                case "Box Trap":
                    bot.trapType = TrapType.BOX;
                    break;
                case "Marasamaw":
                    bot.trapType = TrapType.MARASAMAW;
                    break;
                case "Tortle":
                    bot.trapType = TrapType.TORTLE;
                    break;
                case "Salamander":
                    bot.trapType = TrapType.SALAMANDER;
                    break;
            }

            if(layStyleComboBox.getSelectionModel().getSelectedItem() != null) {
                switch (layStyleComboBox.getSelectionModel().getSelectedItem().toString()) {
                    case "Square":
                        bot.layStyle = LayStyle.BOX;
                        break;
                    case "Line North/South":
                        bot.layStyle = LayStyle.LINE_NS;
                        break;
                    case "Line East/West":
                        bot.layStyle = LayStyle.LINE_EW;
                        break;
                    case "Plus-Sign":
                        bot.layStyle = LayStyle.PLUS_SIGN;
                        break;
                    case "Pentagon":
                        bot.layStyle = LayStyle.PENTAGON;
                        break;
                }
            }

            switch(logicTypeComboBox.getSelectionModel().getSelectedItem().toString()){
                case "Prioritize open spot over successful traps.":
                    bot.logicType = LogicType.PRIORITIZE_SPOTS;
                    break;
                case "Prioritize successful traps over open spots.":
                    bot.logicType = LogicType.PRIORITIZE_TRAPS;
                    break;
            }

            switch(maxTrapsComboBox.getSelectionModel().getSelectedItem().toString()){
                case "Generate from skill level.":
                    bot.maxNumOfTraps = 0;
                    break;
                case "1":
                    bot.maxNumOfTraps = 1;
                    break;
                case "2":
                    bot.maxNumOfTraps = 2;
                    break;
                case "3":
                    bot.maxNumOfTraps = 3;
                    break;
                case "4":
                    bot.maxNumOfTraps = 4;
                    break;
                case "5":
                    bot.maxNumOfTraps = 5;
                    break;
            }

            bot.trapCenterCoord = new Coordinate(Integer.parseInt(xTextField.getText()), Integer.parseInt(yTextField.getText()), Integer.parseInt(planeTextField.getText()));

            bot.worldHopEnabled = worldHopCheckbox.isSelected();
            bot.resetTrapLayout = resetTrapLayoutCheckbox.isSelected();

            start_BT.textProperty().set("Resume");
            prompt_L.textProperty().set(" ");
            super.getStart_BTEvent();
        };
    }

    private boolean startButtonIsDisabled(){
        return trapTypeComboBox.getSelectionModel().getSelectedItem() == null || (layStyleComboBox.getSelectionModel().getSelectedItem() == null && !trapTypeComboBox.getSelectionModel().getSelectedItem().toString().equals("Salamander")) || logicTypeComboBox.getSelectionModel().getSelectedItem() == null || maxTrapsComboBox.getSelectionModel().getSelectedItem() == null || (xTextField.getText().equals("0") && yTextField.getText().equals("0") && planeTextField.getText().equals("0"));
    }
}
