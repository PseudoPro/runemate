package com.sudo.v3.spectre.bots.apexhunter.ui;

import com.runemate.game.api.hybrid.util.Resources;
import com.sudo.v3.spectre.bots.apexhunter.ApexHunter;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.GridPane;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class HunterFXGui extends GridPane implements Initializable {

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setVisible(true);
    }

    public HunterFXGui(ApexHunter bot) {

        FXMLLoader loader = new FXMLLoader();

        Future<InputStream> stream = bot.getPlatform().invokeLater(() -> Resources.getAsStream("com/sudo/v3/spectre/bots/apexhunter/ui/HunterGUI.fxml"));

        // Set this class as root of the fxml file, and SudoBuddyFXGui as the controller.
        loader.setController(new HunterFXController(bot));
        loader.setRoot(this);

        try {
            loader.load(stream.get());
        } catch (IOException | InterruptedException | ExecutionException e) {
            System.err.println("Error loading GUI");
            e.printStackTrace();
        }
    }
}