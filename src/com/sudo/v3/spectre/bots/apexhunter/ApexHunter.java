package com.sudo.v3.spectre.bots.apexhunter;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.GroundItem;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.Skill;
import com.runemate.game.api.hybrid.local.WorldOverview;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.util.Regex;
import com.runemate.game.api.hybrid.util.calculations.CommonMath;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.script.framework.listeners.ChatboxListener;
import com.runemate.game.api.script.framework.listeners.events.MessageEvent;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBot;
import com.sudo.v3.spectre.api.game.entities.ExpirableEntity;
import com.sudo.v3.spectre.bots.apexhunter.branches.*;
import com.sudo.v3.spectre.bots.apexhunter.branches.salamander.IsFallenEquipmentAround;
import com.sudo.v3.spectre.bots.apexhunter.branches.salamander.IsSuccessfulTreeTrapAround;
import com.sudo.v3.spectre.bots.apexhunter.branches.salamander.IsTreeTrapOpen;
import com.sudo.v3.spectre.bots.apexhunter.branches.salamander.SalRoot;
import com.sudo.v3.spectre.bots.apexhunter.enums.LayStyle;
import com.sudo.v3.spectre.bots.apexhunter.enums.LogicType;
import com.sudo.v3.spectre.bots.apexhunter.enums.TrapType;
import com.sudo.v3.spectre.bots.apexhunter.enums.TraversalLocation;
import com.sudo.v3.spectre.bots.apexhunter.leafs.*;
import com.sudo.v3.spectre.bots.apexhunter.ui.HunterFXGui;
import com.sudo.v3.spectre.bots.apexhunter.ui.HunterInfoUI;
import com.sudo.v3.spectre.api.util.SudoTimer;
import com.sudo.v3.spectre.common.leafs.InteractLeaf;
import com.sudo.v3.spectre.statics.Areas;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.sudo.v3.ui.Info;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ApexHunter extends SudoBot implements ChatboxListener {

    private HunterInfoUI infoUI;
    private HunterFXGui configUI;

    public boolean createSpotCoordListBoolean = true, validateTrapPlacement = false, maxTrapsFailsafe = false, worldHopping = false, worldHopEnabled = false, managingInventory = false, resetTrapLayout = true;
    public Coordinate trapCenterCoord, badCoordinate;
    public Area trapArea, traversalArea;
    public ArrayList<Coordinate> spotCoordList = new ArrayList<>();
    public ArrayList<ExpirableEntity> placedTrapCoordList = new ArrayList<>();

    public int maxNumOfTraps = 0;

    public TrapType trapType;
    public LayStyle layStyle;
    public LogicType logicType;
    public TraversalLocation traversalLocation = TraversalLocation.UNDEFINED;

    public GroundItem trapGroundItem;
    public GameObject successTrapObj, unsuccessTrapObj, treeTrapObj;
    public SpriteItem trapSprite;

    public List<GameObject> trapObjects;
    public ArrayList<Coordinate> trapCoords;
    public List<Player> playersList;
    public ArrayList<Coordinate> playerCoords;

    public WorldOverview worldOverview = null;

    public SudoTimer resetTrapLayoutTimer = new SudoTimer(2700000, 4500000);

    public int inventoryTest = 0;

    // Branches
    private Root root = new Root(this);
    public IsTrapSpotOpen isTrapSpotOpen = new IsTrapSpotOpen(this);
    public InventoryHasTrap inventoryHasTrap = new InventoryHasTrap(this);
    public IsTrapObjAround isTrapObjAround = new IsTrapObjAround(this);
    public HasTrapBeenPlaced hasTrapBeenPlaced = new HasTrapBeenPlaced(this);
    public IsTrapPlacementValid isTrapPlacementValid = new IsTrapPlacementValid(this);
    public IsFallenTrapOnLayout isFallenTrapOnLayout = new IsFallenTrapOnLayout(this);

    // Leafs
    public InteractLeaf<GroundItem> pickupGroundTrapLeaf;
    public InteractLeaf<GroundItem> layGroundTrapLeaf;
    public InteractLeaf<GameObject> replaceUnsuccessfulTrapLeaf;
    public InteractLeaf<GameObject> dismantleUnsuccessfulTrapLeaf;
    public InteractLeaf<GameObject> pickupSuccessfulTrapLeaf;
    public ManageInventoryLeaf manageInventoryLeaf = new ManageInventoryLeaf(this);
    public TraversalLeaf traversalLeaf = new TraversalLeaf(this);
    public PlaceTrapLeaf placeTrapLeaf = new PlaceTrapLeaf(this);
    public OutOfTrapLeaf outOfTrapLeaf = new OutOfTrapLeaf(this);
    public AddTrapPlacementLeaf addTrapPlacementLeaf = new AddTrapPlacementLeaf(this);
    public InvalidTrapPlacementLeaf invalidTrapPlacementLeaf = new InvalidTrapPlacementLeaf(this);
    public AdjustSpotCoordLeaf adjustSpotCoordLeaf = new AdjustSpotCoordLeaf(this);
    public MaxTrapsReachedLeaf maxTrapsReachedLeaf = new MaxTrapsReachedLeaf(this);
    public WorldHopLeaf worldHopLeaf = new WorldHopLeaf(this);
    public ResetLayoutStyleLeaf resetLayoutStyleLeaf = new ResetLayoutStyleLeaf(this);

    // Salamander Branches
    public SalRoot salRoot = new SalRoot(this);
    public IsSuccessfulTreeTrapAround isSuccessfulTreeTrapAround = new IsSuccessfulTreeTrapAround(this);
    public IsFallenEquipmentAround isFallenEquipmentAround = new IsFallenEquipmentAround(this);
    public IsTreeTrapOpen isTreeTrapOpen = new IsTreeTrapOpen(this);

    // Salamander Leafs
    public InteractLeaf<GroundItem> pickupEquipmentLeaf;

    public ApexHunter() {
        // Give spectre client this class as the embeddableUI
        setEmbeddableUI(this);
        isPremiumBot = false;
    }

    @Override
    public void onStart(String... args) {
        getEventDispatcher().addListener(this);

        setLoopDelay(100, 200);

        super.onStart();
    }

    @Override
    public String getBotName() {
        return "ApexHunter";
    }

    @Override
    public ObjectProperty<? extends Node> botInterfaceProperty() {
        if (botInterfaceProperty == null) {
            botInterfaceProperty = new SimpleObjectProperty<>(configUI = new HunterFXGui(this));
            guiList = new ArrayList<Node>();
            //guiList.add(_sbGUI = new SudoBuddyFXGui(this));
            guiList.add(configUI);
            guiList.add(infoUI = new HunterInfoUI(this));
        }
        return botInterfaceProperty;
    }

    @Override
    public TreeTask createRootTask() {
        return root;
    }

    @Override
    public void updateInfo() {

        // UpdateUI all current values in the XPInfo List
        XPInfoMap.values().forEach((xpInfo) -> xpInfo.update());

//        displayInfoMap.put("Fish Count: ", Integer.toString(itemCount) +
//                " (" + Integer.toString((int) CommonMath.rate(TimeUnit.HOURS, STOPWATCH.getRuntime(), itemCount)) + "/Hour)");
        displayInfoMap.put("GP: ", Integer.toString(grossIncome) +
                " (" + Integer.toString((int) CommonMath.rate(TimeUnit.HOURS, STOPWATCH.getRuntime(), grossIncome)) + "/Hour)");

        try {
            info = new Info(XPInfoMap,           // XP Info
                    displayInfoMap,
                    STOPWATCH.getRuntimeAsString(),        // Total Runtime
                    currentTaskString,                     // Current Task
                    abTaskString);                         // antiban Task

        } catch (Exception e) {
            e.printStackTrace();
        }

        Platform.runLater(() -> infoUI.update());
    }

    @Override
    public void onMessageReceived(MessageEvent event) {
        String message = event.getMessage();

        if (message.contains("This isn't your trap.")) {
            if (traversalArea.getPosition() != null) {
                UpdateUI.debug(player.getName() + " -> MessageIntercepted: \"This isn't your trap.\"");
                badCoordinate = traversalArea.getPosition();
                adjustSpotCoordinates();
            }
        }
        if (message.contains("at a time at your Hunter level")) {
            UpdateUI.debug(player.getName() + " -> MessageIntercepted: \"Max traps placed at current hunter level.(" + Skill.HUNTER.getCurrentLevel() + ")\"");
            maxTrapsFailsafe = true;
        }
    }

    @Override
    public boolean readyForBreak() {
        if (placedTrapCoordList.size() > 0 && !Inventory.isFull()) {
            UpdateUI.currentTask("Break initiated. Picking up traps.", this);

            isTrapObjAround.trapState = IsTrapObjAround.TrapState.FALLEN;
            if (isTrapObjAround.validate()) {
                pickupGroundTrapLeaf.setObject(trapGroundItem);
                pickupGroundTrapLeaf.execute();
                return false;
            } else {
                isTrapObjAround.trapState = IsTrapObjAround.TrapState.SUCCESS;
                if (isTrapObjAround.validate()) {
                    isTrapObjAround.successTask().execute();
                    return false;
                } else {
                    isTrapObjAround.trapState = IsTrapObjAround.TrapState.FAILED;
                    if (isTrapObjAround.validate()) {
                        dismantleUnsuccessfulTrapLeaf.setObject(unsuccessTrapObj);
                        dismantleUnsuccessfulTrapLeaf.execute();
                        return false;
                    } else {
                        placedTrapCoordList.clear();
                        return true;
                    }
                }
            }
        } else {
            return true;
        }
    }

    public void adjustSpotCoordinates() {
        if (badCoordinate != null) {
            UpdateUI.debug("1 - " + STOPWATCH.getRuntime(TimeUnit.MILLISECONDS));
            UpdateUI.debug(player.getName() + " -> Attempting to calculate new spot for coordinate (" + badCoordinate.getX() + ", " + badCoordinate.getY() + ", " + badCoordinate.getPlane() + ").");

            ArrayList<Coordinate> availableCoords = new ArrayList<>();
            Coordinate newCoord;

            UpdateUI.debug("2 - " + STOPWATCH.getRuntime(TimeUnit.MILLISECONDS));
            availableCoords.add(new Coordinate(badCoordinate.getX() + 1, badCoordinate.getY(), badCoordinate.getPlane()));
            availableCoords.add(new Coordinate(badCoordinate.getX() + 1, badCoordinate.getY() - 1, badCoordinate.getPlane()));
            availableCoords.add(new Coordinate(badCoordinate.getX(), badCoordinate.getY() - 1, badCoordinate.getPlane()));
            availableCoords.add(new Coordinate(badCoordinate.getX() - 1, badCoordinate.getY() - 1, badCoordinate.getPlane()));
            availableCoords.add(new Coordinate(badCoordinate.getX() - 1, badCoordinate.getY(), badCoordinate.getPlane()));
            availableCoords.add(new Coordinate(badCoordinate.getX() - 1, badCoordinate.getY() + 1, badCoordinate.getPlane()));
            availableCoords.add(new Coordinate(badCoordinate.getX(), badCoordinate.getY() + 1, badCoordinate.getPlane()));
            availableCoords.add(new Coordinate(badCoordinate.getX() + 1, badCoordinate.getY() + 1, badCoordinate.getPlane()));

            UpdateUI.debug("3 - " + STOPWATCH.getRuntime(TimeUnit.MILLISECONDS));

            List<GameObject> objList = GameObjects.newQuery().within(trapArea).results().asList();
            ArrayList<Area> objAreaList = new ArrayList<>();

            for (GameObject obj : objList) {
                if (obj != null && obj.getArea() != null)
                    objAreaList.add(obj.getArea());
            }

            for (int i = availableCoords.size() - 1; i > 0; i--) {
                if (trapCoords.contains(availableCoords.get(i)))
                    availableCoords.remove(i);
                else if (spotCoordList.contains(availableCoords.get(i)))
                    availableCoords.remove(i);
                else if (!trapArea.contains(availableCoords.get(i)))
                    availableCoords.remove(i);
                    //else if (!availableCoords.get(i).isReachable())
                    //    availableCoords.remove(i);
                else if (objAreaList.size() > 0 && Areas.anyContainsCoordinate(objAreaList, availableCoords.get(i))) {
                    availableCoords.remove(i);
                }
            }

            UpdateUI.debug("availableCoords.size : " + availableCoords.size());

            UpdateUI.debug("4 - " + STOPWATCH.getRuntime(TimeUnit.MILLISECONDS));
            if (availableCoords.size() > 0) {
                UpdateUI.currentTask("Issues placing trap at destination, adjusting layout style.", this);

                if (availableCoords.size() == 1)
                    newCoord = availableCoords.get(0);
                else
                    newCoord = availableCoords.get(Random.nextInt(0, availableCoords.size() - 1));

                int index = spotCoordList.indexOf(badCoordinate);
                UpdateUI.debug("spotCoordList.indexOf(badCoordinate): " + index);
                if (index >= 0)
                    spotCoordList.set(index, newCoord);
                else {
                    UpdateUI.debug("badCoordinate did not exist in spotCoordList.");
                    UpdateUI.debug("badCoordinate: " + badCoordinate);
                    UpdateUI.debug("spotCoordList Size: " + spotCoordList.size());
                    UpdateUI.debug("spotCoordList: " + spotCoordList.toString());
                    UpdateUI.debug("availableCoords: " + availableCoords.toString());
                    UpdateUI.debug("availableCoords Size: " + availableCoords.size());
                }

                UpdateUI.debug("5 - " + STOPWATCH.getRuntime(TimeUnit.MILLISECONDS));
            }
        } else {
            UpdateUI.debug(player.getName() + " -> Attempting to adjust spot coordinates but badCoordinate == null.");
        }
    }

    public void createSpotCoordList() {
        if(trapType != TrapType.SALAMANDER)
            spotCoordList = generateSpotsFromLayout();
        else {
            trapObjects = GameObjects.newQuery().within(trapArea).names(trapType.getName()).actions(trapType.getLayAction()).results().asList();
            if(trapObjects != null && trapObjects.size() > 0){
                trapObjects.forEach(i -> {
                    Coordinate tempCoord = i.getPosition();
                    if(tempCoord != null && !spotCoordList.contains(tempCoord) && spotCoordList.size() < maxNumOfTraps)
                        spotCoordList.add(tempCoord);
                });
            }
        }

        if (spotCoordList.size() > 0) {
            createSpotCoordListBoolean = false;
        }
    }

    public ArrayList<Coordinate> generateSpotsFromLayout() {
        ArrayList<Coordinate> list = new ArrayList<>();

        if (trapCenterCoord != null) {
            int currentLevel = Skill.HUNTER.getBaseLevel();

            // 1 Trap
            if (currentLevel < 20 || maxNumOfTraps == 1) {
                list.add(trapCenterCoord);
            }
            // 2 Traps
            else if (currentLevel < 40 || maxNumOfTraps == 2) {
                if(trapType != TrapType.SALAMANDER) {
                    if (layStyle == LayStyle.BOX) {
                        list.add(new Coordinate(trapCenterCoord.getX() + 1, trapCenterCoord.getY(), trapCenterCoord.getPlane()));
                        list.add(new Coordinate(trapCenterCoord.getX() - 1, trapCenterCoord.getY(), trapCenterCoord.getPlane()));
                    } else if (layStyle == LayStyle.LINE_NS) {
                        list.add(new Coordinate(trapCenterCoord.getX(), trapCenterCoord.getY() + 1, trapCenterCoord.getPlane()));
                        list.add(trapCenterCoord);
                    } else if (layStyle == LayStyle.LINE_EW) {
                        list.add(new Coordinate(trapCenterCoord.getX() + 1, trapCenterCoord.getY(), trapCenterCoord.getPlane()));
                        list.add(trapCenterCoord);
                    } else if (layStyle == LayStyle.PLUS_SIGN) {
                        list.add(new Coordinate(trapCenterCoord.getX() + 1, trapCenterCoord.getY(), trapCenterCoord.getPlane()));
                        list.add(trapCenterCoord);
                    } else if (layStyle == LayStyle.PENTAGON) {
                        list.add(new Coordinate(trapCenterCoord.getX() + 1, trapCenterCoord.getY(), trapCenterCoord.getPlane()));
                        list.add(new Coordinate(trapCenterCoord.getX() - 1, trapCenterCoord.getY(), trapCenterCoord.getPlane()));
                    }
                } else {

                }
            }
            // 3 Traps
            else if (currentLevel < 60 || maxNumOfTraps == 3) {
                if (layStyle == LayStyle.BOX) {
                    list.add(new Coordinate(trapCenterCoord.getX() + 1, trapCenterCoord.getY() + 1, trapCenterCoord.getPlane()));
                    list.add(trapCenterCoord);
                    list.add(new Coordinate(trapCenterCoord.getX() - 1, trapCenterCoord.getY() + 1, trapCenterCoord.getPlane()));
                } else if (layStyle == LayStyle.LINE_NS) {
                    list.add(new Coordinate(trapCenterCoord.getX(), trapCenterCoord.getY() + 1, trapCenterCoord.getPlane()));
                    list.add(trapCenterCoord);
                    list.add(new Coordinate(trapCenterCoord.getX(), trapCenterCoord.getY() - 1, trapCenterCoord.getPlane()));
                } else if (layStyle == LayStyle.LINE_EW) {
                    list.add(new Coordinate(trapCenterCoord.getX() + 1, trapCenterCoord.getY(), trapCenterCoord.getPlane()));
                    list.add(trapCenterCoord);
                    list.add(new Coordinate(trapCenterCoord.getX() - 1, trapCenterCoord.getY(), trapCenterCoord.getPlane()));
                } else if (layStyle == LayStyle.PLUS_SIGN) {
                    list.add(new Coordinate(trapCenterCoord.getX() + 1, trapCenterCoord.getY(), trapCenterCoord.getPlane()));
                    list.add(trapCenterCoord);
                    list.add(new Coordinate(trapCenterCoord.getX() - 1, trapCenterCoord.getY(), trapCenterCoord.getPlane()));
                } else if (layStyle == LayStyle.PENTAGON) {
                    list.add(new Coordinate(trapCenterCoord.getX() + 1, trapCenterCoord.getY() + 1, trapCenterCoord.getPlane()));
                    list.add(new Coordinate(trapCenterCoord.getX(), trapCenterCoord.getY() - 1, trapCenterCoord.getPlane()));
                    list.add(new Coordinate(trapCenterCoord.getX() - 1, trapCenterCoord.getY() + 1, trapCenterCoord.getPlane()));
                }
            }
            // 4 Traps
            else if (currentLevel < 80 || maxNumOfTraps == 4) {
                if (layStyle == LayStyle.BOX) {
                    list.add(new Coordinate(trapCenterCoord.getX() + 1, trapCenterCoord.getY() + 1, trapCenterCoord.getPlane()));
                    list.add(new Coordinate(trapCenterCoord.getX() - 1, trapCenterCoord.getY() + 1, trapCenterCoord.getPlane()));
                    list.add(new Coordinate(trapCenterCoord.getX() + 1, trapCenterCoord.getY() - 1, trapCenterCoord.getPlane()));
                    list.add(new Coordinate(trapCenterCoord.getX() - 1, trapCenterCoord.getY() - 1, trapCenterCoord.getPlane()));
                } else if (layStyle == LayStyle.LINE_NS) {
                    list.add(new Coordinate(trapCenterCoord.getX(), trapCenterCoord.getY() + 2, trapCenterCoord.getPlane()));
                    list.add(new Coordinate(trapCenterCoord.getX(), trapCenterCoord.getY() + 1, trapCenterCoord.getPlane()));
                    list.add(trapCenterCoord);
                    list.add(new Coordinate(trapCenterCoord.getX(), trapCenterCoord.getY() - 1, trapCenterCoord.getPlane()));
                } else if (layStyle == LayStyle.LINE_EW) {
                    list.add(new Coordinate(trapCenterCoord.getX() + 2, trapCenterCoord.getY(), trapCenterCoord.getPlane()));
                    list.add(new Coordinate(trapCenterCoord.getX() + 1, trapCenterCoord.getY(), trapCenterCoord.getPlane()));
                    list.add(trapCenterCoord);
                    list.add(new Coordinate(trapCenterCoord.getX() - 1, trapCenterCoord.getY(), trapCenterCoord.getPlane()));
                } else if (layStyle == LayStyle.PLUS_SIGN) {
                    list.add(new Coordinate(trapCenterCoord.getX() + 1, trapCenterCoord.getY(), trapCenterCoord.getPlane()));
                    list.add(new Coordinate(trapCenterCoord.getX() - 1, trapCenterCoord.getY(), trapCenterCoord.getPlane()));
                    list.add(new Coordinate(trapCenterCoord.getX(), trapCenterCoord.getY() + 1, trapCenterCoord.getPlane()));
                    list.add(new Coordinate(trapCenterCoord.getX(), trapCenterCoord.getY() - 1, trapCenterCoord.getPlane()));
                } else if (layStyle == LayStyle.PENTAGON) {
                    list.add(new Coordinate(trapCenterCoord.getX() + 2, trapCenterCoord.getY() + 1, trapCenterCoord.getPlane()));
                    list.add(new Coordinate(trapCenterCoord.getX() - 2, trapCenterCoord.getY() + 1, trapCenterCoord.getPlane()));
                    list.add(new Coordinate(trapCenterCoord.getX() + 1, trapCenterCoord.getY() - 1, trapCenterCoord.getPlane()));
                    list.add(new Coordinate(trapCenterCoord.getX() - 1, trapCenterCoord.getY() - 1, trapCenterCoord.getPlane()));
                }
            }
            // 5 Traps
            else {
                if (layStyle == LayStyle.BOX || maxNumOfTraps == 5) {
                    list.add(new Coordinate(trapCenterCoord.getX() + 1, trapCenterCoord.getY() + 1, trapCenterCoord.getPlane()));
                    list.add(new Coordinate(trapCenterCoord.getX() - 1, trapCenterCoord.getY() + 1, trapCenterCoord.getPlane()));
                    list.add(trapCenterCoord);
                    list.add(new Coordinate(trapCenterCoord.getX() + 1, trapCenterCoord.getY() - 1, trapCenterCoord.getPlane()));
                    list.add(new Coordinate(trapCenterCoord.getX() - 1, trapCenterCoord.getY() - 1, trapCenterCoord.getPlane()));
                } else if (layStyle == LayStyle.LINE_NS) {
                    list.add(new Coordinate(trapCenterCoord.getX(), trapCenterCoord.getY() + 2, trapCenterCoord.getPlane()));
                    list.add(new Coordinate(trapCenterCoord.getX(), trapCenterCoord.getY() + 1, trapCenterCoord.getPlane()));
                    list.add(trapCenterCoord);
                    list.add(new Coordinate(trapCenterCoord.getX(), trapCenterCoord.getY() - 1, trapCenterCoord.getPlane()));
                    list.add(new Coordinate(trapCenterCoord.getX(), trapCenterCoord.getY() - 2, trapCenterCoord.getPlane()));
                } else if (layStyle == LayStyle.LINE_EW) {
                    list.add(new Coordinate(trapCenterCoord.getX() + 2, trapCenterCoord.getY(), trapCenterCoord.getPlane()));
                    list.add(new Coordinate(trapCenterCoord.getX() + 1, trapCenterCoord.getY(), trapCenterCoord.getPlane()));
                    list.add(trapCenterCoord);
                    list.add(new Coordinate(trapCenterCoord.getX() - 1, trapCenterCoord.getY(), trapCenterCoord.getPlane()));
                    list.add(new Coordinate(trapCenterCoord.getX() - 2, trapCenterCoord.getY(), trapCenterCoord.getPlane()));
                } else if (layStyle == LayStyle.PLUS_SIGN) {
                    list.add(new Coordinate(trapCenterCoord.getX() + 1, trapCenterCoord.getY(), trapCenterCoord.getPlane()));
                    list.add(trapCenterCoord);
                    list.add(new Coordinate(trapCenterCoord.getX() - 1, trapCenterCoord.getY(), trapCenterCoord.getPlane()));
                    list.add(new Coordinate(trapCenterCoord.getX(), trapCenterCoord.getY() + 1, trapCenterCoord.getPlane()));
                    list.add(new Coordinate(trapCenterCoord.getX(), trapCenterCoord.getY() - 1, trapCenterCoord.getPlane()));
                } else if (layStyle == LayStyle.PENTAGON) {
                    list.add(new Coordinate(trapCenterCoord.getX() + 2, trapCenterCoord.getY(), trapCenterCoord.getPlane()));
                    list.add(new Coordinate(trapCenterCoord.getX(), trapCenterCoord.getY() + 2, trapCenterCoord.getPlane()));
                    list.add(new Coordinate(trapCenterCoord.getX() - 2, trapCenterCoord.getY(), trapCenterCoord.getPlane()));
                    list.add(new Coordinate(trapCenterCoord.getX() + 1, trapCenterCoord.getY() - 2, trapCenterCoord.getPlane()));
                    list.add(new Coordinate(trapCenterCoord.getX() - 1, trapCenterCoord.getY() - 2, trapCenterCoord.getPlane()));
                }
            }
        }
        return list;
    }
}