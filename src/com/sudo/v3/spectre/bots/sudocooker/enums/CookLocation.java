package com.sudo.v3.spectre.bots.sudocooker.enums;

public enum CookLocation {
    GENERAL,
    HOSIDIUS_HOUSE,
    COOKING_GUILD,
    ROGUES_DEN
}
