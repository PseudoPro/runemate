package com.sudo.v3.spectre.bots.sudocooker;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.definitions.ItemDefinition;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.InterfaceComponent;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.util.calculations.CommonMath;
import com.runemate.game.api.script.framework.listeners.ChatboxListener;
import com.runemate.game.api.script.framework.listeners.events.ItemEvent;
import com.runemate.game.api.script.framework.listeners.events.MessageEvent;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBot;
import com.sudo.v3.spectre.bots.sudocooker.branches.*;
import com.sudo.v3.spectre.bots.sudocooker.branches.wine.HasJugsAndGrape;
import com.sudo.v3.spectre.bots.sudocooker.branches.wine.HasWine;
import com.sudo.v3.spectre.bots.sudocooker.enums.BankingType;
import com.sudo.v3.spectre.bots.sudocooker.enums.CookLocation;
import com.sudo.v3.spectre.bots.sudocooker.enums.CookType;
import com.sudo.v3.spectre.bots.sudocooker.enums.TraversalLocation;
import com.sudo.v3.spectre.bots.sudocooker.leafs.*;
import com.sudo.v3.spectre.bots.sudocooker.leafs.location.cookguild.CookGuildLobbyFailSafeLeaf;
import com.sudo.v3.spectre.bots.sudocooker.leafs.wine.AndLetThereBeWineLeaf;
import com.sudo.v3.spectre.bots.sudocooker.tasks.CookTask;
import com.sudo.v3.spectre.bots.sudocooker.ui.CookerFXGui;
import com.sudo.v3.spectre.bots.sudocooker.ui.CookerInfoUI;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.sudo.v3.ui.Info;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * ApexCooker Bot for RuneMate spectre
 */
public class ApexCooker extends SudoBot implements ChatboxListener
{
    private CookerFXGui configUI;
    private CookerInfoUI infoUI;

    public CookTask cookTask;
    public CookType cookType = CookType.GENERIC;
    public BankingType bankingType = BankingType.leaving;
    public TraversalLocation traversalLocation = TraversalLocation.bank;
    public CookLocation cookLocation = CookLocation.GENERAL;
    public String cookLocationString;

    private int cookCount = 0;

    public boolean usingPreset = false, usingBonfire = false;
    public int presetNumber;
    public String rangeName = "Range";
    public GameObject range;

    public Area bankArea, cookArea;

    public InterfaceComponent cookingComp;

    // Branches
    public HasJugsAndGrape hasJugsAndGrape = new HasJugsAndGrape(this);
    public HasWine hasWine = new HasWine(this);
    public HasRaw hasRaw = new HasRaw(this);
    public IsBankNearby isBankNearby = new IsBankNearby(this);
    public IsBankOpen isBankOpen = new IsBankOpen(this);
    public IsCooking isCooking = new IsCooking(this);
    public IsInventoryEmpty isInventoryEmpty = new IsInventoryEmpty(this);
    public Root root = new Root(this);
    public IsRangeNearby isRangeNearby = new IsRangeNearby(this);

    // Leafs
    public AndLetThereBeWineLeaf andLetThereBeWineLeaf = new AndLetThereBeWineLeaf(this);
    public DepositAllLeaf depositAllLeaf = new DepositAllLeaf(this);
    public IsCookingLeaf isCookingLeaf = new IsCookingLeaf(this);
    public OSRSRangeInteractLeaf osrsRangeInteractLeaf = new OSRSRangeInteractLeaf(this);
    public RS3RangeInteractLeaf rs3RangeInteractLeaf = new RS3RangeInteractLeaf(this);
    public StartCookingLeaf startCookingLeaf = new StartCookingLeaf(this);
    public TogglePresetLeaf togglePresetLeaf = new TogglePresetLeaf(this);
    public TraversalLeaf traversalLeaf = new TraversalLeaf(this);
    public WithdrawCookableLeaf withdrawCookableLeaf = new WithdrawCookableLeaf(this);
    public CookGuildLobbyFailSafeLeaf cookGuildLobbyFailSafeLeaf = new CookGuildLobbyFailSafeLeaf(this);

    @Override
    public ObjectProperty<Node> botInterfaceProperty()
    {
        if (botInterfaceProperty == null)
        {
            botInterfaceProperty = new SimpleObjectProperty<>(configUI = new CookerFXGui(this));
            guiList = new ArrayList<Node>();
            //guiList.add(_sbGUI = new SudoBuddyFXGui(this));
            guiList.add(configUI);
            guiList.add(infoUI = new CookerInfoUI(this));
        }
        return botInterfaceProperty;
    }

    public ApexCooker()
    {
        super();
        setEmbeddableUI(this);
    }

    @Override
    public TreeTask createRootTask()
    {
        return root;
    }

    @Override
    public void onStart(String... args)
    {
        super.onStart();

        getEventDispatcher().addListener(this);

        byte someVariable = 0;
        //UpdateUI.debug("onStart: Message for Mr. Party. '4 + 20 = " + partysInt + "'");
    }

    @Override
    public String getBotName() {
        return "ApexCooker";
    }

    @Override
    public void onItemAdded(ItemEvent event)
    {
        if(!Bank.isOpen())
            super.onItemAdded(event);

        ItemDefinition definition = event.getItem().getDefinition();
        if (definition != null)
        {
            try
            {
                if (cookTask.cookable.toLowerCase().replace("raw ", "").equals(definition.getName().toLowerCase()) && !(definition.getName().toLowerCase().contains("raw") || definition.getName().toLowerCase().contains("burnt"))
                        || (cookTask.cookable.toLowerCase().equals("Jug of wine") && definition.getName().toLowerCase().equals("Unfermented wine")))
                {
                    cookTask.currentCooked++;
                    cookCount++;
                }
            }catch(Exception e)
            {
                UpdateUI.debug("Something was added to inventory before bot GUI was finished.");
            }
        }
    }

    @Override
    public void onItemRemoved(ItemEvent event)
    {
        if(!Bank.isOpen())
            super.onItemRemoved(event);
    }

    @Override
    public void onMessageReceived(MessageEvent event)
    {
        String message = event.getMessage();

        if (cookTask != null && cookTask.cookable != null)
        {
            if (message.contains("Item could not be found: " + cookTask.cookable))
            {
                UpdateUI.currentTask("Preset could not find " + cookTask.cookable + ", stopping bot.", this);
                stop();
            }
        }
    }

    @Override
    public void updateInfo()
    {
        // UpdateUI all current values in the XPInfo List
        XPInfoMap.values().forEach((xpInfo) -> xpInfo.update());

        if (cookTask != null)
            if (cookTask.cookAmount == 0)
                displayInfoMap.put("Current Cook Task: ", cookTask.cookable + " (" + cookTask.currentCooked + "/All)");
            else
                displayInfoMap.put("Current Cook Task: ", cookTask.cookable + " (" + cookTask.currentCooked + "/" + cookTask.cookAmount + ")");

        displayInfoMap.put("Cook Count: ", Integer.toString(cookCount) +
                " (" + Integer.toString((int) CommonMath.rate(TimeUnit.HOURS, STOPWATCH.getRuntime(), cookCount)) + "/Hour)");
        displayInfoMap.put("GP: ", Integer.toString(grossIncome) +
                " (" + Integer.toString((int) CommonMath.rate(TimeUnit.HOURS, STOPWATCH.getRuntime(), grossIncome)) + "/Hour)");

        try
        {
            info = new Info(XPInfoMap,           // XP Info
                    displayInfoMap,
                    STOPWATCH.getRuntimeAsString(),        // Total Runtime
                    currentTaskString,                     // Current Task
                    abTaskString);                         // antiban Task

        } catch (Exception e)
        {
            e.printStackTrace();
        }

        Platform.runLater(() -> infoUI.update());
    }
}