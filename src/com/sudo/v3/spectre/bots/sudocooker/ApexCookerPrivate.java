package com.sudo.v3.spectre.bots.sudocooker;


/**
 * ApexCooker Bot for RuneMate spectre
 */
public class ApexCookerPrivate extends ApexCooker
{
    @Override
    public boolean isPrivate(){
        return true;
    }
}