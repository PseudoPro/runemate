package com.sudo.v3.spectre.bots.sudocooker.tasks;

import com.sudo.v3.base.SudoTask;

/**
 * Created by SudoPro on 12/16/2016.
 */
public class CookTask extends SudoTask
{
    public int cookAmount = 0, currentCooked = 0;
    public String cookable;

    public CookTask(String cookable, int cookAmount)
    {
        this.cookable = cookable;
        this.cookAmount = cookAmount;
    }

    @Override
    public boolean isValid()
    {
        return cookAmount == 0 || currentCooked < cookAmount;
    }
}
