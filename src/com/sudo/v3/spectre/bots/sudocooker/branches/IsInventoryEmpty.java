package com.sudo.v3.spectre.bots.sudocooker.branches;

import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudocooker.ApexCooker;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 12/18/2016.
 */
public class IsInventoryEmpty extends SudoBranchTask
{
    private ApexCooker bot;

    public IsInventoryEmpty(ApexCooker bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask()
    {
        return bot.withdrawCookableLeaf;
    }

    @Override
    public boolean validate()
    {
        validate = Inventory.isEmpty();
        UpdateUI.debug(bot.player.getName() + " -> Branch IsInventoryEmpty: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask()
    {
        return bot.depositAllLeaf;
    }
}
