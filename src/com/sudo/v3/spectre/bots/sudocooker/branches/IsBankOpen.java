package com.sudo.v3.spectre.bots.sudocooker.branches;

import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudocooker.ApexCooker;
import com.sudo.v3.spectre.bots.sudocooker.enums.BankingType;
import com.sudo.v3.spectre.bots.sudocooker.enums.CookLocation;
import com.sudo.v3.spectre.bots.sudocooker.enums.CookType;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 12/17/2016.
 */
public class IsBankOpen extends SudoBranchTask {
    private ApexCooker bot;

    public IsBankOpen(ApexCooker bot) {
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        if (bot.bankingType == BankingType.interaction) {
            UpdateUI.debug(bot.player.getName() + " -> Branch UsingPreset: " + bot.usingPreset);
            if (bot.usingPreset)
                return bot.togglePresetLeaf;
            else
                return bot.isInventoryEmpty;
        } else if (bot.bankingType == BankingType.leaving) {
            return bot.closeBankLeaf;
        }

        return bot.emptyLeaf;
    }

    @Override
    public boolean validate() {
        validate = Bank.isOpen();
        UpdateUI.debug(bot.player.getName() + " -> Branch IsBankOpen(" + bot.bankingType + "): " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        if (bot.cookType == CookType.WINE) {
            if(bot.cookLocation == CookLocation.HOSIDIUS_HOUSE) {
                bot.openBankLeaf.setGenericBank(true);
                return bot.openBankLeaf;
            }
        }

        if (bot.bankingType == BankingType.interaction)
            return bot.isBankNearby;
        else if (bot.bankingType == BankingType.leaving)
            return bot.isCooking;

        return bot.emptyLeaf;
    }
}