package com.sudo.v3.spectre.bots.sudocooker.branches;

import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.RuneScape;
import com.runemate.game.api.hybrid.local.Skill;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoRootTask;
import com.sudo.v3.base.SudoTask;
import com.sudo.v3.spectre.bots.sudocooker.ApexCooker;
import com.sudo.v3.spectre.bots.sudocooker.enums.CookLocation;
import com.sudo.v3.spectre.bots.sudocooker.enums.CookType;
import com.sudo.v3.spectre.bots.sudocooker.tasks.CookTask;
import com.sudo.v3.spectre.statics.Areas;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.sudo.v3.ui.model.XPInfo;

/**
 * Created by SudoPro on 11/23/2016.
 */

// Will wait for 2 minutes. If the the bot is never started from the GUI, we will exit
public class Root extends SudoRootTask {
    private ApexCooker bot;

    public Root(ApexCooker bot) {
        super(bot);
        this.bot = bot;
    }

    @Override
    public TreeTask rootTask() {
        if (bot.firstLogin) {
            if (RuneScape.isLoggedIn()) {
                // Add Skills to track to the HashMap
                bot.XPInfoMap.put(Skill.COOKING, new XPInfo(Skill.COOKING));

                bot.firstLogin = false;

                UpdateUI.debug("Cooking Level: " + Skill.COOKING.getCurrentLevel());
                UpdateUI.debug("Location Name: " + bot.cookLocationString);
                UpdateUI.debug("Location Type: " + bot.cookLocation.name());
                UpdateUI.debug("Task Queue:");
                for(SudoTask task : bot.taskHandler.getTasks()){
                    CookTask cookTask = (CookTask)task;
                    UpdateUI.debug("    " + cookTask.cookable + " x " + cookTask.cookAmount);
                }
                UpdateUI.debug("Using Bonfire: " + bot.usingBonfire);
                UpdateUI.debug("UsingPreset: " + bot.usingPreset);
                if(bot.usingPreset)
                    UpdateUI.debug("Preset #: " + bot.presetNumber);

                bot.updateSession(0.0);
            }
        }

        bot.cookTask = (CookTask) bot.taskHandler.getCurrentTask();

        if (bot.getMetaData().getHourlyPrice().doubleValue() > 0 || bot.isValidSession() || Environment.isSDK()) {
            if ((bot.cookArea == null || bot.bankArea == null) && (bot.usingBonfire || bot.cookType == CookType.GENERIC)) {
                bot.bankArea = new Area.Circular(bot.player.getPosition(), 20);
                bot.cookArea = bot.bankArea;
            }

            if (bot.cookTask == null) {
                UpdateUI.currentTask("All tasks completed. Stopping Bot.", bot);
                bot.stop("All tasks completed. Stopping Bot.");
            } else {
                bot.cookType = bot.cookTask.cookable.equals("Jug of wine") ? CookType.WINE : CookType.GENERIC;

                if (!bot.cookTask.isValid()) {
                    bot.taskHandler.getCurrentTask().deactivate();
                }
            }

            if (bot.currentlyBreaking) {
                if (RuneScape.isLoggedIn()) {
                    UpdateUI.currentTask("Attempting to log off.", bot);
                    RuneScape.logout(true);
                }
                return bot.emptyLeaf;
            } else if (bot.cookTask != null) {
                if (bot.cookLocation == CookLocation.COOKING_GUILD && Areas.COOK_GUILD_LOBBY_AREA.contains(bot.player) && !Areas.COOK_GUILD_BANK_AREA.contains(bot.player) && !Areas.COOK_GUILD_RANGE_AREA.contains(bot.player))
                    return bot.cookGuildLobbyFailSafeLeaf;
                else if (bot.cookType == CookType.GENERIC)
                    return bot.hasRaw;
                else // Making wine
                    return bot.hasWine;
            }
            else
                return bot.emptyLeaf;
        } else {
            return bot.liteVersionExpireLeaf;
        }
    }
}