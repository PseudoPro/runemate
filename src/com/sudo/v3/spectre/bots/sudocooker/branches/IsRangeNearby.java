package com.sudo.v3.spectre.bots.sudocooker.branches;

import com.runemate.game.api.hybrid.local.hud.interfaces.Interfaces;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudocooker.ApexCooker;
import com.sudo.v3.spectre.bots.sudocooker.enums.TraversalLocation;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Proxify on 8/19/2017.
 */
public class IsRangeNearby extends SudoBranchTask {
    private ApexCooker bot;

    public IsRangeNearby(ApexCooker bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        if (bot.isRS3) {
            bot.cookingComp = Interfaces.newQuery().containers(1370).texts("Cook").results().first();
            validate = bot.cookingComp != null && bot.cookingComp.isVisible() && bot.cookingComp.isValid();
            UpdateUI.debug(bot.player + " -> IsRangeNearby-CookingCompNull: " + validate);
            if (validate)
                return bot.startCookingLeaf;
            else
                return bot.rs3RangeInteractLeaf;
        } else
            return bot.osrsRangeInteractLeaf;
    }

    @Override
    public boolean validate() {
        validate = bot.player != null && bot.range != null && bot.player.distanceTo(bot.range) < 8;
        UpdateUI.debug(bot.player.getName() + " -> Branch IsRangeNearby: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        bot.traversalLocation = TraversalLocation.cookArea;
        return bot.traversalLeaf;
    }
}
