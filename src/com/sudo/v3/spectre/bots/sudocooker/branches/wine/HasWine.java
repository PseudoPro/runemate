package com.sudo.v3.spectre.bots.sudocooker.branches.wine;

import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.spectre.bots.sudocooker.ApexCooker;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Proxify on 6/9/2017.
 */
public class HasWine extends BranchTask {
    private ApexCooker bot;

    public HasWine(ApexCooker bot){
        this.bot = bot;
    }

    @Override
    public boolean validate() {
        UpdateUI.debug(bot.player.getName() + "-> Branch HasWine: " + Inventory.containsAnyOf("Jug of wine", "Unfermented wine"));
        return Inventory.containsAnyOf("Jug of wine", "Unfermented wine") && !Inventory.containsAnyOf("Grapes", "Jug of water");
        //UpdateUI.debug(bot.player.getName() + " -> Branch HasWine: " + (InventoryContainsOnly("Jug of wine") || InventoryContainsOnly("Unfermented wine")));
        //return InventoryContainsOnly("Jug of wine") || InventoryContainsOnly("Unfermented wine");
    }

    @Override
    public TreeTask successTask() {
        if(Bank.isOpen())
            return bot.depositAllLeaf;
        else
            return bot.openBankLeaf;
    }

    @Override
    public TreeTask failureTask() {
        return bot.hasJugsAndGrape;
    }

    private boolean InventoryContainsOnly(String name){
        return Inventory.newQuery().filter(i -> !i.getDefinition().getName().equals(name)).results().isEmpty() && !Inventory.newQuery().names(name).results().isEmpty();
    }
}
