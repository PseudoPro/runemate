package com.sudo.v3.spectre.bots.sudocooker.branches.wine;

import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.spectre.bots.sudocooker.ApexCooker;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Proxify on 6/9/2017.
 */
public class HasJugsAndGrape extends BranchTask {
    private ApexCooker bot;

    public HasJugsAndGrape(ApexCooker bot){
        this.bot = bot;
    }

    @Override
    public boolean validate() {
        UpdateUI.debug(bot.player.getName() + " -> Branch HasJugsAndGrape: " + Inventory.containsAllOf("Jug of water", "Grapes"));
        return Inventory.containsAllOf("Jug of water", "Grapes");
    }

    @Override
    public TreeTask successTask() {
        if(Bank.isOpen())
            return bot.closeBankLeaf;
        else
            return bot.isCooking;
    }

    @Override
    public TreeTask failureTask() {
        if(Bank.isOpen()) {
            if(Inventory.getQuantity("Jug of water") > 14 || Inventory.getQuantity("Grapes") > 14)
                return bot.depositAllLeaf;
            else
                return bot.withdrawCookableLeaf;
        }
        else
            return bot.openBankLeaf;
    }

}
