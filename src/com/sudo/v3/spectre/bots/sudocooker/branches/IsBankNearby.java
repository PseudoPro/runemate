package com.sudo.v3.spectre.bots.sudocooker.branches;

import com.runemate.game.api.hybrid.entities.LocatableEntity;
import com.runemate.game.api.hybrid.region.Banks;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudocooker.ApexCooker;
import com.sudo.v3.spectre.bots.sudocooker.enums.CookLocation;
import com.sudo.v3.spectre.bots.sudocooker.enums.TraversalLocation;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 12/23/2016.
 */
public class IsBankNearby extends SudoBranchTask
{
    private ApexCooker bot;
    private LocatableEntity bank;

    public IsBankNearby(ApexCooker bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        if (bot.cookLocation == CookLocation.HOSIDIUS_HOUSE)
            bot.openBankLeaf.setGenericBank(true);

        return bot.openBankLeaf;
    }

    @Override
    public boolean validate()
    {
        bank = Banks.newQuery().results().nearest();
        validate = bot.bankArea.contains(bot.player) && bank != null;
        UpdateUI.debug(bot.player.getName() + " -> Branch IsBankNearby: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask()
    {
        bot.traversalLocation = TraversalLocation.bank;
        return bot.traversalLeaf;
    }
}
