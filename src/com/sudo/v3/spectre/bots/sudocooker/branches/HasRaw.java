package com.sudo.v3.spectre.bots.sudocooker.branches;

import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudocooker.ApexCooker;
import com.sudo.v3.spectre.bots.sudocooker.enums.BankingType;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 12/17/2016.
 */
public class HasRaw extends SudoBranchTask
{
    private ApexCooker bot;

    public HasRaw(ApexCooker bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask()
    {
        bot.bankingType = BankingType.leaving;
        return bot.isBankOpen;
    }

    @Override
    public boolean validate()
    {
        validate = bot.cookTask != null && Inventory.contains(bot.cookTask.cookable);
        UpdateUI.debug(bot.player.getName() + " -> Branch HasRaw: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask()
    {
        SpriteItem item;
        if((item = Inventory.getSelectedItem()) != null)
            item.interact("Cancel");

        bot.bankingType = BankingType.interaction;
        return bot.isBankOpen;
    }
}
