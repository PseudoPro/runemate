package com.sudo.v3.spectre.bots.sudocooker.branches;

import com.runemate.game.api.hybrid.local.hud.interfaces.ChatDialog;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.osrs.local.hud.interfaces.MakeAllInterface;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudocooker.ApexCooker;
import com.sudo.v3.spectre.bots.sudocooker.enums.CookType;
import com.sudo.v3.spectre.statics.Animations;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 12/23/2016.
 */
public class IsCooking extends SudoBranchTask {
    private ApexCooker bot;

    public IsCooking(ApexCooker bot) {
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        return bot.isCookingLeaf;
    }

    @Override
    public boolean validate() {
        if (bot.cookType != CookType.WINE) {
            if (!bot.usingBonfire)
                if (bot.isRS3)
                    bot.range = GameObjects.newQuery().actions("Cook-at", "Cook at").results().nearest();
                else
                    bot.range = GameObjects.newQuery().names(bot.rangeName).results().nearest();
            else
                bot.range = GameObjects.newQuery().names("Fire").results().nearest();
        }

        if ((bot.cookType != CookType.WINE && bot.player.distanceTo(bot.range) > 3) || Inventory.getSelectedItem() != null || MakeAllInterface.isOpen()) {
            UpdateUI.debug(bot.player.getName() + " -> Branch IsCooking: false");
            return false;

        } else {
            validate = Execution.delayUntil(() -> (bot.player = Players.getLocal()) != null && Animations.COOK_ANIM.contains(bot.player.getAnimationId()) && ChatDialog.getContinue() == null, bot.cookType == CookType.WINE ? 1500 : 2500);
            UpdateUI.debug(bot.player.getName() + " -> Branch IsCooking: " + validate);
            return validate;
        }
    }

    @Override
    public TreeTask failureTask() {
        if (bot.cookType == CookType.WINE)
            return bot.andLetThereBeWineLeaf;
        else
            return bot.isRangeNearby;
    }
}