package com.sudo.v3.spectre.bots.sudocooker.leafs;

import com.runemate.game.api.hybrid.entities.LocatableEntity;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.queries.results.SpriteItemQueryResults;
import com.runemate.game.api.hybrid.region.Banks;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudocooker.ApexCooker;
import com.sudo.v3.spectre.api.game.hud.SudoCamera;
import com.sudo.v3.spectre.common.playersense.ApexPlayerSense;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 12/23/2016.
 */
public class IsCookingLeaf extends LeafTask
{
    private ApexCooker bot;

    public IsCookingLeaf(ApexCooker bot){
        this.bot = bot;
    }

    @Override
    public void execute()
    {
        SpriteItemQueryResults items = Inventory.getItems();
        if(items != null && items.size() < ApexPlayerSense.Key.HOVER_OVER_DEPOSIT_LOW_INVENTORY_COUNT.getAsInteger()){
            LocatableEntity locatableBank = Banks.newQuery().within(bot.bankArea).results().nearest();

            if(locatableBank != null){
                UpdateUI.currentTask("Inventory nearly cooked, attempting to hover over deposit area...", bot);
                if(locatableBank.getVisibility() < 0.8)
                    SudoCamera.ConcurrentlyTurnToWithYaw(locatableBank);
                else
                    locatableBank.hover();
            }
        }else {
            UpdateUI.currentTask("Currently cooking", bot);
        }
    }
}
