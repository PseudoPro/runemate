package com.sudo.v3.spectre.bots.sudocooker.leafs;

import com.runemate.game.api.hybrid.local.hud.interfaces.InterfaceComponent;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudocooker.ApexCooker;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 12/23/2016.
 */
public class StartCookingLeaf extends LeafTask
{
    private ApexCooker bot;

    public StartCookingLeaf(ApexCooker bot){
        this.bot = bot;
    }

    @Override
    public void execute()
    {
        if(bot.cookingComp != null) {
            UpdateUI.currentTask("Starting to cook", bot);
            bot.cookingComp.click();
        }
    }
}
