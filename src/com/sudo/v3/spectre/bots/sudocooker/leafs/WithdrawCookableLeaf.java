package com.sudo.v3.spectre.bots.sudocooker.leafs;

import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudocooker.ApexCooker;
import com.sudo.v3.spectre.bots.sudocooker.enums.CookType;
import com.sudo.v3.spectre.bots.sudocooker.tasks.CookTask;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 12/18/2016.
 */
public class WithdrawCookableLeaf extends LeafTask {
    private ApexCooker bot;
    private int bankCheckAttempt = 0;

    public WithdrawCookableLeaf(ApexCooker bot) {
        this.bot = bot;
    }

    @Override
    public void execute() {
        if (bot.cookType == CookType.GENERIC) {
            if (Bank.contains(bot.cookTask.cookable)) {
                bankCheckAttempt = 0;
                UpdateUI.currentTask("Withdrawing " + bot.cookTask.cookable, bot);
                if (bot.cookTask.cookAmount != 0 && (bot.cookTask.cookAmount - bot.cookTask.currentCooked) < 28)
                    Bank.withdraw(bot.cookTask.cookable, bot.cookTask.cookAmount - bot.cookTask.currentCooked);
                else
                    Bank.withdraw(bot.cookTask.cookable, 0);
            } else {
                // Included delay due to the bank sometimes returning that it does not contain the item on the first query
                if(bankCheckAttempt < 3) {
                    bankCheckAttempt++;
                    Execution.delay(250, 500);
                    this.execute();
                } else {
                    UpdateUI.currentTask("Out of " + bot.cookTask.cookable + ", changing task...", bot);
                    bot.taskHandler.getCurrentTask().deactivate();
                    bot.cookTask = (CookTask) bot.taskHandler.getCurrentTask();
                }
            }
        } else if (bot.cookType == CookType.WINE) {
            String jugOfWater = "Jug of water";
            String grapes = "Grapes";

            if (!Inventory.contains(jugOfWater)) {
                if (Bank.contains(jugOfWater)) {
                    UpdateUI.currentTask("Withdrawing " + jugOfWater, bot);
                    if (bot.cookTask.cookAmount != 0 && (bot.cookTask.cookAmount - bot.cookTask.currentCooked) < 14)
                        Bank.withdraw(jugOfWater, bot.cookTask.cookAmount - bot.cookTask.currentCooked);
                    else
                        Bank.withdraw(jugOfWater, 14);
                } else {
                    UpdateUI.currentTask("Out of " + jugOfWater + ", changing task...", bot);
                    bot.taskHandler.getCurrentTask().deactivate();
                    bot.cookTask = (CookTask) bot.taskHandler.getCurrentTask();
                }
            } else if (!Inventory.contains(grapes)) {
                if (Bank.contains(grapes)) {
                    UpdateUI.currentTask("Withdrawing " + grapes, bot);
                    if (bot.cookTask.cookAmount != 0 && (bot.cookTask.cookAmount - bot.cookTask.currentCooked) < 14)
                        Bank.withdraw(grapes, bot.cookTask.cookAmount - bot.cookTask.currentCooked);
                    else
                        Bank.withdraw(grapes, 14);
                } else {
                    UpdateUI.currentTask("Out of " + grapes + ", changing task...", bot);
                    bot.taskHandler.getCurrentTask().deactivate();
                    bot.cookTask = (CookTask) bot.taskHandler.getCurrentTask();
                }
            }
        }
    }
}