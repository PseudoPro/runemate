package com.sudo.v3.spectre.bots.sudocooker.leafs.wine;

import com.runemate.game.api.hybrid.input.Mouse;
import com.runemate.game.api.hybrid.local.hud.Menu;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.osrs.local.hud.interfaces.MakeAllInterface;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudocooker.ApexCooker;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Proxify on 6/9/2017.
 */
public class AndLetThereBeWineLeaf extends LeafTask {
    private ApexCooker bot;
    private SpriteItem selectedItem;

    public AndLetThereBeWineLeaf(ApexCooker bot){
        this.bot = bot;
    }

    @Override
    public void execute() {

        if(Bank.isOpen()){
            UpdateUI.currentTask("Closing Bank", bot);
            Bank.close();
        }else {
            UpdateUI.currentTask("Attempting to create wine", bot);

            if (MakeAllInterface.isOpen()) {
                if(MakeAllInterface.selectItem("Unfermented wine"))
                    Execution.delay(500, 1000);
            } else if ((selectedItem = Inventory.getSelectedItem()) == null) {
                SpriteItem item1 = Inventory.newQuery().names("Jug of water").results().last();
                if (item1 != null)
                    item1.interact("Use");
            } else {
                SpriteItem item2 = Inventory.newQuery().names("Grapes").results().first();

                if (item2 != null) {
                   if (item2.interact("Use", selectedItem.getDefinition().getName() + " -> " + item2.getDefinition().getName()))
                       Execution.delay(750, 1500);
                }
            }

//            InterfaceComponent comp = Interfaces.newQuery().containers(309).textContains("Unfermented wine").results().first();
//
//            if (comp != null) {
//                try {
//                    if (Menu.isOpen()) {
//                        if (Menu.contains("Make All")) {
//                            if (Menu.getItem("Make All").click())
//                                Execution.delay(750, 1500);
//                        } else
//                            new Area.Circular(bot.player.getPosition(), 10).getRandomCoordinate().hover();
//                    }
//                    else {
//                        if (comp.hover())
//                            Execution.delayUntil(() -> comp.contains(Mouse.getPosition()), 500);
//                        Menu.open();
//                    }
//
//                } catch (NullPointerException e) {
//                    UpdateUI.debug("ChatDialog became null between null-check and selection");
//
//                }
//            } else if ((selectedItem = Inventory.getSelectedItem()) == null) {
//                SpriteItem item1 = Inventory.newQuery().names("Jug of water").results().last();
//                if (item1 != null)
//                    item1.interact("Use");
//            } else {
//                SpriteItem item2 = Inventory.newQuery().names("Grapes").results().first();
//
//                if (item2 != null) {
//                   if (item2.interact("Use", selectedItem.getDefinition().getName() + " -> " + item2.getDefinition().getName()))
//                       Execution.delay(750, 1500);
//                }
//            }
        }
    }
}
