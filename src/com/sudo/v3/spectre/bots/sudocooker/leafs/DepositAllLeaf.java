package com.sudo.v3.spectre.bots.sudocooker.leafs;

import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudocooker.ApexCooker;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 12/18/2016.
 */
public class DepositAllLeaf extends LeafTask
{
    private ApexCooker bot;

    public DepositAllLeaf(ApexCooker bot)
    {
        this.bot = bot;
    }

    @Override
    public void execute()
    {
        UpdateUI.currentTask("Depositing Inventory", bot);
        Bank.depositInventory();
    }
}