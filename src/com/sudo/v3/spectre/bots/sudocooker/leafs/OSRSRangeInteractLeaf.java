package com.sudo.v3.spectre.bots.sudocooker.leafs;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.osrs.local.hud.interfaces.MakeAllInterface;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudocooker.ApexCooker;
import com.sudo.v3.spectre.api.game.hud.SudoCamera;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 1/2/2017.
 */
public class OSRSRangeInteractLeaf extends LeafTask {
    private ApexCooker bot;
    private GameObject obj;

    public OSRSRangeInteractLeaf(ApexCooker bot) {
        this.bot = bot;
    }

    @Override
    public void execute() {
        if (bot.usingBonfire)
            obj = GameObjects.newQuery().names("Fire").results().nearest();
        else
            obj = GameObjects.newQuery().names(bot.rangeName).results().nearest();

        UpdateUI.currentTask("Attempting to interact with Range/Fire", bot);

        if (obj != null) {
            if (obj.distanceTo(bot.player) > 10 || obj.getVisibility() < 0.8)
                SudoCamera.ConcurrentlyTurnToWithYaw(obj);

            if (ChatDialog.getContinue() != null)
                ChatDialog.getContinue().select(true);

            if (MakeAllInterface.isOpen()) {
                if (bot.cookTask.cookable.equals("Raw karambwan"))
                    MakeAllInterface.selectItem("Thoroughly");
                else
                    MakeAllInterface.selectItem(bot.cookTask.cookable);
            } else if (Inventory.getSelectedItem() == null) {
                SpriteItem spriteItem = Inventory.newQuery().names(bot.cookTask.cookable).results().first();
                if (spriteItem != null)
                    spriteItem.interact("Use");
            } else if (!Inventory.getSelectedItem().getDefinition().getName().contains(bot.cookTask.cookable)) {
                Inventory.getSelectedItem().interact("Cancel");
            } else {
                if (bot.usingBonfire) {
                    if (obj.interact("Use", Inventory.getSelectedItem().getDefinition().getName() + " -> Fire")) {
                        Execution.delay(500, 1000);
                        Execution.delayWhile(bot.player::isMoving, 2000, 3500);
                    }
                } else {
                    if (obj.interact("Use", Inventory.getSelectedItem().getDefinition().getName() + " -> " + bot.rangeName)) {
                        Execution.delay(500, 1000);
                        Execution.delayWhile(bot.player::isMoving, 2000, 3500);
                    }
                }
            }
        }
    }
}