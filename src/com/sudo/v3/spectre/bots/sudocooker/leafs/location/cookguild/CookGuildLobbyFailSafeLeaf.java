package com.sudo.v3.spectre.bots.sudocooker.leafs.location.cookguild;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudocooker.ApexCooker;
import com.sudo.v3.spectre.api.game.hud.SudoCamera;
import com.sudo.v3.spectre.statics.UpdateUI;

public class CookGuildLobbyFailSafeLeaf extends LeafTask {

    private ApexCooker bot;
    private GameObject doorObj = null;
    private Area doorArea = new Area.Rectangular(new Coordinate(3142, 3453, 0), new Coordinate(3144, 3451, 0));

    public CookGuildLobbyFailSafeLeaf(ApexCooker bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        UpdateUI.currentTask("In Cooking Guild Lobby, triggering failsafe to open door", bot);
        doorObj = GameObjects.newQuery().names("Door").actions("Open").within(doorArea).results().nearest();

        if(doorObj != null){
            if(doorObj.getVisibility() < 0.9){
                SudoCamera.ConcurrentlyTurnToWithYaw(doorObj);
            }

            doorObj.interact("Open");
        }

    }
}
