package com.sudo.v3.spectre.bots.sudocooker.leafs;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.definitions.ItemDefinition;
import com.runemate.game.api.hybrid.local.hud.interfaces.InterfaceComponent;
import com.runemate.game.api.hybrid.local.hud.interfaces.Interfaces;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudocooker.ApexCooker;
import com.sudo.v3.spectre.api.game.hud.SudoCamera;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 12/23/2016.
 */
public class RS3RangeInteractLeaf extends LeafTask {
    private ApexCooker bot;
    private GameObject range;
    private InterfaceComponent comp;

    public RS3RangeInteractLeaf(ApexCooker bot) {
        this.bot = bot;
    }

    @Override
    public void execute() {
        UpdateUI.currentTask("Interacting with range/fire", bot);
        if (!bot.usingBonfire) {
            range = GameObjects.newQuery().actions("Cook-at").within(bot.cookArea).results().nearest();

            if (range != null) {
                if (!range.isVisible())
                    SudoCamera.ConcurrentlyTurnToWithYaw(range);

                if (range.interact("Cook-at")) {
                    Execution.delayUntil(bot.player::isMoving, 1000, 5000);
                    Execution.delay(250, 1000);
                }
            } else {

                range = GameObjects.newQuery().actions("Cook at").within(bot.cookArea).results().nearest();

                if (range != null) {
                    if (!range.isVisible())
                        SudoCamera.ConcurrentlyTurnToWithYaw(range);

                    if (range.interact("Cook at")) {
                        Execution.delayUntil(bot.player::isMoving, 1000, 5000);
                        Execution.delay(250, 1000);
                    }
                }
            }
        } else {
            if ((comp = Interfaces.newQuery().containers(1179).containedItem(ItemDefinition.getNamePredicate("Cook")).results().first()) != null)
                comp.click();
            else {
                range = GameObjects.newQuery().names("Fire").actions("Use").results().nearest();
                if (Inventory.getSelectedItem() == null) {
                    SpriteItem spriteItem = Inventory.newQuery().names(bot.cookTask.cookable).results().first();
                    if (spriteItem != null)
                        spriteItem.interact("Use");
                } else {
                    if (range.interact("Use", Inventory.getSelectedItem().getDefinition().getName() + " -> Fire")){
                        Execution.delayUntil(bot.player::isMoving, 1000, 5000);
                        Execution.delay(250, 1000);
                    }
                }
            }

        }
    }
}