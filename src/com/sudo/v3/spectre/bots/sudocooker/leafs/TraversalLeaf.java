package com.sudo.v3.spectre.bots.sudocooker.leafs;

import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.common.playersense.ApexPlayerSense;
import com.sudo.v3.spectre.bots.sudocooker.ApexCooker;
import com.sudo.v3.spectre.bots.sudocooker.enums.TraversalLocation;
import com.sudo.v3.spectre.common.navigation.Traverse;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 11/29/2016.
 */
public class TraversalLeaf extends LeafTask
{
    private ApexCooker bot;

    public TraversalLeaf(ApexCooker bot){
        this.bot = bot;
    }

    @Override
    public void execute()
    {
        if(bot.traversalLocation == TraversalLocation.bank)
        {
            UpdateUI.currentTask("Traveling to bank", bot);
            if(Traverse.smartRegionPath(bot.nav, bot.bankArea)) {
                // Delay after each attempt to traverse
                Execution.delay(500, 750);
                Execution.delayWhile(bot.player::isMoving, ApexPlayerSense.Key.TRAVERSAL_DELAY.getAsInteger());
            }
        }
        else if(bot.traversalLocation == TraversalLocation.cookArea)
        {
            UpdateUI.currentTask("Traveling to cook area", bot);
            if(Traverse.smartRegionPath(bot.nav, bot.cookArea)) {
                // Delay after each attempt to traverse
                Execution.delay(500, 750);
                Execution.delayWhile(bot.player::isMoving, ApexPlayerSense.Key.TRAVERSAL_DELAY.getAsInteger());
            }
        }
    }
}
