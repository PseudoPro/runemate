package com.sudo.v3.spectre.bots.sudocooker.leafs;

import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudocooker.ApexCooker;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 12/17/2016.
 */
public class TogglePresetLeaf extends LeafTask
{
    private ApexCooker bot;

    public TogglePresetLeaf(ApexCooker bot)
    {
        this.bot = bot;
    }

    @Override
    public void execute()
    {
        UpdateUI.currentTask("Loading bank preset", bot);
        Bank.loadPreset(bot.presetNumber);
    }
}