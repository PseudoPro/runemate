package com.sudo.v3.spectre.bots.sudocooker.ui;

import com.sudo.v3.spectre.bots.sudocooker.ApexCooker;
import com.sudo.v3.spectre.bots.sudocooker.enums.CookLocation;
import com.sudo.v3.spectre.bots.sudocooker.tasks.CookTask;
import com.sudo.v3.spectre.statics.Areas;
import com.sudo.v3.ui.base.SudoFXController;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;

/**
 * Java FX Controller for the CookerFXGui class
 */
public class CookerFXController extends SudoFXController implements Initializable {

    private ApexCooker bot;

    private ArrayList<String> allFish = new ArrayList<String>();

    @FXML
    private Button startButton, add_BT, remove_BT;

    @FXML
    private Label prompt_L;

    @FXML
    private TextArea search_TA;

    @FXML
    private TextField amount_TF;

    @FXML
    private ListView fishListView, taskListView;

    @FXML
    private ComboBox location_CB;

    @FXML
    private CheckBox usePreset_CB;

    @FXML
    private RadioButton one_Radio, two_Radio;


    public CookerFXController(ApexCooker bot) {
        super(bot);
        this.bot = bot;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Initialize Super
        super.initialize(location, resources);

        //if (bot.getMetaData().getHourlyPrice().doubleValue() > 0 || bot.isPrivate() || Environment.isSDK()) {
        if(bot.isRS3)
            allFish.addAll(Arrays.asList("Jug of wine", "Potato", "Raw crayfish", "Raw shrimps", "Raw sardine", "Raw anchovies", "Raw herring", "Raw mackerel", "Raw trout", "Raw cod", "Raw pike", "Raw salmon",
                    "Raw slimy eel", "Raw tuna", "Raw rainbow fish", "Raw cave eel", "Raw lobster", "Raw bass", "Raw swordfish", "Raw desert sole", "Raw catfish", "Raw beltfish", "Raw lava eel", "Raw monkfish", "Raw shark", "Raw great white shark", "Raw sea turtle",
                    "Raw cavefish", "Raw manta ray", "Raw rocktail", "Raw beef", "Raw bear meat", "Raw yak meat", "Raw rat meat", "Raw chicken", "Raw ugthanki meat", "Raw rabbit", "Raw bird meat"));
        else
            allFish.addAll(Arrays.asList("Jug of wine", "Potato", "Raw crayfish", "Raw shrimps", "Raw sardine", "Raw anchovies", "Raw karambwan", "Raw herring", "Raw mackerel", "Raw trout", "Raw cod", "Raw pike", "Raw salmon",
                    "Raw slimy eel", "Raw tuna", "Raw rainbow fish", "Raw cave eel", "Raw lobster", "Raw bass", "Raw swordfish", "Raw desert sole", "Raw catfish", "Raw beltfish", "Raw lava eel", "Raw monkfish", "Raw shark", "Raw anglerfish", "Raw great white shark", "Raw sea turtle",
                    "Raw cavefish", "Raw manta ray", "Raw rocktail", "Raw beef", "Raw bear meat", "Raw yak meat", "Raw rat meat", "Raw chicken", "Raw ugthanki meat", "Raw rabbit", "Raw bird meat"));
        //} else {
        //    allFish.addAll(Arrays.asList("Raw crayfish", "Raw shrimps", "Raw karambwanji", "Raw sardine", "Raw anchovies", "Raw karambwan", "Raw herring", "Raw mackerel", "Raw trout", "Raw cod", "Raw pike", "Raw salmon",
        //            "Raw slimy eel", "Raw tuna", "Raw rainbow fish", "Raw cave eel", "Raw lava eel", "Raw sea turtle",
        //            "Raw manta ray", "Raw beef", "Raw bear meat", "Raw yak meat", "Raw rat meat", "Raw chicken", "Raw ugthanki meat", "Raw rabbit", "Raw bird meat"));
        //}

        fishListView.getItems().addAll(allFish);

        if(bot.isRS3)
            location_CB.getItems().addAll("Rogue's Den", "Cooking Guild", "Al Kharid", "Catherby", "Bonfire", "Inventory");
        else
            location_CB.getItems().addAll("Rogue's Den", "Cooking Guild", "Al Kharid", "Catherby", "Hosidius House", "Bonfire", "Inventory");

        fishListView.setOnMouseClicked(getFishListViewAction());
        taskListView.setOnMouseClicked(getTaskListViewAction());
        startButton.setOnAction(getStart_BTAction());
        add_BT.setOnAction(getAdd_BTAction());
        remove_BT.setOnAction(getRemove_BTAction());
        usePreset_CB.setOnAction(getUsePreset_CBAction());
        one_Radio.setOnAction(getOne_RadioAction());
        two_Radio.setOnAction(getTwo_RadioAction());
        location_CB.setOnAction(getLocation_CBAction());

        usePreset_CB.setDisable(!bot.isRS3);

        search_TA.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String s, String s2) {
                fishListView.getItems().clear();

                if (search_TA.getLength() > 0) {
                    for (int i = 0; i < allFish.size(); i++) {
                        //if (search_TA.getText().equalsIgnoreCase(allFish.get(i).substring(4, search_TA.getLength() + 4)) || search_TA.getText().equalsIgnoreCase(allFish.get(i).substring(0, search_TA.getLength())))
                        //    fishListView.getItems().add(allFish.get(i));
                        if(allFish.get(i).toString().toLowerCase().contains(search_TA.getText().toLowerCase()))
                            fishListView.getItems().add(allFish.get(i));
                    }
                } else {
                    fishListView.getItems().clear();
                    fishListView.getItems().addAll(allFish);
                }
            }
        });

        amount_TF.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String s, String s2) {
                if (amount_TF.getText().matches("^-?\\d+$") && !fishListView.getSelectionModel().getSelectedItem().toString().equals(""))
                    add_BT.setDisable(false);
            }
        });

        amount_TF.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent ke) {
                if (ke.getCode().equals(KeyCode.ENTER)) {
                    if (amount_TF.getText().equals("0"))
                        taskListView.getItems().addAll(fishListView.getSelectionModel().getSelectedItem().toString() + " x All");
                    else
                        taskListView.getItems().addAll(fishListView.getSelectionModel().getSelectedItem().toString() + " x " + amount_TF.getText());

                    amount_TF.setText("");
                    add_BT.setDisable(true);

                    usePreset_CB.setDisable(taskListView.getItems().size() != 1);
//                    startButton.setDisable(!(taskListView.getItems().size() > 0 && !location_CB.getSelectionModel().getSelectedItem().toString().equals("")));
                    startButton.setDisable(taskListView.getItems().size() <= 0 || location_CB.getSelectionModel() == null || location_CB.getSelectionModel().getSelectedItem() == null);
                }
            }
        });

    }

    private EventHandler<MouseEvent> getFishListViewAction() {
        return event -> {
            if (amount_TF.getText().matches("^-?\\d+$") && !fishListView.getSelectionModel().getSelectedItem().toString().equals(""))
                add_BT.setDisable(false);
        };
    }

    private EventHandler<ActionEvent> getAdd_BTAction() {
        return event -> {
            if (amount_TF.getText().equals("0"))
                taskListView.getItems().addAll(fishListView.getSelectionModel().getSelectedItem().toString() + " x All");
            else
                taskListView.getItems().addAll(fishListView.getSelectionModel().getSelectedItem().toString() + " x " + amount_TF.getText());

            amount_TF.setText("");
            add_BT.setDisable(true);

            usePreset_CB.setDisable(taskListView.getItems().size() != 1);
//                    startButton.setDisable(!(taskListView.getItems().size() > 0 && !location_CB.getSelectionModel().getSelectedItem().toString().equals("")));
            startButton.setDisable(taskListView.getItems().size() <= 0 || location_CB.getSelectionModel() == null || location_CB.getSelectionModel().getSelectedItem() == null);
        };
    }

    private EventHandler<MouseEvent> getTaskListViewAction() {
        return event -> {
            if (!taskListView.getSelectionModel().getSelectedItem().toString().equals(""))
                remove_BT.setDisable(false);
        };
    }

    private EventHandler<ActionEvent> getRemove_BTAction() {
        return event -> {
            taskListView.getItems().remove(taskListView.getSelectionModel().getSelectedItem());
            remove_BT.setDisable(true);

            usePreset_CB.setDisable(taskListView.getItems().size() != 1);
//                    startButton.setDisable(!(taskListView.getItems().size() > 0 && !location_CB.getSelectionModel().getSelectedItem().toString().equals("")));
            startButton.setDisable(taskListView.getItems().size() <= 0 || location_CB.getSelectionModel() == null || location_CB.getSelectionModel().getSelectedItem() == null);
        };
    }

    private EventHandler<ActionEvent> getLocation_CBAction() {
        return event -> {
            if(location_CB.getSelectionModel().getSelectedItem().toString().equals("Inventory"))
                prompt_L.setText("Only use the \"Inventory\" location if you are combining items (like Wine)");
            else
                prompt_L.setText("");
//                    startButton.setDisable(!(taskListView.getItems().size() > 0 && !location_CB.getSelectionModel().getSelectedItem().toString().equals("")));
            startButton.setDisable(taskListView.getItems().size() <= 0 || location_CB.getSelectionModel() == null || location_CB.getSelectionModel().getSelectedItem() == null);
        };
    }

    private EventHandler<ActionEvent> getUsePreset_CBAction() {
        return event -> {
            one_Radio.setDisable(!usePreset_CB.isSelected());
            two_Radio.setDisable(!usePreset_CB.isSelected());
        };
    }

    private EventHandler<ActionEvent> getOne_RadioAction() {
        return event -> {
            two_Radio.setSelected(!one_Radio.isSelected());
        };
    }

    private EventHandler<ActionEvent> getTwo_RadioAction() {
        return event -> {
            one_Radio.setSelected(!two_Radio.isSelected());
        };
    }

    private EventHandler<ActionEvent> getStart_BTAction() {
        return event -> {

            switch (location_CB.getSelectionModel().getSelectedItem().toString()) {
                case "Rogue's Den":
                    bot.cookLocationString = "Rogue's Den";
                    bot.cookLocation = CookLocation.ROGUES_DEN;
                    if (bot.isRS3) {
                        bot.bankArea = Areas.ROGUE_RS3_BANK_AREA;
                        bot.cookArea = Areas.ROGUE_RS3_FIRE_AREA;
                    } else {
                        bot.usingBonfire = true;
                        bot.bankArea = Areas.ROGUE_OSRS_BANK_AREA;
                        bot.cookArea = Areas.ROGUE_OSRS_FIRE_AREA;
                    }
                    break;

                case "Al Kharid":
                    bot.cookLocationString = "Al Kharid";
                    if (bot.isRS3) {
                        bot.bankArea = Areas.ALKHARID_RS3_BANK_AREA;
                        bot.cookArea = Areas.ALKHARID_RS3_RANGE_AREA;
                    } else {
                        bot.bankArea = Areas.ALKHARID_OSRS_BANK_AREA;
                        bot.cookArea = Areas.ALKHARID_OSRS_RANGE_AREA;
                    }
                    break;

                case "Catherby":
                    bot.cookLocationString = "Catherby";
                    if(bot.isRS3){
                        bot.bankArea = Areas.CATH_RS3_BANK_AREA;
                        bot.cookArea = Areas.CATH_RS3_RANGE_AREA;
                    }else {
                        bot.bankArea = Areas.CATH_OSRS_BANK_AREA;
                        bot.cookArea = Areas.CATH_OSRS_RANGE_AREA;
                    }
                    bot.rangeName = "Range";
                    break;

                case "Cooking Guild":
                    bot.cookLocationString = "Cooking Guild";
                    bot.bankArea = Areas.COOK_GUILD_BANK_AREA;
                    bot.cookArea = Areas.COOK_GUILD_RANGE_AREA;
                    bot.cookLocation = CookLocation.COOKING_GUILD;
                    break;

                case "Hosidius House":
                    bot.cookLocationString = "Hosidius House";
                    bot.bankArea = Areas.HOSIDIUS_BANK_AREA;
                    bot.cookArea = Areas.HOSIDIUS_OSRS_RANGE_AREA;
                    bot.rangeName = "Clay oven";
                    bot.cookLocation = CookLocation.HOSIDIUS_HOUSE;
                    break;

                case "Bonfire":
                    bot.cookLocationString = "Bonfire";
                    bot.usingBonfire = true;
                    break;
            }

            if (bot.usingPreset = (usePreset_CB.isSelected() && !usePreset_CB.isDisabled() && taskListView.getItems().size() == 1))
                if (one_Radio.isSelected())
                    bot.presetNumber = 1;
                else
                    bot.presetNumber = 2;

            bot.cookTask = null;
            bot.taskHandler.removeAll();
            for (int i = 0; i < taskListView.getItems().size(); i++) {
                String[] parts = taskListView.getItems().get(i).toString().split(" x ");
                if (parts[1].equals("All"))
                    bot.taskHandler.addTask(new CookTask(parts[0], 0));
                else
                    bot.taskHandler.addTask(new CookTask(parts[0], Integer.parseInt(parts[1])));
            }

            startButton.textProperty().set("Resume");
            prompt_L.textProperty().set(" ");
            super.getStart_BTEvent();
        };
    }
}