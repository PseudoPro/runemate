package com.sudo.v3.spectre.bots.exampleflaxpicker.leafs;

import com.runemate.game.api.script.framework.tree.LeafTask;

/**
 * Created by SudoPro on 12/28/2016.
 */
public class EmptyLeaf extends LeafTask {
    @Override
    public void execute() {
        System.out.println("Empty Leaf Reached");
    }
}