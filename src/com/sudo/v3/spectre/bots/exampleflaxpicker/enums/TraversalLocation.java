package com.sudo.v3.spectre.bots.exampleflaxpicker.enums;

/**
 * Created by SudoPro on 12/28/2016.
 */
public enum TraversalLocation {
    flaxArea,
    bankArea
}