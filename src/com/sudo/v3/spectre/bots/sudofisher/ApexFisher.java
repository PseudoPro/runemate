package com.sudo.v3.spectre.bots.sudofisher;

import com.runemate.game.api.hybrid.RuneScape;
import com.runemate.game.api.hybrid.entities.LocatableEntity;
import com.runemate.game.api.hybrid.entities.definitions.NpcDefinition;
import com.runemate.game.api.hybrid.local.Skill;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.osrs.net.OSBuddyExchange;
import com.runemate.game.api.script.framework.listeners.ChatboxListener;
import com.runemate.game.api.script.framework.listeners.events.MessageEvent;
import com.sudo.v3.spectre.bots.sudofisher.enums.FishLocation;
import com.sudo.v3.spectre.bots.sudofisher.branches.*;
import com.sudo.v3.spectre.bots.sudofisher.enums.BankingType;
import com.sudo.v3.spectre.bots.sudofisher.enums.TraversalLocation;
import com.sudo.v3.spectre.bots.sudofisher.leafs.*;
import com.sudo.v3.spectre.statics.Areas;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.sudo.v3.ui.Info;
import com.sudo.v3.spectre.bots.sudofisher.ui.FishInfoUI;
import com.sudo.v3.spectre.bots.sudofisher.ui.FisherFXGui;
import com.sudo.v3.base.SudoBot;
import com.sudo.v3.spectre.api.util.SudoTimer;
import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.entities.definitions.ItemDefinition;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.hybrid.util.calculations.CommonMath;
import com.runemate.game.api.script.framework.listeners.events.ItemEvent;
import com.runemate.game.api.script.framework.tree.TreeTask;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by SudoPro on 11/23/2016.
 */
public class ApexFisher extends SudoBot implements ChatboxListener {
    private FishInfoUI infoUI;
    private FisherFXGui configUI;

    public SudoTimer timer;
    public Area bankArea;

    public List<Area> fishAreas = new ArrayList<>();
    public List<Area> fishSpotAreas = new ArrayList<>();

    public int fishCount = 0, presetNumber = 0, powerFishInventoryCheck = 0, possibleFishCountFromLocation = 1;
    public String fishAction = "", fishSpotAction = "", fishNameString = "", locationNameString = "", fishSpotName = "";

    public List<String> equipment = new ArrayList<>();
    public List<String> equipmentAccessory = new ArrayList<>();
    public String[] allEquipment;

    public ArrayList<Coordinate> seenFishSpots = new ArrayList<>();
    public Npc fishingSpot, bankNpc;
    public LocatableEntity locatableBank;

    public TraversalLocation traversalLocation = TraversalLocation.bank;
    public BankingType bankingType = BankingType.depositAllExceptEquipment;
    public FishLocation fishLocation = FishLocation.GENERIC;

    public boolean useDepositBox = false, disableEquipmentCheck = false, currentlyDepositing = false, usingPreset = false, usingDragonHarpoon = false, isPowerFishing = false, currentlyDropping = false;

    // Branches
    public IsBankOpen isBankOpen = new IsBankOpen(this);
    public IsFishing isFishing = new IsFishing(this);
    public IsInArea isInArea = new IsInArea(this);
    public IsInventoryFull isInventoryFull = new IsInventoryFull(this);
    public IsSpotNearby isSpotNearby = new IsSpotNearby(this);
    public IsBankAround isBankAround = new IsBankAround(this);
    public Root root = new Root(this);

    // Leafs
    public DepositAllLeaf depositAllLeaf = new DepositAllLeaf(this);
    public IsFishingLeaf isFishingLeaf = new IsFishingLeaf(this);
    public StartFishingLeaf startFishingLeaf = new StartFishingLeaf(this);
    public TraversalLeaf traversalLeaf = new TraversalLeaf(this);
    public UseDepositBoxLeaf useDepositBoxLeaf = new UseDepositBoxLeaf(this);
    public WithdrawEquipmentLeaf withdrawEquipmentLeaf = new WithdrawEquipmentLeaf(this);
    public WieldHarpoonLeaf wieldHarpoonLeaf = new WieldHarpoonLeaf(this);
    //public ConfigureActionBarLeaf configureActionBarLeaf = new ConfigureActionBarLeaf(this);

    // Hashmap holding the rotation of coordinates for Minnow Fishing
    public HashMap<Coordinate, Coordinate> minnowNextCoordinateMap = new HashMap<Coordinate, Coordinate>(){{
        // East Dock
        put(new Coordinate(2620, 3443, 0), new Coordinate(2619, 3443, 0));
        put(new Coordinate(2619, 3443, 0), new Coordinate(2618, 3443, 0));
        put(new Coordinate(2618, 3443, 0), new Coordinate(2617, 3443, 0));
        put(new Coordinate(2617, 3443, 0), new Coordinate(2617, 3444, 0));
        put(new Coordinate(2617, 3444, 0), new Coordinate(2618, 3444, 0));
        put(new Coordinate(2618, 3444, 0), new Coordinate(2619, 3444, 0));
        put(new Coordinate(2619, 3444, 0), new Coordinate(2620, 3444, 0));
        put(new Coordinate(2620, 3444, 0), new Coordinate(2620, 3443, 0));

        // West Dock
        put(new Coordinate(2612, 3443, 0), new Coordinate(2611, 3443, 0));
        put(new Coordinate(2611, 3443, 0), new Coordinate(2610, 3443, 0));
        put(new Coordinate(2610, 3443, 0), new Coordinate(2609, 3443, 0));
        put(new Coordinate(2609, 3443, 0), new Coordinate(2609, 3444, 0));
        put(new Coordinate(2609, 3444, 0), new Coordinate(2610, 3444, 0));
        put(new Coordinate(2610, 3444, 0), new Coordinate(2611, 3444, 0));
        put(new Coordinate(2611, 3444, 0), new Coordinate(2612, 3444, 0));
        put(new Coordinate(2612, 3444, 0), new Coordinate(2612, 3443, 0));
    }};

    // Hashmap holding the opposite of coordinates for Minnow Fishing
    public HashMap<Coordinate, Coordinate> minnowOppositeCoordinateMap = new HashMap<Coordinate, Coordinate>(){{
        // East Dock
        put(new Coordinate(2620, 3443, 0), new Coordinate( 2617, 3444, 0));
        put(new Coordinate(2619, 3443, 0), new Coordinate( 2618, 3444, 0));
        put(new Coordinate(2618, 3443, 0), new Coordinate( 2619, 3444, 0));
        put(new Coordinate(2617, 3443, 0), new Coordinate( 2620, 3444, 0 ));
        put(new Coordinate(2617, 3444, 0), new Coordinate( 2620, 3443, 0 ));
        put(new Coordinate(2618, 3444, 0), new Coordinate( 2619, 3443, 0 ));
        put(new Coordinate(2619, 3444, 0), new Coordinate( 2618, 3443, 0 ));
        put(new Coordinate(2620, 3444, 0), new Coordinate( 2617, 3443, 0 ));

        // West Dock
        put(new Coordinate(2612, 3443, 0), new Coordinate( 2609, 3444, 0 ));
        put(new Coordinate(2611, 3443, 0), new Coordinate( 2610, 3444, 0 ));
        put(new Coordinate(2610, 3443, 0), new Coordinate( 2611, 3444, 0 ));
        put(new Coordinate(2609, 3443, 0), new Coordinate( 2612, 3444, 0 ));
        put(new Coordinate(2609, 3444, 0), new Coordinate( 2612, 3443, 0 ));
        put(new Coordinate(2610, 3444, 0), new Coordinate( 2611, 3443, 0 ));
        put(new Coordinate(2611, 3444, 0), new Coordinate( 2610, 3443, 0 ));
        put(new Coordinate(2612, 3444, 0), new Coordinate( 2609, 3443, 0 ));
    }};

    // Default Constructor
    public ApexFisher() {
        // Give spectre client this class as the embeddableUI
        setEmbeddableUI(this);
    }

    @Override
    public void onStart(String... args) {
        super.onStart();
        getEventDispatcher().addListener(this);

        checkSpecialTimer.start();
    }

    @Override
    public String getBotName() {
        return "ApexFisher";
    }

    @Override
    public ObjectProperty<? extends Node> botInterfaceProperty() {
        if (botInterfaceProperty == null) {
            botInterfaceProperty = new SimpleObjectProperty<>(configUI = new FisherFXGui(this));
            guiList = new ArrayList<Node>();
            //guiList.add(_sbGUI = new SudoBuddyFXGui(this));
            guiList.add(configUI);
            guiList.add(infoUI = new FishInfoUI(this));

        }
        return botInterfaceProperty;
    }

    @Override
    public TreeTask createRootTask() {
        return new Root(this);
    }

    @Override
    public void updateInfo() {
        // UpdateUI all current values in the XPInfo List
        XPInfoMap.values().forEach((xpInfo) -> xpInfo.update());

        if(fishLocation == FishLocation.FISHGUILD_MINNOW){
            grossIncome = (fishCount / 40) * OSBuddyExchange.getGuidePrice(383).getSelling();
        }

        displayInfoMap.put("Fish Count: ", Integer.toString(fishCount) +
                " (" + Integer.toString((int) CommonMath.rate(TimeUnit.HOURS, STOPWATCH.getRuntime(), fishCount)) + "/Hour)");
        displayInfoMap.put("GP: ", Integer.toString(grossIncome) +
                " (" + Integer.toString((int) CommonMath.rate(TimeUnit.HOURS, STOPWATCH.getRuntime(), grossIncome)) + "/Hour)");

        try {
            info = new Info(XPInfoMap,           // XP Info
                    displayInfoMap,
                    STOPWATCH.getRuntimeAsString(),        // Total Runtime
                    currentTaskString,                     // Current Task
                    abTaskString);                         // antiban Task

        } catch (Exception e) {
            e.printStackTrace();
        }

        Platform.runLater(() -> infoUI.update());
    }

    @Override
    public void onItemAdded(ItemEvent event) {
        if (RuneScape.isLoggedIn() && player != null && !Bank.isOpen() && !bankArea.contains(player))
            super.onItemAdded(event);

        if (event != null) {
            ItemDefinition definition = event.getItem().getDefinition();

            if (definition != null && (definition.getName().contains("Raw") || definition.getName().contains("Leaping") || definition.getName().contains("Minnow"))) {
                fishCount += event.getQuantityChange();
            }
        }
    }

    @Override
    public void onItemRemoved(ItemEvent event) {
        if (RuneScape.isLoggedIn() && player != null && !Bank.isOpen() && !bankArea.contains(player))
            super.onItemRemoved(event);
    }

    public Npc findFishingSpot() {
        if (fishLocation == FishLocation.DEEP_SEA) {
            return Npcs.newQuery().within(fishSpotAreas).names(fishSpotName).actions(fishAction).results().nearest();
        }

        return findFishingSpot("Fishing spot", "Rod Fishing spot");
    }

    public Npc findFishingSpot(String... names) {
//        for (String name : names)
//            UpdateUI.debug("FishingSpot Names: " + name);
//
//        ArrayList<Npc> s1 = new ArrayList<>(Arrays.asList(Npcs.newQuery().within(fishSpotAreas).names(names).actions(fishAction).results().sortByDistance().toArray()));
//        s1.retainAll(new ArrayList<>(Arrays.asList(Npcs.newQuery().within(fishSpotAreas).names(names).actions(fishSpotAction).results().sortByDistance().toArray())));
//
//        if (s1.size() > 0) {
//            if (!seenFishSpots.contains(s1.get(0).getPosition()))
//                seenFishSpots.add(s1.get(0).getPosition());
//            return s1.get(0);
//        } else
//            return null;

        Npc npc = null;
        Area currentArea = Areas.getAreaContaining(fishSpotAreas, player);

        if(currentArea != null)
            npc = Npcs.newQuery().names(names).within(currentArea).filter(n -> {
                List<String> actions;
                NpcDefinition def = n.getDefinition();
                return def != null && (actions = def.getActions()) != null && actions.contains(fishAction) && actions.contains(fishSpotAction);
            }).results().nearest();

        if(npc == null) {
            for (int i = 0; i < fishSpotAreas.size(); i++) {
                Area loopArea = fishSpotAreas.get(i);
                npc = Npcs.newQuery().names(names).within(loopArea).filter(n -> {
                    List<String> actions;
                    NpcDefinition def = n.getDefinition();
                    return def != null && (actions = def.getActions()) != null && actions.contains(fishAction) && actions.contains(fishSpotAction);
                }).results().nearest();

                if (npc != null)
                    break;
            }
        }

        return npc;
    }

    private boolean wieldingHarpoon() {
        try {
            SpriteItem item;
            ItemDefinition itemDef;

            return (item = Equipment.getItemIn(Equipment.Slot.WEAPON)) != null && (itemDef = item.getDefinition()) != null &&
                    (itemDef.getName().equals("Barb-tail harpoon") || (Skill.FISHING.getCurrentLevel() >= 61 && itemDef.getName().equals("Dragon harpoon")));
        } catch (Exception e) {
            return false;
        }
    }

    public boolean hasEquipment() {
        if (disableEquipmentCheck)
            return true;

        // If equipment is not found in the inventory, return false
        if (!isRS3) {
            if (fishAction.equals("Harpoon")) {
                if (Skill.FISHING.getCurrentLevel() >= 61) {
                    SpriteItem dragHarp;
                    ItemDefinition harpDef;
                    if (Inventory.contains("Dragon harpoon") ||
                            (dragHarp = Equipment.getItemIn(Equipment.Slot.WEAPON)) != null && (harpDef = dragHarp.getDefinition()) != null &&
                                    (harpDef.getName().equals("Dragon harpoon"))) {
                        usingDragonHarpoon = true;
                        return true;
                    }
                }
                return Inventory.containsAnyOf("Harpoon", "Barb-tail harpoon") || wieldingHarpoon();
            } else if (fishAction.equals("Bait"))
                return Inventory.containsAnyOf("Fishing rod", "Barbarian rod") && Inventory.contains("Fishing bait");
            else if (equipmentAccessory.contains("Barbarian"))
                return Inventory.containsAllOf(equipment.toArray(new String[0])) && Inventory.containsAnyOf("Feather", "Fishing bait");
            else
                return allEquipment != null && Inventory.containsAllOf(allEquipment);
        } else {
            if(equipmentAccessory.contains("Barbarian"))
                return Inventory.containsAnyOf("Feather", "Fishing bait", "Fish offcuts");
            else
                return equipmentAccessory != null && Inventory.containsAllOf(equipmentAccessory.toArray(new String[0]));
        }
    }

    public List<String> getMissingEquipment() {
        List<String> returnList = new ArrayList<>();

        if (fishAction.equals("Harpoon")) {
            if (Skill.FISHING.getCurrentLevel() >= 61) {
                if (!Inventory.containsAnyOf("Harpoon", "Barb-tail harpoon", "Dragon harpoon") || wieldingHarpoon()) {
                    returnList.add("Dragon harpoon");
                    returnList.add("Barb-tail harpoon");
                    returnList.add("Harpoon");
                }
            } else {
                if (!Inventory.containsAnyOf("Harpoon", "Barb-tail harpoon") || wieldingHarpoon()) {
                    returnList.add("Barb-tail harpoon");
                    returnList.add("Harpoon");
                }
            }
        } else if (fishAction.equals("Bait")) {
            if (!Inventory.containsAnyOf("Fishing rod", "Barbarian rod")) {
                if(fishLocation == FishLocation.BAXTORIAN_FALLS)
                    returnList.add("Barbarian rod");
                else
                    returnList.add("Fishing rod");
            }
        } else {
            for (String i : equipment) {
                if (!Inventory.contains(i))
                    returnList.add(i);
            }
        }

        return returnList;
    }

    public String getMissingAccessory() {
        if (equipmentAccessory.contains("Barbarian")) {
            if (!Inventory.contains("Feather") || Inventory.contains("Fishing bait"))
                return "Barbarian";
        }
        for (String i : equipmentAccessory) {
            if (!Inventory.contains(i))
                return i;
        }

        return "";
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        String message = messageEvent.getMessage();

        if (message.contains("jumps up and eats some of your minnows")) {
            fishingSpot = Npcs.newQuery().names("Fishing spot").actions(fishAction).results().nearestTo(minnowOppositeCoordinateMap.get(fishingSpot.getPosition()));
        }
    }
}