package com.sudo.v3.spectre.bots.sudofisher.branches;

import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudofisher.ApexFisher;
import com.sudo.v3.spectre.bots.sudofisher.enums.FishLocation;
import com.sudo.v3.spectre.api.game.entities.ApexPlayer;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.runemate.game.api.script.framework.tree.TreeTask;

/**
 * Created by SudoPro on 11/24/2016.
 */
public class IsFishing extends SudoBranchTask
{
    private ApexFisher bot;

    public IsFishing(ApexFisher bot)
    {
        this.bot = bot;
    }

    @Override
    public TreeTask successTask()
    {
        return bot.isFishingLeaf;
    }

    @Override
    public boolean validate()
    {
        if(bot.fishLocation == FishLocation.DEEP_SEA)
            validate = ApexPlayer.isTargetting(bot.fishSpotName);
        else
            validate = ApexPlayer.isTargetting("Fishing spot", "Rod Fishing spot");

        UpdateUI.debug("Branch IsFishing: " + validate);

        return validate;
    }

    @Override
    public TreeTask failureTask()
    {
        return bot.startFishingLeaf;
    }
}
