package com.sudo.v3.spectre.bots.sudofisher.branches;

import com.runemate.game.api.hybrid.util.calculations.Random;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudofisher.ApexFisher;
import com.sudo.v3.spectre.bots.sudofisher.enums.BankingType;
import com.sudo.v3.spectre.bots.sudofisher.enums.FishLocation;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.script.framework.tree.TreeTask;

/**
 * Created by SudoPro on 11/24/2016.
 */
public class IsInventoryFull extends SudoBranchTask {
    private ApexFisher bot;

    public IsInventoryFull(ApexFisher bot) {
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        if (bot.isPowerFishing) {
            if(bot.fishLocation == FishLocation.BAXTORIAN_FALLS){
                bot.powerDropLeaf.setContainsString("Leaping");
            } else {
                bot.powerDropLeaf.setContainsString("Raw");
            }
            return bot.powerDropLeaf;
        } else {
            bot.bankingType = BankingType.depositAllExceptEquipment;
            return bot.isBankOpen;
        }
    }

    @Override
    public boolean validate() {
        if(bot.powerFishInventoryCheck <= 0)
            bot.powerFishInventoryCheck = Random.nextInt(10, 27);

        validate =  bot.isPowerFishing ? Inventory.getItems().size() > bot.powerFishInventoryCheck : Inventory.isFull();
        //validate = Inventory.isFull();
        UpdateUI.debug("Branch IsInventoryFull: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        return bot.isSpotNearby;
    }
}