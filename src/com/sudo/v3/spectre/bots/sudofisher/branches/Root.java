package com.sudo.v3.spectre.bots.sudofisher.branches;

import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.RuneScape;
import com.runemate.game.api.hybrid.local.Skill;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.util.Regex;
import com.sudo.v3.base.SudoRootTask;
import com.sudo.v3.spectre.bots.sudofisher.ApexFisher;
import com.sudo.v3.spectre.bots.sudofisher.enums.BankingType;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.spectre.bots.sudofisher.enums.FishLocation;
import com.sudo.v3.spectre.common.leafs.PowerDropLeaf;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.sudo.v3.ui.model.XPInfo;

/**
 * Created by SudoPro on 11/23/2016.
 */

public class Root extends SudoRootTask {
    private ApexFisher bot;

    public Root(ApexFisher bot) {
        super(bot);
        this.bot = bot;
    }

    @Override
    public TreeTask rootTask() {

        if (bot.firstLogin) {
            if (RuneScape.isLoggedIn()) {
                // Add Skills to track to the HashMap
                bot.XPInfoMap.put(Skill.FISHING, new XPInfo(Skill.FISHING));
                bot.XPInfoMap.put(Skill.STRENGTH, new XPInfo(Skill.STRENGTH));
                bot.XPInfoMap.put(Skill.AGILITY, new XPInfo(Skill.AGILITY));

                bot.firstLogin = false;

                UpdateUI.debug("Fishing Level: " + Skill.FISHING.getCurrentLevel());
                UpdateUI.debug("Location: " + bot.locationNameString);
                UpdateUI.debug("Fish: " + bot.fishNameString);
                UpdateUI.debug("Powerfishing: " + bot.isPowerFishing);
                UpdateUI.debug("UseDepositBox: " + bot.useDepositBox);
                UpdateUI.debug("UsingPreset: " + bot.usingPreset);
                if (bot.usingPreset)
                    UpdateUI.debug("Preset #: " + bot.presetNumber);
                UpdateUI.debug("UsingDragonHarpoon: " + bot.usingDragonHarpoon);

                bot.updateSession(0.0);

                bot.powerDropLeaf = new PowerDropLeaf(bot) {
                    @Override
                    public void successAction() {
                        bot.powerFishInventoryCheck = 0;
                    }
                };

                bot.powerDropLeaf.setValues("Raw", 6 / bot.possibleFishCountFromLocation, 20 / bot.possibleFishCountFromLocation);
            }
        }

        if (bot.currentlyBreaking) {
            if (RuneScape.isLoggedIn()) {
                UpdateUI.currentTask("Attempting to log off.", bot);
                RuneScape.logout(true);
            }
            return bot.emptyLeaf;
        }

        if (bot.getMetaData().getHourlyPrice().doubleValue() > 0 || bot.isValidSession() || Environment.isSDK()) {
            validate = !Inventory.newQuery().names(Regex.getPatternsForContainsStrings("Raw", "Leaping")).results().isEmpty();
            UpdateUI.debug("Root:HasRaw: " + validate);
            UpdateUI.debug("Root:CurrentlyDepositing: " + bot.currentlyDepositing);
            UpdateUI.debug("Root:CurrentlyDropping: " + bot.currentlyDropping);
            if (validate) {
                if (bot.currentlyDepositing) {
                    return bot.useDepositBoxLeaf;
                } else if (bot.currentlyDropping) {
                    if (bot.fishLocation == FishLocation.BAXTORIAN_FALLS) {
                        bot.powerDropLeaf.setContainsString("Leaping");
                    } else {
                        bot.powerDropLeaf.setContainsString("Raw");
                    }
                    return bot.powerDropLeaf;
                }
            } else {
                if (bot.currentlyDepositing) {
                    bot.currentlyDepositing = false;
                }
                if (bot.currentlyDropping) {
                    bot.currentlyDropping = false;
                }
            }
            validate = bot.hasEquipment();
            UpdateUI.debug("RootHasEquipment: " + validate);
            if (validate) {
                if (Skill.ATTACK.getCurrentLevel() >= 60 && Skill.FISHING.getCurrentLevel() >= 61) {
                    if (!bot.isRS3 && bot.fishAction.equals("Harpoon") && Inventory.contains("Dragon harpoon")) {
                        return bot.wieldHarpoonLeaf;
                    } else if (bot.usingDragonHarpoon && bot.checkSpecialTimer.hasExpired() && !bot.bankArea.contains(bot.player) && !Bank.isOpen()) {
                        return bot.useSpecialLeaf;
                    }
                }
                return bot.isInventoryFull;
            } else {
                bot.bankingType = BankingType.withdrawEquipment;
                return bot.isBankOpen;
            }
        } else {
            return bot.liteVersionExpireLeaf;
        }
    }
}