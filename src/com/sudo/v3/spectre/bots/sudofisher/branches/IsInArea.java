package com.sudo.v3.spectre.bots.sudofisher.branches;

import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudofisher.ApexFisher;
import com.sudo.v3.spectre.bots.sudofisher.enums.FishLocation;
import com.sudo.v3.spectre.bots.sudofisher.enums.TraversalLocation;
import com.sudo.v3.spectre.statics.Areas;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.runemate.game.api.script.framework.tree.TreeTask;

/**
 * Created by SudoPro on 11/23/2016.
 */
public class IsInArea extends SudoBranchTask
{
    private ApexFisher bot;

    public IsInArea(ApexFisher bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask()
    {
        if(bot.traversalLocation == TraversalLocation.bank)
            return bot.openBankLeaf;

        else if(bot.traversalLocation == TraversalLocation.fishingArea)
            return bot.isFishing;

        else if(bot.traversalLocation == TraversalLocation.fishingSpot)
            return bot.traversalLeaf;

        else
            return bot.emptyLeaf;
    }

    @Override
    public boolean validate()
    {
        if(bot.traversalLocation == TraversalLocation.bank)
        {
            UpdateUI.debug("Branch IsInArea(BANK_AREA): " + bot.bankArea.contains(bot.player));
            return bot.bankArea.contains(bot.player);
        }

        else if(bot.traversalLocation == TraversalLocation.fishingArea)
        {
            if(bot.fishLocation != FishLocation.FISHGUILD) {
                validate = Areas.anyContainsCoordinate(bot.fishAreas, bot.player.getPosition());
                UpdateUI.debug("Branch IsInArea(fishArea): " + validate);
                return validate;
            } else {
                validate = Areas.anyContainsCoordinate(bot.fishAreas, bot.player.getPosition());
                UpdateUI.debug("Branch IsInArea(fishArea): " + validate);
                return validate;
            }
        }

        else if(bot.traversalLocation == TraversalLocation.fishingSpot)
        {
            UpdateUI.debug("Branch IsInArea(fishSpotArea): always true");
            return true;
        }

        else
            return false;
    }

    @Override
    public TreeTask failureTask()
    {
        return bot.traversalLeaf;
    }
}
