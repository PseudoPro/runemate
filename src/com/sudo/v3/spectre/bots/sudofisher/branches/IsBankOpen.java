package com.sudo.v3.spectre.bots.sudofisher.branches;

import com.runemate.game.api.hybrid.local.hud.interfaces.DepositBox;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudofisher.ApexFisher;
import com.sudo.v3.spectre.bots.sudofisher.enums.BankingType;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.script.framework.tree.TreeTask;

/**
 * Created by SudoPro on 11/23/2016.
 */
public class IsBankOpen extends SudoBranchTask
{
    private ApexFisher bot;

    public IsBankOpen(ApexFisher bot){
        this.bot = bot;
        //this.type = BankingType.depositAllExceptEquipment;
    }

    @Override
    public TreeTask successTask()
    {
        if (bot.bankingType == BankingType.withdrawEquipment)
            return bot.withdrawEquipmentLeaf;
        else
            return bot.depositAllLeaf;
    }

    @Override
    public boolean validate()
    {
        validate = Bank.isOpen() || DepositBox.isOpen();
        UpdateUI.debug("Branch IsBankOpen(" + bot.bankingType + "): " + validate);
        return validate;

    }

    @Override
    public TreeTask failureTask()
    {
        return bot.isBankAround;
    }
}
