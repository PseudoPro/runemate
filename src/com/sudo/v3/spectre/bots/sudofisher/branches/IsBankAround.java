package com.sudo.v3.spectre.bots.sudofisher.branches;

import com.runemate.game.api.hybrid.region.*;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudofisher.ApexFisher;
import com.sudo.v3.spectre.bots.sudofisher.enums.FishLocation;
import com.sudo.v3.spectre.bots.sudofisher.enums.TraversalLocation;
import com.sudo.v3.spectre.api.game.hud.SudoCamera;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.runemate.game.api.script.framework.tree.TreeTask;

/**
 * Created by SudoPro on 11/26/2016.
 */
public class IsBankAround extends SudoBranchTask
{
    private ApexFisher bot;

    public IsBankAround(ApexFisher bot)
    {
        this.bot = bot;
    }

    @Override
    public TreeTask successTask()
    {
        if (bot.useDepositBox)
            return bot.useDepositBoxLeaf;
        else {
            if(bot.fishLocation == FishLocation.MENAPHOS)
                bot.openBankLeaf.setGenericBank(true);
            return bot.openBankLeaf;
        }
    }

    @Override
    public boolean validate()
    {
        if (bot.useDepositBox)
        {
            bot.locatableBank = GameObjects.newQuery().names("Bank Deposit Box", "Bank deposit box", "Deposit box", "Magical net").actions("Deposit").within(bot.bankArea).results().nearest();

            if (bot.locatableBank != null)
            {
                if (bot.locatableBank.getVisibility() < .85)
                    SudoCamera.ConcurrentlyTurnToWithYaw(bot.locatableBank);
                validate = bot.locatableBank.getVisibility() > 0.8 && bot.locatableBank.isValid() && bot.locatableBank.distanceTo(bot.player) < 14;
                UpdateUI.debug("Branch IsAround(depositBox): " + validate);
                return validate;
            }else{
                UpdateUI.debug("Branch IsAround(depositBox): false - Item is null");
            }
        } else
        {
            bot.locatableBank = Banks.newQuery().results().nearest();

            if (bot.locatableBank != null)
            {
                if (bot.locatableBank.getVisibility() < .85)
                    SudoCamera.ConcurrentlyTurnToWithYaw(bot.locatableBank);
                validate = bot.locatableBank.getVisibility() > 0.8 && bot.locatableBank.isValid();
                UpdateUI.debug("Branch IsAround: " + validate);
                return validate;
            }

            bot.bankNpc = Npcs.newQuery().actions("Bank").results().nearest();
            if (bot.bankNpc != null)
            {
                if (bot.bankNpc.getVisibility() < .85)
                    SudoCamera.ConcurrentlyTurnToWithYaw(bot.bankNpc);
                validate = bot.bankNpc.getVisibility() > 0.8 && bot.bankNpc.isValid();
                UpdateUI.debug("Branch IsAround: " + validate);
                return validate;
            }
        }

        return false;
    }

    @Override
    public TreeTask failureTask()
    {
        bot.traversalLocation = TraversalLocation.bank;
        return bot.traversalLeaf;
    }
}