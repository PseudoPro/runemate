package com.sudo.v3.spectre.bots.sudofisher.branches;

import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.DepositBox;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudofisher.ApexFisher;
import com.sudo.v3.spectre.bots.sudofisher.enums.TraversalLocation;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.runemate.game.api.script.framework.tree.TreeTask;

/**
 * Created by SudoPro on 11/24/2016.
 */
public class IsSpotNearby extends SudoBranchTask
{
    private ApexFisher bot;

    public IsSpotNearby(ApexFisher bot)
    {
        this.bot = bot;
    }

    @Override
    public TreeTask successTask()
    {
        // Add all of the seen spots to the seen positions
        //spots.forEach(i -> bot.seenFishSpots.add(i.getPosition()));

        if(Bank.isOpen())
            return bot.closeBankLeaf;
        else if (DepositBox.isOpen())
            return bot.closeDepositBoxLeaf;
        else
            return bot.isFishing;
    }

    @Override
    public boolean validate()
    {
        bot.fishingSpot = bot.findFishingSpot();
        validate = bot.fishingSpot != null && bot.fishingSpot.distanceTo(bot.player) < 12;
        UpdateUI.debug("Branch IsSpotNearby: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask()
    {
        bot.traversalLocation = TraversalLocation.fishingSpot;
        return bot.isInArea;
    }
}
