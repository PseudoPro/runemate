package com.sudo.v3.spectre.bots.sudofisher.branches;

import com.runemate.game.api.hybrid.entities.definitions.ItemDefinition;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.hybrid.util.Regex;
import com.runemate.game.api.rs3.local.hud.interfaces.eoc.ActionBar;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudofisher.ApexFisher;

public class ActionBarConfiguredBranch extends SudoBranchTask {

    private ApexFisher bot;

    public ActionBarConfiguredBranch (ApexFisher bot){
        this.bot = bot;
    }

    @Override
    public boolean validate() {
        SpriteItem[] rawItemsFromInventory = Inventory.newQuery().names(Regex.getPatternForContainsString("Raw")).results().toArray();

        for(SpriteItem raw : rawItemsFromInventory){
            ItemDefinition def = raw.getDefinition();

            if(def != null){
                ActionBar.Slot slot = ActionBar.newQuery().names(def.getName()).results().first();
            }
        }

        return false;
    }

    @Override
    public TreeTask failureTask() {
        return null;
    }

    @Override
    public TreeTask successTask() {
        return null;
    }
}
