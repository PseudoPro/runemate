package com.sudo.v3.spectre.bots.sudofisher.enums;

/**
 * Created by SudoPro on 11/23/2016.
 */
public enum TraversalLocation
{
    bank,
    fishingArea,
    fishingSpot
}
