package com.sudo.v3.spectre.bots.sudofisher.enums;

/**
 * Created by Proxify on 6/15/2017.
 */
public enum FishLocation {
    GENERIC,
    MENAPHOS,
    FISHGUILD,
    FISHGUILD_MINNOW,
    DEEP_SEA,
    BAXTORIAN_FALLS
}
