package com.sudo.v3.spectre.bots.sudofisher.ui;

import com.sudo.v3.spectre.bots.sudofisher.enums.FishLocation;
import com.sudo.v3.spectre.bots.sudofisher.ApexFisher;
import com.sudo.v3.spectre.statics.Areas;
import com.sudo.v3.ui.base.SudoFXController;
import com.runemate.game.api.hybrid.Environment;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Java FX Controller for the FisherFXGui class
 */
public class FisherFXController extends SudoFXController implements Initializable {

    private final ApexFisher bot;
    boolean rs3 = false;

    @FXML
    private ListView<String> locationsListView, fishListView;

    @FXML
    private Button startButton;

    @FXML
    private CheckBox usePreset_CB, disableEquipmentCheck_CB, powerFish_CB, useDepositBox_CB;

    @FXML
    private RadioButton one_Radio, two_Radio;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        // Add Locations to the ListView
        if (rs3 = Environment.isRS3())
            locationsListView.getItems().addAll("Deep Sea", "Draynor", "Lumbridge", "Menaphos Port", "Menaphos VIP", "Al Kharid", "Barbarian Village", "Baxtorian Falls", "Catherby", "Piscatoris", "Fishing Guild", "Mobilising Armies", "Shilo Village (beta)");
         else
            locationsListView.getItems().addAll("Draynor", "Lumbridge", "Barbarian Village", "Baxtorian Falls", "Al Kharid", "Catherby", "Piscatoris", "Fishing Guild", "Shilo Village (beta)");


        locationsListView.setOnMouseClicked(getLocationClickAction());
        fishListView.setOnMouseClicked(getfishListClickAction());

        usePreset_CB.setOnAction(getUsePreset_CBAction());

        useDepositBox_CB.setOnAction(useDepositBox_CBAction());
        powerFish_CB.setOnAction(powerFish_CBAction());

        startButton.setOnAction(getStartButtonAction());

        // If the bot is OSRS, disable preset option
        usePreset_CB.setDisable(!rs3);
    }

    public FisherFXController(ApexFisher bot) {
        super(bot);
        this.bot = bot;
    }

    private EventHandler<MouseEvent> getLocationClickAction() {
        return event -> {
            fishListView.getItems().clear();
            useDepositBox_CB.setDisable(false);
            usePreset_CB.setDisable(!rs3);

            startButton.setDisable(true);
            try {
                switch (locationsListView.getSelectionModel().getSelectedItem().toString()) {
                    case "Deep Sea":
                        fishListView.getItems().add("Swarm");
                        break;

                    case "Draynor":
                        fishListView.getItems().add("Shrimp/Anchovies");
                        fishListView.getItems().add("Sardines/Herring");
                        break;

                    case "Lumbridge":
                        fishListView.getItems().add("Trout/Salmon");
                        fishListView.getItems().add("Pike");
                        fishListView.getItems().add("Rainbow");
                        if(!rs3){
                            useDepositBox_CB.setDisable(true);
                            powerFish_CB.setSelected(true);
                        }

                        break;

                    case "Menaphos Port":
                        fishListView.getItems().add("Desert Sole/Beltfish/Catfish");
                        break;

                    case "Menaphos VIP":
                        fishListView.getItems().add("Desert Sole/Beltfish/Catfish");
                        break;

                    case "Al Kharid":
                        fishListView.getItems().add("Shrimp/Anchovies");
                        fishListView.getItems().add("Sardines/Herring");
                        break;

                    case "Barbarian Village":
                        fishListView.getItems().add("Trout/Salmon");
                        fishListView.getItems().add("Pike");
                        fishListView.getItems().add("Rainbow");
                        break;

                    case "Baxtorian Falls":
                        fishListView.getItems().add("Leaping trout/salmon/sturgeon");
                        break;

                    case "Catherby":
                        fishListView.getItems().add("Shrimp/Anchovies");
                        fishListView.getItems().add("Sardines/Herring");
                        fishListView.getItems().add("Mackerel/Cod/Bass");
                        fishListView.getItems().add("Lobster");
                        fishListView.getItems().add("Tuna/Swordfish");
                        fishListView.getItems().add("Shark");
                        break;

                    case "Piscatoris":
                        fishListView.getItems().add("Monkfish");
                        fishListView.getItems().add("Tuna/Swordfish");
                        break;

                    case "Fishing Guild":
                        fishListView.getItems().add("Mackerel/Cod/Bass");
                        fishListView.getItems().add("Lobster");
                        fishListView.getItems().add("Tuna/Swordfish");
                        fishListView.getItems().add("Shark");
                        if(!rs3)
                            fishListView.getItems().add("Minnow");
                        break;

                    case "Bhurg De Rott":
                        fishListView.getItems().add("Sharks");
                        break;

                    case "Mobilising Armies":
                        fishListView.getItems().add("Shrimp/Anchovies");
                        fishListView.getItems().add("Sardines/Herring");
                        fishListView.getItems().add("Mackerel/Cod/Bass");
                        fishListView.getItems().add("Lobster");
                        fishListView.getItems().add("Tuna/Swordfish");
                        break;

                    case "Shilo Village (beta)":
                        fishListView.getItems().add("Trout/Salmon");
                        fishListView.getItems().add("Pike/Eels");
                        fishListView.getItems().add("Rainbow");
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
    }

    private EventHandler<MouseEvent> getfishListClickAction() {
        return event -> {
            startButton.setDisable(false);

            switch(fishListView.getSelectionModel().getSelectedItem()){
                case "Swarm":
                    useDepositBox_CB.setDisable(false);
                    useDepositBox_CB.setSelected(true);

                    usePreset_CB.setSelected(false);
                    usePreset_CB.setDisable(true);
                    break;
                default:
                    if(locationsListView.getSelectionModel().getSelectedItem().equals("Lumbridge") && !rs3)
                        useDepositBox_CB.setDisable(true);
                    else
                        useDepositBox_CB.setDisable(false);
                    usePreset_CB.setDisable(!rs3);
                    break;
            }
        };
    }

    public EventHandler<ActionEvent> getStartButtonAction() {
        return event -> {

            String location = "", fish = "";

            bot.fishLocation = FishLocation.GENERIC;

            try {
                location = locationsListView.getSelectionModel().getSelectedItem();
                fish = fishListView.getSelectionModel().getSelectedItem();
            } catch (Exception e) {
                System.out.println("Make sure you have both Fish and Location selected.");
            }

            if (location != null && fish != null) {
                try {
                    bot.equipment = new ArrayList<>();
                    bot.equipmentAccessory = new ArrayList<>();
                    switch (fish) {
                        case "Shrimp/Anchovies":
                            if(rs3)
                                bot.fishAction = "Net";
                            else
                                bot.fishAction = "Small Net";
                            bot.fishSpotAction = "Bait";
                            bot.equipment.add("Small fishing net");
                            bot.possibleFishCountFromLocation = 2;
                            break;
                        case "Sardines/Herring":
                            bot.fishAction = "Bait";
                            bot.fishSpotAction = "Bait";
                            bot.equipment.add("Fishing rod");
                            bot.equipmentAccessory.add("Fishing bait");
                            bot.possibleFishCountFromLocation = 2;
                            break;
                        case "Trout/Salmon":
                            bot.fishAction = "Lure";
                            bot.fishSpotAction = "Lure";
                            bot.equipment.add("Fly fishing rod");
                            bot.equipmentAccessory.add("Feather");
                            bot.possibleFishCountFromLocation = 2;
                            break;
                        case "Leaping trout/salmon/sturgeon":
                            bot.fishAction = "Use-rod";
                            bot.fishSpotAction = "Use-rod";
                            bot.equipment.add("Barbarian rod");
                            bot.equipmentAccessory.add("Barbarian");
                            bot.possibleFishCountFromLocation = 3;
                            break;
                        case "Pike":
                            bot.fishAction = "Bait";
                            bot.fishSpotAction = "Lure";
                            bot.equipment.add("Fishing rod");
                            bot.equipmentAccessory.add("Fishing bait");
                            bot.possibleFishCountFromLocation = 1;
                            break;
                        case "Pike/Eels":
                            bot.fishAction = "Bait";
                            bot.fishSpotAction = "Lure";
                            bot.equipment.add("Fishing rod");
                            bot.equipmentAccessory.add("Fishing bait");
                            bot.possibleFishCountFromLocation = 2;
                            break;
                        case "Mackerel/Cod/Bass":
                            if(rs3)
                                bot.fishAction = "Net";
                            else
                                bot.fishAction = "Big Net";
                            bot.fishSpotAction = "Harpoon";
                            bot.equipment.add("Big fishing net");
                            bot.possibleFishCountFromLocation = 3;
                            break;
                        case "Rainbow":
                            bot.fishAction = "Lure";
                            bot.fishSpotAction = "Lure";
                            bot.equipment.add("Fly fishing rod");
                            bot.equipmentAccessory.add("Stripy feather");
                            bot.possibleFishCountFromLocation = 1;
                            break;
                        case "Lobster":
                            bot.fishAction = "Cage";
                            bot.fishSpotAction = "Cage";
                            bot.equipment.add("Lobster pot");
                            bot.possibleFishCountFromLocation = 1;
                            break;
                        case "Monkfish":
                            bot.fishAction = "Net";
                            bot.fishSpotAction = "Harpoon";
                            bot.equipment.add("Small fishing net");
                            bot.possibleFishCountFromLocation = 1;
                            break;
                        case "Tuna/Swordfish":
                            switch (location) {
                                case "Piscatoris":
                                    bot.fishAction = "Harpoon";
                                    bot.fishSpotAction = "Net";
                                    bot.equipment.add("Harpoon");
                                    break;

                                default:
                                    bot.fishAction = "Harpoon";
                                    bot.fishSpotAction = "Cage";
                                    bot.equipment.add("Harpoon");
                                    break;

                            }
                            bot.possibleFishCountFromLocation = 2;
                            break;
                        case "Shark":
                            if(location.equals("Catherby") && !rs3)
                                bot.fishSpotAction = "Big Net";
                            else
                                bot.fishSpotAction = "Net";

                            bot.fishAction = "Harpoon";

                            bot.equipment.add("Harpoon");
                            bot.possibleFishCountFromLocation = 1;
                            break;
                        case "Desert Sole/Beltfish/Catfish":
                            bot.fishAction = "Bait";
                            bot.fishSpotAction = "Bait";
                            bot.equipment.add("Fishing rod");
                            bot.equipmentAccessory.add("Fishing bait");
                            bot.possibleFishCountFromLocation = 3;
                            break;
                        case "Swarm":
                            bot.fishSpotName = "Swarm";
                            bot.fishAction = "Net";
                            bot.fishSpotAction = "Net";
                            bot.possibleFishCountFromLocation = 12;
                            break;
                        case "Minnow":
                            bot.fishAction = "Small Net";
                            bot.fishSpotAction = "Small Net";
                            bot.equipment.add("Small fishing net");
                            bot.possibleFishCountFromLocation = 1;
                            break;
                    }

                    switch (location) {
                        case "Deep Sea":
                            bot.fishLocation = FishLocation.DEEP_SEA;
                            switch(fish){
                                case "Swarm":
                                    bot.bankArea = Areas.DEEP_SEA_SWARM_DEPOSITBOX_AREA;
                                    bot.fishAreas.add(Areas.DEEP_SEA_SWARM_FISH_AREA);
                                    bot.fishSpotAreas.add(Areas.DEEP_SEA_SWARM_FISH_AREA);
                                    break;
                            }
                            break;
                        case "Lumbridge":
                            bot.bankArea = Areas.LUMB_BANK_AREA;
                            bot.fishAreas.add(Areas.LUMB_FISH_AREA);
                            bot.fishSpotAreas.add(Areas.LUMB_FISH_SPOT_AREA);
                            break;
                        case "Draynor":
                            bot.bankArea = Areas.DRAY_BANK_AREA;
                            bot.fishAreas.add(Areas.DRAY_FISH_AREA);
                            bot.fishSpotAreas.add(Areas.DRAY_FISH_AREA);
                            break;
                        case "Menaphos Port":
                            bot.bankArea = Areas.MENAPHOS_PORT_DEPOSITBOX_AREA;
                            bot.fishAreas.add(Areas.MENAPHOS_PORT_FISH_AREA);
                            bot.fishSpotAreas.add(Areas.MENAPHOS_PORT_FISH_AREA);
                            bot.fishLocation = FishLocation.MENAPHOS;
                            break;
                        case "Menaphos VIP":
                            bot.bankArea = Areas.MENAPHOS_VIP_BANK_AREA;
                            bot.fishAreas.add(Areas.MENAPHOS_VIP_FISH_AREA);
                            bot.fishSpotAreas.add(Areas.MENAPHOS_VIP_FISH_AREA);
                            bot.fishLocation = FishLocation.MENAPHOS;
                            break;
                        case "Al Kharid":
                            if(rs3) {
                                bot.bankArea = Areas.ALKHARID_RS3_BANK_AREA;
                                bot.fishAreas.add(Areas.ALKHARID_RS3_FISH_AREA);
                                bot.fishSpotAreas.add(Areas.ALKHARID_RS3_FISH_AREA);
                            }
                            else {
                                bot.bankArea = Areas.ALKHARID_OSRS_BANK_AREA;
                                bot.fishAreas.add(Areas.ALKHARID_OSRS_FISH_AREA);
                                bot.fishSpotAreas.add(Areas.ALKHARID_OSRS_FISH_AREA);
                            }
                            break;
                        case "Barbarian Village":
                            bot.bankArea = Areas.EDGE_BANK_AREA;
                            bot.fishAreas.add(Areas.BARB_FISH_AREA);
                            bot.fishSpotAreas.add(Areas.BARB_FISH_AREA);
                            break;
                        case "Baxtorian Falls":
                            bot.bankArea = Areas.BARB_ASSAULT_BANK_AREA;
                            bot.fishAreas.add(Areas.BAXTORIAN_FISH_AREA);
                            bot.fishSpotAreas.add(Areas.BAXTORIAN_FISH_AREA);
                            bot.fishLocation = FishLocation.BAXTORIAN_FALLS;
                            break;
                        case "Catherby":
                            if (bot.isRS3)
                                bot.bankArea = Areas.CATH_RS3_BANK_AREA;
                            else
                                bot.bankArea = Areas.CATH_OSRS_BANK_AREA;
                            bot.fishAreas.add(Areas.CATH_FISH_AREA);
                            bot.fishSpotAreas.add(Areas.CATH_FISH_AREA);
                            break;
                        case "Piscatoris":
                            bot.bankArea = Areas.PISCATORIS_BANK_AREA;
                            bot.fishAreas.add(Areas.PISCATORIS_FISH_AREA);
                            bot.fishSpotAreas.add(Areas.PISCATORIS_FISH_AREA);
                            break;
                        case "Fishing Guild":
                            if (rs3)
                                bot.bankArea = Areas.GUILD_BANK_RS3_AREA;
                            else
                                bot.bankArea = Areas.GUILD_BANK_OSRS_AREA;
                            if(fish.equals("Minnow")){
                                bot.fishAreas.add(Areas.GUILD_MINNOW_FISH_EAST_AREA);
                                bot.fishAreas.add(Areas.GUILD_MINNOW_FISH_WEST_AREA);
                                bot.fishSpotAreas.add(Areas.GUILD_MINNOW_FISH_EAST_AREA);
                                bot.fishSpotAreas.add(Areas.GUILD_MINNOW_FISH_WEST_AREA);
                                bot.fishLocation = FishLocation.FISHGUILD_MINNOW;
                            } else {
                                bot.fishAreas.add(Areas.GUILD_DOCK1_FISH_AREA);
                                bot.fishAreas.add(Areas.GUILD_DOCK2_FISH_AREA);
                                bot.fishSpotAreas.add(Areas.GUILD_DOCK1_FISH_SPOT_AREA);
                                bot.fishSpotAreas.add(Areas.GUILD_DOCK2_FISH_SPOT_AREA);
                                bot.fishLocation = FishLocation.FISHGUILD;
                            }
                            break;
                        case "Mobilising Armies":
                            bot.bankArea = Areas.ARMIES_BANK_AREA;
                            bot.fishAreas.add(Areas.ARMIES_FISH_AREA);
                            bot.fishSpotAreas.add(Areas.ARMIES_FISH_AREA);
                            break;
                        case "Shilo Village (beta)":
                            bot.bankArea = Areas.SHILO_BANK_AREA;
                            bot.fishAreas.add(Areas.SHILO_FISH_AREA);
                            bot.fishSpotAreas.add(Areas.SHILO_FISH_AREA);
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                super.getStart_BTEvent();

                bot.useDepositBox = useDepositBox_CB.isSelected();

                bot.disableEquipmentCheck = disableEquipmentCheck_CB.isSelected();

                bot.isPowerFishing = powerFish_CB.isSelected();

                bot.locationNameString = location;
                bot.fishNameString = fish;

                if (bot.usingPreset = (usePreset_CB.isSelected() && !usePreset_CB.isDisabled()))
                    if (one_Radio.isSelected())
                        bot.presetNumber = 1;
                    else
                        bot.presetNumber = 2;

                // Create temp arraylist and combine equipment and equipmentAccessory lists
                List<String> temp = new ArrayList<>();
                temp.addAll(bot.equipment);
                temp.addAll(bot.equipmentAccessory);

                // Create String array so it can later be used in BankInventoryTask
                // These items will not be banked
                bot.allEquipment = temp.toArray(new String[temp.size()]);
                temp = null;

                startButton.textProperty().set("Resume");
                bot.guiWait = false;
            }
        };
    }

    private EventHandler<ActionEvent> getUsePreset_CBAction() {
        return event -> {
            one_Radio.setDisable(!usePreset_CB.isSelected());
            two_Radio.setDisable(!usePreset_CB.isSelected());

            useDepositBox_CB.setSelected(false);
            powerFish_CB.setSelected(false);
        };
    }

    private EventHandler<ActionEvent> useDepositBox_CBAction() {
        return event -> {
            usePreset_CB.setSelected(false);
            one_Radio.setDisable(!usePreset_CB.isSelected());
            two_Radio.setDisable(!usePreset_CB.isSelected());

            powerFish_CB.setSelected(false);
        };
    }

    private EventHandler<ActionEvent> powerFish_CBAction() {
        return event -> {
            usePreset_CB.setSelected(false);
            one_Radio.setDisable(!usePreset_CB.isSelected());
            two_Radio.setDisable(!usePreset_CB.isSelected());

            useDepositBox_CB.setSelected(false);
        };
    }
}