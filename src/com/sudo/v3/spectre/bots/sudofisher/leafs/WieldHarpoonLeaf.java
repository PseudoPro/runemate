package com.sudo.v3.spectre.bots.sudofisher.leafs;

import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudofisher.ApexFisher;
import com.sudo.v3.spectre.statics.UpdateUI;

public class WieldHarpoonLeaf extends LeafTask {

    private ApexFisher bot;

    public WieldHarpoonLeaf(ApexFisher bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        bot.usingDragonHarpoon = true;
        if(Bank.isOpen()) {
            UpdateUI.currentTask("Closing bank to equip Dragon harpoon", bot);
            Bank.close();
        }
        else{
            UpdateUI.currentTask("Attempting to equip Dragon harpoon", bot);
            SpriteItem harpoon = Inventory.newQuery().names("Dragon harpoon").results().first();
            if (harpoon != null)
                harpoon.interact("Wield");
        }
    }
}
