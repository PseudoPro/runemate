package com.sudo.v3.spectre.bots.sudofisher.leafs;

import com.runemate.game.api.hybrid.local.hud.interfaces.DepositBox;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudofisher.ApexFisher;
import com.sudo.v3.spectre.bots.sudofisher.enums.FishLocation;
import com.sudo.v3.spectre.api.game.hud.SudoCamera;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 12/24/2016.
 */
public class UseDepositBoxLeaf extends LeafTask {
    private ApexFisher bot;

    public UseDepositBoxLeaf(ApexFisher bot) {
        this.bot = bot;
    }

    @Override
    public void execute() {
        bot.currentlyDepositing = true;

        if (bot.locatableBank != null && bot.locatableBank.getVisibility() < .8)
            SudoCamera.ConcurrentlyTurnToWithYaw(bot.locatableBank);

        if (DepositBox.isOpen()) {
            UpdateUI.currentTask("Depositing items", bot);
            DepositBox.depositAllExcept(bot.allEquipment);
        } else {
            if (bot.fishLocation == FishLocation.DEEP_SEA) {
                UpdateUI.currentTask("Opening Deposit Box", bot);
                if(bot.locatableBank.interact("Deposit-All"))
                    Execution.delayUntil(DepositBox::isOpen, 1000, 3000);
            } else if (bot.fishLocation == FishLocation.FISHGUILD) {
                UpdateUI.currentTask("Opening Deposit Box", bot);
                if(bot.locatableBank.interact("Deposit"))
                    Execution.delayUntil(DepositBox::isOpen, 1000, 3000);
            } else {
                UpdateUI.currentTask("Opening Deposit Box", bot);
                DepositBox.open();
            }
        }
    }
}