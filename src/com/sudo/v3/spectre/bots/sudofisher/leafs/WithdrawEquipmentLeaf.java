package com.sudo.v3.spectre.bots.sudofisher.leafs;

import com.runemate.game.api.hybrid.util.calculations.Random;
import com.sudo.v3.spectre.bots.sudofisher.ApexFisher;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.script.framework.tree.LeafTask;

import java.util.Arrays;

/**
 * Created by SudoPro on 11/26/2016.
 */
public class WithdrawEquipmentLeaf extends LeafTask {
    private ApexFisher bot;

    public WithdrawEquipmentLeaf(ApexFisher bot) {
        this.bot = bot;
    }

    @Override
    public void execute() {
        String[] missingEquipment = bot.getMissingEquipment().toArray(new String[0]);
        String missingAccessory = bot.getMissingAccessory();
        if (!bot.isRS3 && missingEquipment.length > 0) {
            if (Bank.containsAnyOf(missingEquipment)) {
                for (int i = 0; i < missingEquipment.length; i++) {
                    UpdateUI.debug(missingEquipment[i]);
                    if (Bank.contains(missingEquipment[i])) {
                        UpdateUI.currentTask("Withdrawing the equipment " + missingEquipment[i], bot);
                        Bank.withdraw(missingEquipment[i], 1);

                        if (missingEquipment[i].equals("Dragon harpoon"))
                            bot.usingDragonHarpoon = true;
                        break;
                    }
                }
            } else {
                UpdateUI.currentTask("Bot stopping, couldn't find any of " + Arrays.toString(missingEquipment), bot);
                bot.stop("Bot stopping, couldn't find any of " + Arrays.toString(missingEquipment));
            }
        } else if (!missingAccessory.equals("")) {
            if (missingAccessory.equals("Barbarian")) {
                if(Bank.contains("Feather")){
                    Bank.withdraw("Feather", Random.nextInt(100, 1000));
                } else if (Bank.contains("Fishing bait"))
                    Bank.withdraw("Fishing bait", Random.nextInt(100, 1000));
                else {
                    bot.setCurrentTask("Bot stopping, couldn't find \"Feathers\" or \"Bait\".");
                    bot.stop("Bot stopping, couldn't find \"Feathers\" or \"Bait\".");
                }
            } else if (Bank.containsAnyOf(missingAccessory)) {
                UpdateUI.currentTask("Withdrawing the equipment accessory " + missingAccessory, bot);
                Bank.withdraw(missingAccessory, 100 + (((int) (Math.random() * 18)) * 50));
            } else {
                UpdateUI.currentTask("Bot stopping, couldn't find " + missingAccessory, bot);
                bot.stop("Couldn't find any " + missingAccessory);
            }
        }
    }
}