package com.sudo.v3.spectre.bots.sudofisher.leafs;

import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.sudo.v3.spectre.bots.sudofisher.ApexFisher;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.script.framework.tree.LeafTask;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by SudoPro on 11/23/2016.
 */
public class DepositAllLeaf extends LeafTask {
    private ApexFisher bot;

    List<String> temp = new ArrayList<>();

    public DepositAllLeaf(ApexFisher bot) {
        this.bot = bot;
    }

    @Override
    public void execute() {
        if (bot.usingPreset) {
            UpdateUI.currentTask("Loading bank preset", bot);
            Bank.loadPreset(bot.presetNumber);
        } else {
            UpdateUI.currentTask("Depositing items", bot);

            if(bot.fishAction.equals("Harpoon") && Inventory.contains("Dragon harpoon")) {
                if(temp.size() <= 0) {
                    temp.addAll(Arrays.asList(bot.allEquipment));
                    temp.add("Dragon Harpoon");
                }
                Bank.depositAllExcept(temp.toArray(new String[temp.size()]));
            } else {
                Bank.depositAllExcept(bot.allEquipment);
            }

        }
    }
}