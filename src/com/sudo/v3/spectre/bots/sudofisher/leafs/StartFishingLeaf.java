package com.sudo.v3.spectre.bots.sudofisher.leafs;

import com.sudo.v3.spectre.bots.sudofisher.ApexFisher;
import com.sudo.v3.spectre.bots.sudofisher.enums.FishLocation;
import com.sudo.v3.spectre.api.game.entities.ApexPlayer;
import com.sudo.v3.spectre.api.game.hud.SudoCamera;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;

/**
 * Created by SudoPro on 11/23/2016.
 */
public class StartFishingLeaf extends LeafTask
{
    private ApexFisher bot;

    public StartFishingLeaf(ApexFisher bot)
    {
        this.bot = bot;
    }

    @Override
    public void execute()
    {
        UpdateUI.currentTask("Attempting to start fishing", bot);
        if (bot.fishingSpot.distanceTo(bot.player) > 6 || bot.fishingSpot.getVisibility() < 0.8)
            SudoCamera.ConcurrentlyTurnToWithYaw(bot.fishingSpot);

        if (bot.fishingSpot.interact(bot.fishAction))
            Execution.delayUntil(() -> bot.player.distanceTo(bot.fishingSpot) < 3 && (bot.fishLocation == FishLocation.DEEP_SEA ? ApexPlayer.isTargettingNameContaining(bot.fishSpotName) : ApexPlayer.isTargettingNameContaining("fishing spot")), 2000, 5000);
    }
}
