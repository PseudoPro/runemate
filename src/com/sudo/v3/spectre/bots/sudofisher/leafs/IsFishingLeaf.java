package com.sudo.v3.spectre.bots.sudofisher.leafs;

import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.queries.results.SpriteItemQueryResults;
import com.runemate.game.api.hybrid.region.Banks;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.sudo.v3.spectre.bots.sudofisher.ApexFisher;
import com.sudo.v3.spectre.api.game.hud.SudoCamera;
import com.sudo.v3.spectre.bots.sudofisher.enums.FishLocation;
import com.sudo.v3.spectre.common.playersense.ApexPlayerSense;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;

/**
 * Created by SudoPro on 11/25/2016.
 */
public class IsFishingLeaf extends LeafTask {
    private ApexFisher bot;

    public IsFishingLeaf(ApexFisher bot) {
        this.bot = bot;
    }

    @Override
    public void execute() {
        SpriteItemQueryResults items = Inventory.getItems();
        if (items != null && items.size() > ApexPlayerSense.Key.HOVER_OVER_DEPOSIT_HIGH_INVENTORY_COUNT.getAsInteger()) {
            if (bot.useDepositBox) {
                bot.locatableBank = GameObjects.newQuery().names("Bank Deposit Box", "Bank deposit box", "Deposit box", "Magical net").actions("Deposit").within(bot.bankArea).results().nearest();
            } else {
                bot.locatableBank = Banks.newQuery().within(bot.bankArea).results().nearest();
            }

            if (bot.locatableBank != null) {
                UpdateUI.currentTask("Inventory nearly full, attempting to hover over deposit area...", bot);
                if (bot.locatableBank.getVisibility() < 0.9 || bot.locatableBank.distanceTo(bot.player) > 4)
                    SudoCamera.ConcurrentlyTurnToWithYaw(bot.locatableBank);
                else
                    bot.locatableBank.hover();
            }
        } else {
            if(bot.fishLocation == FishLocation.FISHGUILD_MINNOW && Random.nextInt(0, 100) <= ApexPlayerSense.Key.MINNOW_HOVER_NEXT_SPOT_CHANCE.getAsInteger()){
                if(bot.fishingSpot != null && bot.fishingSpot.isValid()){
                    if(bot.minnowNextCoordinateMap.get(bot.fishingSpot.getPosition()).hover()){
                        UpdateUI.currentTask("Hovering over next fishing spot.", bot);
                        Execution.delay(50);
                    }
                }
            }

            UpdateUI.currentTask("Currently fishing, delaying next action...", bot);
            Execution.delay(2000, 12500);
        }
    }
}