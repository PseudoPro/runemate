package com.sudo.v3.spectre.bots.sudofisher.leafs;

import com.runemate.game.api.script.Execution;
import com.sudo.v3.spectre.common.playersense.ApexPlayerSense;
import com.sudo.v3.spectre.bots.sudofisher.ApexFisher;
import com.sudo.v3.spectre.bots.sudofisher.enums.TraversalLocation;
import com.sudo.v3.spectre.common.navigation.Traverse;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.script.framework.tree.LeafTask;

/**
 * Created by SudoPro on 11/23/2016.
 */
public class TraversalLeaf extends LeafTask {
    private ApexFisher bot;

    public TraversalLeaf(ApexFisher bot) {
        this.bot = bot;
    }

    @Override
    public void execute() {
        if (bot.traversalLocation == TraversalLocation.bank) {
            UpdateUI.currentTask("Traveling to bank", bot);
            Traverse.webPath(bot.nav, bot.bankArea);
        } else if (bot.traversalLocation == TraversalLocation.fishingArea) {
            UpdateUI.currentTask("Traveling to fishing area", bot);
            Traverse.webPath(bot.nav, bot.fishAreas.get(0));
        } else if (bot.traversalLocation == TraversalLocation.fishingSpot) {
            if (bot.seenFishSpots.size() > 0) {
                UpdateUI.currentTask("Traveling to previously seen fishing spot", bot);
                Traverse.smartRegionPath(bot.nav, new Area.Circular(bot.seenFishSpots.get((int) (Math.random() * bot.seenFishSpots.size())), 4));
            } else {
                UpdateUI.currentTask("Attempting to find new fishing spot", bot);
                Traverse.smartRegionPath(bot.nav, bot.fishAreas.get(0));
            }
        }

        // Delay after each attempt to traverse
        Execution.delay(500, 750);
        Execution.delayWhile(bot.player::isMoving, ApexPlayerSense.Key.TRAVERSAL_DELAY.getAsInteger());
    }
}