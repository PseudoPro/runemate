package com.sudo.v3.spectre.bots.apexcrabber;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.GroundItem;
import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.local.Quest;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.queries.results.LocatableEntityQueryResults;
import com.runemate.game.api.hybrid.queries.results.SpriteItemQueryResults;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBot;
import com.sudo.v3.spectre.bots.apexcrabber.branches.*;
import com.sudo.v3.spectre.bots.apexcrabber.enums.BotLocation;
import com.sudo.v3.spectre.bots.apexcrabber.enums.Playstyle;
import com.sudo.v3.spectre.bots.apexcrabber.enums.Potion;
import com.sudo.v3.spectre.bots.apexcrabber.enums.TraversalLocation;
import com.sudo.v3.spectre.bots.apexcrabber.leafs.*;
import com.sudo.v3.spectre.bots.apexcrabber.statics.CrabAreas;
import com.sudo.v3.spectre.bots.apexcrabber.statics.SpotAreas;
import com.sudo.v3.spectre.bots.apexcrabber.ui.CrabFXGui;
import com.sudo.v3.spectre.bots.apexcrabber.ui.CrabInfoUI;
import com.sudo.v3.spectre.api.util.SudoTimer;
import com.sudo.v3.spectre.common.leafs.InteractLeaf;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.sudo.v3.ui.Info;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SudoPro on 11/23/2016.
 */
public class ApexCrabber extends SudoBot {
    private CrabInfoUI infoUI;
    private CrabFXGui configUI;

    public Area refreshArea = CrabAreas.ZEAH_SHORE_SANDCRAB_REFRESH_AREA, crabArea = CrabAreas.ZEAH_SHORE_SANDCRAB_AREA, randomRefreshArea = null, bankArea = null;
    public String crabName = "Sand Crab", shellName = "Sandy rocks", foodName = "", thrownEquipmentName = "";
    public String ectoTokenName = "Ecto-token", energyBarrierName = "Energy Barrier", energyBarrierAction = "Pay-toll(2-Ecto)";
    public Npc crab = null, crabShell = null, islandNpc = null;
    public GameObject bankObj = null, bankDoorObj = null;
    public GroundItem nearestThrownEquipment;
    public LocatableEntityQueryResults<GroundItem> thrownEquipments;
    public int crabShellCheckCount = 0, playerRunCount = 2, potionQuantity = 5, foodQuantity = 10;
    public boolean worldHop = false, resetRun = false, bankingEnabled = false, usingPotion = false, usingFood = false, bankRun = false, stopIfNoPotion = false, pickupThrownEquipment = true, pickingUpThrown = false;
    public Boolean ghostAhoyComplete = null;
    public Quest ghostAhoyQuest = null;
    public Playstyle playStyle = Playstyle.SPOT_JUMP;
    public Potion potionType = Potion.SP_ATT_STR;
    public double healPercent = 50, userProvidedPercent = 50;
    public SpriteItem food;
    public TraversalLocation traversalLocation = TraversalLocation.CRAB_AREA;
    public BotLocation botLocation = BotLocation.ZEAH_SHORE;

    // An List of a list of coordinates
    // This is a double list because we could have multiple coordinates cover the same 'spots'
    public ArrayList<ArrayList<Coordinate>> spotLists = SpotAreas.sandCrabShoreSpots;

    // Normally contains the spot with the least amount of players
    public List<Coordinate> leastPlayerSpot = new ArrayList<>();

    // Coordinate that the bot will go back to in the case of Stand Still playstyle
    public Coordinate standStillCoord = null;

    // Timer for when to pick up thrown equipment
    public SudoTimer thrownPickupTimer = new SudoTimer(25000, 75000);

    // Branches
    private Root root = new Root(this);
    public IsCrabShellNull isCrabShellNull = new IsCrabShellNull(this);
    public IsStandStillPlaystyle isStandStillPlaystyle = new IsStandStillPlaystyle(this);
    public AttackBranch attackBranch = new AttackBranch(this);
    public DoesAvailableSpotContainPlayer doesAvailableSpotContainPlayer = new DoesAvailableSpotContainPlayer(this);
    public IsAnotherOnSpot isAnotherOnSpot = new IsAnotherOnSpot(this);
    public IsActivePlaystyle isActivePlaystyle = new IsActivePlaystyle(this);
    public IsAttacking isAttacking = new IsAttacking(this);
    public HealBranch healBranch = new HealBranch(this);
    public IsInBankArea isInBankArea = new IsInBankArea(this);
    public IsBankOpen isBankOpen = new IsBankOpen(this);
    public IsRefillRequired isRefillRequired = new IsRefillRequired(this);
    public AreSkillsLow areSkillsLow = new AreSkillsLow(this);
    public IsThrownEquipmentAround isThrownEquipmentAround = new IsThrownEquipmentAround(this);
    public InventoryHasEquipment inventoryHasEquipment = new InventoryHasEquipment(this);
    public IsCrabSpotAvailable isCrabSpotAvailable = new IsCrabSpotAvailable(this);

    // Leaf Tasks
    public CrabNullLeaf crabNullLeaf = new CrabNullLeaf(this);
    public CurrentlyAttackingLeaf currentlyAttackingLeaf = new CurrentlyAttackingLeaf(this);
    public WorldHopLeaf worldHopLeaf = new WorldHopLeaf(this);
    public AttackCrabLeaf attackCrabLeaf = new AttackCrabLeaf(this);
    public HandleLeastPlayerSpotLeaf handleLeastPlayerSpotLeaf = new HandleLeastPlayerSpotLeaf(this);
    public HealLeaf healLeaf = new HealLeaf(this);
    public NoFoodLeaf noFoodLeaf = new NoFoodLeaf(this);
    public WaitingForRespawnLeaf waitingForRespawnLeaf = new WaitingForRespawnLeaf(this);
    public CheckShellFailSafeLeaf checkShellFailSafeLeaf = new CheckShellFailSafeLeaf(this);
    public TraversalLeaf traversalLeaf = new TraversalLeaf(this);
    public TriggerRefreshLeaf triggerRefreshLeaf = new TriggerRefreshLeaf(this);
    public SuccessfulRefreshLeaf successfulRefreshLeaf = new SuccessfulRefreshLeaf(this);
    public DisableBankRunLeaf disableBankRunLeaf = new DisableBankRunLeaf(this);
    public RefillInventoryLeaf refillInventoryLeaf = new RefillInventoryLeaf(this);
    public UsePotionLeaf usePotionLeaf = new UsePotionLeaf(this);
    public WieldThrownEquipmentLeaf wieldThrownEquipmentLeaf = new WieldThrownEquipmentLeaf(this);
    public InteractLeaf pickupThrownEquipmentLeaf = new InteractLeaf(this, nearestThrownEquipment, thrownEquipmentName, "Take");

    // Default Constructor
    public ApexCrabber() {
        // Give spectre client this class as the embeddableUI
        setEmbeddableUI(this);

        openBankLeaf.setGenericBank(true);
    }

    @Override
    public void onStart(String... args) {
        getEventDispatcher().addListener(this);

        checkSpecialTimer.start();
        thrownPickupTimer.start();

        super.onStart();
    }

    @Override
    public String getBotName() {
        return "ApexCrabber";
    }

    @Override
    public ObjectProperty<? extends Node> botInterfaceProperty() {
        if (botInterfaceProperty == null) {
            botInterfaceProperty = new SimpleObjectProperty<>(configUI = new CrabFXGui(this));
            guiList = new ArrayList<Node>();
            //guiList.add(_sbGUI = new SudoBuddyFXGui(this));
            guiList.add(configUI);
            guiList.add(infoUI = new CrabInfoUI(this));

        }
        return botInterfaceProperty;
    }

    @Override
    public TreeTask createRootTask() {
        return root;
    }

    @Override
    public void updateInfo() {

        // UpdateUI all current values in the XPInfo List
        XPInfoMap.values().forEach((xpInfo) -> xpInfo.update());

//        displayInfoMap.put("Fish Count: ", Integer.toString(fishCount) +
//                " (" + Integer.toString((int) CommonMath.rate(TimeUnit.HOURS, STOPWATCH.getRuntime(), fishCount)) + "/Hour)");
//        displayInfoMap.put("GP: ",Integer.toString(grossIncome) +
//                " (" + Integer.toString((int)CommonMath.rate(TimeUnit.HOURS, STOPWATCH.getRuntime(), grossIncome)) + "/Hour)");

        try {
            info = new Info(XPInfoMap,           // XP Info
                    displayInfoMap,
                    STOPWATCH.getRuntimeAsString(),        // Total Runtime
                    currentTaskString,                     // Current Task
                    abTaskString);                         // antiban Task

        } catch (Exception e) {
            e.printStackTrace();
        }

        Platform.runLater(() -> infoUI.update());
    }

    // Determines if the player's inventory is lacking potions or food
    public boolean isRefillInventoryRequired() {
        if (bankingEnabled) {
            if (potionType == Potion.SP_ATT_STR) {
                SpriteItemQueryResults foodResults = Inventory.newQuery().actions("Eat").results();
                SpriteItemQueryResults attackPotionResults = Inventory.newQuery().names(Potion.SP_ATT.getPattern()).results();
                SpriteItemQueryResults strengthPotionResults = Inventory.newQuery().names(Potion.SP_STR.getPattern()).results();

                return (usingFood && foodResults.isEmpty()) || (usingPotion && (attackPotionResults.isEmpty() || strengthPotionResults.isEmpty()));
            } else if (potionType == Potion.ATT_STR) {
                SpriteItemQueryResults foodResults = Inventory.newQuery().actions("Eat").results();
                SpriteItemQueryResults attackPotionResults = Inventory.newQuery().names(Potion.ATT.getPattern()).results();
                SpriteItemQueryResults strengthPotionResults = Inventory.newQuery().names(Potion.STR.getPattern()).results();

                return (usingFood && foodResults.isEmpty()) || (usingPotion && (attackPotionResults.isEmpty() || strengthPotionResults.isEmpty()));
            } else {
                SpriteItemQueryResults foodResults = Inventory.newQuery().actions("Eat").results();
                SpriteItemQueryResults potionResults = Inventory.newQuery().names(potionType.getPattern()).results();

                return (usingFood && foodResults.isEmpty()) || (usingPotion && potionResults.isEmpty());
            }
        }
        return false;
    }

    // Attempts to determine if the player is in one of the crab spots
    public boolean isPlayerOnCrabSpot() {
        if (player != null) {
            if (playStyle != Playstyle.STAND_STILL) {
                for (ArrayList<Coordinate> spotList : spotLists) {
                    if (spotList.contains(player.getPosition()))
                        return true;
                }
            } else
                return standStillCoord.distanceTo(player.getPosition()) < 1;
        }
        return false;
    }

    public Area getRandomSpotArea() {
        ArrayList<ArrayList<Coordinate>> lists = new ArrayList<ArrayList<Coordinate>>();
        if (isPlayerOnCrabSpot()) {
            spotLists.forEach(i -> lists.add(i));
            lists.remove(getLocalCrabSpot());
        }

        UpdateUI.debug(Integer.toString(lists.size()));
        return getNearestCrabSpot(lists);
    }

    private Area getNearestCrabSpot(ArrayList<ArrayList<Coordinate>> lists) {
        Coordinate coord = null;
        double distance = 500;

        for (ArrayList<Coordinate> i : lists) {
            for (Coordinate c : i) {
                if (player.distanceTo(c) < distance) {
                    coord = c;
                    distance = player.distanceTo(c);
                }
            }
        }

        if (coord == null)
            return crabArea;
        else
            return new Area.Rectangular(coord, coord);
    }

    public ArrayList<Coordinate> getLocalCrabSpot() {
        for (int i = 0; i < spotLists.size(); i++) {
            for (int j = 0; j < spotLists.get(i).size(); j++) {
                if (player.getPosition() == spotLists.get(i).get(j))
                    return spotLists.get(i);
            }
        }

        return null;
    }
}