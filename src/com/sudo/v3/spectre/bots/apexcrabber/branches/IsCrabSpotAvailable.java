package com.sudo.v3.spectre.bots.apexcrabber.branches;

import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.queries.results.LocatableEntityQueryResults;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.statics.UpdateUI;

import java.util.regex.Pattern;

/**
 * Created by Joey on 3/15/2017.
 */
public class IsCrabSpotAvailable extends SudoBranchTask {
    private ApexCrabber bot;
    private LocatableEntityQueryResults<Player> playerQuery;

    public IsCrabSpotAvailable(ApexCrabber bot) {
        this.bot = bot;
    }

    @Override
    public boolean validate() {
        int spotSize = 500;
        validate = false;

        // See if there are any spots where there are NOT multiple players already there.
        for (int i = 0; i < bot.spotLists.size(); i++) {
            // If the player count around the nth spot has a lower count than the player run count
            if ((playerQuery = Players.newQuery().within(new Area.Circular(bot.spotLists.get(i).get(0), 3)).names(Pattern.compile("^(?!.*(" + bot.player.getName() + "))")).results()).size() < bot.playerRunCount) {
                validate = true;

                // If this spot's size is less than spotSize, then we have our current least populated spot
                if (playerQuery.size() < spotSize) {
                    bot.leastPlayerSpot = bot.spotLists.get(i);
                    spotSize = playerQuery.size();
                }
            }
        }

        UpdateUI.debug("Branch: IsCrabSpotAvailable: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        return bot.doesAvailableSpotContainPlayer;
    }

    @Override
    public TreeTask failureTask() {
        return bot.doesAvailableSpotContainPlayer;
    }

}