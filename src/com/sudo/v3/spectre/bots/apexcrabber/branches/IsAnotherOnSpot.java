package com.sudo.v3.spectre.bots.apexcrabber.branches;

import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.bots.apexcrabber.enums.Playstyle;
import com.sudo.v3.spectre.statics.UpdateUI;


/**
 * Created by Joey on 3/15/2017.
 */
public class IsAnotherOnSpot extends SudoBranchTask {
    private ApexCrabber bot;

    public IsAnotherOnSpot(ApexCrabber bot) {
        this.bot = bot;
    }

    @Override
    public boolean validate() {
        validate = Players.newQuery().within(new Area.Circular(bot.player.getPosition(), 3)).results().size() > bot.playerRunCount && bot.playStyle != Playstyle.ACTIVE;
        UpdateUI.debug("Branch: IsAnotherOnSpot: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        return bot.isStandStillPlaystyle;
    }

    @Override
    public TreeTask failureTask() {
        return bot.attackBranch;
    }
}