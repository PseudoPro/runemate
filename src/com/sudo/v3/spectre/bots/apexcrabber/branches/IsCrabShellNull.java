package com.sudo.v3.spectre.bots.apexcrabber.branches;

import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.bots.apexcrabber.enums.BotLocation;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Joey on 3/14/2017.
 */
public class IsCrabShellNull extends SudoBranchTask {
    private ApexCrabber bot;

    public IsCrabShellNull(ApexCrabber bot) {
        this.bot = bot;
    }

    @Override
    public boolean validate() {
        // Crab shell around the user
        bot.crabShell = Npcs.newQuery().names(bot.shellName).within(bot.crabArea).within(new Area.Circular(bot.player.getPosition(), bot.botLocation == BotLocation.FOSSIL_ISLAND ? 3 : 2)).results().nearest();

        validate = bot.crabShell != null;
        UpdateUI.debug("Branch: IsCrabShell!Null: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        UpdateUI.debug("Branch: CrabShellNearby(" + bot.crabShellCheckCount + "): " + (bot.player.distanceTo(bot.crabShell.getPosition()) < (bot.botLocation == BotLocation.FOSSIL_ISLAND ? 3 : 2)));

        if (bot.player.distanceTo(bot.crabShell.getPosition()) < (bot.botLocation == BotLocation.FOSSIL_ISLAND ? 3 : 2)) {
            // We want to check 3 times if the shell is around us and not turning into an attackable crab
            if (bot.crabShellCheckCount >= (bot.botLocation == BotLocation.FOSSIL_ISLAND ? 6 : 3)) {
                return bot.triggerRefreshLeaf;
            } else {
                return bot.checkShellFailSafeLeaf;
            }
        } else
            return bot.attackBranch;
    }

    @Override
    public TreeTask failureTask() {
        return bot.isActivePlaystyle;
    }
}