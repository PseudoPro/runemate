package com.sudo.v3.spectre.bots.apexcrabber.branches;

import com.runemate.game.api.hybrid.region.GroundItems;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.common.leafs.InteractLeaf;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by joeyw on 5/12/2017.
 */
public class IsThrownEquipmentAround extends SudoBranchTask {
    private ApexCrabber bot;

    public IsThrownEquipmentAround(ApexCrabber bot) {
        this.bot = bot;
    }

    @Override
    public boolean validate() {
        bot.thrownEquipments = GroundItems.newQuery().names(bot.thrownEquipmentName).filter(i -> i.distanceTo(bot.player) < 4).results();

        validate = !bot.thrownEquipments.isEmpty() && bot.thrownEquipments.nearest() != null;
        UpdateUI.debug("Branch: IsThrownEquipmentAround: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        bot.pickingUpThrown = true;
        bot.nearestThrownEquipment = bot.thrownEquipments.get(Random.nextInt(0, bot.thrownEquipments.size() - 1));
        bot.pickupThrownEquipmentLeaf.setObject(bot.nearestThrownEquipment);
        bot.pickupThrownEquipmentLeaf.setName(bot.thrownEquipmentName);
        return bot.pickupThrownEquipmentLeaf;
    }

    @Override
    public TreeTask failureTask() {
        bot.pickingUpThrown = false;
        bot.thrownPickupTimer.reset();
        return bot.inventoryHasEquipment;
    }
}