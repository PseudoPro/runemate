package com.sudo.v3.spectre.bots.apexcrabber.branches;

import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.ChatDialog;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by joeyw on 4/25/2017.
 */
public class IsBankOpen extends SudoBranchTask {
    private ApexCrabber bot;

    public IsBankOpen(ApexCrabber bot) {
        this.bot = bot;
    }

    @Override
    public boolean validate() {
        validate = Bank.isOpen();
        UpdateUI.debug("Branch: IsBankOpen: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        return bot.refillInventoryLeaf;
    }

    @Override
    public TreeTask failureTask() {
        if(ChatDialog.getContinue() != null && ChatDialog.getText().contains("ghost banker wails")){
            ChatDialog.getContinue().select();
            Execution.delay(1000, 1500);
        }

        bot.openBankLeaf.setGenericBank(true);
        return bot.openBankLeaf;
    }
}