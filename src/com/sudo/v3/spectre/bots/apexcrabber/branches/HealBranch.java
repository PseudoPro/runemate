package com.sudo.v3.spectre.bots.apexcrabber.branches;

import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.bots.apexcrabber.enums.TraversalLocation;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Joey 2.0 on 3/20/2017.
 */
public class HealBranch extends SudoBranchTask {
    private ApexCrabber bot;

    public HealBranch(ApexCrabber bot) {
        this.bot = bot;
    }


    @Override
    public boolean validate() {
        bot.food = Inventory.newQuery().actions("Eat").results().first();

        validate = bot.food != null;
        UpdateUI.debug("Branch: HealBranch: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        return bot.healLeaf;
    }

    @Override
    public TreeTask failureTask() {
        if (bot.refreshArea != null && bot.refreshArea.contains(bot.player)) {
            return bot.noFoodLeaf;
        } else {
            bot.traversalLocation = TraversalLocation.REFRESH_AREA;
            return bot.traversalLeaf;
        }
    }
}