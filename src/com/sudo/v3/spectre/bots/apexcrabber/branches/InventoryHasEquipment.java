package com.sudo.v3.spectre.bots.apexcrabber.branches;

import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by joeyw on 5/12/2017.
 */
public class InventoryHasEquipment extends SudoBranchTask {
    private ApexCrabber bot;

    public InventoryHasEquipment (ApexCrabber bot){
        this.bot = bot;
    }

    @Override
    public boolean validate() {
        validate = Inventory.containsAnyOf(bot.thrownEquipmentName);
        UpdateUI.debug("Branch: InventoryHasEquipment: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        return bot.wieldThrownEquipmentLeaf;
    }

    @Override
    public TreeTask failureTask() {
        return bot.areSkillsLow;
    }
}
