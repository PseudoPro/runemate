package com.sudo.v3.spectre.bots.apexcrabber.branches;

import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.queries.results.SpriteItemQueryResults;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.bots.apexcrabber.enums.BotLocation;
import com.sudo.v3.spectre.bots.apexcrabber.enums.Potion;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by joeyw on 4/25/2017.
 */
public class IsRefillRequired extends SudoBranchTask {
    private ApexCrabber bot;

    public IsRefillRequired(ApexCrabber bot) {
        this.bot = bot;
    }

    @Override
    public boolean validate() {
        validate = isRefillRequired();
        UpdateUI.debug("Branch: IsRefillRequired: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        return bot.isBankOpen;
    }

    @Override
    public TreeTask failureTask() {
        return bot.disableBankRunLeaf;
    }

    private boolean isRefillRequired() {
        SpriteItemQueryResults foodResults = Inventory.newQuery().actions("Eat").results();
        int coinQuantity = Inventory.getQuantity("Coins"), ectoTokenQuantity = Inventory.getQuantity(bot.ectoTokenName);

        if (bot.potionType == Potion.SP_ATT_STR) {
            SpriteItemQueryResults attackPotionResults = Inventory.newQuery().names(Potion.SP_ATT.getPattern()).results();
            SpriteItemQueryResults strengthPotionResults = Inventory.newQuery().names(Potion.SP_STR.getPattern()).results();

            return (bot.usingFood && foodResults.size() < bot.foodQuantity) || (bot.usingPotion && (attackPotionResults.size() < bot.potionQuantity / 2 || strengthPotionResults.size() < bot.potionQuantity / 2)) || (bot.botLocation == BotLocation.CRAB_ISLAND && coinQuantity < 10000) || ((bot.botLocation == BotLocation.SLEPE || bot.botLocation == BotLocation.PHASMATYS_WEST || bot.botLocation == BotLocation.PHASMATYS_EAST) && !bot.ghostAhoyComplete && ectoTokenQuantity < 2);
        } else if (bot.potionType == Potion.ATT_STR) {
            SpriteItemQueryResults attackPotionResults = Inventory.newQuery().names(Potion.ATT.getPattern()).results();
            SpriteItemQueryResults strengthPotionResults = Inventory.newQuery().names(Potion.STR.getPattern()).results();

            return (bot.usingFood && foodResults.size() < bot.foodQuantity) || (bot.usingPotion && (attackPotionResults.size() < bot.potionQuantity / 2 || strengthPotionResults.size() < bot.potionQuantity / 2)) || (bot.botLocation == BotLocation.CRAB_ISLAND && coinQuantity < 10000) || ((bot.botLocation == BotLocation.SLEPE || bot.botLocation == BotLocation.PHASMATYS_WEST || bot.botLocation == BotLocation.PHASMATYS_EAST) && !bot.ghostAhoyComplete && ectoTokenQuantity < 2);
        } else {
            SpriteItemQueryResults potionResults = Inventory.newQuery().names(bot.potionType.getPattern()).results();

            return (bot.usingFood && foodResults.size() < bot.foodQuantity) || (bot.usingPotion && potionResults.size() < bot.potionQuantity) || (bot.botLocation == BotLocation.CRAB_ISLAND && coinQuantity < 10000) || ((bot.botLocation == BotLocation.SLEPE || bot.botLocation == BotLocation.PHASMATYS_WEST || bot.botLocation == BotLocation.PHASMATYS_EAST) && !bot.ghostAhoyComplete && ectoTokenQuantity < 2);
        }
    }
}