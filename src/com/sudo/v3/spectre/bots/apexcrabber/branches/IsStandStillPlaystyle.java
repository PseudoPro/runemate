package com.sudo.v3.spectre.bots.apexcrabber.branches;

import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.bots.apexcrabber.enums.Playstyle;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Joey on 3/15/2017.
 */
public class IsStandStillPlaystyle extends SudoBranchTask {
    private ApexCrabber bot;

    public IsStandStillPlaystyle(ApexCrabber bot) {
        this.bot = bot;
    }

    @Override
    public boolean validate() {
        validate = bot.playStyle == Playstyle.STAND_STILL;
        UpdateUI.debug("Branch: IsStandStillPlaystyle: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        if (bot.worldHop) {
            // Hop to a random members world
            UpdateUI.currentTask("Multiple player(s) found, hopping to new world", bot);
            return bot.worldHopLeaf;
        } else
            return bot.waitingForRespawnLeaf;
    }

    @Override
    public TreeTask failureTask() {

        return bot.isCrabSpotAvailable;
    }
}