package com.sudo.v3.spectre.bots.apexcrabber.branches;

import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by joeyw on 4/27/2017.
 */
public class AreSkillsLow extends SudoBranchTask {

    private ApexCrabber bot;

    public AreSkillsLow(ApexCrabber bot) {
        this.bot = bot;
    }

    @Override
    public boolean validate() {
        validate = bot.usingPotion && bot.crabArea.contains(bot.player) && bot.potionType.areSkillsLow();
        UpdateUI.debug("Branch: AreSkillsLow: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        return bot.usePotionLeaf;
    }

    @Override
    public TreeTask failureTask() {
        return bot.isAttacking;
    }
}