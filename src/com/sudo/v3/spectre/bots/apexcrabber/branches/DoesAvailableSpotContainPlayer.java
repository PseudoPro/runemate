package com.sudo.v3.spectre.bots.apexcrabber.branches;

import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Joey 2.0 on 3/21/2017.
 */
public class DoesAvailableSpotContainPlayer extends SudoBranchTask {
    private ApexCrabber bot;

    public DoesAvailableSpotContainPlayer(ApexCrabber bot){
        this.bot = bot;
    }

    @Override
    public boolean validate() {
        validate = !bot.leastPlayerSpot.contains(bot.player.getPosition());
        UpdateUI.debug("Branch: AvailableSpotContainPlayer: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        return bot.handleLeastPlayerSpotLeaf;
    }

    @Override
    public TreeTask failureTask() {
        return bot.attackBranch;
    }
}
