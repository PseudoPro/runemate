package com.sudo.v3.spectre.bots.apexcrabber.branches;

import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.api.game.entities.ApexPlayer;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Joey on 3/14/2017.
 */
public class IsAttacking extends SudoBranchTask {
    private ApexCrabber bot;

    public IsAttacking(ApexCrabber bot) {
        this.bot = bot;
    }

    @Override
    public boolean validate() {
        validate = ApexPlayer.isTargetting(bot.crabName);
        UpdateUI.debug("Branch: IsAttacking: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        return bot.currentlyAttackingLeaf;
    }

    @Override
    public TreeTask failureTask() {
        return bot.isCrabShellNull;
    }
}