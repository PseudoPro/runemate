package com.sudo.v3.spectre.bots.apexcrabber.branches;

import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.RuneScape;
import com.runemate.game.api.hybrid.local.Quest;
import com.runemate.game.api.hybrid.local.Quests;
import com.runemate.game.api.hybrid.local.Skill;
import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.local.hud.interfaces.Health;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoRootTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.bots.apexcrabber.enums.Playstyle;
import com.sudo.v3.spectre.bots.apexcrabber.enums.TraversalLocation;
import com.sudo.v3.spectre.bots.apexcrabber.statics.TraversalAreas;
import com.sudo.v3.spectre.api.game.entities.ApexPlayer;
import com.sudo.v3.spectre.common.navigation.Traverse;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.sudo.v3.ui.model.XPInfo;


/**
 * Created by SudoPro on 3/6/2017.
 */
public class Root extends SudoRootTask {
    private ApexCrabber bot;
    private SpriteItem equipment;

    public Root(ApexCrabber bot) {
        super(bot);
        this.bot = bot;
    }

    @Override
    public TreeTask rootTask() {
        if (bot.firstLogin) {
            if (RuneScape.isLoggedIn()) {
                // Add Skills to track to the HashMap
                bot.XPInfoMap.put(Skill.ATTACK, new XPInfo(Skill.ATTACK));
                bot.XPInfoMap.put(Skill.STRENGTH, new XPInfo(Skill.STRENGTH));
                bot.XPInfoMap.put(Skill.DEFENCE, new XPInfo(Skill.DEFENCE));
                bot.XPInfoMap.put(Skill.CONSTITUTION, new XPInfo(Skill.CONSTITUTION));
                bot.XPInfoMap.put(Skill.MAGIC, new XPInfo(Skill.MAGIC));
                bot.XPInfoMap.put(Skill.RANGED, new XPInfo(Skill.RANGED));

                bot.firstLogin = false;
                if(bot.crabName.equals("Swamp Crab")) {
                    bot.ghostAhoyQuest = Quests.get("Ghosts Ahoy");
                    if (bot.ghostAhoyQuest != null) {
                        Quest.Status status = bot.ghostAhoyQuest.getStatus();
                        bot.ghostAhoyComplete = status != null && status == Quest.Status.COMPLETE;
                    }
                }

                bot.updateSession(0.0);
            }
        }

        if(bot.guiResume){
            UpdateUI.debug("Location: " + bot.botLocation.name());
            UpdateUI.debug("PlayStyle: " + bot.playStyle);
            if(bot.playStyle == Playstyle.STAND_STILL)
                UpdateUI.debug("StandStill Coord: " + bot.standStillCoord);
            UpdateUI.debug("HP Level: " + Skill.CONSTITUTION.getCurrentLevel());
            UpdateUI.debug("Heal Percent: " + bot.userProvidedPercent);
            UpdateUI.debug("Using Food: " + bot.usingFood);
            UpdateUI.debug("Food Type: " + bot.foodName);
            UpdateUI.debug("Food Quantity: " + bot.foodQuantity);
            UpdateUI.debug("Using Potions: " + bot.usingPotion);
            UpdateUI.debug("Potion Type: " + bot.potionType.name());
            UpdateUI.debug("Potion Quantity: " + bot.potionQuantity);
            UpdateUI.debug("Stop Out Of Potions: " + bot.stopIfNoPotion);
            UpdateUI.debug("Pickup Thrown Equipment: " + bot.pickupThrownEquipment);
            UpdateUI.debug("Player Run Count: " + bot.playerRunCount);
            UpdateUI.debug("World Hop: " + bot.worldHop);

            Coordinate playerPosition;
            if(bot.player != null && (playerPosition = bot.player.getPosition()) != null)
                UpdateUI.debug("Player Position: " + playerPosition);

            bot.guiResume = false;
        }

        if(bot.crabName.equals("Swamp Crab") && bot.ghostAhoyComplete == null) {
            bot.ghostAhoyQuest = Quests.get("Ghosts Ahoy");
            if (bot.ghostAhoyQuest != null) {
                Quest.Status status = bot.ghostAhoyQuest.getStatus();
                bot.ghostAhoyComplete = status == Quest.Status.COMPLETE;
            }
        }

        if (bot.currentlyBreaking) {
            if (RuneScape.isLoggedIn()) {
                //Execution.delayUntil(() -> ApexPlayer.isTargetting(bot.crabName), 2500, 7500);
                if (bot.refreshArea.contains(bot.player)) {
                    UpdateUI.currentTask("Player safely in refresh area, attempting to log off.", bot);
                    RuneScape.logout(true);
                } else {
                    UpdateUI.currentTask("Running to refresh area to break.", bot);
                    Traverse.bresenhamPath(bot.refreshArea);
                }
            }
        } else if (bot.player != null) {
            if(bot.thrownEquipmentName.equals("")){
                if((equipment = Equipment.getItemIn(Equipment.Slot.WEAPON)) != null){
                    bot.thrownEquipmentName = equipment.getDefinition().getName();
                }
            }


            if (validate = bot.bankingEnabled && bot.bankRun) {
                UpdateUI.debug("Root Branch: ShouldBank: " +  validate);
                return bot.isInBankArea;
            } else if (bot.bankingEnabled && bot.isRefillInventoryRequired()) {
                UpdateUI.currentTask("Inventory refill required. Running to bank.", bot);
                bot.bankRun = true;
            } else {
                validate = Health.getCurrentPercent() < bot.healPercent;
                UpdateUI.debug("Root Branch: IsHealingNeeded: " + validate);
                UpdateUI.debug("ActualHealthPercent: " + Health.getCurrentPercent());
                UpdateUI.debug("GeneratedHealthPercent: " + bot.healPercent);
                if (!validate) {
                    // First determine whether the user is on the PRO or Lite version of the bot. If on the Lite version, bot must stop after 10 minutes of use.
                    if (bot.getMetaData().getHourlyPrice().doubleValue() > 0 || bot.isValidSession() || Environment.isSDK()) {
                        if (validate = (bot.player.distanceTo(bot.crabArea.getCenter()) > 250 && bot.player.distanceTo(TraversalAreas.PHASMATYS_SOUTH_ENTRANCE) > 250)) {
                            UpdateUI.debug("Root Branch: Outside Botting Area: " + validate);
                            UpdateUI.currentTask("Please start the bot within the supported areas. Check the Bot's Overview page for more information.", bot);
                            Execution.delay(1000);
                            bot.pause();
                        } else if (validate = bot.resetRun) {
                            UpdateUI.debug("Root Branch: ResetRun: " + validate);
                            if ((validate = bot.refreshArea != null && bot.refreshArea.contains(bot.player)) || (bot.randomRefreshArea != null && bot.randomRefreshArea.contains(bot.player))) {
                                UpdateUI.debug("Root Branch: Refresh Contains Player: " + validate);
                                bot.randomRefreshArea = null;
                                return bot.successfulRefreshLeaf;
                            } else {
                                UpdateUI.debug("Root Branch: Refresh Contains Player: " + validate);
                                if (bot.randomRefreshArea == null) {
                                    if(bot.refreshArea != null && bot.refreshArea.getCoordinates().size() > 1)
                                        bot.randomRefreshArea = new Area.Circular(bot.refreshArea.getRandomCoordinate(), 6);
                                    else
                                        return bot.triggerRefreshLeaf;
                                }
                                bot.traversalLocation = TraversalLocation.REFRESH_AREA;
                                return bot.traversalLeaf;
                            }
                        } else if (validate = !bot.crabArea.contains(bot.player)) {
                            UpdateUI.debug("Root Branch: CrabArea!ContainsPlayer: " + validate);
                            bot.traversalLocation = bot.playStyle == Playstyle.ACTIVE ? TraversalLocation.CRAB_AREA : TraversalLocation.CRAB_SPOT;
                            return bot.traversalLeaf;
                        } else if (validate = !bot.isPlayerOnCrabSpot() && bot.playStyle == Playstyle.STAND_STILL && !bot.pickingUpThrown) {
                            UpdateUI.debug("Root Branch: OffSpotCoord: " + validate);
                            bot.traversalLocation = TraversalLocation.CRAB_SPOT;
                            return bot.traversalLeaf;
                        } else if (validate = !Inventory.isFull() && bot.pickupThrownEquipment && bot.thrownPickupTimer.hasExpired()) {
                            UpdateUI.debug("Root Branch: ReadyForThrownPickup: " + validate);
                            return bot.isThrownEquipmentAround;
                        } else if (validate = readyToUseSpecial()) {
                            UpdateUI.debug("Root Branch: ReadyToUseSpecial: " + validate);
                            return bot.useSpecialLeaf;
                        } else {
                            return bot.inventoryHasEquipment;
                        }
                    } else {
                        return bot.liteVersionExpireLeaf;
                    }
                } else {
                    return bot.healBranch;
                }
            }
        }
        return bot.emptyLeaf;
    }

    private boolean readyToUseSpecial(){
        if(bot.useSpecial)
            if(bot.checkSpecialTimer.hasExpired())
                if(!TraversalAreas.ZEAH_BANK_AREA.contains(bot.player))
                    if(bot.refreshArea != null)
                        if(!bot.refreshArea.contains(bot.player))
                            if(ApexPlayer.isTargetting(bot.crabName))
                                return true;

        return false;
    }
}