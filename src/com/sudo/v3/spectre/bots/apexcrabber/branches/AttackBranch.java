package com.sudo.v3.spectre.bots.apexcrabber.branches;

import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.bots.apexcrabber.enums.BotLocation;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Joey on 3/16/2017.
 */
public class AttackBranch extends SudoBranchTask {
    private ApexCrabber bot;

    public AttackBranch(ApexCrabber bot) {
        this.bot = bot;
    }

    @Override
    public boolean validate() {
        bot.crab = Npcs.newQuery().names(bot.crabName).within(bot.crabArea).within(new Area.Circular(bot.player.getPosition(), bot.botLocation == BotLocation.FOSSIL_ISLAND ? 3 : 2)).results().random();

        validate = bot.crab != null;
        UpdateUI.debug("Branch: AttackBranch: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {

        return bot.attackCrabLeaf;
    }

    @Override
    public TreeTask failureTask() {
        return bot.crabNullLeaf;
    }
}