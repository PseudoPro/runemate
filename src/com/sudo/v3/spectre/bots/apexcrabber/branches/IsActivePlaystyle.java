package com.sudo.v3.spectre.bots.apexcrabber.branches;

import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.bots.apexcrabber.enums.Playstyle;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Joey on 3/14/2017.
 */
public class IsActivePlaystyle extends SudoBranchTask {
    private ApexCrabber bot;

    public IsActivePlaystyle(ApexCrabber bot) {
        this.bot = bot;
    }

    @Override
    public boolean validate() {
        validate = bot.playStyle == Playstyle.ACTIVE;
        UpdateUI.debug("Branch: IsActivePlaystyle: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        return bot.attackBranch;
    }

    @Override
    public TreeTask failureTask() {
        return bot.isAnotherOnSpot;
    }
}