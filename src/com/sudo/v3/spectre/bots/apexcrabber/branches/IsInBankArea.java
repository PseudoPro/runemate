package com.sudo.v3.spectre.bots.apexcrabber.branches;

import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.bots.apexcrabber.enums.BotLocation;
import com.sudo.v3.spectre.bots.apexcrabber.enums.TraversalLocation;
import com.sudo.v3.spectre.bots.apexcrabber.statics.TraversalAreas;
import com.sudo.v3.spectre.statics.Areas;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by joeyw on 4/25/2017.
 */
public class IsInBankArea extends SudoBranchTask {

    private ApexCrabber bot;

    public IsInBankArea(ApexCrabber bot){
        this.bot = bot;
    }

    @Override
    public boolean validate() {
//        if(bot.botLocation == BotLocation.ZEAH_SHORE || bot.botLocation == BotLocation.CRAB_ISLAND)
//            validate = TraversalAreas.ZEAH_BANK_AREA.contains(bot.player);
//        else if(bot.botLocation == BotLocation.PHASMATYS_EAST || bot.botLocation == BotLocation.PHASMATYS_WEST || bot.botLocation == BotLocation.SLEPE)
//            validate = Areas.PHASMATYS_BANK_AREA.contains(bot.player);
        validate = bot.bankArea.contains(bot.player);
        UpdateUI.debug("Branch: IsInBankArea: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        return bot.isRefillRequired;
    }

    @Override
    public TreeTask failureTask() {
        bot.traversalLocation = TraversalLocation.BANK_AREA;
        return bot.traversalLeaf;
    }
}
