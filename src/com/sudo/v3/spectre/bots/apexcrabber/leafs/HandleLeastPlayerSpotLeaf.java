package com.sudo.v3.spectre.bots.apexcrabber.leafs;

import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.common.navigation.Traverse;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Joey 2.0 on 3/21/2017.
 */
public class HandleLeastPlayerSpotLeaf extends LeafTask {
    private ApexCrabber bot;

    public HandleLeastPlayerSpotLeaf(ApexCrabber bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        if (bot.leastPlayerSpot.size() > 0)
        {
            UpdateUI.currentTask("Traveling to least active crab spot", bot);
            Coordinate coord = bot.leastPlayerSpot.get((int)(Math.random() * bot.leastPlayerSpot.size()));
            Traverse.bresenhamPath(new Area.Rectangular(coord, coord));
        }
        else
        {
            UpdateUI.currentTask("Traveling to available crab spot", bot);
            Traverse.bresenhamPath(bot.getRandomSpotArea());
        }
    }
}
