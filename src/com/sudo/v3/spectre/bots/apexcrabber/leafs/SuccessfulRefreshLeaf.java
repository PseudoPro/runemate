package com.sudo.v3.spectre.bots.apexcrabber.leafs;

import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Joey 2.0 on 3/21/2017.
 */
public class SuccessfulRefreshLeaf extends LeafTask {
    private ApexCrabber bot;

    public SuccessfulRefreshLeaf(ApexCrabber bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        UpdateUI.currentTask("Successful area refresh", bot);
        bot.resetRun = false;
    }
}
