package com.sudo.v3.spectre.bots.apexcrabber.leafs;

import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.bots.apexcrabber.enums.Potion;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by joeyw on 4/27/2017.
 */
public class UsePotionLeaf extends LeafTask {
    private ApexCrabber bot;

    public UsePotionLeaf (ApexCrabber bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        SpriteItem item = null;

        if(bot.potionType == Potion.SP_ATT_STR){
            if(Potion.SP_ATT.areSkillsLow())
                item = Inventory.newQuery().names(Potion.SP_ATT.getPattern()).results().first();
            else
                item = Inventory.newQuery().names(Potion.SP_STR.getPattern()).results().first();
        }else if(bot.potionType == Potion.ATT_STR){
            if(Potion.SP_ATT.areSkillsLow())
                item = Inventory.newQuery().names(Potion.ATT.getPattern()).results().first();
            else
                item = Inventory.newQuery().names(Potion.STR.getPattern()).results().first();
        }else{
             item = Inventory.newQuery().actions("Drink").results().first();
        }

        if(item != null){
            UpdateUI.currentTask("Attempting to drink potion", bot);
            if(item.click()){
                Execution.delay(1000, 1500);
            }
        }
    }
}
