package com.sudo.v3.spectre.bots.apexcrabber.leafs;

import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Joey 2.0 on 3/21/2017.
 */
public class WaitingForRespawnLeaf extends LeafTask {
    private ApexCrabber bot;

    public WaitingForRespawnLeaf(ApexCrabber bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        UpdateUI.currentTask("Waiting for respawn...", bot);
        Execution.delay(3000, 6000);
    }
}
