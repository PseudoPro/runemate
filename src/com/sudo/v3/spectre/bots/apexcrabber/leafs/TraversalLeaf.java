package com.sudo.v3.spectre.bots.apexcrabber.leafs;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.ChatDialog;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexcrabber.statics.CrabAreas;
import com.sudo.v3.spectre.common.playersense.ApexPlayerSense;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.bots.apexcrabber.enums.BotLocation;
import com.sudo.v3.spectre.bots.apexcrabber.enums.Playstyle;
import com.sudo.v3.spectre.bots.apexcrabber.enums.TraversalLocation;
import com.sudo.v3.spectre.bots.apexcrabber.statics.TraversalAreas;
import com.sudo.v3.spectre.api.game.hud.SudoCamera;
import com.sudo.v3.spectre.common.navigation.Traverse;
import com.sudo.v3.spectre.statics.Areas;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 11/29/2016.
 */
public class TraversalLeaf extends LeafTask {
    private ApexCrabber bot;

    public TraversalLeaf(ApexCrabber bot) {
        this.bot = bot;
    }

    @Override
    public void execute() {
        if (bot.traversalLocation == TraversalLocation.CRAB_SPOT) {
            if (bot.botLocation == BotLocation.CRAB_ISLAND && !bot.crabArea.contains(bot.player)) {
                enterCrabIsland();
            } else if (Areas.CITY_OF_PORT_PHASMATYS_AREA.contains(bot.player) && (bot.botLocation == BotLocation.PHASMATYS_EAST || bot.botLocation == BotLocation.PHASMATYS_WEST || bot.botLocation == BotLocation.SLEPE)) {
                exitPortPhasmatys();
            } else {
                UpdateUI.currentTask("Traveling to crab spot.", bot);
                if (bot.botLocation == BotLocation.SLEPE && CrabAreas.SLEPE_SWAMPCRAB_AREA.distanceTo(bot.player) > 50) {
                    Traverse.webPath(bot.nav, CrabAreas.SLEPE_SWAMPCRAB_AREA);
                } else {
                    if (bot.playStyle == Playstyle.STAND_STILL)
                        Traverse.smartRegionPath(bot.nav, bot.standStillCoord.getArea());
                    else
                        Traverse.smartRegionPath(bot.nav, bot.getRandomSpotArea());
                }
            }
        } else if (bot.traversalLocation == TraversalLocation.CRAB_AREA) {
            if (bot.botLocation == BotLocation.CRAB_ISLAND && !bot.crabArea.contains(bot.player)) {
                enterCrabIsland();
            } else if (Areas.CITY_OF_PORT_PHASMATYS_AREA.contains(bot.player) && (bot.botLocation == BotLocation.PHASMATYS_EAST || bot.botLocation == BotLocation.PHASMATYS_WEST || bot.botLocation == BotLocation.SLEPE)) {
                exitPortPhasmatys();
            } else if(bot.botLocation == BotLocation.SLEPE && CrabAreas.SLEPE_SWAMPCRAB_AREA.distanceTo(bot.player) > 50) {
                UpdateUI.currentTask("Web Traversing to Slepe crab area.", bot);
                Traverse.webPath(bot.nav, bot.crabArea);
            } else {
                UpdateUI.currentTask("Traversing to general crab area.", bot);
                Traverse.smartRegionPath(bot.nav, bot.crabArea);
            }
        } else if (bot.traversalLocation == TraversalLocation.CRAB_SPOT_AREA) {
            if (bot.botLocation == BotLocation.CRAB_ISLAND && !bot.crabArea.contains(bot.player)) {
                enterCrabIsland();
            } else if (Areas.CITY_OF_PORT_PHASMATYS_AREA.contains(bot.player) && (bot.botLocation == BotLocation.PHASMATYS_EAST || bot.botLocation == BotLocation.PHASMATYS_WEST || bot.botLocation == BotLocation.SLEPE)) {
                exitPortPhasmatys();
            } else {
                UpdateUI.currentTask("Traversing to a nearby crab.", bot);
                bot.crab = Npcs.newQuery().names(bot.shellName).within(bot.crabArea).results().random();
                Traverse.smartRegionPath(bot.nav, bot.crab.getArea());
            }
        } else if (bot.traversalLocation == TraversalLocation.REFRESH_AREA) {
            UpdateUI.currentTask("Traveling to refresh area.", bot);
            Traverse.smartRegionPath(bot.nav, bot.randomRefreshArea);
        } else if (bot.traversalLocation == TraversalLocation.BANK_AREA) {
            if (!Areas.CITY_OF_PORT_PHASMATYS_AREA.contains(bot.player) && (bot.botLocation == BotLocation.SLEPE || bot.botLocation == BotLocation.PHASMATYS_EAST || bot.botLocation == BotLocation.PHASMATYS_WEST)) {
                enterPortPhasmatys();
            } else if (bot.botLocation == BotLocation.CRAB_ISLAND && bot.crabArea.contains(bot.player)) {
                npcLeaveIslandInteract();
            } else {
                UpdateUI.currentTask("Traveling to bank.", bot);
//                if(bot.botLocation == BotLocation.SLEPE || bot.botLocation == BotLocation.PHASMATYS_EAST || bot.botLocation == BotLocation.PHASMATYS_WEST)
//                    Traverse.smartRegionPath(bot.nav, Areas.PHASMATYS_BANK_AREA);
//                else
//                    Traverse.smartRegionPath(bot.nav, TraversalAreas.ZEAH_BANK_AREA);
                Traverse.smartRegionPath(bot.nav, bot.bankArea);
            }
        }

        // Delay after each attempt to traverse
        Execution.delay(750, 1500);
        Execution.delayWhile(bot.player::isMoving, ApexPlayerSense.Key.TRAVERSAL_DELAY.getAsInteger());
    }

    private void enterCrabIsland() {
        if (TraversalAreas.ZEAH_CRAB_ISLAND_GOTO_AREA.contains(bot.player)) {
            npcEnterIslandInteract();
        } else {
            UpdateUI.currentTask("Traveling to boat guy.", bot);
            Traverse.smartRegionPath(bot.nav, TraversalAreas.ZEAH_CRAB_ISLAND_GOTO_AREA);
        }
    }

    private void npcLeaveIslandInteract() {

        bot.islandNpc = Npcs.newQuery().actions("Quick-travel").results().first();

        if (bot.islandNpc != null && bot.islandNpc.distanceTo(bot.player) < 10) {
            if (bot.islandNpc.getVisibility() < 0.8)
                SudoCamera.ConcurrentlyTurnToWithYaw(bot.islandNpc);

            UpdateUI.currentTask("Attempting to leave island.", bot);
            bot.islandNpc.interact("Quick-travel");
        } else {
            UpdateUI.currentTask("Traveling to boat guy.", bot);
            Traverse.smartRegionPath(bot.nav, TraversalAreas.ZEAH_CRAB_ISLAND_LEAVE_AREA);
        }
    }

    private void npcEnterIslandInteract() {
        int coinQuantity = Inventory.getQuantity("Coins");
        bot.islandNpc = Npcs.newQuery().actions("Quick-travel").results().first();
        UpdateUI.debug("coinQuantity: " + coinQuantity);

        if (coinQuantity < 10000) {
            // Delay for a few seconds or until we are on the island. If we delay the full amount and still are not on the island, then trigger bank run.
            if (!Execution.delayUntil(() -> bot.crabArea.contains(bot.player), 2000, 5000)) {
                UpdateUI.currentTask("FailSafe: Banking didn't withdraw coins, restarting bank task.", bot);
                bot.bankRun = true;
            }
        } else {
            if (bot.islandNpc != null && bot.islandNpc.distanceTo(bot.player) < 10) {
                if (bot.islandNpc.getVisibility() < 0.8)
                    SudoCamera.ConcurrentlyTurnToWithYaw(bot.islandNpc);

                UpdateUI.currentTask("Attempting to enter island.", bot);
                if (bot.islandNpc.interact("Quick-travel"))
                    Execution.delayUntil(() -> bot.crabArea.contains(bot.player), 1000, 5000);
            } else {
                UpdateUI.currentTask("Traveling to boat guy.", bot);
                Traverse.smartRegionPath(bot.nav, TraversalAreas.ZEAH_CRAB_ISLAND_GOTO_AREA);
            }
        }
    }

    private void enterPortPhasmatys() {
        int ectoTokenCount = Inventory.getQuantity(bot.ectoTokenName);
        if (!bot.ghostAhoyComplete && ectoTokenCount < 2) {
            UpdateUI.currentTask("User does not have 2 Ecto-token's to enter Port Phasmatys. Continuing without banking.", bot);
            bot.bankingEnabled = false;
        } else {
            GameObject energyBarrier = GameObjects.newQuery().names(bot.energyBarrierName).actions(bot.energyBarrierAction).within(TraversalAreas.PHASMATYS_NORTH_ENTRANCE, TraversalAreas.PHASMATYS_SOUTH_ENTRANCE).results().nearest();

            if (energyBarrier != null && energyBarrier.isValid()) {
                if (ChatDialog.getContinue() != null && ChatDialog.getText().contains("ghostly guards wail")){
                    UpdateUI.currentTask("Prevented from entering Port Phasmatys. Do you have Ghostspeak Amulet equipped?", bot);
                    Execution.delay(500);
                    bot.stop("Prevented from entering Port Phasmatys. Do you have Ghostspeak Amulet equipped?");
                } else if (energyBarrier.distanceTo(bot.player) < 8) {
                    if (energyBarrier.getVisibility() < 0.8)
                        SudoCamera.ConcurrentlyTurnToWithYaw(energyBarrier);
                    UpdateUI.currentTask("Attempting to enter through Energy Barrier.", bot);
                    energyBarrier.interact(bot.energyBarrierAction);
                } else {
                    UpdateUI.currentTask("Attempting to travel to energy barrier.", bot);
                    Traverse.smartRegionPath(bot.nav, new Area.Circular(energyBarrier.getArea().getCenter(), 5));
                }
            } else {
                if (bot.ghostAhoyComplete != null && bot.ghostAhoyComplete == true) {
                    SpriteItem ectophialItem = Inventory.getItems("Ectophial").first();

                    if (ectophialItem != null && ectophialItem.isValid()) {
                        UpdateUI.currentTask("Teleporting using Ectophial.", bot);
                        if (ectophialItem.interact("Empty")) {
                            Execution.delay(3000, 3500);
                            Execution.delayUntil(() -> Inventory.contains("Ectophial"), 3000, 3500);
                        }
                    }
                } else {
                    // This only occurs if the energy barrier is not loaded and the player does not have an ectophial... therefore we will run to the south energy barrier.
                    UpdateUI.currentTask("Running to south entrance of Port Phasmatys.", bot);
                    Traverse.webPath(bot.nav, TraversalAreas.PHASMATYS_SOUTH_ENTRANCE);
                }
            }
        }
    }

    private void exitPortPhasmatys() {
        UpdateUI.debug("City Of Port Phasmatys Contains Player: " + Areas.CITY_OF_PORT_PHASMATYS_AREA.contains(bot.player));

        if (Bank.isOpen()) {
            UpdateUI.currentTask("Closing Bank.", bot);
            Bank.close();
        } else {
            GameObject energyBarrier = GameObjects.newQuery().names(bot.energyBarrierName).actions(bot.energyBarrierAction).within(TraversalAreas.PHASMATYS_SOUTH_ENTRANCE).results().nearest();

            if (energyBarrier != null && energyBarrier.isValid() && energyBarrier.distanceTo(bot.player) < 8) {
                if (energyBarrier.getVisibility() < 0.8)
                    SudoCamera.ConcurrentlyTurnToWithYaw(energyBarrier);

                UpdateUI.currentTask("Attempting to exit through Energy Barrier.", bot);
                energyBarrier.interact(bot.energyBarrierAction);
            } else {
                UpdateUI.currentTask("Traveling to Port Phasmatys South entrance.", bot);
                Traverse.smartRegionPath(bot.nav, TraversalAreas.PHASMATYS_SOUTH_ENTRANCE);
            }
        }
    }
}