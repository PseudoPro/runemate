package com.sudo.v3.spectre.bots.apexcrabber.leafs;

import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.api.game.hud.SudoCamera;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by joeyw on 4/25/2017.
 */
public class OpenBankLeaf extends LeafTask{

    private ApexCrabber bot;

    public OpenBankLeaf(ApexCrabber bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        bot.bankObj = GameObjects.newQuery().actions("Bank").results().first();

        if(bot.bankObj != null) {
            if (bot.bankObj.getVisibility() < 0.8)
                SudoCamera.ConcurrentlyTurnToWithYaw(bot.bankObj);
        }

        UpdateUI.currentTask("Opening Bank", bot);
        Bank.open();
    }
}
