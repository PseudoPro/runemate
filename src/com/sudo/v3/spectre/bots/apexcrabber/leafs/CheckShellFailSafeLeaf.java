package com.sudo.v3.spectre.bots.apexcrabber.leafs;

import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;

/**
 * Created by Joey 2.0 on 3/21/2017.
 */
public class CheckShellFailSafeLeaf extends LeafTask{
    private ApexCrabber bot;

    public CheckShellFailSafeLeaf(ApexCrabber bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        bot.crabShellCheckCount++;
        Execution.delay(300, 700);
    }
}
