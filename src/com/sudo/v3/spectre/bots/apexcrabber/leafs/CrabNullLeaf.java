package com.sudo.v3.spectre.bots.apexcrabber.leafs;

import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.bots.apexcrabber.enums.Playstyle;
import com.sudo.v3.spectre.common.navigation.Traverse;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Joey 2.0 on 3/21/2017.
 */
public class CrabNullLeaf extends LeafTask
{
    private ApexCrabber bot;

    public CrabNullLeaf(ApexCrabber bot)
    {
        this.bot = bot;
    }

    @Override
    public void execute()
    {
        if (bot.playStyle == Playstyle.STAND_STILL)
        {
            UpdateUI.currentTask("Waiting for respawn...", bot);
            Execution.delay(1000, 2000);
        }
        else
        {
            Npc crabShell = Npcs.newQuery().names(bot.shellName).within(bot.crabArea).results().nearest();

            if (crabShell != null)
            {
                UpdateUI.currentTask("Traveling to nearest crab shell", bot);
                Traverse.bresenhamPath(crabShell.getArea());
                Execution.delayWhile(bot.player::isMoving, 750, 1500);
            }
            else
            {
                UpdateUI.currentTask("Traveling to crab area", bot);
                Traverse.bresenhamPath(bot.crabArea);
            }
        }
    }
}