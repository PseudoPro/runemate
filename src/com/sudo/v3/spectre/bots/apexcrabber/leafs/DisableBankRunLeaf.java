package com.sudo.v3.spectre.bots.apexcrabber.leafs;

import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by joeyw on 4/25/2017.
 */
public class DisableBankRunLeaf extends LeafTask {

    private ApexCrabber bot;

    public DisableBankRunLeaf(ApexCrabber bot) {
        this.bot = bot;
    }

    @Override
    public void execute() {
        if(Bank.isOpen()){
            UpdateUI.currentTask("Closing bank", bot);
            Bank.close();
        }else {
            UpdateUI.currentTask("Successfully replenished inventory", bot);
            bot.bankRun = false;
        }
    }
}