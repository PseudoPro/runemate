package com.sudo.v3.spectre.bots.apexcrabber.leafs;

import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.bots.apexcrabber.enums.BotLocation;
import com.sudo.v3.spectre.bots.apexcrabber.enums.Potion;
import com.sudo.v3.spectre.statics.UpdateUI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by joeyw on 4/25/2017.
 */
public class RefillInventoryLeaf extends LeafTask {

    private ApexCrabber bot;

    public RefillInventoryLeaf(ApexCrabber bot) {
        this.bot = bot;
    }

    @Override
    public void execute() {
        // Get items we need and withdraw them from bank
        int inventoryItemSize = 0;

        List<String> fullInventoryList = new ArrayList<String>() {{
            add(bot.foodName);
            if (bot.thrownEquipmentName != null && !bot.thrownEquipmentName.equals(""))
                add(bot.thrownEquipmentName);
            if (bot.potionType != null)
                addAll(Arrays.asList(bot.potionType.getPotionList()));
            if (bot.botLocation == BotLocation.CRAB_ISLAND)
                add("Coins");
            if (bot.botLocation == BotLocation.SLEPE || bot.botLocation == BotLocation.PHASMATYS_WEST || bot.botLocation == BotLocation.PHASMATYS_EAST)
                add(bot.ectoTokenName);
        }};

        if (Inventory.contains("Coins")) {
            inventoryItemSize = Inventory.getQuantity("Coins");
        }
        if (Inventory.contains("Vial")) {
            UpdateUI.currentTask("Depositing Vial(s)", bot);
            Bank.deposit("Vial", 0);
        } else if (Inventory.containsAnyExcept(fullInventoryList.toArray(new String[0]))) {
            UpdateUI.currentTask("Depositing other items", bot);
            Bank.depositAllExcept(fullInventoryList.toArray(new String[0]));
        } else if (bot.botLocation == BotLocation.CRAB_ISLAND && inventoryItemSize < 10000) {
            if (Bank.containsAnyOf("Coins")) {
                UpdateUI.currentTask("Withdrawing Coins", bot);
                Bank.withdraw("Coins", 10000 - inventoryItemSize);
            } else {
                UpdateUI.currentTask("Bank is out of Coins, stopping execution since bot cannot enter island.", bot);
                Execution.delay(500);
                bot.stop("Bank is out of Coins, stopping execution since bot cannot enter island.");
            }
        } else if ((inventoryItemSize = Inventory.getQuantity(bot.ectoTokenName)) < 2  && bot.ghostAhoyComplete != null && bot.ghostAhoyComplete == false && (bot.botLocation == BotLocation.SLEPE || bot.botLocation == BotLocation.PHASMATYS_WEST || bot.botLocation == BotLocation.PHASMATYS_EAST))  {
            if (Bank.containsAnyOf(bot.ectoTokenName)) {
                UpdateUI.currentTask("Withdrawing " + bot.ectoTokenName, bot);
                Bank.withdraw(bot.ectoTokenName, 2 - inventoryItemSize);
            } else {
                UpdateUI.currentTask("Bank is out of " + bot.ectoTokenName + ", stopping bot.", bot);
                Execution.delay(500);
                bot.stop("Bank is out of " + bot.ectoTokenName + ", stopping bot.");
            }
        } else if (bot.usingFood && (inventoryItemSize = Inventory.newQuery().names(bot.foodName).results().size()) < bot.foodQuantity) {
            if (Bank.containsAnyOf(bot.foodName)) {
                UpdateUI.currentTask("Withdrawing " + bot.foodName, bot);
                Bank.withdraw(Bank.newQuery().names(bot.foodName).filter(i -> i.getQuantity() > 0).results().first(), bot.foodQuantity - inventoryItemSize);
            } else {
                UpdateUI.currentTask("Bank is out of " + bot.foodName + ", stopping bot.", bot);
                Execution.delay(500);
                bot.stop("Bank is out of " + bot.foodName + ", stopping bot.");
            }
        } else if (bot.usingPotion) {
            if (bot.potionType == Potion.SP_ATT_STR) {
                if ((Inventory.newQuery().names(Potion.SP_ATT.getPattern()).results().size() < bot.potionQuantity / 2) || (Inventory.newQuery().names(Potion.SP_STR.getPattern()).results().size() < bot.potionQuantity / 2)) {
                    if ((inventoryItemSize = Inventory.newQuery().names(Potion.SP_ATT.getPattern()).results().size()) < bot.potionQuantity / 2) {
                        if (Bank.containsAnyOf(Potion.SP_ATT.getPotionList())) {
                            // If you are using super attack and strength potions, we want to split the count between the two.
                            // Ex. If you have 10 potions, 5 should be att, 5 str. If you bank and you have 4 att and 5 str, you need to withdraw 1 att, which would be: (total count of potions / 2) - inventory count of that potion
                            SpriteItem item = Bank.newQuery().names(Potion.SP_ATT.getPotionList()).results().first();
                            if (item != null) {
                                UpdateUI.debug("itemName = " + item.getDefinition().getName());
                                UpdateUI.debug("itemQuantity = " + item.getQuantity());
                                UpdateUI.currentTask("Withdrawing Super Attack potion(s)", bot);
                                Bank.withdraw(item, (bot.potionQuantity / 2) - inventoryItemSize);
                            }
                        } else {
                            if (!bot.stopIfNoPotion) {
                                UpdateUI.currentTask("Bank is out of Super Attack potions, we are no longer using potions.", bot);
                                bot.usingPotion = false;
                            } else {
                                UpdateUI.currentTask("Bank is out of Sp Attack potions, stopping bot per user settings.", bot);
                                Execution.delay(500);
                                bot.stop("Bank is out of Sp Attack potions, stopping bot per user settings.");
                            }
                        }
                    } else if ((inventoryItemSize = Inventory.newQuery().names(Potion.SP_STR.getPattern()).results().size()) < bot.potionQuantity / 2) {
                        if (Bank.containsAnyOf(Potion.SP_STR.getPotionList())) {
                            SpriteItem item = Bank.newQuery().names(Potion.SP_STR.getPotionList()).results().first();
                            if (item != null) {
                                UpdateUI.debug("itemName = " + item.getDefinition().getName());
                                UpdateUI.debug("itemQuantity = " + item.getQuantity());
                                UpdateUI.currentTask("Withdrawing Super Strength potion(s)", bot);
                                Bank.withdraw(item, (bot.potionQuantity / 2) - inventoryItemSize);
                            }
                        } else {
                            if (!bot.stopIfNoPotion) {
                                UpdateUI.currentTask("Bank is out of Super Strength potions, we are no longer using potions.", bot);
                                bot.usingPotion = false;
                            } else {
                                UpdateUI.currentTask("Bank is out of Sp Strength potions, stopping bot per user settings.", bot);
                                Execution.delay(500);
                                bot.stop("Bank is out of Sp Strength potions, stopping bot per user settings.");
                            }
                        }
                    }
                }
            } else if (bot.potionType == Potion.ATT_STR) {
                if ((Inventory.newQuery().names(Potion.ATT.getPattern()).results().size() < bot.potionQuantity / 2) || (Inventory.newQuery().names(Potion.STR.getPattern()).results().size() < bot.potionQuantity / 2)) {
                    if ((inventoryItemSize = Inventory.newQuery().names(Potion.ATT.getPattern()).results().size()) < bot.potionQuantity / 2) {
                        if (Bank.containsAnyOf(Potion.ATT.getPotionList())) {
                            // If you are using super attack and strength potions, we want to split the count between the two.
                            // Ex. If you have 10 potions, 5 should be att, 5 str. If you bank and you have 4 att and 5 str, you need to withdraw 1 att, which would be: (total count of potions / 2) - inventory count of that potion
                            SpriteItem item = Bank.newQuery().names(Potion.ATT.getPotionList()).results().first();
                            if (item != null) {
                                UpdateUI.debug("itemName = " + item.getDefinition().getName());
                                UpdateUI.debug("itemQuantity = " + item.getQuantity());
                                UpdateUI.currentTask("Withdrawing Attack potion(s)", bot);
                                Bank.withdraw(item, (bot.potionQuantity / 2) - inventoryItemSize);
                            }
                        } else {
                            if (!bot.stopIfNoPotion) {
                                UpdateUI.currentTask("Bank is out of Attack potions, we are no longer using potions.", bot);
                                bot.usingPotion = false;
                            } else {
                                UpdateUI.currentTask("Bank is out of Attack potions, stopping bot per user settings.", bot);
                                bot.stop("Bank is out of Attack potions, stopping bot per user settings.");
                            }
                        }
                    } else if ((inventoryItemSize = Inventory.newQuery().names(Potion.STR.getPattern()).results().size()) < bot.potionQuantity / 2) {
                        if (Bank.containsAnyOf(Potion.STR.getPotionList())) {
                            SpriteItem item = Bank.newQuery().names(Potion.STR.getPotionList()).results().first();
                            if (item != null) {
                                UpdateUI.debug("itemName = " + item.getDefinition().getName());
                                UpdateUI.debug("itemQuantity = " + item.getQuantity());
                                UpdateUI.currentTask("Withdrawing Super Strength potion(s)", bot);
                                Bank.withdraw(item, (bot.potionQuantity / 2) - inventoryItemSize);
                            }
                        } else {
                            if (!bot.stopIfNoPotion) {
                                UpdateUI.currentTask("Bank is out of Strength potions, we are no longer using potions.", bot);
                                bot.usingPotion = false;
                            } else {
                                UpdateUI.currentTask("Bank is out of Strength potions, stopping bot per user settings.", bot);
                                bot.stop("Bank is out of Strength potions, stopping bot per user settings.");
                            }
                        }
                    }
                }
            } else {
                if ((inventoryItemSize = Inventory.newQuery().names(bot.potionType.getPattern()).results().size()) < bot.potionQuantity) {
                    if (Bank.containsAnyOf(bot.potionType.getPotionList())) {
                        // If you are using super attack and strength potions, we want to split the count between the two.
                        // Ex. If you have 10 potions, 5 should be att, 5 str. If you bank and you have 4 att and 5 str, you need to withdraw 1 att, which would be: (total count of potions / 2) - inventory count of that potion
                        SpriteItem item = Bank.newQuery().names(bot.potionType.getPotionList()).results().first();
                        if (item != null) {
                            UpdateUI.debug("itemName = " + item.getDefinition().getName());
                            UpdateUI.debug("itemQuantity = " + item.getQuantity());
                            UpdateUI.currentTask("Withdrawing " + bot.potionType.getPotionName() + " potion(s)", bot);
                            Bank.withdraw(item, bot.potionQuantity - inventoryItemSize);
                        }
                    } else {
                        if (bot.stopIfNoPotion) {
                            UpdateUI.currentTask("Bank is out of " + bot.potionType.getPotionName() + " potions, stopping bot per user settings.", bot);
                            bot.stop("Bank is out of " + bot.potionType.getPotionName() + " potions, stopping bot per user settings.");
                        } else {
                            UpdateUI.currentTask("Bank is out of " + bot.potionType.getPotionName() + " potions, we are no longer using potions.", bot);
                            bot.usingPotion = false;
                        }
                    }
                }
            }
        }
    }
}