package com.sudo.v3.spectre.bots.apexcrabber.leafs;

import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.bots.apexcrabber.enums.BotLocation;
import com.sudo.v3.spectre.statics.UpdateUI;

import java.util.List;

/**
 * Created by Joey 2.0 on 3/21/2017.
 */
public class TriggerRefreshLeaf extends LeafTask {
    private ApexCrabber bot;

    public TriggerRefreshLeaf(ApexCrabber bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        UpdateUI.currentTask("Crab no longer aggressive, refreshing zone", bot);

        if(bot.botLocation == BotLocation.CRAB_ISLAND || bot.botLocation == BotLocation.FOSSIL_ISLAND) {
            List<Coordinate> coordCollection = bot.crabArea.getCoordinates();
            Area areaAroundPlayer = new Area.Circular(bot.player.getPosition(), bot.botLocation == BotLocation.FOSSIL_ISLAND ? 25 : 30);
            if(areaAroundPlayer != null) {
                coordCollection.removeAll(areaAroundPlayer.getCoordinates());
                bot.refreshArea = new Area.Absolute(coordCollection);
            }
        }

        bot.resetRun = true;
        bot.crabShellCheckCount = 0;
    }
}
