package com.sudo.v3.spectre.bots.apexcrabber.leafs;

import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.api.game.entities.ApexPlayer;
import com.sudo.v3.spectre.api.game.hud.SudoCamera;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Joey 2.0 on 3/21/2017.
 */
public class AttackCrabLeaf extends LeafTask {
    private ApexCrabber bot;

    public AttackCrabLeaf(ApexCrabber bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        UpdateUI.currentTask("Attempting to attack crab", bot);
        if (!bot.crab.isVisible())
            SudoCamera.ConcurrentlyTurnToWithYaw(bot.crab);

        if (bot.crab.interact("Attack", bot.crabName)) {
            Execution.delayUntil(() -> !ApexPlayer.isTargetting(bot.crabName), 750, 1500);
            Execution.delay(500, 1000);
        }
    }
}
