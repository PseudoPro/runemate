package com.sudo.v3.spectre.bots.apexcrabber.leafs;

import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Joey 2.0 on 3/21/2017.
 */
public class NoFoodLeaf extends LeafTask {
    private ApexCrabber bot;

    public NoFoodLeaf(ApexCrabber bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        UpdateUI.currentTask("Bot out of edible food. Stopping bot.", bot);
        bot.stop();
    }
}
