package com.sudo.v3.spectre.bots.apexcrabber.leafs;

import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by joeyw on 5/12/2017.
 */
public class WieldThrownEquipmentLeaf extends LeafTask {
    private ApexCrabber bot;
    private SpriteItem equipment;

    public WieldThrownEquipmentLeaf (ApexCrabber bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        equipment = Inventory.newQuery().names(bot.thrownEquipmentName).results().first();

        if(equipment != null){
            UpdateUI.currentTask("Wielding thrown equipment", bot);
            equipment.interact("Wield");
        }
    }
}
