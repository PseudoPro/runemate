package com.sudo.v3.spectre.bots.apexcrabber.leafs;

import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.bots.apexcrabber.enums.Playstyle;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Joey 2.0 on 3/21/2017.
 */
public class CurrentlyAttackingLeaf extends LeafTask {
    private ApexCrabber bot;

    public CurrentlyAttackingLeaf(ApexCrabber bot) {
        this.bot = bot;
    }

    @Override
    public void execute() {
        UpdateUI.currentTask("Currently Attacking", bot);
        bot.crabShellCheckCount = 0;
        if(bot.playStyle == Playstyle.STAND_STILL)
            Execution.delay(4000, 10000);
        else
            Execution.delay(2000, 6000);
    }
}