package com.sudo.v3.spectre.bots.apexcrabber.leafs;

import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Joey 2.0 on 3/21/2017.
 */
public class HealLeaf extends LeafTask {
    private ApexCrabber bot;

    public HealLeaf(ApexCrabber bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        UpdateUI.currentTask("Health tolerance exceeded, eating food...", bot);
        if(bot.food.interact("Eat")){
            bot.healPercent = (bot.userProvidedPercent - 7.5) + (Math.random() * 15);
            UpdateUI.currentTask("Eating successful, new health tolerance is " + bot.healPercent, bot);
        }
    }
}
