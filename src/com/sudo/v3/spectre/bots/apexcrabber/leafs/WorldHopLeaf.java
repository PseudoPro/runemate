package com.sudo.v3.spectre.bots.apexcrabber.leafs;

import com.runemate.game.api.hybrid.local.Worlds;
import com.runemate.game.api.hybrid.local.hud.interfaces.WorldHop;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Joey 2.0 on 3/21/2017.
 */
public class WorldHopLeaf extends LeafTask {
    private ApexCrabber bot;

    public WorldHopLeaf(ApexCrabber bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        // Hop to a random members world
        WorldHop.hopTo(Worlds.newQuery().member().results().random());
    }
}
