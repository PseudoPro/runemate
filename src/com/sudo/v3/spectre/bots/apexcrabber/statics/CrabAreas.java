package com.sudo.v3.spectre.bots.apexcrabber.statics;

import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;

/**
 * Created by Joey on 3/15/2017.
 */
public class CrabAreas
{
    public final static Area ZEAH_SHORE_SANDCRAB_AREA = new Area.Polygonal(new Coordinate(1726, 3471, 0), new Coordinate(1726, 3456, 0), new Coordinate(1731, 3457, 0), new Coordinate(1742, 3467, 0), new Coordinate(1748, 3460, 0), new Coordinate(1754, 3466, 0), new Coordinate(1776, 3454, 0), new Coordinate(1798, 3457, 0), new Coordinate(1800, 3469, 0), new Coordinate(1795, 3474, 0), new Coordinate(1775, 3472, 0), new Coordinate(1761, 3473, 0), new Coordinate(1744, 3480, 0));
    public final static Area ZEAH_SHORE_SANDCRAB_REFRESH_AREA = new Area.Rectangular(new Coordinate(1765, 3500, 0), new Coordinate(1791, 3509, 0));

    public final static Area ZEAH_ISLAND_SANDCRAB_AREA = new Area.Polygonal(new Coordinate(1775, 3446, 0), new Coordinate(1771, 3449, 0), new Coordinate(1765, 3450, 0), new Coordinate(1757, 3442, 0), new Coordinate(1752, 3437, 0), new Coordinate(1750, 3430, 0), new Coordinate(1747, 3425, 0), new Coordinate(1746, 3422, 0), new Coordinate(1748, 3419, 0), new Coordinate(1744, 3411, 0), new Coordinate(1746, 3405, 0), new Coordinate(1753, 3404, 0), new Coordinate(1759, 3408, 0), new Coordinate(1767, 3404, 0), new Coordinate(1771, 3405, 0), new Coordinate(1775, 3408, 0), new Coordinate(1778, 3404, 0), new Coordinate(1783, 3401, 0), new Coordinate(1788, 3400, 0), new Coordinate(1796, 3404, 0), new Coordinate(1799, 3409, 0), new Coordinate(1796, 3417, 0), new Coordinate(1794, 3412, 0), new Coordinate(1790, 3411, 0), new Coordinate(1788, 3414, 0), new Coordinate(1786, 3412, 0), new Coordinate(1781, 3415, 0), new Coordinate(1784, 3428, 0), new Coordinate(1783, 3436, 0), new Coordinate(1779, 3446, 0), new Coordinate(1778, 3443, 0), new Coordinate(1774, 3431, 0), new Coordinate(1772, 3427, 0), new Coordinate(1767, 3428, 0), new Coordinate(1764, 3432, 0), new Coordinate(1765, 3439, 0), new Coordinate(1769, 3444, 0));
    public final static Area ZEAH_ISLAND_SANDCRAB_REFRESH_AREA = new Area.Rectangular(new Coordinate(1765, 3500, 0), new Coordinate(1791, 3509, 0));

    public final static Area FREM_ROCKCRAB_AREA = new Area.Rectangular(new Coordinate(2721, 3713, 0), new Coordinate(2696, 3730, 0));
    public final static Area FREM_ROCKCRAB_REFRESH_AREA = new Area.Rectangular(new Coordinate(2730, 3668, 0), new Coordinate(2696, 3684, 0));

    public final static Area SLEPE_SWAMPCRAB_AREA = new Area.Polygonal(new Coordinate(3754, 3369, 0), new Coordinate(3750, 3365, 0), new Coordinate(3748, 3357, 0), new Coordinate(3751, 3354, 0), new Coordinate(3749, 3350, 0), new Coordinate(3768, 3353, 0), new Coordinate(3761, 3361, 0), new Coordinate(3762, 3367, 0));
    public final static Area SLEPE_SWAMPCRAB_REFRESH_AREA = new Area.Polygonal(new Coordinate(3753, 3330, 0), new Coordinate(3754, 3326, 0), new Coordinate(3757, 3324, 0), new Coordinate(3757, 3314, 0), new Coordinate(3766, 3314, 0), new Coordinate(3771, 3330, 0));
    
    public final static Area PHASMATYS_EAST_SWAMPCRAB_AREA = new Area.Rectangular(new Coordinate(3684, 3446, 0), new Coordinate(3698, 3436, 0));
    public final static Area PHASMATYS_EAST_REFRESH_AREA = new Area.Rectangular(new Coordinate(3663, 3427, 0), new Coordinate(3642, 3456, 0));

    public final static Area PHASMATYS_WEST_SWAMPCRAB_AREA = new Area.Rectangular(new Coordinate(3630, 3428, 0), new Coordinate(3442, 3420, 0));
    public final static Area PHASMATYS_WEST_REFRESH_AREA = new Area.Rectangular(new Coordinate(3665, 3434, 0), new Coordinate(3680, 3452, 0));

    public final static Area FOSSIL_ISLAND_AMMONITECRAB_AREA = new Area.Polygonal(new Coordinate(3710, 3899, 0), new Coordinate(3707, 3846, 0), new Coordinate(3719, 3838, 0), new Coordinate(3739, 3838, 0), new Coordinate(3753, 3849, 0), new Coordinate(3761, 3850, 0), new Coordinate(3760, 3853, 0), new Coordinate(3751, 3856, 0), new Coordinate(3740, 3848, 0), new Coordinate(3729, 3852, 0), new Coordinate(3729, 3845, 0), new Coordinate(3722, 3847, 0), new Coordinate(3716, 3857, 0), new Coordinate(3720, 3861, 0), new Coordinate(3724, 3867, 0), new Coordinate(3724, 3873, 0), new Coordinate(3721, 3879, 0), new Coordinate(3726, 3885, 0), new Coordinate(3731, 3887, 0), new Coordinate(3737, 3884, 0), new Coordinate(3741, 3886, 0), new Coordinate(3738, 3887, 0), new Coordinate(3737, 3892, 0), new Coordinate(3729, 3899, 0), new Coordinate(3723, 3902, 0), new Coordinate(3717, 3902, 0));
}
