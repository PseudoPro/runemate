package com.sudo.v3.spectre.bots.apexcrabber.statics;

import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;

/**
 * Created by joeyw on 4/25/2017.
 */
public class TraversalAreas {

    public final static Area ZEAH_BANK_AREA = new Area.Rectangular(new Coordinate(1711, 3469, 0), new Coordinate(1722, 3460, 0));

    public final static Area ZEAH_CRAB_ISLAND_GOTO_AREA = new Area.Polygonal(new Coordinate(1774, 3461, 0), new Coordinate(1773, 3457, 0), new Coordinate(1776, 3454, 0), new Coordinate(1780, 3453, 0), new Coordinate(1785, 3454, 0), new Coordinate(1787, 3457, 0), new Coordinate(1786, 3460, 0), new Coordinate(1782, 3462, 0), new Coordinate(1778, 3462, 0));
    public final static Area ZEAH_CRAB_ISLAND_LEAVE_AREA = new Area.Polygonal(new Coordinate(1780, 3421, 0), new Coordinate(1777, 3420, 0), new Coordinate(1777, 3416, 0), new Coordinate(1777, 3414, 0), new Coordinate(1780, 3413, 0), new Coordinate(1783, 3413, 0), new Coordinate(1784, 3416, 0), new Coordinate(1783, 3419, 0));

    public final static Area PHASMATYS_NORTH_ENTRANCE= new Area.Rectangular(new Coordinate(3657, 3509, 0), new Coordinate(3662, 3506, 0));
    public final static Area PHASMATYS_SOUTH_ENTRANCE = new Area.Rectangular(new Coordinate(3667, 3455, 0), new Coordinate(3672, 3451, 0));
}
