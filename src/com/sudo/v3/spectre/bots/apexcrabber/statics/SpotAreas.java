package com.sudo.v3.spectre.bots.apexcrabber.statics;

import com.runemate.game.api.hybrid.location.Coordinate;

import java.util.ArrayList;

/**
 * Created by Joey on 3/15/2017.
 */
public class SpotAreas {
    public static ArrayList<ArrayList<Coordinate>> sandCrabShoreSpots = new ArrayList<ArrayList<Coordinate>>() {{
        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(1776, 3468, 0));
            add(new Coordinate(1775, 3469, 0));
            add(new Coordinate(1775, 3468, 0));
            add(new Coordinate(1776, 3469, 0));
        }});

        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(1790, 3468, 0));
            add(new Coordinate(1791, 3468, 0));
        }});

        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(1772, 3460, 0));
            add(new Coordinate(1773, 3461, 0));
            add(new Coordinate(1773, 3460, 0));
            add(new Coordinate(1772, 3462, 0));
        }});

        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(1764, 3469, 0));
            add(new Coordinate(1765, 3470, 0));
            add(new Coordinate(1764, 3470, 0));
            add(new Coordinate(1765, 3469, 0));
        }});
    }};

    public static ArrayList<ArrayList<Coordinate>> sandCrabIslandSpots = new ArrayList<ArrayList<Coordinate>>() {{
        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(1764, 3445, 0));
        }});

        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(1758, 3439, 0));
        }});

        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(1751, 3425, 0));
        }});

        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(1749, 3412, 0));
        }});

        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(1768, 3409, 0));
        }});

        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(1779, 3428, 0));
            add(new Coordinate(1777, 3429, 0));
        }});

        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(1780, 3438, 0));
        }});

        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(1780, 3407, 0));
        }});

        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(1786, 3404, 0));
        }});
    }};

    public static ArrayList<ArrayList<Coordinate>> rockCrabSpots = new ArrayList<ArrayList<Coordinate>>() {{
        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(2701, 3719, 0));
        }});

        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(2705, 3726, 0));
            add(new Coordinate(2704, 3726, 0));
        }});

        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(2718, 3720, 0));
            add(new Coordinate(2717, 3720, 0));
            add(new Coordinate(2718, 3721, 0));
            add(new Coordinate(2717, 3721, 0));
        }});

        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(2718, 3720, 0));
            add(new Coordinate(2719, 3720, 0));
            add(new Coordinate(2718, 3719, 0));
            add(new Coordinate(2719, 3719, 0));
        }});

        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(2709, 3719, 0));
            add(new Coordinate(2709, 3720, 0));
            add(new Coordinate(2709, 3721, 0));
        }});
    }};

    public static ArrayList<ArrayList<Coordinate>> swampCrabSlepeSpots = new ArrayList<ArrayList<Coordinate>>() {{
        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(3755, 3366, 0));
        }});

        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(3753, 3357, 0));
            add(new Coordinate(3753, 3358, 0));
            add(new Coordinate(3752, 3357, 0));
            add(new Coordinate(3753, 3359, 0));
            add(new Coordinate(3751, 3357, 0));
        }});

        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(3755, 3354, 0));
        }});
    }};

    public static ArrayList<ArrayList<Coordinate>> swampCrabPhasmatysEastSpots = new ArrayList<ArrayList<Coordinate>>() {{
        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(3695, 3442, 0));
            add(new Coordinate(3696, 3442, 0));
            add(new Coordinate(3695, 3443, 0));
            add(new Coordinate(3696, 3443, 0));
        }});

        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(3690, 3443, 0));
            add(new Coordinate(3691, 3443, 0));
            add(new Coordinate(3689, 3443, 0));
            add(new Coordinate(3690, 3444, 0));
            add(new Coordinate(3691, 3444, 0));
            add(new Coordinate(3689, 3444, 0));
        }});

        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(3690, 3439, 0));
            add(new Coordinate(3691, 3439, 0));
            add(new Coordinate(3689, 3439, 0));
            add(new Coordinate(3690, 3438, 0));
            add(new Coordinate(3691, 3438, 0));
            add(new Coordinate(3689, 3438, 0));
            add(new Coordinate(3690, 3437, 0));
            add(new Coordinate(3691, 3437, 0));
            add(new Coordinate(3689, 3437, 0));
        }});

        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(3693, 3439, 0));
            add(new Coordinate(3693, 3440, 0));
            add(new Coordinate(3693, 3438, 0));
            add(new Coordinate(3694, 3439, 0));
            add(new Coordinate(3694, 3440, 0));
            add(new Coordinate(3694, 3438, 0));
        }});
    }};

    public static ArrayList<ArrayList<Coordinate>> swampCrabPhasmatysWestSpots = new ArrayList<ArrayList<Coordinate>>() {{
        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(3636, 3423, 0));
            add(new Coordinate(3636, 3424, 0));
        }});

        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(3638, 3427, 0));
            add(new Coordinate(3638, 3428, 0));
            add(new Coordinate(3638, 3426, 0));
            add(new Coordinate(3639, 3427, 0));
            add(new Coordinate(3639, 3428, 0));
            add(new Coordinate(3639, 3426, 0));
        }});
    }};

    public static ArrayList<ArrayList<Coordinate>> AmmoniteCrabFossilIslandSpots = new ArrayList<ArrayList<Coordinate>>() {{
        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(3721, 3876, 0));
            add(new Coordinate(3721, 3875, 0));
            add(new Coordinate(3721, 3874, 0));
        }});

        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(3716, 3879, 0));
            add(new Coordinate(3716, 3880, 0));
        }});

        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(3714, 3884, 0));
            add(new Coordinate(3714, 3885, 0));
            add(new Coordinate(3715, 3884, 0));
            add(new Coordinate(3715, 3885, 0));
        }});

        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(3711, 3897, 0));
            add(new Coordinate(3710, 3897, 0));
            add(new Coordinate(3712, 3897, 0));
            add(new Coordinate(3710, 3898, 0));
            add(new Coordinate(3712, 3898, 0));
        }});

        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(3721, 3895, 0));
            add(new Coordinate(3721, 3896, 0));
            add(new Coordinate(3722, 3895, 0));
            add(new Coordinate(3722, 3896, 0));
            add(new Coordinate(3720, 3895, 0));
            add(new Coordinate(3721, 3894, 0));
            add(new Coordinate(3720, 3894, 0));
        }});

        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(3718, 3846, 0));
            add(new Coordinate(3717, 3846, 0));
            add(new Coordinate(3718, 3847, 0));
            add(new Coordinate(3717, 3847, 0));
            add(new Coordinate(3719, 3846, 0));
            add(new Coordinate(3718, 3845, 0));
            add(new Coordinate(3719, 3845, 0));
        }});

        add(new ArrayList<Coordinate>() {{
            add(new Coordinate(3733, 3845, 0));
            add(new Coordinate(3734, 3845, 0));
            add(new Coordinate(3733, 3846, 0));
            add(new Coordinate(3734, 3846, 0));
            add(new Coordinate(3733, 3844, 0));
            add(new Coordinate(3734, 3844, 0));
        }});
    }};
}
