package com.sudo.v3.spectre.bots.apexcrabber.ui;

import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.Players;
import com.sudo.v3.spectre.bots.apexcrabber.ApexCrabber;
import com.sudo.v3.spectre.bots.apexcrabber.enums.BotLocation;
import com.sudo.v3.spectre.bots.apexcrabber.enums.Playstyle;
import com.sudo.v3.spectre.bots.apexcrabber.enums.Potion;
import com.sudo.v3.spectre.bots.apexcrabber.statics.CrabAreas;
import com.sudo.v3.spectre.bots.apexcrabber.statics.SpotAreas;
import com.sudo.v3.spectre.bots.apexcrabber.statics.TraversalAreas;
import com.sudo.v3.spectre.statics.Areas;
import com.sudo.v3.ui.base.SudoFXController;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Java FX Controller for the CrabFXGui class
 */
public class CrabFXController extends SudoFXController implements Initializable {

    private final ApexCrabber bot;

    @FXML
    private ComboBox playstyleComboBox, crabTypeComboBox, foodSelectionComboBox, potSelectionComboBox, foodQuantityComboBox, potionQuantityComboBox;

    @FXML
    private CheckBox worldHopCheckBox, bankCheckBox, outOfPotionsCheckBox, pickupThrownCheckBox, useSpecialCheckBox;

    @FXML
    private Slider playerTriggerSlider, playerHealSlider;

    @FXML
    private Text playerCountText, promptText, playerHealText;

    @FXML
    private TextField xTextField, yTextField, planeTextField;

    @FXML
    private Button startButton, setLocationButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        if (!bot.isRS3) {
            playstyleComboBox.getItems().addAll("Active", "Spot Hop", "Stand Still");
            crabTypeComboBox.getItems().addAll("Sand Crab (Zeah Shore)", "Sand Crab (Crab Island)", "Rock Crab (Fremennik)", "Swamp Crab (Slepe)", "Swamp Crab (Phasmatys East)", "Swamp Crab (Phasmatys West)", "Ammonite Crab (Fossil Island)");
            crabTypeComboBox.getSelectionModel().select("Sand Crab (Zeah Shore)");
        } else {
            playstyleComboBox.getItems().addAll("Active", "Stand Still");
            crabTypeComboBox.getItems().addAll("Rock Crab (Fremennik)");
            crabTypeComboBox.getSelectionModel().select("Rock Crab (Fremennik)");
            bankCheckBox.setDisable(true);
        }

        foodSelectionComboBox.getItems().addAll("Select", "Manta ray", "Shark", "Monkfish", "Swordfish", "Lobster", "Tuna", "Trout", "Salmon", "Pineapple pizza");
        foodSelectionComboBox.getSelectionModel().select("Select");
        potSelectionComboBox.getItems().addAll("Select", "Sp Att (4)", "Sp Str (4)", "Sp Att & Str (4)", "Sp Combat (4)", "Ranged (4)", "Zamorak brew (4)", "Attack (4)", "Strength (4)", "Att & Str (4)");
        potSelectionComboBox.getSelectionModel().select("Select");

        // Handle player trigger value slider
        playerTriggerSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            playerCountText.setText(Integer.toString(newValue.intValue()));
            bot.playerRunCount = newValue.intValue();
        });

        // Handle player heal value slider
        playerHealSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            playerHealText.setText(Integer.toString(newValue.intValue()) + "%");
            bot.healPercent = newValue.doubleValue();
            bot.userProvidedPercent = newValue.doubleValue();
        });

        foodSelectionComboBox.setOnAction(getFoodComboBoxEvent());
        potSelectionComboBox.setOnAction(getPotionComboBoxEvent());

        foodQuantityComboBox.setOnAction(getFoodQuantityEvent());
        potionQuantityComboBox.setOnAction(getPotionQuantityEvent());

        playstyleComboBox.setOnAction(getplaystyleComboBoxEvent());
        crabTypeComboBox.setOnAction(getcrabTypeComboBoxEvent());
        bankCheckBox.setOnAction(getbankCheckBoxEvent());
        startButton.setOnAction(getStartButtonAction());

        setLocationButton.setOnAction(getSetLocationButtonAction());
    }

    public CrabFXController(ApexCrabber bot) {
        super(bot);
        this.bot = bot;
    }

    private EventHandler<ActionEvent> getSetLocationButtonAction() {
        return event -> {
            promptText.setText("");
            System.out.println("Attempting to get player position.");
            bot.getPlatform().invokeLater(() -> {
                Player player = Players.getLocal();
                if (player != null) {
                    bot.standStillCoord = player.getPosition();

                    Platform.runLater(() -> {
                        xTextField.textProperty().set(Integer.toString(bot.standStillCoord.getX()));
                        yTextField.textProperty().set(Integer.toString(bot.standStillCoord.getY()));
                        planeTextField.textProperty().set(Integer.toString(bot.standStillCoord.getPlane()));
                        handleStartButton();
                    });
                }
            });
        };
    }

    public EventHandler<ActionEvent> getplaystyleComboBoxEvent() {
        return event ->
        {
            String playstyleText = playstyleComboBox.getSelectionModel().getSelectedItem().toString();
            if (playstyleText.equals("Stand Still")) {
                xTextField.setDisable(false);
                yTextField.setDisable(false);
                planeTextField.setDisable(false);
                setLocationButton.setDisable(false);
            } else {
                promptText.setText(" ");
                xTextField.setDisable(true);
                yTextField.setDisable(true);
                planeTextField.setDisable(true);
                setLocationButton.setDisable(true);
            }

            worldHopCheckBox.setDisable(!playstyleText.equals("Stand Still"));
            playerTriggerSlider.setDisable(playstyleText.equals("Active"));

            handleStartButton();
        };
    }

    public EventHandler<ActionEvent> getcrabTypeComboBoxEvent() {
        return event -> {
            if (crabTypeComboBox.getSelectionModel().getSelectedItem().equals("Rock Crab (Fremennik)")) {
                playstyleComboBox.getItems().clear();
                playstyleComboBox.getItems().addAll("Active", "Stand Still");
                bankCheckBox.setDisable(true);
            }
            if (crabTypeComboBox.getSelectionModel().getSelectedItem().equals("Sand Crab (Crab Island)")) {
                playstyleComboBox.getItems().clear();
                playstyleComboBox.getItems().addAll("Spot Hop", "Stand Still");
                bankCheckBox.setDisable(false);
            } else {
                playstyleComboBox.getItems().clear();
                playstyleComboBox.getItems().addAll("Active", "Spot Hop", "Stand Still");
                bankCheckBox.setDisable(false);
            }
            handleStartButton();
        };
    }

    public EventHandler<ActionEvent> getbankCheckBoxEvent() {
        return event -> {
            foodSelectionComboBox.setDisable(!bankCheckBox.isSelected());
            potSelectionComboBox.setDisable(!bankCheckBox.isSelected());

            handleStartButton();
        };
    }

    public EventHandler<ActionEvent> getFoodComboBoxEvent() {
        return event -> {
            String selectedFood = getFoodSelected();
            foodQuantityComboBox.setDisable(selectedFood.equals("Select"));

            foodQuantityComboBox.getItems().clear();

            if (!selectedFood.equals("Select")) {
                for (int i = 1; i < 28; i++) { // - potionQuantity; i++) {
                    foodQuantityComboBox.getItems().add(i);
                }
                foodQuantityComboBox.getSelectionModel().select("10");
            }

            handleStartButton();
        };
    }

    public EventHandler<ActionEvent> getPotionComboBoxEvent() {
        return event -> {
            String selectedPotion = getPotionSelected();
            potionQuantityComboBox.setDisable(selectedPotion.equals("Select"));

            potionQuantityComboBox.getItems().clear();

            if (!selectedPotion.equals("Select")) {
                if (selectedPotion.equals("Sp Att & Str (4)")) {
                    for (int i = 1; i < 15; i++) { // - (foodQuantity * 2); i++) {
                        potionQuantityComboBox.getItems().add(i + " each");
                    }
                    potionQuantityComboBox.getSelectionModel().select("5 each");
                } else {
                    for (int i = 1; i < 28; i++) { // - foodQuantity; i++) {
                        potionQuantityComboBox.getItems().add(i);
                    }

                    potionQuantityComboBox.getSelectionModel().select("5");
                }
            }

            handleStartButton();
        };
    }

    public EventHandler<ActionEvent> getFoodQuantityEvent() {
        return event -> {
            String foodQuantityString = getFoodQuantity();

            if (!foodQuantityString.equals("Select")) {
                int foodQuantity = Integer.valueOf(foodQuantityString);
                String potQuantityString = getPotionQuantityString();

                if (!potQuantityString.equals("")) {
                    int potQuantity = Integer.valueOf(potQuantityString);
                    if (getPotionSelected().equals("Sp Att & Str (4)")) {
                        potQuantity = potQuantity * 2;
                    }

                    if (foodQuantity + potQuantity >= 26)
                        promptText.setText("Please ensure that you are not exceeding inventory capacity.");
                    else
                        promptText.setText(" ");
                }
            } else {
                foodQuantityComboBox.getItems().clear();
            }

            handleStartButton();
        };
    }

    public EventHandler<ActionEvent> getPotionQuantityEvent() {
        return event -> {
            String potionQuantityString = getPotionQuantityString();

            if (!potionQuantityString.equals("Select")) {
                int potQuantity = Integer.valueOf(potionQuantityString);
                String foodQuantityString = getFoodQuantity();

                if (!foodQuantityString.equals("")) {
                    int foodQuantity = Integer.valueOf(foodQuantityString);
                    if (getPotionSelected().equals("Sp Att & Str (4)")) {
                        potQuantity = potQuantity * 2;
                    }

                    if (foodQuantity + potQuantity >= 26)
                        promptText.setText("Please ensure that you are not exceeding inventory capacity.");
                    else
                        promptText.setText(" ");
                }
            } else {
                potionQuantityComboBox.getItems().clear();
            }

            handleStartButton();
        };
    }

    private void handleStartButton() {
        String foodSelection = getFoodSelected();
        String potSelection = getPotionSelected();

        String foodQuantity = getFoodQuantity();
        String potionQuantity = getPotionQuantityString();

        startButton.setDisable(playstyleComboBox.getSelectionModel().getSelectedItem() == null ||
                (playstyleComboBox.getSelectionModel().getSelectedItem().toString().equals("Stand Still") && bot.standStillCoord == null) ||
                (bankCheckBox.isSelected() && ((foodSelection.equals("Select") || (!foodSelection.equals("Select") && foodQuantity.equals("")))
                        && (potSelection.equals("Select") || (!potSelection.equals("Select") && potionQuantity.equals(""))))));
    }

    public EventHandler<ActionEvent> getStartButtonAction() {
        return event ->
        {
            if (crabTypeComboBox.getSelectionModel().getSelectedItem() != null && playstyleComboBox.getSelectionModel().getSelectedItem() != null) {
                switch (crabTypeComboBox.getSelectionModel().getSelectedItem().toString()) {
                    case "Rock Crab (Fremennik)":
                        bot.crabName = "Rock Crab";
                        bot.shellName = "Rocks";
                        bot.crabArea = CrabAreas.FREM_ROCKCRAB_AREA;
                        bot.spotLists = SpotAreas.rockCrabSpots;
                        bot.refreshArea = CrabAreas.FREM_ROCKCRAB_REFRESH_AREA;
                        bot.botLocation = BotLocation.FREM_SHORE;
                        bot.bankArea = Areas.SEER_BANK_AREA;
                        break;

                    case "Sand Crab (Zeah Shore)":
                        bot.crabName = "Sand Crab";
                        bot.shellName = "Sandy rocks";
                        bot.crabArea = CrabAreas.ZEAH_SHORE_SANDCRAB_AREA;
                        bot.spotLists = SpotAreas.sandCrabShoreSpots;
                        bot.refreshArea = CrabAreas.ZEAH_SHORE_SANDCRAB_REFRESH_AREA;
                        bot.botLocation = BotLocation.ZEAH_SHORE;
                        bot.bankArea = TraversalAreas.ZEAH_BANK_AREA;
                        break;

                    case "Sand Crab (Crab Island)":
                        bot.crabName = "Sand Crab";
                        bot.shellName = "Sandy rocks";
                        bot.crabArea = CrabAreas.ZEAH_ISLAND_SANDCRAB_AREA;
                        bot.spotLists = SpotAreas.sandCrabIslandSpots;
                        bot.refreshArea = null;
                        bot.botLocation = BotLocation.CRAB_ISLAND;
                        bot.bankArea = TraversalAreas.ZEAH_BANK_AREA;
                        break;

                    case "Swamp Crab (Slepe)":
                        bot.crabName = "Swamp Crab";
                        bot.shellName = "Swampy log";
                        bot.crabArea = CrabAreas.SLEPE_SWAMPCRAB_AREA;
                        bot.refreshArea = CrabAreas.SLEPE_SWAMPCRAB_REFRESH_AREA;
                        bot.spotLists = SpotAreas.swampCrabSlepeSpots;
                        bot.botLocation = BotLocation.SLEPE;
                        bot.bankArea = Areas.PHASMATYS_BANK_AREA;
                        break;

                    case "Swamp Crab (Phasmatys East)":
                        bot.crabName = "Swamp Crab";
                        bot.shellName = "Swampy log";
                        bot.crabArea = CrabAreas.PHASMATYS_EAST_SWAMPCRAB_AREA;
                        bot.refreshArea = CrabAreas.PHASMATYS_EAST_REFRESH_AREA;
                        bot.spotLists = SpotAreas.swampCrabPhasmatysEastSpots;
                        bot.botLocation = BotLocation.PHASMATYS_EAST;
                        bot.bankArea = Areas.PHASMATYS_BANK_AREA;
                        break;

                    case "Swamp Crab (Phasmatys West)":
                        bot.crabName = "Swamp Crab";
                        bot.shellName = "Swampy log";
                        bot.crabArea = CrabAreas.PHASMATYS_WEST_SWAMPCRAB_AREA;
                        bot.refreshArea = CrabAreas.PHASMATYS_WEST_REFRESH_AREA;
                        bot.spotLists = SpotAreas.swampCrabPhasmatysWestSpots;
                        bot.botLocation = BotLocation.PHASMATYS_WEST;
                        bot.bankArea = Areas.PHASMATYS_BANK_AREA;
                        break;

                    case "Ammonite Crab (Fossil Island)":
                        bot.crabName = "Ammonite Crab";
                        bot.shellName = "Fossil Rock";
                        bot.crabArea = CrabAreas.FOSSIL_ISLAND_AMMONITECRAB_AREA;
                        bot.refreshArea = null;
                        bot.spotLists = SpotAreas.swampCrabPhasmatysWestSpots;
                        bot.botLocation = BotLocation.FOSSIL_ISLAND;
                        bot.bankArea = Areas.FOSSIL_ISLAND_BANK_AREA;
                }

                switch (playstyleComboBox.getSelectionModel().getSelectedItem().toString()) {
                    case "Active":
                        bot.playStyle = Playstyle.ACTIVE;
                        break;

                    case "Spot Hop":
                        bot.playStyle = Playstyle.SPOT_JUMP;
                        break;

                    case "Stand Still":
                        bot.playStyle = Playstyle.STAND_STILL;
                        break;
                }

                String potion = potSelectionComboBox.getSelectionModel().getSelectedItem().toString();
                switch (potion) {
                    case "Sp Att (4)":
                        bot.potionType = Potion.SP_ATT;
                        bot.usingPotion = true;
                        break;
                    case "Sp Str (4)":
                        bot.potionType = Potion.SP_STR;
                        bot.usingPotion = true;
                        break;
                    case "Sp Att & Str (4)":
                        bot.potionType = Potion.SP_ATT_STR;
                        bot.usingPotion = true;
                        break;
                    case "Sp Combat (4)":
                        bot.potionType = Potion.SP_COMBAT;
                        bot.usingPotion = true;
                        break;
                    case "Ranged (4)":
                        bot.potionType = Potion.RANGED;
                        bot.usingPotion = true;
                        break;
                    case "Zamorak brew (4)":
                        bot.potionType = Potion.ZAM;
                        bot.usingPotion = true;
                        break;
                    case "Attack (4)":
                        bot.potionType = Potion.ATT;
                        bot.usingPotion = true;
                        break;
                    case "Strength (4)":
                        bot.potionType = Potion.STR;
                        bot.usingPotion = true;
                        break;
                    case "Att & Str (4)":
                        bot.potionType = Potion.ATT_STR;
                        bot.usingPotion = true;
                        break;
                    default:
                        bot.usingPotion = false;
                }

                String food = foodSelectionComboBox.getSelectionModel().getSelectedItem().toString();
                if (!food.equals("Select")) {
                    bot.foodName = food;
                    bot.foodQuantity = getFoodQuantityAsInt();
                    bot.usingFood = true;
                }

                if (xTextField.getText() != null)
                    bot.standStillCoord = new Coordinate(Integer.parseInt(xTextField.getText()), Integer.parseInt(yTextField.getText()), Integer.parseInt(planeTextField.getText()));

                bot.stopIfNoPotion = outOfPotionsCheckBox.isSelected();

                bot.bankingEnabled = bankCheckBox.isSelected();

                bot.worldHop = worldHopCheckBox.isSelected() && !worldHopCheckBox.isDisable();

                bot.pickupThrownEquipment = pickupThrownCheckBox.isSelected();

                bot.potionQuantity = getPotionQuantityAsInt();

                bot.useSpecial = useSpecialCheckBox.isSelected();

                super.getStart_BTEvent();

                startButton.textProperty().set("Resume");
            } else {
                promptText.setText("Please select your preferred playstyle and crabtype.");
            }
        };
    }

    private String getFoodSelected() {
        Object selectedObj = foodSelectionComboBox.getSelectionModel().getSelectedItem();

        if (selectedObj != null)
            return selectedObj.toString();
        else
            return "Select";
    }

    private String getPotionSelected() {
        Object selectedObj = potSelectionComboBox.getSelectionModel().getSelectedItem();

        if (selectedObj != null)
            return selectedObj.toString();
        else
            return "Select";
    }

    private String getFoodQuantity() {
        Object selectedObj = foodQuantityComboBox.getSelectionModel().getSelectedItem();

        if (selectedObj != null)
            return selectedObj.toString();
        else
            return "";
    }

    private String getPotionQuantityString() {
        Object selectedObj = potionQuantityComboBox.getSelectionModel().getSelectedItem();

        if (selectedObj != null) {
            String quantity = selectedObj.toString();

            if (quantity.contains("each"))
                return quantity.substring(0, quantity.indexOf(" "));
            else
                return quantity;
        } else
            return "";
    }

    private int getPotionQuantityAsInt() {
        Object selectedObj = potionQuantityComboBox.getSelectionModel().getSelectedItem();

        if (selectedObj != null) {
            String quantity = selectedObj.toString();

            if (quantity.contains("each"))
                return Integer.valueOf(quantity.substring(0, quantity.indexOf(" "))) * 2;
            else
                return Integer.valueOf(quantity);
        } else
            return -1;
    }

    private int getFoodQuantityAsInt() {
        Object selectedObj = foodQuantityComboBox.getSelectionModel().getSelectedItem();

        if (selectedObj != null) {
            String quantity = selectedObj.toString();

            return Integer.valueOf(quantity);
        } else
            return -1;
    }

}