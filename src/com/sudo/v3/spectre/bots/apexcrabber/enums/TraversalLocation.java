package com.sudo.v3.spectre.bots.apexcrabber.enums;

/**
 * Created by SudoPro on 11/23/2016.
 */
public enum TraversalLocation
{
    REFRESH_AREA,
    CRAB_SPOT,
    CRAB_AREA,
    CRAB_SPOT_AREA,
    BANK_AREA,
}
