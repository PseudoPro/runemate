package com.sudo.v3.spectre.bots.apexcrabber.enums;

/**
 * Created by Proxify on 4/10/2017.
 */
public enum BotLocation {
    ZEAH_SHORE,
    CRAB_ISLAND,
    FREM_SHORE,
    SLEPE,
    PHASMATYS_EAST,
    PHASMATYS_WEST,
    FOSSIL_ISLAND
}
