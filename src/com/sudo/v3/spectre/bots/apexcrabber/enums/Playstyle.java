package com.sudo.v3.spectre.bots.apexcrabber.enums;

/**
 * Created by Joey on 3/14/2017.
 */
public enum Playstyle
{
    ACTIVE,
    STAND_STILL,
    SPOT_JUMP
}
