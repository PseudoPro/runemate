package com.sudo.v3.spectre.bots.sudochopper.ui;

import com.sudo.v3.spectre.bots.sudochopper.ApexChopper;
import com.sudo.v3.spectre.bots.sudochopper.enums.DropType;
import com.sudo.v3.spectre.bots.sudochopper.enums.LocationType;
import com.sudo.v3.spectre.statics.Areas;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.sudo.v3.ui.base.SudoFXController;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Java FX Controller for the CookerFXGui class
 */
public class CutterFXController extends SudoFXController implements Initializable {

    private ApexChopper bot;

    @FXML
    private ListView locationListView, treeListView, treeP_LV;

    @FXML
    private Button startB_BT, startP_BT;

    @FXML
    private TabPane tabPane;

    @FXML
    private Tab bank_Tab, powerChop_Tab;

    @FXML
    private ComboBox dropMethod_ComB;

    @FXML
    private CheckBox Nest_CB, powerChop_CB, bank_CB, superheat_CB, ignoreHatchetCheckboxBank, ignoreHatchetCheckboxPower;

    @FXML
    private Label Prompt_L, PromptP_L;

    @FXML
    private CheckBox usePreset_CB;

    @FXML
    private RadioButton one_Radio, two_Radio;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Initialize Super
        super.initialize(location, resources);

        //if(bot.getMetaData().getHourlyPrice().doubleValue() > 0 || bot.isPrivate() || Environment.isSDK()){
            // Add Locations to the ListView
        if(rs3) {
            treeListView.getItems().addAll("Tree", "Evergreen", "Oak", "Willow", "Acadia", "Maple", "Yew", "Magic");
            treeP_LV.getItems().addAll("Tree", "Evergreen", "Oak", "Willow", "Maple", "Yew", "Magic");
        } else {
            treeListView.getItems().addAll("Tree", "Evergreen", "Oak", "Willow", "Maple", "Yew", "Magic", "Redwood");
            treeP_LV.getItems().addAll("Tree", "Evergreen", "Oak", "Willow", "Maple", "Yew", "Magic");
        }
        //}else{
            // Add Locations to the ListView
        //    treeListView.getItems().addAll("Tree", "Evergreen", "Oak", "Willow");
        //    treeP_LV.getItems().addAll("Tree", "Evergreen", "Oak", "Willow");
        //}

        dropMethod_ComB.getItems().addAll("fullDrop", "randomDrop");
        dropMethod_ComB.getSelectionModel().select("fullDrop");

        treeListView.setOnMouseClicked(getTreeClickAction());
        treeP_LV.setOnMouseClicked(getTreeP_LVAction());

        locationListView.setOnMouseClicked(getLocationClickAction());

        startB_BT.setOnAction(getStart_BTAction());
        startP_BT.setOnAction(getStart_BTAction());

        powerChop_CB.setOnAction(getPowerChop_CBAction());
        bank_CB.setOnAction(getBank_CB_CBAction());

        dropMethod_ComB.setOnAction(getDropMethod_ComBAction());

        // If the bot is OSRS, disable preset option
        usePreset_CB.setDisable(!bot.isRS3);
        superheat_CB.setDisable(!bot.isRS3);

        usePreset_CB.setOnAction(getUsePreset_CBAction());

        //powerChop_Tab.setDisable(bot.isRS3);
        //powerChop_CB.setDisable(bot.isRS3);
    }

    public CutterFXController(ApexChopper bot) {
        super(bot);
        this.bot = bot;
    }

    private EventHandler<ActionEvent> getPowerChop_CBAction(){
        return event -> {
            if(treeP_LV.getSelectionModel().getSelectedItem() != null && dropMethod_ComB.getSelectionModel().getSelectedItem() != null && powerChop_CB.isSelected()) {
                startP_BT.setDisable(false);
                startB_BT.setDisable(true);
            }
            else if(locationListView.getSelectionModel().getSelectedItem() != null && bank_CB.isSelected()) {
                startP_BT.setDisable(true);
                startB_BT.setDisable(false);
            }
            else {
                startP_BT.setDisable(true);
                startB_BT.setDisable(true);
            }

            if(powerChop_CB.isSelected()) {
                tabPane.getSelectionModel().select(powerChop_Tab);
                bank_CB.setSelected(false);
                treeP_LV.setDisable(false);
                dropMethod_ComB.setDisable(false);
                locationListView.setDisable(true);
                treeListView.setDisable(true);
                Nest_CB.setDisable(true);
            }
            else{
                powerChop_CB.setSelected(false);
                bank_CB.setSelected(true);
                treeP_LV.setDisable(true);
                dropMethod_ComB.setDisable(true);
                locationListView.setDisable(false);
                treeListView.setDisable(false);
                Nest_CB.setDisable(false);
            }
        };
    }

    private EventHandler<ActionEvent> getBank_CB_CBAction(){
        return event -> {
            if(treeP_LV.getSelectionModel().getSelectedItem() != null && dropMethod_ComB.getSelectionModel().getSelectedItem() != null && powerChop_CB.isSelected()) {
                startP_BT.setDisable(false);
                startB_BT.setDisable(true);
            }
            else if(locationListView.getSelectionModel().getSelectedItem() != null && bank_CB.isSelected()) {
                startP_BT.setDisable(true);
                startB_BT.setDisable(false);
            }
            else {
                startP_BT.setDisable(true);
                startB_BT.setDisable(true);
            }

            if(bank_CB.isSelected()) {
                tabPane.getSelectionModel().select(bank_Tab);
                powerChop_CB.setSelected(false);
                treeP_LV.setDisable(true);
                dropMethod_ComB.setDisable(true);
                locationListView.setDisable(false);
                treeListView.setDisable(false);
                Nest_CB.setDisable(false);
            }
            else{
                powerChop_CB.setSelected(true);
                bank_CB.setSelected(false);
                treeP_LV.setDisable(false);
                dropMethod_ComB.setDisable(false);
                locationListView.setDisable(true);
                treeListView.setDisable(true);
                Nest_CB.setDisable(true);
            }
        };
    }

    private EventHandler<MouseEvent> getTreeP_LVAction(){
        return event -> {
            if(treeP_LV.getSelectionModel().getSelectedItem() != null && dropMethod_ComB.getSelectionModel().getSelectedItem() != null && powerChop_CB.isSelected()) {
                startP_BT.setDisable(false);
                startB_BT.setDisable(true);
            }
            else if(locationListView.getSelectionModel().getSelectedItem() != null && bank_CB.isSelected()) {
                startP_BT.setDisable(true);
                startB_BT.setDisable(false);
            }
            else {
                startP_BT.setDisable(true);
                startB_BT.setDisable(true);
            }
        };
    }

    private EventHandler<ActionEvent> getDropMethod_ComBAction() {
        return event -> {
            if (treeP_LV.getSelectionModel().getSelectedItem() != null && dropMethod_ComB.getSelectionModel().getSelectedItem() != null && powerChop_CB.isSelected()) {
                startP_BT.setDisable(false);
                startB_BT.setDisable(true);
            } else if (locationListView.getSelectionModel().getSelectedItem() != null && bank_CB.isSelected()) {
                startP_BT.setDisable(true);
                startB_BT.setDisable(false);
            } else {
                startP_BT.setDisable(true);
                startB_BT.setDisable(true);
            }
        };
    }

    private EventHandler<MouseEvent> getLocationClickAction() {
        return event -> {
            if(treeP_LV.getSelectionModel().getSelectedItem() != null && dropMethod_ComB.getSelectionModel().getSelectedItem() != null && powerChop_CB.isSelected()) {
                startP_BT.setDisable(false);
                startB_BT.setDisable(true);
            }
            else if(locationListView.getSelectionModel().getSelectedItem() != null && bank_CB.isSelected()) {
                startP_BT.setDisable(true);
                startB_BT.setDisable(false);
            }
            else {
                startP_BT.setDisable(true);
                startB_BT.setDisable(true);
            }
        };
    }


    private EventHandler<MouseEvent> getTreeClickAction() {
        return event -> {
            locationListView.getItems().clear();
            if(treeP_LV.getSelectionModel().getSelectedItem() != null && dropMethod_ComB.getSelectionModel().getSelectedItem() != null && powerChop_CB.isSelected()) {
                startP_BT.setDisable(false);
                startB_BT.setDisable(true);
            }
            else if(locationListView.getSelectionModel().getSelectedItem() != null && bank_CB.isSelected()) {
                startP_BT.setDisable(true);
                startB_BT.setDisable(false);
            }
            else {
                startP_BT.setDisable(true);
                startB_BT.setDisable(true);
            }

            try {
                switch (treeListView.getSelectionModel().getSelectedItem().toString()) {
                    case "Tree":
                        locationListView.getItems().add("Draynor North");
                        //locationListView.getItems().add("Draynor South-East");
                        locationListView.getItems().add("Falador");
                        if(!rs3) {
                            locationListView.getItems().add("Woodcutting Guild");
                        }
                        break;

                    case "Evergreen":
                        locationListView.getItems().add("Catherby");
                        break;

                    case "Oak":
                        //locationListView.getItems().add("Draynor South-East");
                        locationListView.getItems().add("Falador");
                        locationListView.getItems().add("Catherby");
                        if(!rs3) {
                            locationListView.getItems().add("Woodcutting Guild");
                        }
                        break;

                    case "Willow":
                        locationListView.getItems().add("Draynor");
                        locationListView.getItems().add("Catherby");
                        locationListView.getItems().add("Seers");
                        locationListView.getItems().add("Woodcutting Guild");
                        break;

                    case "Acadia":
                        locationListView.getItems().add("Menaphos VIP");
                        break;

                    case "Maple":
                        locationListView.getItems().add("Seers");
                        if(!rs3) {
                            locationListView.getItems().add("Woodcutting Guild");
                        }
                        break;

                    case "Yew":
                        locationListView.getItems().add("G.E.");
                        locationListView.getItems().add("Edgeville");
                        //locationListView.getItems().add("Falador South-East");
                        //locationListView.getItems().add("Falador South-West");
                        locationListView.getItems().add("Catherby");
                        locationListView.getItems().add("Seers");
                        if(!rs3) {
                            locationListView.getItems().add("Woodcutting Guild");
                        }
                        break;

                    case "Magic":
                        locationListView.getItems().add("Seers South-West");
                        locationListView.getItems().add("Seers South");
                        locationListView.getItems().add("Zeah (Land's End)");
                        if(!rs3) {
                            locationListView.getItems().add("Woodcutting Guild");
                        }
                        break;

                    case "Redwood":
                        locationListView.getItems().add("Woodcutting Guild");
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
    }
    private EventHandler<ActionEvent> getStart_BTAction() {
        return event -> {
            String location = null, tree = null;

            if(bank_CB.isSelected()) {
                // Handle Banking Startup
                powerChop_CB.setDisable(true);
                powerChop_Tab.setDisable(true);
                bot.powerChop = false;

                try {
                    location = locationListView.getSelectionModel().getSelectedItem().toString();
                    tree = treeListView.getSelectionModel().getSelectedItem().toString();
                } catch (Exception e) {
                    Prompt_L.textProperty().set("Be sure to have location and tree both selected.");
                    UpdateUI.debug("Be sure to have location and tree both selected.");
                }

                if (location != null && tree != null) {
                    bot.treeName = tree;
                    bot.treeAction = "Chop down";
                    bot.pickUpNest = Nest_CB.isSelected();
                    bot.locationType = LocationType.GENERIC;
                    bot.locationNameString = location;
                    try {
                        switch (tree) {
                            case "Tree":
                                switch (location) {
                                    case "Draynor North":
                                        bot.chopArea = Areas.DRAY_NORM_N_TREE_AREA;
                                        bot.treeArea = Areas.DRAY_NORM_N_TREE_AREA;
                                        bot.bankArea = Areas.DRAY_BANK_AREA;
                                        bot.bresenhamLocation = false;
                                        break;
                                    case "Draynor South-East":
                                        bot.chopArea = Areas.DRAY_NORM_SE_TREE_AREA;
                                        bot.treeArea = Areas.DRAY_NORM_SE_TREE_AREA;
                                        bot.bankArea = Areas.DRAY_BANK_AREA;
                                        bot.bresenhamLocation = false;
                                        break;
                                    case "Falador":
                                        bot.chopArea = Areas.FALA_NORM_OAK_TREE_AREA;
                                        bot.treeArea = Areas.FALA_NORM_OAK_TREE_AREA;
                                        bot.bankArea = Areas.FALA_E_BANK_AREA;
                                        bot.bresenhamLocation = false;
                                        break;
                                    case "Woodcutting Guild":
                                        bot.locationType = LocationType.WOOD_GUILD;
                                        bot.chopArea = Areas.WOOD_GUILD_NORMAL_TREE_AREA;
                                        bot.treeArea = Areas.WOOD_GUILD_NORMAL_TREE_AREA;
                                        bot.bankArea = Areas.WOOD_GUILD_BANK_AREA;
                                        bot.bresenhamLocation = false;
                                        break;
                                }
                                break;

                            case "Evergreen":
                                switch (location) {
                                    case "Catherby":
                                        bot.chopArea = Areas.CATH_EVER_TREE_AREA;
                                        bot.treeArea = Areas.CATH_EVER_TREE_AREA;
                                        if(bot.isRS3)
                                            bot.bankArea = Areas.CATH_RS3_BANK_AREA;
                                        else
                                            bot.bankArea = Areas.CATH_OSRS_BANK_AREA;
                                        bot.bresenhamLocation = false;
                                        break;
                                }
                                break;
                            case "Oak":
                                switch (location) {
                                    case "Draynor South-East":
                                        bot.chopArea = Areas.DRAY_NORM_SE_TREE_AREA;
                                        bot.treeArea = Areas.DRAY_NORM_SE_TREE_AREA;
                                        bot.bankArea = Areas.DRAY_BANK_AREA;
                                        bot.bresenhamLocation = false;
                                        break;
                                    case "Falador":
                                        bot.chopArea = Areas.FALA_NORM_OAK_TREE_AREA;
                                        bot.treeArea = Areas.FALA_NORM_OAK_TREE_AREA;
                                        bot.bankArea = Areas.FALA_E_BANK_AREA;
                                        bot.bresenhamLocation = false;
                                        break;
                                    case "Catherby":
                                        if(bot.isRS3) {
                                            bot.bankArea = Areas.CATH_RS3_BANK_AREA;
                                            bot.chopArea = Areas.CATH_RS3_OAK_TREE_AREA;
                                            bot.treeArea = Areas.CATH_RS3_OAK_TREE_AREA;
                                        }
                                        else {
                                            bot.bankArea = Areas.CATH_OSRS_BANK_AREA;
                                            bot.chopArea = Areas.CATH_OSRS_OAK_TREE_AREA;
                                            bot.treeArea = Areas.CATH_OSRS_OAK_TREE_AREA;
                                        }
                                        bot.bresenhamLocation = false;
                                        break;
                                    case "Woodcutting Guild":
                                        bot.locationType = LocationType.WOOD_GUILD;
                                        bot.chopArea = Areas.WOOD_GUILD_OAK_TREE_AREA;
                                        bot.treeArea = Areas.WOOD_GUILD_OAK_TREE_AREA;
                                        bot.bankArea = Areas.WOOD_GUILD_BANK_AREA;
                                        bot.bresenhamLocation = false;
                                        break;
                                }
                                break;
                            case "Willow":
                                switch (location) {
                                    case "Draynor":
                                        bot.chopArea = Areas.DRAY_WILL_TREE_AREA;
                                        bot.treeArea = Areas.DRAY_WILL_TREE_ACTION_AREA;
                                        bot.bankArea = Areas.DRAY_BANK_AREA;
                                        bot.bresenhamLocation = false;
                                        break;
                                    case "Catherby":
                                        bot.chopArea = Areas.CATH_WILL_TREE_AREA;
                                        bot.treeArea = Areas.CATH_WILL_TREE_ACTION_AREA;
                                        if(bot.isRS3)
                                            bot.bankArea = Areas.CATH_RS3_BANK_AREA;
                                        else
                                            bot.bankArea = Areas.CATH_OSRS_BANK_AREA;
                                        bot.bresenhamLocation = false;
                                        break;
                                    case "Seers":
                                        bot.chopArea = Areas.SEER_WILL_TREE_AREA;
                                        bot.treeArea = Areas.SEER_WILL_TREE_AREA;
                                        bot.bankArea = Areas.SEER_BANK_AREA;
                                        bot.bresenhamLocation = false;
                                        break;
                                    case "Woodcutting Guild":
                                        bot.locationType = LocationType.WOOD_GUILD;
                                        bot.chopArea = Areas.WOOD_GUILD_WILLOW_TREE_AREA;
                                        bot.treeArea = Areas.WOOD_GUILD_WILLOW_TREE_AREA;
                                        bot.bankArea = Areas.WOOD_GUILD_BANK_AREA;
                                        bot.bresenhamLocation = false;
                                        break;
                                }
                                break;

                            case "Acadia":
                                bot.treeName = "Acadia tree";
                                bot.treeAction = "Cut down";
                                switch(location){
                                    case "Menaphos VIP":
                                        bot.chopArea = Areas.MENAPHOS_VIP_TREE_AREA;
                                        bot.treeArea = Areas.MENAPHOS_VIP_TREE_AREA;
                                        bot.bankArea = Areas.MENAPHOS_VIP_BANK_AREA;
                                        bot.locationType = LocationType.MENAPHOS;
                                        break;
                                }
                                break;

                            case "Maple":
                                if(bot.isRS3)
                                    bot.treeName = "Maple Tree";
                                else
                                    bot.treeName = "Maple tree";

                                switch (location) {
                                    case "Seers":
                                        bot.chopArea = Areas.SEER_MAPLE_TREE_AREA;
                                        bot.treeArea = Areas.SEER_MAPLE_TREE_AREA;
                                        bot.bankArea = Areas.SEER_BANK_AREA;
                                        bot.bresenhamLocation = false;
                                        break;
                                    case "Woodcutting Guild":
                                        bot.locationType = LocationType.WOOD_GUILD;
                                        bot.chopArea = Areas.WOOD_GUILD_MAPLE_TREE_AREA;
                                        bot.treeArea = Areas.WOOD_GUILD_MAPLE_TREE_AREA;
                                        bot.bankArea = Areas.WOOD_GUILD_BANK_AREA;
                                        bot.bresenhamLocation = false;
                                        break;
                                }
                                break;
                            case "Yew":
                                switch (location) {
                                    case "G.E.":
                                        bot.chopArea = Areas.GE_YEW_TREE_AREA;
                                        bot.treeArea = Areas.GE_YEW_TREE_AREA;
                                        if(bot.isRS3)
                                            bot.bankArea = Areas.GE_RS3_BANK_AREA;
                                        else
                                            bot.bankArea = Areas.GE_OSRS_BANK_AREA;
                                        bot.bresenhamLocation = false;
                                        break;
                                    case "Edgeville":
                                        bot.chopArea = Areas.EDGE_YEW_TREE_AREA;
                                        bot.treeArea = Areas.EDGE_YEW_TREE_AREA;
                                        bot.bankArea = Areas.EDGE_BANK_AREA;
                                        bot.bresenhamLocation = false;
                                        break;
                                    case "Falador South-East":
                                        bot.chopArea = Areas.FALA_YEW_SE_TREE_AREA;
                                        bot.treeArea = Areas.FALA_YEW_SE_TREE_AREA;
                                        bot.bankArea = Areas.FALA_E_BANK_AREA;
                                        bot.bresenhamLocation = false;
                                        break;
                                    case "Falador South-West":
                                        bot.chopArea = Areas.FALA_YEW_SW_TREE_AREA;
                                        bot.treeArea = Areas.FALA_YEW_SW_TREE_AREA;
                                        bot.bankArea = Areas.FALA_E_BANK_AREA;
                                        bot.bresenhamLocation = false;
                                        break;
                                    case "Catherby":
                                        bot.chopArea = Areas.CATH_YEW_TREE_AREA;
                                        bot.treeArea = Areas.CATH_YEW_TREE_ACTION_AREA;
                                        if(bot.isRS3)
                                            bot.bankArea = Areas.CATH_RS3_BANK_AREA;
                                        else
                                            bot.bankArea = Areas.CATH_OSRS_BANK_AREA;
                                        bot.bresenhamLocation = false;
                                        break;
                                    case "Seers":
                                        bot.chopArea = Areas.SEER_YEW_TREE_AREA;
                                        bot.treeArea = Areas.SEER_YEW_TREE_AREA;
                                        bot.bankArea = Areas.SEER_BANK_AREA;
                                        bot.bresenhamLocation = false;
                                        break;
                                    case "Woodcutting Guild":
                                        bot.locationType = LocationType.WOOD_GUILD;
                                        bot.chopArea = Areas.WOOD_GUILD_YEW_TREE_AREA;
                                        bot.treeArea = Areas.WOOD_GUILD_YEW_TREE_AREA;
                                        bot.bankArea = Areas.WOOD_GUILD_BANK_AREA;
                                        bot.bresenhamLocation = false;
                                        break;
                                }
                                break;
                            case "Magic":
                                bot.treeName = "Magic tree";
                                switch (location) {
                                    case "Seers South-West":
                                        bot.chopArea = Areas.SEER_MAGIC_SW_TREE_AREA;
                                        bot.treeArea = Areas.SEER_MAGIC_SW_TREE_AREA;
                                        bot.bankArea = Areas.SEER_BANK_AREA;
                                        bot.bresenhamLocation = false;
                                        break;
                                    case "Seers South":
                                        bot.chopArea = Areas.SEER_MAGIC_S_TREE_AREA;
                                        bot.treeArea = Areas.SEER_MAGIC_S_TREE_ACTION_AREA;
                                        bot.bankArea = Areas.SEER_BANK_AREA;
                                        bot.bresenhamLocation = false;
                                        break;
                                    case "Zeah (Land's End)":
                                        bot.chopArea = Areas.ZEAH_LAND_END_MAGIC_TREE_AREA;
                                        bot.treeArea = Areas.ZEAH_LAND_END_MAGIC_TREE_AREA;
                                        bot.bankArea = Areas.ZEAH_LAND_END_BANK_AREA;
                                        bot.bresenhamLocation = false;
                                        break;
                                    case "Woodcutting Guild":
                                        bot.locationType = LocationType.WOOD_GUILD;
                                        bot.chopArea = Areas.WOOD_GUILD_MAGIC_TREE_AREA;
                                        bot.treeArea = Areas.WOOD_GUILD_MAGIC_TREE_AREA;
                                        bot.bankArea = Areas.WOOD_GUILD_BANK_AREA;
                                        bot.bresenhamLocation = false;
                                        break;
                                }
                                break;
                            case "Redwood":
                                bot.treeAction = "Cut";
                                switch (location) {
                                    case "Woodcutting Guild":
                                        bot.locationType = LocationType.REDWOOD;
                                        bot.chopArea = Areas.WOOD_GUILD_REDWOOD_LEVEL1_AREA;
                                        bot.treeArea = Areas.WOOD_GUILD_REDWOOD_LEVEL1_AREA;
                                        bot.bankArea = Areas.WOOD_GUILD_BANK_AREA;
                                        bot.bresenhamLocation = false;
                                        break;
                                }
                                break;
                        }

                        bot.ignoreHatchetCheck = ignoreHatchetCheckboxBank.isSelected();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }// Handle Power Chopping Startup
            else {
                bank_CB.setDisable(true);
                bank_Tab.setDisable(true);

                try {
                    bot.treeName = treeP_LV.getSelectionModel().getSelectedItem().toString();

                    switch (bot.treeName) {
                        case "Maple":
                            bot.treeName = "Maple Tree";
                            break;
                        case "Magic":
                            bot.treeName = "Magic tree";
                            break;
                    }

                    switch(dropMethod_ComB.getSelectionModel().getSelectedIndex()){
                        case 0:
                            bot.dropMethod = DropType.fullDrop;
                            break;
                        case 1:
                            bot.dropMethod = DropType.random;
                            break;
                        case 2:
                            bot.dropMethod = DropType.c1d1;
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                    PromptP_L.textProperty().set("Be sure to have tree and dropType both selected.");
                    UpdateUI.debug("Be sure to have tree and dropType both selected.");
                }
                bot.powerChop = true;

                bot.ignoreHatchetCheck = ignoreHatchetCheckboxPower.isSelected();
            }

            ignoreHatchetCheckboxBank.setDisable(true);
            ignoreHatchetCheckboxPower.setDisable(true);

            if (bot.usingPreset = (usePreset_CB.isSelected() && !usePreset_CB.isDisabled())) {
                if (one_Radio.isSelected())
                    bot.presetNumber = 1;
                else
                    bot.presetNumber = 2;
            }

            bot.usingSuperHeatForm = superheat_CB.isSelected() && !superheat_CB.isDisable();

            startB_BT.textProperty().set("Resume");
            startP_BT.textProperty().set("Resume");
            Prompt_L.textProperty().set(" ");
            super.getStart_BTEvent();
        };
    }

    private EventHandler<ActionEvent> getUsePreset_CBAction()
    {
        return event -> {
            one_Radio.setDisable(!usePreset_CB.isSelected());
            two_Radio.setDisable(!usePreset_CB.isSelected());
        };
    }
}