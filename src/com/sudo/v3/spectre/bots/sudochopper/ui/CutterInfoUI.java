package com.sudo.v3.spectre.bots.sudochopper.ui;

import com.runemate.game.api.hybrid.util.Resources;
import com.sudo.v3.spectre.bots.sudochopper.ApexChopper;
import com.sudo.v3.ui.base.InfoUIController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Info GUI for the sudodivination Bot
 *
 * This will show various live stats on the bot
 */
public class CutterInfoUI extends InfoUIController implements Initializable {

    private ApexChopper bot;

    @FXML
    ImageView Logo_IV, Title_IV;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        Logo_IV.setImage(new Image(Resources.getAsStream("com/sudo/v3/ui/resources/apex_render_small_green.png")));
        if(bot.getMetaData().getHourlyPrice().doubleValue() > 0 || bot.isPrivate())
            Title_IV.setImage(new Image(Resources.getAsStream("com/sudo/v3/spectre/bots/sudochopper/ui/resources/ApexChopperPRO.png")));
        else
            Title_IV.setImage(new Image(Resources.getAsStream("com/sudo/v3/spectre/bots/sudochopper/ui/resources/ApexChopperLite.png")));
    }

    public CutterInfoUI(ApexChopper bot) {
        super(bot, 6);
        this.bot = bot;

        // Load the fxml file using RuneMate's resources class.
        FXMLLoader loader = new FXMLLoader();

        Future<InputStream> stream = bot.getPlatform().invokeLater(() -> Resources.getAsStream("com/sudo/v3/ui/fxml/SudoInfo.fxml"));

        // Set this class as root and controller of the fxml file
        loader.setController(this);
        loader.setRoot(this);

        try {
            loader.load(stream.get());
        } catch (IOException | InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}