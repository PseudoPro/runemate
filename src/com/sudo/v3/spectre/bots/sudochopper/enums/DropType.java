package com.sudo.v3.spectre.bots.sudochopper.enums;

/**
 * Created by SudoPro on 7/6/2016.
 */

public enum DropType
{
    c1d1,
    fullDrop,
    random,
    noDrop
}
