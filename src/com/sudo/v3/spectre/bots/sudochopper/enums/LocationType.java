package com.sudo.v3.spectre.bots.sudochopper.enums;

public enum LocationType {
    GENERIC,
    MENAPHOS,
    REDWOOD,
    WOOD_GUILD
}
