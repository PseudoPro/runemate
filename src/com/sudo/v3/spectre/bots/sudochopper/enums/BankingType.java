package com.sudo.v3.spectre.bots.sudochopper.enums;

/**
 * Created by SudoPro on 11/26/2016.
 */
public enum BankingType
{
    withdrawEquipment,
    depositAllExceptEquipment,
    leavingBank
}
