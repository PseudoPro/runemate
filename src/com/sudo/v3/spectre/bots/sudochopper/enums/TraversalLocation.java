package com.sudo.v3.spectre.bots.sudochopper.enums;

/**
 * Created by SudoPro on 11/23/2016.
 */
public enum TraversalLocation
{
    bank,
    chopArea,
    chopSpot
}
