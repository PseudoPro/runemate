package com.sudo.v3.spectre.bots.sudochopper;

import com.runemate.game.api.client.embeddable.EmbeddableUI;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.GroundItem;
import com.runemate.game.api.hybrid.entities.LocatableEntity;
import com.runemate.game.api.hybrid.entities.definitions.ItemDefinition;
import com.runemate.game.api.hybrid.local.Skill;
import com.runemate.game.api.hybrid.local.Worlds;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.queries.results.WorldQueryResults;
import com.runemate.game.api.hybrid.util.Regex;
import com.runemate.game.api.hybrid.util.calculations.CommonMath;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.script.framework.listeners.InventoryListener;
import com.runemate.game.api.script.framework.listeners.SkillListener;
import com.runemate.game.api.script.framework.listeners.events.ItemEvent;
import com.runemate.game.api.script.framework.listeners.events.SkillEvent;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBot;
import com.sudo.v3.spectre.bots.sudochopper.branches.*;
import com.sudo.v3.spectre.bots.sudochopper.branches.redwood.IsInRedWoodLevel1Branch;
import com.sudo.v3.spectre.bots.sudochopper.branches.redwood.IsInRedWoodLevel2Branch;
import com.sudo.v3.spectre.bots.sudochopper.enums.BankingType;
import com.sudo.v3.spectre.bots.sudochopper.enums.DropType;
import com.sudo.v3.spectre.bots.sudochopper.enums.LocationType;
import com.sudo.v3.spectre.bots.sudochopper.enums.TraversalLocation;
import com.sudo.v3.spectre.bots.sudochopper.leafs.*;
import com.sudo.v3.spectre.bots.sudochopper.leafs.redwood.GoToRedWoodBaseLeaf;
import com.sudo.v3.spectre.bots.sudochopper.leafs.redwood.GoToRedWoodLevel1Leaf;
import com.sudo.v3.spectre.bots.sudochopper.leafs.redwood.GoToRedWoodLevel2Leaf;
import com.sudo.v3.spectre.bots.sudochopper.ui.CutterFXGui;
import com.sudo.v3.spectre.bots.sudochopper.ui.CutterInfoUI;
import com.sudo.v3.spectre.common.leafs.InteractLeaf;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.sudo.v3.ui.Info;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * Created by joeyw on 4/13/2016.
 */
public class ApexChopper extends SudoBot implements EmbeddableUI, InventoryListener, SkillListener {
    public Area bankArea = new Area.Rectangular(new Coordinate(5, 5, 0), new Coordinate(5, 5, 0));
    public Area chopArea = new Area.Rectangular(new Coordinate(5, 5, 0), new Coordinate(5, 5, 0));
    public Area treeArea = new Area.Rectangular(new Coordinate(5, 5, 0), new Coordinate(5, 5, 0));

    public String treeName = "", hatchet = "", treeAction = "Chop down", locationNameString = "";

    public LocationType locationType = LocationType.GENERIC;

    public String[] hatchetList = null;

    public boolean pickUpNest = true, powerChop = false, wieldHatchet = false, bresenhamLocation = false, usingPreset = false, usingSuperHeatForm = false, ignoreHatchetCheck = false;

    private CutterFXGui configUI;
    private CutterInfoUI infoUI;

    private int logCount = 0, dropCount = -1;
    public int presetNumber = 0, prayerPotionWithdrawAmount = Random.nextInt(8, 24), powerDropInventoryCheck = 0;
    public double percentToPrayer = 0.5; // 0.4 - 0.6
    public Pattern prayerRenewalRegex = Regex.getPatternForContainsString("Super prayer renewal potion");

    public GameObject tree;//bankObj, tree;
    public LocatableEntity bankEntity;
    public GroundItem birdNest;
    public SpriteItem prayerPotion;

    public DropType dropMethod = DropType.noDrop;
    public BankingType bankingType;
    public TraversalLocation traversalLocation;

    public ArrayList<Coordinate> seenTreeSpots = new ArrayList<>();

    // Branch Tasks
    public IsBankAround isBankAround = new IsBankAround(this);
    public IsBankOpen isBankOpenBranch = new IsBankOpen(this);
    public IsChopping isChoppingBranch = new IsChopping(this);
    public IsNestNull isNestNullBranch = new IsNestNull(this);
    public IsInArea isInAreaBranch = new IsInArea(this);
    public IsInventoryFull isInventoryFullBranch = new IsInventoryFull(this);
    public IsTreeNull isTreeNullBranch = new IsTreeNull(this);
    public InventoryContainsPrayerPotionBranch inventoryContainsPrayerPotionBranch = new InventoryContainsPrayerPotionBranch(this);

    // Leaf Tasks
    public DepositAllLeaf depositAllLeaf = new DepositAllLeaf(this);
    public IsChoppingLeaf isChoppingLeaf = new IsChoppingLeaf(this);
    public StartChoppingLeaf startChoppingLeaf = new StartChoppingLeaf(this);
    public TraversalLeaf traversalLeaf = new TraversalLeaf(this);
    public WaitingForTreeRespawnLeaf waitingForTreeRespawnLeaf = new WaitingForTreeRespawnLeaf(this);
    public WithdrawEquipmentLeaf withdrawEquipmentLeaf = new WithdrawEquipmentLeaf(this);
    public DrinkPrayerPotionLeaf drinkPrayerPotionLeaf = new DrinkPrayerPotionLeaf(this);
    public WithdrawPrayerPotionLeaf withdrawPrayerPotionLeaf = new WithdrawPrayerPotionLeaf(this);
    public ActivateSuperHeatFormLeaf activateSuperHeatFormLeaf = new ActivateSuperHeatFormLeaf(this);


    // RedWood Branch Tasks
    public IsInRedWoodLevel1Branch isInRedWoodLevel1Branch = new IsInRedWoodLevel1Branch(this);
    public IsInRedWoodLevel2Branch isInRedWoodLevel2Branch = new IsInRedWoodLevel2Branch(this);

    // RedWood Leaf Tasks
    public GoToRedWoodLevel1Leaf goToRedWoodLevel1Leaf = new GoToRedWoodLevel1Leaf(this);
    public GoToRedWoodLevel2Leaf goToRedWoodLevel2Leaf = new GoToRedWoodLevel2Leaf(this);
    public GoToRedWoodBaseLeaf goToRedWoodBaseLeaf = new GoToRedWoodBaseLeaf(this);


    // Interact Leaf tasks
    public InteractLeaf<GroundItem> rs3NestInteract = new InteractLeaf<>(this, birdNest, "Bird's Nest", "Take");
    public InteractLeaf<GroundItem> osrsNestInteract = new InteractLeaf<>(this, birdNest, "Bird nest", "Take");

    public ApexChopper() {
        super();
        setEmbeddableUI(this);
    }

    @Override
    public TreeTask createRootTask() {
        return new Root(this);
    }

    @Override
    public ObjectProperty<? extends Node> botInterfaceProperty() {
        if (botInterfaceProperty == null) {
            botInterfaceProperty = new SimpleObjectProperty<>(configUI = new CutterFXGui(this));
            guiList = new ArrayList<Node>();
            //guiList.add(_sbGUI = new SudoBuddyFXGui(this));
            guiList.add(configUI);
            guiList.add(infoUI = new CutterInfoUI(this));
        }
        return botInterfaceProperty;
    }

    @Override
    public void onStart(String... args) {
        super.onStart();

        checkSpecialTimer.start();

        getEventDispatcher().addListener(this);
    }

    @Override
    public String getBotName() {
        return "ApexChopper";
    }

    @Override
    public void onItemAdded(ItemEvent event) {
        if (!Bank.isOpen())
            super.onItemAdded(event);
        ItemDefinition definition = event.getItem().getDefinition();
        if (definition != null) {
            if (definition.getName().toLowerCase().contains("log")) {
                logCount++;
            }
        }
    }

    @Override
    public void onItemRemoved(ItemEvent event) {
        if (!Bank.isOpen())
            super.onItemRemoved(event);
    }

    @Override
    public void onLevelUp(SkillEvent event) {
        if (event != null) {
            if (!isRS3) {
                if (event.getSkill() == Skill.WOODCUTTING) {
                    int lvl = Skill.WOODCUTTING.getCurrentLevel();

                    switch (lvl) {
                        case 6:
                        case 11:
                        case 21:
                        case 31:
                        case 41:
                        case 61:
                            hatchet = "";
                            hatchetList = getHatchetList();
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }

    @Override
    public void updateInfo() {

        // UpdateUI all current values in the XPInfo List
        XPInfoMap.values().forEach((xpInfo) -> xpInfo.update());

        displayInfoMap.put("Log Count: ", Integer.toString(logCount) +
                " (" + Integer.toString((int) CommonMath.rate(TimeUnit.HOURS, STOPWATCH.getRuntime(), logCount)) + "/Hour)");
        displayInfoMap.put("GP: ", Integer.toString(grossIncome) +
                " (" + Integer.toString((int) CommonMath.rate(TimeUnit.HOURS, STOPWATCH.getRuntime(), grossIncome)) + "/Hour)");

        try {
            info = new Info(XPInfoMap,           // XP Info
                    displayInfoMap,
                    STOPWATCH.getRuntimeAsString(),        // Total Runtime
                    currentTaskString,                     // Current Task
                    abTaskString);                         // antiban Task

        } catch (Exception e) {
            e.printStackTrace();
        }

        Platform.runLater(() -> infoUI.update());
    }

    public boolean hasHatchet() {
        if (ignoreHatchetCheck) {
            UpdateUI.debug(player.getName() + " -> ignoreHatchetCheck is enabled. Returning true.");
            return true;
        }

        if (!hatchet.equals("")) {
            if (Inventory.contains(hatchet) && wieldHatchet)
                Inventory.newQuery().names(hatchet).actions("Wield").results().first().interact("Wield", hatchet);

            return Inventory.contains(hatchet) || Equipment.newQuery().names(hatchet).results().size() > 0;
        } else
            return false;
    }

    public String getCurrentHatchet() {
        if (Inventory.containsAnyOf(hatchetList))
            return Inventory.getItems(hatchetList).first().toString();
        else if (Equipment.newQuery().names(hatchetList).results().size() > 0)
            return Equipment.newQuery().names(hatchetList).results().first().toString();
        else
            return "";
    }

    public String[] getHatchetList() {
        int woodLvl = Skill.WOODCUTTING.getCurrentLevel();

        wieldHatchet = woodLvl <= Skill.ATTACK.getCurrentLevel();

        if (woodLvl > 60) //&& Worlds.getOverview(Worlds.getCurrent()).isMembersOnly())
            return new String[]{"Infernal axe", "Dragon axe", "Rune axe", "Adamant axe", "Mithril axe",
                    "Black axe", "Steel axe", "Iron axe", "Bronze axe"};
        else if (woodLvl > 40)
            return new String[]{"Rune axe", "Adamant axe", "Mithril axe", "Black axe", "Steel axe", "Iron axe", "Bronze axe"};
        else if (woodLvl > 30)
            return new String[]{"Adamant axe", "Mithril axe", "Black axe", "Steel axe", "Iron axe", "Bronze axe"};
        else if (woodLvl > 20)
            return new String[]{"Mithril axe", "Black axe", "Steel axe", "Iron axe", "Bronze axe"};
        else if (woodLvl > 5)
            return new String[]{"Black axe", "Steel axe", "Iron axe", "Bronze axe"};
        else
            return new String[]{"Iron axe", "Bronze axe"};
    }
}