package com.sudo.v3.spectre.bots.sudochopper.branches;

import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.RuneScape;
import com.runemate.game.api.hybrid.local.Skill;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.rs3.local.hud.Powers;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoRootTask;
import com.sudo.v3.spectre.bots.sudochopper.ApexChopper;
import com.sudo.v3.spectre.bots.sudochopper.enums.BankingType;
import com.sudo.v3.spectre.common.leafs.PowerDropLeaf;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.sudo.v3.ui.model.XPInfo;

/**
 * Created by SudoPro on 11/23/2016.
 */

// Will wait for 2 minutes. If the the bot is never started from the GUI, we will exit
public class Root extends SudoRootTask
{
    private ApexChopper bot;

    public Root(ApexChopper bot)
    {
        super(bot);
        this.bot = bot;
    }

    private void handleFirstLogin()
    {
        if (RuneScape.isLoggedIn())
        {
            if (!bot.isRS3)
            {
                if (bot.hatchetList == null)
                    bot.hatchetList = bot.getHatchetList();
                if (bot.hatchet.equals(""))
                    bot.hatchet = bot.getCurrentHatchet();

                if (!bot.hatchet.equals(""))
                    UpdateUI.currentTask("Current Hatchet set to \"" + bot.hatchet + "\"", bot);
                else
                    UpdateUI.currentTask("No Hatchet found on player, running to check bank.", bot);
            }

            if(bot.powerChop){
                bot.treeArea = new Area.Circular(bot.player.getPosition(), 30);
                bot.chopArea = bot.treeArea;
            }

            // Add Skills to track to the HashMap
            bot.XPInfoMap.put(Skill.WOODCUTTING, new XPInfo(Skill.WOODCUTTING));
            bot.XPInfoMap.put(Skill.FIREMAKING, new XPInfo(Skill.FIREMAKING));
            bot.firstLogin = false;

            UpdateUI.debug("Woodcutting Level: " + Skill.WOODCUTTING.getCurrentLevel());
            UpdateUI.debug("Location: " + bot.locationNameString);
            UpdateUI.debug("Tree: " + bot.treeName);
            UpdateUI.debug("Ignore Hatchet Check: " + bot.ignoreHatchetCheck);
            UpdateUI.debug("Pick Up Nest: " + bot.pickUpNest);
            UpdateUI.debug("UsingPreset: " + bot.usingPreset);
            UpdateUI.debug("UsingSuperHeatForm: " + bot.usingSuperHeatForm);
            UpdateUI.debug("IsPowerChopping: " + bot.powerChop);
            UpdateUI.debug("DropType: " + bot.dropMethod.name());
            UpdateUI.debug("Current Prayer Points: " + Powers.Prayer.getPoints());
            UpdateUI.debug("DEBUG: Max Prayer Points: " + Powers.Prayer.getMaximumPoints());
            if(bot.usingPreset)
                UpdateUI.debug("Preset #: " + bot.presetNumber);

            bot.updateSession(0.0);

            bot.powerDropLeaf = new PowerDropLeaf(bot) {
                @Override
                public void successAction() {
                    bot.powerDropInventoryCheck = 0;
                }
            };

            bot.powerDropLeaf.setValues("log", 5, 20);
        }
    }

    @Override
    public TreeTask rootTask() {

        if (bot.firstLogin)
            handleFirstLogin();

        if (bot.getMetaData().getHourlyPrice().doubleValue() > 0 || bot.isValidSession() || Environment.isSDK()) {
            if (bot.currentlyBreaking) {
                if (RuneScape.isLoggedIn()) {
                    UpdateUI.currentTask("Attempting to log off.", bot);
                    RuneScape.logout(true);
                }
                return bot.emptyLeaf;
            } else if (bot.player != null) {
                validate = bot.hatchet.equals("Dragon axe") && bot.hasHatchet() && !Inventory.contains(bot.hatchet) && !bot.bankArea.contains(bot.player) && bot.checkSpecialTimer.hasExpired();
                UpdateUI.debug(bot.player.getName() + " -> Root: UseSpecial: " + validate);
                if (validate) {
                    return bot.useSpecialLeaf;
                } else if (Inventory.getSelectedItem() != null) {
                    return bot.deselectInventoryItemLeaf;
                } else if (bot.hasHatchet() || bot.isRS3) {
                    UpdateUI.debug(bot.player.getName() + " -> DEBUG: UsingSuperHeatForm: " + bot.usingSuperHeatForm);
                    UpdateUI.debug(bot.player.getName() + " -> DEBUG: Current Prayer Points: " + Powers.Prayer.getPoints());
                    UpdateUI.debug(bot.player.getName() + " -> DEBUG: Max Prayer Points: " + Powers.Prayer.getMaximumPoints());
                    UpdateUI.debug(bot.player.getName() + " -> DEBUG: Percent to Prayer: " + (Powers.Prayer.getMaximumPoints() * bot.percentToPrayer) + " (" + bot.percentToPrayer + ")");
                    validate = bot.usingSuperHeatForm && Powers.Prayer.getPoints() < Powers.Prayer.getMaximumPoints() * bot.percentToPrayer;
                    UpdateUI.debug(bot.player.getName() + " -> RootBranch: PrayerLevelLow: " + validate);
                    if(validate)
                        return bot.inventoryContainsPrayerPotionBranch;
                    else {
                        validate = bot.usingSuperHeatForm && !Powers.Prayer.Curse.SUPERHEAT_FORM.isActivated();
                        UpdateUI.debug(bot.player.getName() + " -> RootBranch: SuperHeatNotActivated: " + validate);
                        if (validate)
                            return bot.activateSuperHeatFormLeaf;
                        else
                            return bot.isInventoryFullBranch;
                    }
                }
                else {
                    if(bot.powerChop && !bot.ignoreHatchetCheck){
                        UpdateUI.currentTask("Bot has been paused because your player does not have an axe. Please equip and resume.", bot);
                        bot.firstLogin = true;
                        bot.pause();
                        return bot.emptyLeaf;
                    }else {
                        bot.bankingType = BankingType.withdrawEquipment;
                        return bot.isBankOpenBranch;
                    }
                }
            } else {
                return bot.emptyLeaf;
            }
        } else {
            return bot.liteVersionExpireLeaf;
        }
    }
}

