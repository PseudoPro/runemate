package com.sudo.v3.spectre.bots.sudochopper.branches;

import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudochopper.ApexChopper;
import com.sudo.v3.spectre.bots.sudochopper.enums.TraversalLocation;
import com.sudo.v3.spectre.common.leafs.OpenBankLeaf;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 11/23/2016.
 */
public class IsInArea extends SudoBranchTask {
    private ApexChopper bot;

    public IsInArea(ApexChopper bot) {
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        if (bot.traversalLocation == TraversalLocation.bank)
            return new OpenBankLeaf(bot);
        else if (bot.traversalLocation == TraversalLocation.chopArea)
            return bot.waitingForTreeRespawnLeaf;
        else if (bot.traversalLocation == TraversalLocation.chopSpot)
            return bot.traversalLeaf;
        else {
            UpdateUI.debug(bot.player.getName() + " -> Error processing IsInArea for " + bot.traversalLocation);
            return bot.emptyLeaf;
        }
    }

    @Override
    public boolean validate() {
        validate = false;
        if (bot.traversalLocation == TraversalLocation.bank) {
            validate = bot.bankArea.contains(bot.player);
            UpdateUI.debug(bot.player.getName() + " -> Branch IsInArea(BANK_AREA): " + validate);
        } else if (bot.traversalLocation == TraversalLocation.chopArea) {
            validate = bot.treeArea.contains(bot.player);
            UpdateUI.debug(bot.player.getName() + " -> Branch IsInArea(chopArea): " + validate);
        } else if (bot.traversalLocation == TraversalLocation.chopSpot) {
            validate = true;
            UpdateUI.debug(bot.player.getName() + " -> Branch IsInArea(chopSpot): always true");
        }

        return validate;
    }

    @Override
    public TreeTask failureTask() {
        return bot.traversalLeaf;
    }
}