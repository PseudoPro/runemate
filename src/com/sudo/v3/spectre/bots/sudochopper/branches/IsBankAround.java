package com.sudo.v3.spectre.bots.sudochopper.branches;

import com.runemate.game.api.hybrid.region.Banks;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudochopper.ApexChopper;
import com.sudo.v3.spectre.bots.sudochopper.enums.LocationType;
import com.sudo.v3.spectre.bots.sudochopper.enums.TraversalLocation;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 11/26/2016.
 */
public class IsBankAround extends SudoBranchTask{
    private ApexChopper bot;

    public IsBankAround(ApexChopper bot) {
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        if(bot.locationType == LocationType.MENAPHOS || bot.locationType == LocationType.REDWOOD || bot.locationType == LocationType.WOOD_GUILD){
            bot.openBankLeaf.setGenericBank(true);
        }
        return bot.openBankLeaf;

    }

    @Override
    public boolean validate() {
        bot.bankEntity = Banks.newQuery().filter(i -> i.distanceTo(bot.bankArea.getCenter()) < 8 || bot.bankArea.contains(i.getPosition())).results().nearest();
        validate = bot.bankEntity != null && bot.bankEntity.distanceTo(bot.player) < 12;

        UpdateUI.debug(bot.player.getName() + " -> IsBankAround: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        if(bot.locationType == LocationType.REDWOOD){
            bot.traversalLocation = TraversalLocation.bank;
            return bot.isInRedWoodLevel1Branch;
        }else {
            bot.traversalLocation = TraversalLocation.bank;
            return bot.traversalLeaf;
        }
    }
}