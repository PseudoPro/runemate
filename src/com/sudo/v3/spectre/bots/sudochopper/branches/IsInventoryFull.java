package com.sudo.v3.spectre.bots.sudochopper.branches;

import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.queries.results.SpriteItemQueryResults;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudochopper.ApexChopper;
import com.sudo.v3.spectre.bots.sudochopper.enums.BankingType;
import com.sudo.v3.spectre.bots.sudochopper.enums.DropType;
import com.sudo.v3.spectre.statics.UpdateUI;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SudoPro on 11/24/2016.
 */
public class IsInventoryFull extends SudoBranchTask
{
    private ApexChopper bot;

    public IsInventoryFull(ApexChopper bot)
    {
        this.bot = bot;
    }

    @Override
    public TreeTask successTask()
    {
        if(bot.powerChop){
            bot.isDropping = true;
            return bot.powerDropLeaf;
        }else {
            bot.bankingType = BankingType.depositAllExceptEquipment;
            return bot.isBankOpenBranch;
        }
    }

    @Override
    public boolean validate()
    {
        if(bot.powerDropInventoryCheck <= 0)
            bot.powerDropInventoryCheck = Random.nextInt(5, 27);

        if(bot.powerChop){
            if(bot.dropMethod == DropType.fullDrop){
                validate = Inventory.isFull();
                UpdateUI.debug(bot.player.getName() + " -> Branch IsInventoryFull(fullDrop): " + validate);
                return validate;
            } else {
                validate = Inventory.getItems().size() > bot.powerDropInventoryCheck;
                UpdateUI.debug(bot.player.getName() + " -> Branch IsInventoryFull(randomDrop): " + validate);
                return validate;
            }
        } else {
            validate = Inventory.isFull();
            UpdateUI.debug(bot.player.getName() + " -> Branch IsInventoryFull: " + validate);
            return validate;
        }
    }

    @Override
    public TreeTask failureTask()
    {
        bot.bankingType = BankingType.leavingBank;
        return bot.isBankOpenBranch;
    }
}
