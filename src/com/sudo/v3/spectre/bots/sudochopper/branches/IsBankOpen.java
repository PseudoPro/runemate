package com.sudo.v3.spectre.bots.sudochopper.branches;

import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudochopper.ApexChopper;
import com.sudo.v3.spectre.bots.sudochopper.enums.BankingType;
import com.sudo.v3.spectre.bots.sudochopper.enums.TraversalLocation;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 11/23/2016.
 */
public class IsBankOpen extends SudoBranchTask{
    private ApexChopper bot;

    public IsBankOpen(ApexChopper bot) {
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        if (bot.bankingType == BankingType.withdrawEquipment)
            return bot.withdrawEquipmentLeaf;
        else if (bot.bankingType == BankingType.depositAllExceptEquipment)
            return bot.depositAllLeaf;
        else {
            bot.traversalLocation = TraversalLocation.chopArea;
            return bot.traversalLeaf;
        }
    }

    @Override
    public boolean validate() {
        validate = Bank.isOpen();
        UpdateUI.debug(bot.player.getName() + " -> Branch IsBankOpen: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        if (bot.bankingType == BankingType.leavingBank) {
            if (bot.pickUpNest) {
                return bot.isNestNullBranch;
            } else
                return bot.isTreeNullBranch;
        } else {
            return bot.isBankAround;
        }
    }
}