package com.sudo.v3.spectre.bots.sudochopper.branches;

import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudochopper.ApexChopper;
import com.sudo.v3.spectre.statics.UpdateUI;

public class InventoryContainsPrayerPotionBranch extends SudoBranchTask {
    private ApexChopper bot;

    public InventoryContainsPrayerPotionBranch(ApexChopper bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        return bot.drinkPrayerPotionLeaf;
    }

    @Override
    public boolean validate() {
        bot.prayerPotion = Inventory.newQuery().names(bot.prayerRenewalRegex).results().first();
        validate = bot.prayerPotion != null;
        UpdateUI.debug(bot.player.getName() + " -> Branch: InventoryContainsPrayerPotion: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        validate = Bank.isOpen();
        UpdateUI.debug(bot.player.getName() + " -> SubBranch: IsBankOpen: " + validate);
        if(validate)
            return bot.withdrawPrayerPotionLeaf;
        else
            return bot.isBankAround;
    }
}
