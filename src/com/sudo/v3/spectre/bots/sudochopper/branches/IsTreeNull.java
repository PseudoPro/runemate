package com.sudo.v3.spectre.bots.sudochopper.branches;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.queries.results.LocatableEntityQueryResults;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudochopper.ApexChopper;
import com.sudo.v3.spectre.bots.sudochopper.enums.LocationType;
import com.sudo.v3.spectre.bots.sudochopper.enums.TraversalLocation;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by joeyw on 5/12/2017.
 */
public class IsTreeNull extends SudoBranchTask {
    private ApexChopper bot;

    public IsTreeNull(ApexChopper bot) {
        this.bot = bot;
    }

    @Override
    public boolean validate() {
        LocatableEntityQueryResults<GameObject> gameObjectResults;

        if (bot.locationType == LocationType.REDWOOD) {
            gameObjectResults = GameObjects.newQuery().names(bot.treeName).actions(bot.treeAction).filter(i ->
                i.distanceTo(bot.player) < 6
            ).results();
        } else {
            gameObjectResults = GameObjects.newQuery().names(bot.treeName).actions(bot.treeAction).within(bot.treeArea).results();
        }

        if (gameObjectResults.size() > 0) {
            bot.tree = gameObjectResults.nearest();

            gameObjectResults.forEach(i -> {
                if (!bot.seenTreeSpots.contains(i.getPosition())) {
                    bot.seenTreeSpots.add(i.getPosition());
                    UpdateUI.debug("Tree added to seenTreeSpots[" + (bot.seenTreeSpots.size() - 1) + "]");
                }
            });
        }

        Coordinate playerPosition = bot.player.getPosition();

        validate = bot.tree == null || (playerPosition != null && playerPosition.getPlane() < 1 && bot.locationType == LocationType.REDWOOD);
        UpdateUI.debug(bot.player.getName() + " -> Branch IsTreeNull: " + validate);

        return validate;
    }

    @Override
    public TreeTask successTask() {
        bot.traversalLocation = TraversalLocation.chopArea;
        if(bot.locationType == LocationType.REDWOOD){
            return bot.isInRedWoodLevel1Branch;
        } else {
            return bot.isInAreaBranch;
        }
    }

    @Override
    public TreeTask failureTask() {
        return bot.isChoppingBranch;
    }
}