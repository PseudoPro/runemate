package com.sudo.v3.spectre.bots.sudochopper.branches;

import com.runemate.game.api.hybrid.region.GroundItems;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudochopper.ApexChopper;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 11/28/2016.
 */
public class IsNestNull extends SudoBranchTask
{
    private ApexChopper bot;

    public IsNestNull(ApexChopper bot)
    {
        this.bot = bot;
    }

    @Override
    public TreeTask successTask()
    {
        return bot.isTreeNullBranch;
    }

    @Override
    public boolean validate()
    {
        if (bot.pickUpNest)
            bot.birdNest = GroundItems.newQuery().names("Bird's nest", "Bird nest").results().nearest();

        validate = bot.birdNest == null;
        UpdateUI.debug(bot.player.getName() + " -> Branch IsNestNull: " + validate);

        return validate;
    }

    @Override
    public TreeTask failureTask()
    {
        if(bot.isRS3) {
            bot.rs3NestInteract.setObject(bot.birdNest);
            return bot.rs3NestInteract;
        }
        else {
            bot.osrsNestInteract.setObject(bot.birdNest);
            return bot.osrsNestInteract;
        }
    }
}
