package com.sudo.v3.spectre.bots.sudochopper.branches;

import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudochopper.ApexChopper;
import com.sudo.v3.spectre.api.game.entities.ApexPlayer;
import com.sudo.v3.spectre.statics.Animations;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 11/24/2016.
 */
public class IsChopping extends SudoBranchTask
{
    private ApexChopper bot;

    public IsChopping(ApexChopper bot)
    {
        this.bot = bot;
    }

    @Override
    public TreeTask successTask()
    {
        return bot.isChoppingLeaf;
    }

    @Override
    public boolean validate() {
        validate = bot.tree.distanceTo(bot.player) < 3 && ApexPlayer.isInAnimation(Animations.CHOPPING_ANIM);
        UpdateUI.debug(bot.player.getName() + " -> Branch IsChopping: " + validate);
        return validate;
//        if (bot.tree.distanceTo(bot.player) < 3 && ApexPlayer.isInAnimation(Animations.CHOPPING_ANIM)) {
//            UpdateUI.debug(bot.player.getName() + " -> Branch IsChopping: true");
//            return true;
//        } else {
//            UpdateUI.debug(bot.player.getName() + " -> Branch IsChopping: false");
//            return false;
//        }
    }

    @Override
    public TreeTask failureTask()
    {
        return bot.startChoppingLeaf;
    }
}
