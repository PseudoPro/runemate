package com.sudo.v3.spectre.bots.sudochopper.branches.redwood;

import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudochopper.ApexChopper;
import com.sudo.v3.spectre.bots.sudochopper.enums.TraversalLocation;
import com.sudo.v3.spectre.statics.Areas;
import com.sudo.v3.spectre.statics.UpdateUI;

public class IsInRedWoodLevel2Branch extends SudoBranchTask {
    private ApexChopper bot;

    public IsInRedWoodLevel2Branch(ApexChopper bot){
        this.bot = bot;
    }

    @Override
    public boolean validate() {
        validate = Areas.WOOD_GUILD_REDWOOD_LEVEL2_AREA.contains(bot.player);
        UpdateUI.debug(bot.player.getName() + " -> IsInRedWoodLevel2Branch: " + validate);
        return validate;
    }

    @Override
    public TreeTask successTask() {
        if(bot.traversalLocation == TraversalLocation.bank)
            return bot.goToRedWoodLevel1Leaf;
        else // traversalLocation is TraversalLocation.chopArea or chopSpot
            return bot.goToRedWoodLevel1Leaf;
    }

    @Override
    public TreeTask failureTask() {
        return bot.traversalLeaf;
    }
}
