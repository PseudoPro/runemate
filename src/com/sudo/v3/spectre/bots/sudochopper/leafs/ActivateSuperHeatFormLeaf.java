package com.sudo.v3.spectre.bots.sudochopper.leafs;

import com.runemate.game.api.rs3.local.hud.Powers;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudochopper.ApexChopper;
import com.sudo.v3.spectre.statics.UpdateUI;

public class ActivateSuperHeatFormLeaf extends LeafTask {
    private ApexChopper bot;

    public ActivateSuperHeatFormLeaf (ApexChopper bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        UpdateUI.currentTask("Attempting to activate SuperHeat Form.", bot);
        if(Powers.Prayer.Curse.SUPERHEAT_FORM.toggle())
            Execution.delayUntil(() -> Powers.Prayer.Curse.SUPERHEAT_FORM.isActivated(), 750, 1500);
    }
}
