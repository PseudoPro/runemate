package com.sudo.v3.spectre.bots.sudochopper.leafs;

import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudochopper.ApexChopper;
import com.sudo.v3.spectre.api.game.hud.SudoCamera;
import com.sudo.v3.spectre.common.navigation.Traverse;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 11/23/2016.
 */
public class StartChoppingLeaf extends LeafTask
{
    private ApexChopper bot;

    public StartChoppingLeaf(ApexChopper bot)
    {
        this.bot = bot;
    }

    @Override
    public void execute()
    {
        if(bot.tree.distanceTo(bot.player) < 10) {
            UpdateUI.currentTask("Attempting to chop tree", bot);
            if (bot.tree.getVisibility() < .8 || bot.tree.distanceTo(bot.player) > 8)
                SudoCamera.ConcurrentlyTurnToWithYaw(bot.tree);

            if (bot.tree.interact(bot.treeAction, bot.treeName)) {
                Execution.delayWhile(bot.player::isMoving, 1500);
                Execution.delay(1000, 1500);
            }
        }else{
            Area tempArea = new Area.Circular(bot.treeArea.getPosition(), 4);
            UpdateUI.currentTask("Walking to new tree", bot);
            Traverse.bresenhamPath(tempArea);
        }
    }
}