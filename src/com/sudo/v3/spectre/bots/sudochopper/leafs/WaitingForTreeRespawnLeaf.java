package com.sudo.v3.spectre.bots.sudochopper.leafs;

import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudochopper.ApexChopper;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Proxify on 5/10/2017.
 */
public class WaitingForTreeRespawnLeaf extends LeafTask {
    private ApexChopper bot;

    public WaitingForTreeRespawnLeaf(ApexChopper bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        UpdateUI.currentTask("Waiting for tree to respawn. Delaying next action...", bot);
        Execution.delayUntil(() -> !GameObjects.newQuery().names(bot.treeName).actions(bot.treeAction).within(bot.treeArea).results().isEmpty(), 3000, 5000);
    }
}
