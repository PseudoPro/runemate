package com.sudo.v3.spectre.bots.sudochopper.leafs;

import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.common.playersense.ApexPlayerSense;
import com.sudo.v3.spectre.bots.sudochopper.ApexChopper;
import com.sudo.v3.spectre.bots.sudochopper.enums.TraversalLocation;
import com.sudo.v3.spectre.common.navigation.Traverse;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 11/23/2016.
 */
public class TraversalLeaf extends LeafTask
{
    private ApexChopper bot;

    public TraversalLeaf(ApexChopper bot)
    {
        this.bot = bot;
    }

    @Override
    public void execute()
    {
        if(bot.traversalLocation == TraversalLocation.bank)
        {
            UpdateUI.currentTask("Traveling to bank", bot);
            if(bot.bresenhamLocation)
                Traverse.bresenhamPath(bot.bankArea);
            else
                Traverse.smartRegionPath(bot.nav, bot.bankArea);
        }
        else if(bot.traversalLocation == TraversalLocation.chopArea)
        {
            UpdateUI.currentTask("Traveling to " + bot.treeName + " area", bot);
            if(bot.bresenhamLocation)
            {
                UpdateUI.debug("Using bresenmham");
                Traverse.bresenhamPath(bot.treeArea);
            }
            else
                Traverse.smartRegionPath(bot.nav, bot.treeArea);
        }
        else if(bot.traversalLocation == TraversalLocation.chopSpot)
        {
            UpdateUI.currentTask("Finding a new tree...", bot);
            if(bot.seenTreeSpots.size() > 0)
                Traverse.bresenhamPath(new Area.Circular(bot.seenTreeSpots.get((int)(Math.random() * bot.seenTreeSpots.size())), 4));
            else
            {
                if(bot.bresenhamLocation)
                    Traverse.bresenhamPath(bot.treeArea);
                else
                    Traverse.smartRegionPath(bot.nav, bot.treeArea);
            }
        }

        // Delay after each attempt to traverse
        Execution.delay(500, 750);
        Execution.delayWhile(bot.player::isMoving, ApexPlayerSense.Key.TRAVERSAL_DELAY.getAsInteger());
    }
}
