package com.sudo.v3.spectre.bots.sudochopper.leafs;

import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudochopper.ApexChopper;
import com.sudo.v3.spectre.statics.UpdateUI;

public class DrinkPrayerPotionLeaf extends LeafTask {
    private ApexChopper bot;

    public DrinkPrayerPotionLeaf(ApexChopper bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        UpdateUI.currentTask("Attempting to drink prayer renewal potion.", bot);
        if(bot.prayerPotion.interact("Drink"))
            Execution.delay(750, 1500);
    }
}
