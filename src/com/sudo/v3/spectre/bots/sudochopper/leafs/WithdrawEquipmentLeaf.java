package com.sudo.v3.spectre.bots.sudochopper.leafs;

import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudochopper.ApexChopper;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 11/26/2016.
 */
public class WithdrawEquipmentLeaf extends LeafTask
{
    private ApexChopper bot;

    public WithdrawEquipmentLeaf(ApexChopper bot)
    {
        this.bot = bot;
    }

    @Override
    public void execute()
    {
        if (Bank.containsAnyOf(bot.hatchetList)) {
            for (String hatchet : bot.hatchetList) {
                if(Inventory.contains(hatchet)){
                    UpdateUI.currentTask("(No upgrade found) - Highest level hatchet found in inventory: " + hatchet, bot);
                    bot.hatchet = hatchet;
                    break;
                }else if (Bank.containsAnyOf(hatchet)) {
                    UpdateUI.currentTask("(Upgrading to new Hatchet) - Withdrawing highest level hatchet: " + hatchet, bot);
                    Bank.withdraw(hatchet, 1);
                    bot.hatchet = hatchet;
                    break;
                }
            }
        } else {
            UpdateUI.currentTask("Stopping bot. No Hatchet found.", bot);
            bot.stop();
        }
    }
}
