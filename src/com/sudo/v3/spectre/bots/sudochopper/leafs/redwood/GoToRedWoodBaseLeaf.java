package com.sudo.v3.spectre.bots.sudochopper.leafs.redwood;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudochopper.ApexChopper;
import com.sudo.v3.spectre.api.game.hud.SudoCamera;
import com.sudo.v3.spectre.common.navigation.Traverse;
import com.sudo.v3.spectre.statics.Areas;
import com.sudo.v3.spectre.statics.UpdateUI;

public class GoToRedWoodBaseLeaf extends LeafTask {
    private ApexChopper bot;

    public GoToRedWoodBaseLeaf(ApexChopper bot){
        this.bot = bot;
    }

    @Override
    public void execute() {

        GameObject obj;

        Coordinate playerPosition;
        int currentPlane = -1;

        if((playerPosition = bot.player.getPosition()) != null) {
            currentPlane = playerPosition.getPlane();
        }

        if(currentPlane == 0){

            UpdateUI.debug(bot.player.getName() + " -> GoToRedWoodLevel1Leaf: Already in Plane 1");

        }else if(currentPlane == 1){
            obj = GameObjects.newQuery().names("Rope ladder").actions("Climb-down").within(Areas.WOOD_GUILD_REDWOOD_LADDER_LEVEL1_AREA).results().nearest();

            if(obj != null) {
                if (obj.getVisibility() < 0.9 || obj.distanceTo(bot.player) > 6)
                    SudoCamera.ConcurrentlyTurnToWithYaw(obj);

                if(obj.interact("Climb-down", "Rope ladder")){
                    Execution.delayWhile(bot.player::isMoving, 1000, 5000);
                    Execution.delay(750, 1500);
                }

            }else{
                Traverse.smartRegionPath(bot.nav, Areas.WOOD_GUILD_REDWOOD_LADDER_LEVEL1_AREA);
            }

        }else if(currentPlane == 2){
            obj = GameObjects.newQuery().names("Carved redwood").actions("Enter").within(Areas.WOOD_GUILD_REDWOOD_LADDER_LEVEL2_AREA).results().nearest();

            if(obj != null) {
                if (obj.getVisibility() < 0.9 || obj.distanceTo(bot.player) > 6)
                    SudoCamera.ConcurrentlyTurnToWithYaw(obj);

                if(obj.interact("Enter", "Carved redwood")){
                    Execution.delayWhile(bot.player::isMoving, 1000, 5000);
                    Execution.delay(750, 1500);
                }
            }else{
                Traverse.smartRegionPath(bot.nav, Areas.WOOD_GUILD_REDWOOD_LADDER_LEVEL2_AREA);
            }
        }
    }
}
