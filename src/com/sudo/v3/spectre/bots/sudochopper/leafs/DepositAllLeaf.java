package com.sudo.v3.spectre.bots.sudochopper.leafs;

import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudochopper.ApexChopper;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 11/23/2016.
 */
public class DepositAllLeaf extends LeafTask
{
    private ApexChopper bot;

    public DepositAllLeaf(ApexChopper bot)
    {
        this.bot = bot;
    }

    @Override
    public void execute() {
        UpdateUI.currentTask("Depositing items in bank", bot);
        if (!bot.isRS3)
            Bank.depositAllExcept(bot.hatchet);
        else {
            if (bot.usingPreset)
            {
                UpdateUI.currentTask("Loading bank preset", bot);
                Bank.loadPreset(bot.presetNumber);
            } else
            {
                UpdateUI.currentTask("Depositing items", bot);
                Bank.depositAllExcept(bot.hatchet);
            }
        }
    }
}
