package com.sudo.v3.spectre.bots.sudochopper.leafs;

import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudochopper.ApexChopper;
import com.sudo.v3.spectre.statics.UpdateUI;

public class WithdrawPrayerPotionLeaf extends LeafTask {
    private ApexChopper bot;

    public WithdrawPrayerPotionLeaf(ApexChopper bot) {
        this.bot = bot;
    }

    @Override
    public void execute() {
        if (Bank.contains(bot.prayerRenewalRegex)) {
            if(bot.usingPreset){
                if(Bank.loadPreset(bot.presetNumber))
                    Execution.delayUntil(() -> Inventory.contains(bot.prayerRenewalRegex), 750, 1500);
            } else if(Bank.withdraw(bot.prayerRenewalRegex, bot.prayerPotionWithdrawAmount)){
                bot.prayerPotionWithdrawAmount = Random.nextInt(2, 8);
                Execution.delayUntil(() -> Inventory.contains(bot.prayerRenewalRegex), 750, 1500);
            }
        } else {
            UpdateUI.currentTask("Bank does not contain a prayer renewal potion, stopping bot.", bot);
            bot.stop("Bank does not contain a prayer renewal potion.");
        }
    }
}
