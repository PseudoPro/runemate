package com.sudo.v3.spectre.bots.sudochopper.leafs;

import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.region.Banks;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudochopper.ApexChopper;
import com.sudo.v3.spectre.api.game.hud.SudoCamera;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 11/25/2016.
 */
public class IsChoppingLeaf extends LeafTask {
    private ApexChopper bot;

    public IsChoppingLeaf(ApexChopper bot) {
        this.bot = bot;
    }

    @Override
    public void execute() {
        if (Inventory.getItems().size() > 24) {
            bot.bankEntity = Banks.newQuery().within(bot.bankArea).results().nearest();

            if (bot.bankEntity != null) {
                UpdateUI.currentTask("Inventory nearly full, attempting to hover over deposit area...", bot);
                if (bot.bankEntity.getVisibility() < 0.9 || bot.bankEntity.distanceTo(bot.player) > 4)
                    SudoCamera.ConcurrentlyTurnToWithYaw(bot.bankEntity);
                else
                    bot.bankEntity.hover();
            }
        } else {
            UpdateUI.currentTask("Currently chopping \"" + bot.treeName + "\", delaying next action...", bot);
            if (!bot.treeName.equals("Tree") && !bot.treeName.equals("Evergreen"))
                Execution.delay(1500, 5000);
        }
    }
}