package com.sudo.v3.spectre.bots.apexswamptoad.ui;

import com.sudo.v3.spectre.bots.apexswamptoad.ApexSwampToad;
import com.sudo.v3.ui.base.SudoFXController;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Java FX Controller for the ToadFXGui class
 */
public class ToadFXController extends SudoFXController implements Initializable {

    private ApexSwampToad bot;

    //<editor-fold desc="Configure Tab">
    @FXML
    private Button start_BT;
    //</editor-fold>


    public ToadFXController(ApexSwampToad bot) {
        super(bot);
        this.bot = bot;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Initialize Super
        super.initialize(location, resources);

        start_BT.setOnAction(getStartButtonAction());
    }

    public EventHandler<ActionEvent> getStartButtonAction() {
        return event -> {
            super.getStart_BTEvent();
        };
    }
}