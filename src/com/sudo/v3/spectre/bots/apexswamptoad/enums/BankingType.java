package com.sudo.v3.spectre.bots.apexswamptoad.enums;

/**
 * Created by Proxify on 9/18/2017.
 */
public enum BankingType {
    LEAVING,
    DEPOSITING
}
