package com.sudo.v3.spectre.bots.apexswamptoad.enums;

/**
 * Created by Proxify on 10/17/2017.
 */
public enum SwampToadType {
    CURRENT,
    NEXT
}
