package com.sudo.v3.spectre.bots.apexswamptoad.enums;

/**
 * Created by Proxify on 9/18/2017.
 */
public enum TraversalLocation {
    TOAD_AREA,
    BANK_AREA,
    STAIRS_FLOOR_ONE,
    STAIRS_FLOOR_TWO
}
