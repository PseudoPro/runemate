package com.sudo.v3.spectre.bots.apexswamptoad.branches;

import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.spectre.bots.apexswamptoad.ApexSwampToad;
import com.sudo.v3.spectre.bots.apexswamptoad.enums.BankingType;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Proxify on 9/18/2017.
 */
public class IsBankOpen extends BranchTask {
    private ApexSwampToad bot;

    public IsBankOpen(ApexSwampToad bot){
        this.bot = bot;
    }

    @Override
    public boolean validate() {
        UpdateUI.debug(bot.player.getName() + " -> Branch IsBankOpen(" + bot.bankingType + "): " + Bank.isOpen());
        return Bank.isOpen();
    }

    @Override
    public TreeTask successTask() {
        if(bot.bankingType == BankingType.DEPOSITING)
            return bot.depositAllLeaf;
        else { // when BankingType is LEAVING
            //if (SwampPlayerSense.Key.CLOSE_BANK_BEFORE_LEAVING.getAsBoolean())
                return bot.closeBankLeaf;
            //else
            //   return bot.isBankNearbyBranch;
        }
    }

    @Override
    public TreeTask failureTask() {
        if(bot.bankingType == BankingType.DEPOSITING)
            return bot.openBankLeaf;
        else { // when BankingType is LEAVING
            return bot.isBankNearbyBranch;
        }
    }

}
