package com.sudo.v3.spectre.bots.apexswamptoad.branches;

import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.GroundItems;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.spectre.common.playersense.ApexPlayerSense;
import com.sudo.v3.spectre.bots.apexswamptoad.ApexSwampToad;
import com.sudo.v3.spectre.bots.apexswamptoad.enums.SwampToadType;
import com.sudo.v3.spectre.bots.apexswamptoad.enums.TraversalLocation;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Proxify on 9/18/2017.
 */
public class IsToadNearby extends BranchTask {
    private ApexSwampToad bot;

    public IsToadNearby(ApexSwampToad bot){
        this.bot = bot;
    }

    @Override
    public boolean validate() {
        if(bot.swampToadItem == null || !bot.swampToadItem.isValid() || bot.swampToadInteractTimer.hasExpired())
        {
            Area aroundPlayer = new Area.Circular(bot.player.getPosition(), bot.scanRadius);

            bot.swampToadItem = GroundItems.newQuery().names(bot.swampToadName).within(aroundPlayer).results().random();

            if (bot.swampToadItem == null || !bot.swampToadItem.isValid())
                bot.swampToadItem = bot.getNearestSwampToadOnPath(SwampToadType.CURRENT);

            bot.swampToadInteractTimer.reset();
        }

        UpdateUI.debug(bot.player.getName() + " -> Branch IsToadNearby: " + (bot.swampToadItem != null && bot.swampToadItem.distanceTo(bot.player) < 15));
        return bot.swampToadItem != null && bot.swampToadItem.distanceTo(bot.player) < 15;
    }

    @Override
    public TreeTask successTask() {
        if(ApexPlayerSense.Key.HOVER_OVER_NEXT_PICKUP.getAsBoolean())
            bot.grabToadLeaf.setObjects(bot.swampToadItem, bot.getNearestSwampToadOnPath(SwampToadType.NEXT));
        else
            bot.grabToadLeaf.setObject(bot.swampToadItem);
        return bot.grabToadLeaf;
    }

    @Override
    public TreeTask failureTask() {
        bot.traversalLocation = TraversalLocation.TOAD_AREA;
        return bot.traversalLeaf;
    }

}
