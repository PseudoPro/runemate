package com.sudo.v3.spectre.bots.apexswamptoad.branches;

import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.RuneScape;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoRootTask;
import com.sudo.v3.spectre.bots.apexswamptoad.ApexSwampToad;
import com.sudo.v3.spectre.statics.UpdateUI;


/**
 * Created by SudoPro on 9/18/2017.
 */
public class Root extends SudoRootTask {
    private ApexSwampToad bot;

    public Root(ApexSwampToad bot) {
        super(bot);
        this.bot = bot;
    }

    @Override
    public TreeTask rootTask() {
        if (bot.firstLogin) {
            if (RuneScape.isLoggedIn()) {
                bot.firstLogin = false;
                bot.updateSession(0.0);
            }
        }

        if (bot.getMetaData().getHourlyPrice().doubleValue() > 0 || bot.isValidSession() || Environment.isSDK()) {
            if (bot.currentlyBreaking) {
                if (RuneScape.isLoggedIn()) {
                    UpdateUI.currentTask("Breaking triggered. Attempting to log off...", bot);
                    RuneScape.logout(true);
                }
            } else if (bot.player != null) {
                return bot.isInventoryFullBranch;
            }
            return bot.emptyLeaf;
        } else {
            return bot.liteVersionExpireLeaf;
        }
    }
}