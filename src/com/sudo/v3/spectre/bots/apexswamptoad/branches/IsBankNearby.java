package com.sudo.v3.spectre.bots.apexswamptoad.branches;

import com.runemate.game.api.hybrid.entities.LocatableEntity;
import com.runemate.game.api.hybrid.region.Banks;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.spectre.bots.apexswamptoad.ApexSwampToad;
import com.sudo.v3.spectre.bots.apexswamptoad.enums.BankingType;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Proxify on 9/18/2017.
 */
public class IsBankNearby extends BranchTask {
    private ApexSwampToad bot;
    private LocatableEntity bank = null;

    public IsBankNearby(ApexSwampToad bot){
        this.bot = bot;
    }

    @Override
    public boolean validate() {
        bank = Banks.newQuery().within(bot.bankArea).results().nearest();

        UpdateUI.debug(bot.player.getName() + " -> Branch IsBankNearby(" + bot.bankingType + "): " + (bank != null && bank.distanceTo(bot.player) < 8));
        return bank != null && bank.distanceTo(bot.player) < 8;
    }

    @Override
    public TreeTask successTask() {
        if(bot.bankingType == BankingType.DEPOSITING)
            return bot.isBankOpenBranch;
        else // when BankingType is LEAVING
            return bot.areStairsNearbyBranch;
    }

    @Override
    public TreeTask failureTask() {
        if(bot.bankingType == BankingType.DEPOSITING)
            return bot.areStairsNearbyBranch;
        else // when BankingType is LEAVING
            return bot.isToadNearbyBranch;
    }

}
