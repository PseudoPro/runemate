package com.sudo.v3.spectre.bots.apexswamptoad.branches;

import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.spectre.bots.apexswamptoad.ApexSwampToad;
import com.sudo.v3.spectre.bots.apexswamptoad.enums.BankingType;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Proxify on 9/18/2017.
 */
public class IsInventoryFull extends BranchTask {
    private ApexSwampToad bot;

    public IsInventoryFull(ApexSwampToad bot){
        this.bot = bot;
    }

    @Override
    public boolean validate() {
        UpdateUI.debug(bot.player.getName() + " -> Branch IsInventoryFull: " + Inventory.isFull());
        return Inventory.isFull();
    }

    @Override
    public TreeTask successTask() {
        bot.bankingType = BankingType.DEPOSITING;
        return bot.isBankNearbyBranch;
    }

    @Override
    public TreeTask failureTask(){
        bot.bankingType = BankingType.LEAVING;
        return bot.isBankOpenBranch;
    }
}
