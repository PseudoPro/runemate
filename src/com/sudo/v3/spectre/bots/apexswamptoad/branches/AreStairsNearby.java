package com.sudo.v3.spectre.bots.apexswamptoad.branches;

import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.spectre.bots.apexswamptoad.ApexSwampToad;
import com.sudo.v3.spectre.bots.apexswamptoad.enums.BankingType;
import com.sudo.v3.spectre.bots.apexswamptoad.enums.TraversalLocation;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Proxify on 9/18/2017.
 */
public class AreStairsNearby extends BranchTask {
    private ApexSwampToad bot;

    public AreStairsNearby(ApexSwampToad bot){
        this.bot = bot;
    }

    @Override
    public boolean validate() {
        // Trying to go up the stairs
        if(bot.bankingType == BankingType.DEPOSITING)
            bot.stairsObj = GameObjects.newQuery().names(bot.stairsName).actions(bot.climbUpAction).within(bot.floorOneStairArea).results().nearest();
        else // when BankingType is LEAVING
            bot.stairsObj = GameObjects.newQuery().names(bot.stairsName).actions(bot.climbDownAction).within(bot.floorTwoStairArea).results().nearest();

        UpdateUI.debug(bot.player.getName() + " -> Branch AreStairsNearby(" + bot.bankingType + "): " + (bot.stairsObj != null && bot.stairsObj.distanceTo(bot.player) < 8));
        return bot.stairsObj != null && bot.stairsObj.distanceTo(bot.player) < 8;
    }

    @Override
    public TreeTask successTask() {
        if(bot.bankingType == BankingType.DEPOSITING) {
            //if(ApexPlayerSense.Key.HOVER_OVER_NEXT_PICKUP.getAsBoolean())
            //    bot.climbDownStairsLeaf.setObjects(bot.stairsObj, Banks.newQuery().results().nearest());
            //else
                bot.climbUpStairsLeaf.setObject(bot.stairsObj);
            return bot.climbUpStairsLeaf;
        }
        else {  // when BankingType is LEAVING
            bot.climbDownStairsLeaf.setObject(bot.stairsObj);
            return bot.climbDownStairsLeaf;
        }
    }

    @Override
    public TreeTask failureTask() {
        if(bot.bankingType == BankingType.DEPOSITING){
            bot.traversalLocation = TraversalLocation.STAIRS_FLOOR_ONE;
            return bot.traversalLeaf;
        }
        else { // when BankingType is LEAVING
            bot.traversalLocation = TraversalLocation.STAIRS_FLOOR_TWO;
            return bot.traversalLeaf;
        }
    }
}
