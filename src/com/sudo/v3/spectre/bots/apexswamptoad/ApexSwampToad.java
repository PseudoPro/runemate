package com.sudo.v3.spectre.bots.apexswamptoad;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.GroundItem;
import com.runemate.game.api.hybrid.entities.definitions.ItemDefinition;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.queries.results.LocatableEntityQueryResults;
import com.runemate.game.api.hybrid.region.GroundItems;
import com.runemate.game.api.hybrid.util.calculations.CommonMath;
import com.runemate.game.api.hybrid.util.collections.Pair;
import com.runemate.game.api.script.framework.listeners.events.ItemEvent;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBot;
import com.sudo.v3.spectre.bots.apexswamptoad.branches.*;
import com.sudo.v3.spectre.bots.apexswamptoad.enums.BankingType;
import com.sudo.v3.spectre.bots.apexswamptoad.enums.SwampToadType;
import com.sudo.v3.spectre.bots.apexswamptoad.enums.TraversalLocation;
import com.sudo.v3.spectre.bots.apexswamptoad.leafs.TraversalLeaf;
import com.sudo.v3.spectre.bots.apexswamptoad.playersense.SwampPlayerSense;
import com.sudo.v3.spectre.bots.apexswamptoad.ui.ToadFXGui;
import com.sudo.v3.spectre.bots.apexswamptoad.ui.ToadInfoUI;
import com.sudo.v3.spectre.api.util.SudoTimer;
import com.sudo.v3.spectre.common.leafs.InteractLeaf;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.sudo.v3.ui.Info;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by SudoPro on 11/23/2016.
 */
public class ApexSwampToad extends SudoBot {
    private ToadInfoUI infoUI;
    private ToadFXGui configUI;

    public BankingType bankingType = BankingType.DEPOSITING;
    public TraversalLocation traversalLocation = TraversalLocation.TOAD_AREA;

    public int swampToadCount = 0, scanRadius = 2;

    public Area bankArea = new Area.Rectangular(new Coordinate(2443, 3421, 1), new Coordinate(2448, 3428, 1)),
            swampArea = new Area.Polygonal(new Coordinate(2404, 3510, 0), new Coordinate(2404, 3521, 0), new Coordinate(2425, 3521, 0), new Coordinate(2433, 3513, 0), new Coordinate(2432, 3502, 0), new Coordinate(2412, 3504, 0)),
            swampEntrance = new Area.Rectangular(new Coordinate(2420, 3504, 0), new Coordinate(2426, 3508, 0));
    public Area floorOneStairArea = new Area.Rectangular(new Coordinate(2449, 3436, 0), new Coordinate(2443, 3431, 0)),
            floorTwoStairArea = new Area.Rectangular(new Coordinate(2449, 3436, 1), new Coordinate(2443, 3431, 1));

    public GameObject stairsObj = null;
    public String stairsName = "Staircase", climbUpAction = "Climb-up", climbDownAction = "Climb-down";

    public GroundItem swampToadItem = null;
    public String swampToadName = "Swamp toad", swampToadAction = "Take";

    public SudoTimer swampToadInteractTimer = new SudoTimer(5000, 10000);

    private List<Pair<Coordinate, Boolean>> swampToadCoordinates = new ArrayList<Pair<Coordinate, Boolean>>(){{
        add(new Pair<>(new Coordinate(2421, 3509, 0), true));
        add(new Pair<>(new Coordinate(2417, 3508, 0), true));
        add(new Pair<>(new Coordinate(2418, 3511, 0), true));
        add(new Pair<>(new Coordinate(2417, 3512, 0), true));
        add(new Pair<>(new Coordinate(2416, 3512, 0), true));
        add(new Pair<>(new Coordinate(2413, 3511, 0), true));
        add(new Pair<>(new Coordinate(2411, 3512, 0), true));
        add(new Pair<>(new Coordinate(2409, 3514, 0), true));
        add(new Pair<>(new Coordinate(2407, 3516, 0), true));
        add(new Pair<>(new Coordinate(2412, 3519, 0), true));
        add(new Pair<>(new Coordinate(2415, 3518, 0), true));
        add(new Pair<>(new Coordinate(2417, 3515, 0), true));
        add(new Pair<>(new Coordinate(2417, 3516, 0), true));
        add(new Pair<>(new Coordinate(2418, 3517, 0), true));
        add(new Pair<>(new Coordinate(2421, 3519, 0), true));
        add(new Pair<>(new Coordinate(2424, 3517, 0), true));
        add(new Pair<>(new Coordinate(2424, 3514, 0), true));
        add(new Pair<>(new Coordinate(2428, 3510, 0), true));
    }};

    // Branch Task
    public Root root = new Root(this);
    public IsInventoryFull isInventoryFullBranch = new IsInventoryFull(this);
    public IsBankNearby isBankNearbyBranch = new IsBankNearby(this);
    public AreStairsNearby areStairsNearbyBranch = new AreStairsNearby(this);
    public IsBankOpen isBankOpenBranch = new IsBankOpen(this);
    public IsToadNearby isToadNearbyBranch = new IsToadNearby(this);

    // Leaf Tasks
    public TraversalLeaf traversalLeaf = new TraversalLeaf(this);
    public InteractLeaf<GroundItem> grabToadLeaf = new InteractLeaf<>(this, swampToadItem, swampToadName, swampToadAction);
    public InteractLeaf<GameObject> climbUpStairsLeaf = new InteractLeaf<>(this, stairsObj, stairsName, climbUpAction);
    public InteractLeaf<GameObject> climbDownStairsLeaf = new InteractLeaf<>(this, stairsObj, stairsName, climbDownAction);

    // Default Constructor
    public ApexSwampToad() {
        // Give spectre client this class as the embeddableUI
        setEmbeddableUI(this);
    }

    @Override
    public void onStart(String... args) {
        getEventDispatcher().addListener(this);

        // Initialize PlayerSense
        SwampPlayerSense.initializeKeys();

        if(!SwampPlayerSense.Key.CLOCKWISE_PICKUP.getAsBoolean())
            Collections.reverse(swampToadCoordinates);

        swampToadInteractTimer.start();

        super.onStart();
    }

    @Override
    public String getBotName() {
        return "ApexSwampToad";
    }

    @Override
    public ObjectProperty<? extends Node> botInterfaceProperty() {
        if (botInterfaceProperty == null) {
            botInterfaceProperty = new SimpleObjectProperty<>(configUI = new ToadFXGui(this));
            guiList = new ArrayList<Node>();
            //guiList.add(_sbGUI = new SudoBuddyFXGui(this));
            guiList.add(configUI);
            guiList.add(infoUI = new ToadInfoUI(this));

        }
        return botInterfaceProperty;
    }

    @Override
    public TreeTask createRootTask() {
        return root;
    }

    @Override
    public void updateInfo() {

        // UpdateUI all current values in the XPInfo List
        //XPInfoMap.values().forEach((xpInfo) -> xpInfo.update());

        displayInfoMap.put("Swamp Toad Count: ", Integer.toString(swampToadCount) +
                " (" + Integer.toString((int) CommonMath.rate(TimeUnit.HOURS, STOPWATCH.getRuntime(), swampToadCount)) + "/Hour)");
        displayInfoMap.put("GP: ", Integer.toString(grossIncome) +
                " (" + Integer.toString((int) CommonMath.rate(TimeUnit.HOURS, STOPWATCH.getRuntime(), grossIncome)) + "/Hour)");

        try {
            info = new Info(XPInfoMap,           // XP Info
                    displayInfoMap,
                    STOPWATCH.getRuntimeAsString(),        // Total Runtime
                    currentTaskString,                     // Current Task
                    abTaskString);                         // antiban Task

        } catch (Exception e) {
            e.printStackTrace();
        }

        Platform.runLater(() -> infoUI.update());
    }

    @Override
    public void onItemAdded(ItemEvent event) {
        super.onItemAdded(event);
        ItemDefinition definition = event.getItem().getDefinition();

        if (definition != null) {
            if (definition.getName().contains(swampToadName)) {
                swampToadCount++;
            }

        }
    }

    @Override
    public void onItemRemoved(ItemEvent event){
        if(!bankArea.contains(player))
            super.onItemRemoved(event);
    }

    public GroundItem getNearestSwampToadOnPath(SwampToadType swampToadType){
        GroundItem item = null;

        LocatableEntityQueryResults<GroundItem> swampToadQuery = GroundItems.newQuery().names(swampToadName).results();
        HashSet<Coordinate> currentSwampToadCoordinates = new HashSet<>();

        // Fill HashSet with live swamp toad coordinates
        for(GroundItem i : swampToadQuery){
            if(i != null && i.getPosition() != null)
                currentSwampToadCoordinates.add(i.getPosition());
        }

        for(int i = 0; i < swampToadCoordinates.size(); i++) {
            if (swampToadCoordinates.get(i).getRight()) {
                if (currentSwampToadCoordinates.contains(swampToadCoordinates.get(i).getLeft())) {
                    if(swampToadType == SwampToadType.CURRENT)
                        item = swampToadQuery.nearestTo(swampToadCoordinates.get(i).getLeft());
                    else{
                        if(i < swampToadCoordinates.size() - 1)
                            item = swampToadQuery.nearestTo(swampToadCoordinates.get(i + 1).getLeft());
                        else
                            item = swampToadQuery.nearestTo(swampToadCoordinates.get(0).getLeft());
                    }
                    return item;
                } else {
                    UpdateUI.debug("Setting Index[" + i + "] to false");
                    swampToadCoordinates.get(i).setRight(false);
                }
            }

            // Set all values back to true
            if(i >= swampToadCoordinates.size() - 1){
                for(Pair<Coordinate, Boolean> pair : swampToadCoordinates){
                    pair.setRight(true);
                }
            }
        }

        return item;
    }
}