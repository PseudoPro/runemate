package com.sudo.v3.spectre.bots.apexswamptoad.leafs;

import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.common.playersense.ApexPlayerSense;
import com.sudo.v3.spectre.bots.apexswamptoad.ApexSwampToad;
import com.sudo.v3.spectre.bots.apexswamptoad.enums.TraversalLocation;
import com.sudo.v3.spectre.common.navigation.Traverse;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by Proxify on 9/18/2017.
 */
public class TraversalLeaf extends LeafTask {
    private ApexSwampToad bot;

    public TraversalLeaf(ApexSwampToad bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        if(bot.traversalLocation == TraversalLocation.TOAD_AREA) {
            UpdateUI.currentTask("Traversing to Swamp Area", bot);
            Traverse.smartRegionPath(bot.nav, bot.swampEntrance);
        }
        else if(bot.traversalLocation == TraversalLocation.BANK_AREA) {
            UpdateUI.currentTask("Traversing to Bank Area", bot);
            Traverse.smartRegionPath(bot.nav, bot.bankArea);
        }
        else if(bot.traversalLocation == TraversalLocation.STAIRS_FLOOR_ONE) {
            UpdateUI.currentTask("Traversing to Bank Stairs (Entrance)", bot);
            Traverse.smartRegionPath(bot.nav, bot.floorOneStairArea);
        }
        else { // traversal location is STAIRS_FLOOR_TWO
            UpdateUI.currentTask("Traversing to Bank Stairs (Leaving)", bot);
            Traverse.smartRegionPath(bot.nav, bot.floorTwoStairArea);
        }

        // Delay after each attempt to traverse
        Execution.delay(500, 750);
        Execution.delayWhile(bot.player::isMoving, ApexPlayerSense.Key.TRAVERSAL_DELAY.getAsInteger());
    }
}
