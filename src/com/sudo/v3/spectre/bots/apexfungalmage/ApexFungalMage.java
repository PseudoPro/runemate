package com.sudo.v3.spectre.bots.apexfungalmage;

import com.runemate.game.api.hybrid.RuneScape;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.GroundItem;
import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.util.calculations.CommonMath;
import com.runemate.game.api.script.framework.listeners.events.ItemEvent;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBot;
import com.sudo.v3.spectre.bots.apexfungalmage.branches.*;
import com.sudo.v3.spectre.bots.apexfungalmage.leafs.*;
import com.sudo.v3.spectre.bots.apexfungalmage.ui.FungalFXGui;
import com.sudo.v3.spectre.bots.apexfungalmage.ui.FungalInfoUI;
import com.sudo.v3.spectre.common.leafs.InteractLeaf;
import com.sudo.v3.spectre.statics.Areas;
import com.sudo.v3.ui.Info;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class ApexFungalMage extends SudoBot {
    private FungalInfoUI infoUI;
    private FungalFXGui configUI;

    public double healthPercent = -1.0;
    public int presetNumber = 1;
    public String foodName = "Swordfish", fungalMageName = "Fungal mage", teleportTabName = "Varrock teleport", dungeonEntranceName = "Cave entrance", dungeonAction = "Enter";

    public Area fungalArea = new Area.Circular(new Coordinate(4642, 5487, 3), 8),
            dungeonEntranceArea = new Area.Rectangular(new Coordinate(3406, 3324, 0), new Coordinate(3415, 3332, 0)),
            dungeonEntranceWalkwayArea = new Area.Rectangular(new Coordinate(3331, 3333, 0), new Coordinate(3415, 3324, 0)),
            bankArea = Areas.VARROCK_EAST_BANK_AREA,
            cityOfVarrockArea = Areas.CITY_OF_VARROCK_AREA;
    public Area traversalArea;

    public GameObject dungeonEntranceObj;
    public GroundItem pickupGroundItem;
    public Npc mageNpc;

    public ArrayList<String> itemNamesToPickup = new ArrayList<String>(){
        {
            add("Potato cactus");
        }
    };

    // Branches
    public Root root = new Root(this);
    public IsInFungalArea isInFungalArea = new IsInFungalArea(this);
    public IsInDungeonArea isInDungeonArea = new IsInDungeonArea(this);
    public IsInDungeonWalkwayArea isInDungeonWalkwayArea = new IsInDungeonWalkwayArea(this);
    public IsDungeonEntranceAround isDungeonEntranceAround = new IsDungeonEntranceAround(this);
    public IsItemPickupAround isItemPickupAround = new IsItemPickupAround(this);
    public IsPlayerInCombat isPlayerInCombat = new IsPlayerInCombat(this);
    public IsBankOpen isBankOpen = new IsBankOpen(this);
    public IsInBankArea isInBankArea = new IsInBankArea(this);
    public IsInVarrock isInVarrock = new IsInVarrock(this);

    // Leafs
    public HealLeaf healLeaf = new HealLeaf(this);
    public TeleportLeaf teleportLeaf = new TeleportLeaf(this);
    public TraversalLeaf traversalLeaf = new TraversalLeaf(this);
    public WebTraversalLeaf webTraversalLeaf = new WebTraversalLeaf(this);
    public InteractLeaf<GameObject> enterDungeonLeaf = new InteractLeaf<>(this, dungeonEntranceObj, dungeonEntranceName, dungeonAction);
    public InteractLeaf<GroundItem> pickupGroundItemLeaf = new InteractLeaf<>(this, pickupGroundItem, "Name", "Take");
    public InteractLeaf<Npc> attackMageLeaf = new InteractLeaf<>(this, mageNpc, fungalMageName, "Attack");
    public InCombatLeaf inCombatLeaf = new InCombatLeaf(this);
    public TogglePresetLeaf togglePresetLeaf = new TogglePresetLeaf(this);

    @Override
    public ObjectProperty<? extends Node> botInterfaceProperty() {
        if (botInterfaceProperty == null) {
            botInterfaceProperty = new SimpleObjectProperty<>(configUI = new FungalFXGui(this));
            guiList = new ArrayList<Node>();
            //guiList.add(_sbGUI = new SudoBuddyFXGui(this));
            guiList.add(configUI);
            guiList.add(infoUI = new FungalInfoUI(this));

        }
        return botInterfaceProperty;
    }

    public ApexFungalMage(){
        // Give spectre client this class as the embeddableUI
        setEmbeddableUI(this);
    }

    @Override
    public TreeTask createRootTask() {
        return root;
    }

    @Override
    public void onStart(String... args) {
        getEventDispatcher().addListener(this);
        super.onStart();
    }

    @Override
    public void onItemRemoved(ItemEvent event){
        if(RuneScape.isLoggedIn() && player != null && (!Bank.isOpen() || !bankArea.contains(player)))
            super.onItemRemoved(event);
    }

    @Override
    public boolean isPrivate(){
        return true;
    }

    @Override
    public String getBotName() {
        return "ApexFungalMage";
    }

    @Override
    public void updateInfo() {
        // UpdateUI all current values in the XPInfo List
        XPInfoMap.values().forEach((xpInfo) -> xpInfo.update());

        displayInfoMap.put("GP: ", Integer.toString(grossIncome) +
                " (" + Integer.toString((int) CommonMath.rate(TimeUnit.HOURS, STOPWATCH.getRuntime(), grossIncome)) + "/Hour)");

        try {
            info = new Info(XPInfoMap,           // XP Info
                    displayInfoMap,
                    STOPWATCH.getRuntimeAsString(),        // Total Runtime
                    currentTaskString,                     // Current Task
                    abTaskString);                         // antiban Task

        } catch (Exception e) {
            e.printStackTrace();
        }

        Platform.runLater(() -> infoUI.update());
    }
}
