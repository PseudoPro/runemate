package com.sudo.v3.spectre.bots.apexfungalmage.branches;

import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexfungalmage.ApexFungalMage;
import com.sudo.v3.spectre.statics.UpdateUI;

public class IsInDungeonArea extends SudoBranchTask {
    private ApexFungalMage bot;
    private Coordinate playerPosition;

    public IsInDungeonArea (ApexFungalMage bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        bot.traversalArea = bot.fungalArea;
        UpdateUI.currentTask("Traversing to Fungal Mage area.", bot);
        return bot.traversalLeaf;
    }

    @Override
    public boolean validate() {
        validate = (playerPosition = bot.player.getPosition()) != null && playerPosition.getPlane() == 3;
        UpdateUI.debug(bot.player.getName() + " -> Branch: IsInDungeonArea: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        return bot.isDungeonEntranceAround;
    }
}
