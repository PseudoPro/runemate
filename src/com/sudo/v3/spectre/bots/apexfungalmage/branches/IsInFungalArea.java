package com.sudo.v3.spectre.bots.apexfungalmage.branches;

import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexfungalmage.ApexFungalMage;
import com.sudo.v3.spectre.statics.UpdateUI;

public class IsInFungalArea extends SudoBranchTask {
    private ApexFungalMage bot;

    public IsInFungalArea (ApexFungalMage bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        return bot.isPlayerInCombat;
    }

    @Override
    public boolean validate() {
        validate = bot.fungalArea.contains(bot.player);
        UpdateUI.debug(bot.player.getName() + " -> Branch: IsInFungalArea: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        return bot.isInDungeonArea;
    }
}
