package com.sudo.v3.spectre.bots.apexfungalmage.branches;

import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexfungalmage.ApexFungalMage;
import com.sudo.v3.spectre.api.game.entities.ApexPlayer;
import com.sudo.v3.spectre.statics.UpdateUI;

public class IsPlayerInCombat extends SudoBranchTask{
    private ApexFungalMage bot;

    public IsPlayerInCombat (ApexFungalMage bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        return bot.inCombatLeaf;
    }

    @Override
    public boolean validate() {
        validate = ApexPlayer.isTargetting(bot.fungalMageName);
        UpdateUI.debug(bot.player.getName() + " -> Branch: IsPlayerInCombat: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        return bot.isItemPickupAround;
    }
}
