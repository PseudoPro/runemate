package com.sudo.v3.spectre.bots.apexfungalmage.branches;

import com.runemate.game.api.hybrid.entities.definitions.ItemDefinition;
import com.runemate.game.api.hybrid.region.GroundItems;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexfungalmage.ApexFungalMage;
import com.sudo.v3.spectre.statics.UpdateUI;

public class IsItemPickupAround extends SudoBranchTask {
    private ApexFungalMage bot;
    private ItemDefinition def;

    public IsItemPickupAround (ApexFungalMage bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        bot.pickupGroundItemLeaf.setObject(bot.pickupGroundItem);

        if((def = bot.pickupGroundItem.getDefinition()) != null && def.getName() != null)
            bot.pickupGroundItemLeaf.setName(def.getName());

        return bot.pickupGroundItemLeaf;
    }

    @Override
    public boolean validate() {
        bot.pickupGroundItem = GroundItems.newQuery().names(bot.itemNamesToPickup.toArray(new String[0])).results().nearest();

        validate = bot.pickupGroundItem != null && bot.pickupGroundItem.isValid();
        UpdateUI.debug(bot.player.getName() + " -> Branch: IsItemPickupAround: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        if(bot.mageNpc == null || !bot.mageNpc.isValid() || !bot.mageNpc.isVisible())
            bot.mageNpc = Npcs.newQuery().names(bot.fungalMageName).within(bot.fungalArea).results().random();

        if(bot.mageNpc != null && bot.mageNpc.isValid()) {
            bot.attackMageLeaf.setObject(bot.mageNpc);
            return bot.attackMageLeaf;
        }else{
            UpdateUI.currentTask("Tried to attack Fungal Mage, but NPC returned as null or invalid.", bot);
            return bot.emptyLeaf;
        }
    }
}
