package com.sudo.v3.spectre.bots.apexfungalmage.branches;

import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexfungalmage.ApexFungalMage;
import com.sudo.v3.spectre.statics.UpdateUI;

public class IsInBankArea extends SudoBranchTask {
    private ApexFungalMage bot;

    public IsInBankArea (ApexFungalMage bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        bot.openBankLeaf.setGenericBank(true);
        return bot.openBankLeaf;
    }

    @Override
    public boolean validate() {
        validate = bot.bankArea.contains(bot.player);
        UpdateUI.debug(bot.player.getName() + " -> Branch: IsInBankArea: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        return bot.isInVarrock;
    }
}
