package com.sudo.v3.spectre.bots.apexfungalmage.branches;

import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexfungalmage.ApexFungalMage;
import com.sudo.v3.spectre.statics.UpdateUI;

public class IsDungeonEntranceAround extends SudoBranchTask {
    private ApexFungalMage bot;

    public IsDungeonEntranceAround (ApexFungalMage bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        bot.enterDungeonLeaf.setObject(bot.dungeonEntranceObj);
        return bot.enterDungeonLeaf;
    }

    @Override
    public boolean validate() {
        bot.dungeonEntranceObj = GameObjects.newQuery().names(bot.dungeonEntranceName).filter(i -> i.distanceTo(bot.player) < 15).results().nearest();

        validate = bot.dungeonEntranceObj != null && bot.dungeonEntranceObj.isValid();
        UpdateUI.debug(bot.player.getName() + " -> Branch: IsDungeonEntranceAround: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        return bot.isInDungeonWalkwayArea;
    }
}
