package com.sudo.v3.spectre.bots.apexfungalmage.branches;

import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexfungalmage.ApexFungalMage;
import com.sudo.v3.spectre.statics.UpdateUI;

public class IsInVarrock extends SudoBranchTask {
    private ApexFungalMage bot;

    public IsInVarrock (ApexFungalMage bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        bot.traversalArea = bot.bankArea;
        UpdateUI.currentTask("Traversing to bank area.", bot);
        return bot.traversalLeaf;
    }

    @Override
    public boolean validate() {
        validate = bot.cityOfVarrockArea.contains(bot.player);
        UpdateUI.debug(bot.player.getName() + " -> Branch: IsInVarrock: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        return bot.teleportLeaf;
    }
}
