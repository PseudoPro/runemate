package com.sudo.v3.spectre.bots.apexfungalmage.branches;

import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.RuneScape;
import com.runemate.game.api.hybrid.local.Camera;
import com.runemate.game.api.hybrid.local.Skill;
import com.runemate.game.api.hybrid.local.hud.interfaces.Health;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoRootTask;
import com.sudo.v3.spectre.bots.apexfungalmage.ApexFungalMage;
import com.sudo.v3.spectre.api.game.hud.SudoCamera;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.sudo.v3.ui.model.XPInfo;

public class Root extends SudoRootTask {
    private ApexFungalMage bot;
    boolean containsFood;

    public Root(ApexFungalMage bot) {
        super(bot);
        this.bot = bot;
    }


    @Override
    public TreeTask rootTask() {

        if (bot.getMetaData().getHourlyPrice().doubleValue() > 0 || bot.isValidSession() || Environment.isSDK()) {
            if (bot.firstLogin) {
                if (RuneScape.isLoggedIn()) {
                    // Add Skills to track to the HashMap
                    bot.XPInfoMap.put(Skill.MAGIC, new XPInfo(Skill.MAGIC));

                    bot.firstLogin = false;

                    UpdateUI.debug("Magic Level: " + Skill.MAGIC.getBaseLevel());
                    bot.updateSession(0.0);
                }
            }

            if (bot.healthPercent <= 0) {
                bot.healthPercent = Random.nextDouble(40, 65);
            }

            validate = Health.getCurrentPercent() < bot.healthPercent;
            UpdateUI.debug(bot.player.getName() + " -> Root: SubBranch: HealthPercentLow: " + validate);
            UpdateUI.debug(bot.player.getName() + " -> ActualHealthPercent: " + Health.getCurrentPercent());
            UpdateUI.debug(bot.player.getName() + " -> GeneratedHealthPercent: " + bot.healthPercent);

            containsFood = Inventory.contains(bot.foodName);
            UpdateUI.debug(bot.player.getName() + " -> Root: SubBranch: InventoryContainsFood: " + containsFood);

            if(Random.nextInt(0, 100) < 5 || Camera.getPitch() < 0.325){
                SudoCamera.ConcurrentlyTurnToWithYaw(bot.traversalArea);
            }

            if (validate) {
                if (containsFood) {
                    return bot.healLeaf;
                } else {
                    return bot.teleportLeaf;
                }
            } else {
                if (containsFood) {
                    return bot.isInFungalArea;
                } else {
                    return bot.isBankOpen;
                }
            }
        } else {
            return bot.liteVersionExpireLeaf;
        }
    }
}
