package com.sudo.v3.spectre.bots.apexfungalmage.branches;

import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexfungalmage.ApexFungalMage;
import com.sudo.v3.spectre.statics.UpdateUI;

public class IsInDungeonWalkwayArea extends SudoBranchTask {
    private ApexFungalMage bot;

    public IsInDungeonWalkwayArea (ApexFungalMage bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        bot.traversalArea = bot.dungeonEntranceArea;
        UpdateUI.currentTask("Taversing to dungeon entrance.", bot);
        return bot.traversalLeaf;
    }

    @Override
    public boolean validate() {
        validate = bot.dungeonEntranceWalkwayArea.contains(bot.player);
        UpdateUI.debug(bot.player.getName() + " -> Branch: IsInDungeonWalkwayArea: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        bot.traversalArea = bot.dungeonEntranceWalkwayArea;
        return bot.webTraversalLeaf;
    }
}
