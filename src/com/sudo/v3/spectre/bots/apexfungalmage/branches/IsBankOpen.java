package com.sudo.v3.spectre.bots.apexfungalmage.branches;

import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexfungalmage.ApexFungalMage;
import com.sudo.v3.spectre.statics.UpdateUI;

public class IsBankOpen extends SudoBranchTask {
    private ApexFungalMage bot;

    public IsBankOpen (ApexFungalMage bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        return bot.togglePresetLeaf;
    }

    @Override
    public boolean validate() {
        validate = Bank.isOpen();
        UpdateUI.debug(bot.player.getName() + " -> Branch: IsBankOpen: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        return bot.isInBankArea;
    }
}
