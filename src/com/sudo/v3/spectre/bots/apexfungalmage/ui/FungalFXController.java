package com.sudo.v3.spectre.bots.apexfungalmage.ui;

import com.sudo.v3.spectre.bots.apexfungalmage.ApexFungalMage;
import com.sudo.v3.ui.base.SudoFXController;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.net.URL;
import java.util.ResourceBundle;

public class FungalFXController extends SudoFXController implements Initializable {
    private ApexFungalMage bot;

    //<editor-fold desc="Configure Tab">
    @FXML
    private Button start_BT;
    //</editor-fold>


    public FungalFXController(ApexFungalMage bot) {
        super(bot);
        this.bot = bot;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Initialize Super
        super.initialize(location, resources);

        start_BT.setOnAction(getStartButtonAction());
    }

    public EventHandler<ActionEvent> getStartButtonAction() {
        return event -> {
            super.getStart_BTEvent();
        };
    }
}