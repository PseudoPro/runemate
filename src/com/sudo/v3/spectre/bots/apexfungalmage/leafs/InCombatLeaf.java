package com.sudo.v3.spectre.bots.apexfungalmage.leafs;

import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexfungalmage.ApexFungalMage;
import com.sudo.v3.spectre.statics.UpdateUI;

public class InCombatLeaf extends LeafTask {
    private ApexFungalMage bot;

    public InCombatLeaf (ApexFungalMage bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        UpdateUI.currentTask("Health is within threshold and player is in combat. Waiting...", bot);
        Execution.delay(500, 1500);
    }
}
