package com.sudo.v3.spectre.bots.apexfungalmage.leafs;

import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexfungalmage.ApexFungalMage;
import com.sudo.v3.spectre.statics.UpdateUI;

public class TeleportLeaf extends LeafTask {
    private ApexFungalMage bot;
    private SpriteItem teleportTab;

    public TeleportLeaf (ApexFungalMage bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        teleportTab = Inventory.getItems(bot.teleportTabName).first();

        if(teleportTab != null && teleportTab.isValid()){
            UpdateUI.currentTask("Teleporting with " + bot.teleportTabName, bot);
            if(teleportTab.interact("Break"))
                Execution.delayUntil(() -> bot.cityOfVarrockArea.contains(bot.player), 1000, 2000);
        }else{
            UpdateUI.currentTask("Error finding " + bot.teleportTabName + " within Inventory.", bot);
        }
    }
}
