package com.sudo.v3.spectre.bots.apexfungalmage.leafs;

import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexfungalmage.ApexFungalMage;
import com.sudo.v3.spectre.common.navigation.Traverse;

public class TraversalLeaf extends LeafTask {
    private ApexFungalMage bot;

    public TraversalLeaf (ApexFungalMage bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        if(Traverse.smartRegionPath(bot.nav, bot.traversalArea)) {
            Execution.delay(300, 500);
            Execution.delayWhile(bot.player::isMoving, 1000, 2000);
        }
    }
}
