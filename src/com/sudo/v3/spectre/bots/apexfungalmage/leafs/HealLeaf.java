package com.sudo.v3.spectre.bots.apexfungalmage.leafs;

import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexfungalmage.ApexFungalMage;
import com.sudo.v3.spectre.statics.UpdateUI;

public class HealLeaf extends LeafTask {
    private ApexFungalMage bot;
    private SpriteItem food;

    public HealLeaf(ApexFungalMage bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        food = Inventory.getItems(bot.foodName).first();

        if(food != null && food.isValid()){
            UpdateUI.currentTask("Attempting to eat " + bot.foodName, bot);
            food.interact("Eat");
        }else{
            UpdateUI.currentTask("Error finding " + bot.foodName + " within Inventory.", bot);
        }
    }
}
