package com.sudo.v3.spectre.bots.apexfungalmage.leafs;

import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexfungalmage.ApexFungalMage;
import com.sudo.v3.spectre.common.navigation.Traverse;

public class WebTraversalLeaf extends LeafTask {
    private ApexFungalMage bot;

    public WebTraversalLeaf (ApexFungalMage bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        Traverse.webPath(bot.nav, bot.traversalArea);
    }
}
