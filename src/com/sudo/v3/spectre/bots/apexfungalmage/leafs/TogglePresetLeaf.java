package com.sudo.v3.spectre.bots.apexfungalmage.leafs;

import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexfungalmage.ApexFungalMage;
import com.sudo.v3.spectre.statics.UpdateUI;

public class TogglePresetLeaf extends LeafTask {
    private ApexFungalMage bot;

    public TogglePresetLeaf(ApexFungalMage bot)
    {
        this.bot = bot;
    }

    @Override
    public void execute()
    {
        UpdateUI.currentTask("Loading bank preset.", bot);
        Bank.loadPreset(bot.presetNumber);
    }
}
