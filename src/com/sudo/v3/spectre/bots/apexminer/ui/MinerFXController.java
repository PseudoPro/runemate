package com.sudo.v3.spectre.bots.apexminer.ui;

import com.sudo.v3.spectre.bots.apexminer.ApexMiner;
import com.sudo.v3.spectre.bots.apexminer.enums.MineLocation;
import com.sudo.v3.spectre.bots.apexminer.enums.PlayStyle;
import com.sudo.v3.spectre.bots.apexminer.game.MineAreas;
import com.sudo.v3.spectre.statics.Areas;
import com.sudo.v3.ui.base.SudoFXController;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class MinerFXController extends SudoFXController implements Initializable {

    private final ApexMiner bot;

    @FXML
    private ListView<String> locationsListView, rockListView;

    @FXML
    private Button startButton;

    @FXML
    private CheckBox usePreset_CB, powerDrop_CB, mineRockertunitiesCheckBox;

    @FXML
    private ComboBox playstyleComboBox;

    @FXML
    private RadioButton one_Radio, two_Radio;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

        // Add Locations to the ListView
        rockListView.getItems().addAll("Copper rock", "Tin rock", "Iron rock", "Coal rock", "Mithril rock", "Adamantite rock", "Luminite rock", "Gold rock", "Runite rock", "Orichalcite rock", "Drakolith rock", "Phasmatite rock", "Banite rock", "Dark animica rock");

        playstyleComboBox.getItems().addAll("Active (Four-tick)", "Normal", "AFK");
        playstyleComboBox.getSelectionModel().select("Normal");

        locationsListView.setOnMouseClicked(getLocationClickAction());
        rockListView.setOnMouseClicked(getRockListClickAction());

        usePreset_CB.setOnAction(getUsePreset_CBAction());
        powerDrop_CB.setOnAction(powerDrop_CBAction());

        startButton.setOnAction(getStartButtonAction());
    }

    public MinerFXController(ApexMiner bot) {
        super(bot);
        this.bot = bot;
    }

    private EventHandler<MouseEvent> getLocationClickAction() {
        return event -> {
            startButton.setDisable(false);
        };
    }

    private EventHandler<MouseEvent> getRockListClickAction() {
        return event -> {
            locationsListView.getItems().clear();

            startButton.setDisable(true);
            try {
                switch (rockListView.getSelectionModel().getSelectedItem()) {
                    case "Copper rock":
                        locationsListView.getItems().add("Varrock East");
                        locationsListView.getItems().add("Varrock West");
                        break;
                    case "Tin rock":
                        locationsListView.getItems().add("Varrock West");
                        break;
                    case "Iron rock":
                        locationsListView.getItems().add("Varrock West");
                        break;
                    case "Coal rock":
                        locationsListView.getItems().add("Mining Guild");
                        break;
                    case "Silver rock":
                        locationsListView.getItems().add("Al Kharid Mine");
                        break;
                    case "Mithril rock":
                        locationsListView.getItems().add("Varrock East");
                        locationsListView.getItems().add("Varrock West");
                        break;
                    case "Adamantite rock":
                        locationsListView.getItems().add("Varrock East");
                        break;
                    case "Luminite rock":
                        locationsListView.getItems().add("Mining Guild Resource Dungeon (45dg)");
                        break;
                    case "Gold rock":
                        locationsListView.getItems().add("Al Kharid Mine");
                        break;
                    case "Runite rock":
                        locationsListView.getItems().add("Yanille");
                        locationsListView.getItems().add("Mining Guild");
                        break;
                    case "Orichalcite rock":
                        locationsListView.getItems().add("Mining Guild");
                        break;
                    case "Drakolith rock":
                        locationsListView.getItems().add("Mining Guild Resource Dungeon (45dg)");
                        break;
                    case "Phasmatite rock":
                        locationsListView.getItems().add("Port Phasmatys");
                        break;
                    case "Banite rock":
                        locationsListView.getItems().add("North Fremennik");
                        break;
                    case "Dark animica rock":
                        locationsListView.getItems().add("Empty Throne Room");
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
    }

    public EventHandler<ActionEvent> getStartButtonAction() {
        return event -> {

            String location = "", rock = "";

            try {
                location = locationsListView.getSelectionModel().getSelectedItem();
                rock = rockListView.getSelectionModel().getSelectedItem();
            } catch (Exception e) {
                System.out.println("Make sure you have both Fish and Location selected.");
            }

            if (location != null && rock != null) {
                try {
                    bot.locationNameString = location;
                    bot.rockName = rock;

                    switch (rock) {
                        case "Copper rock":
                            bot.oreName = "Copper ore";
                            switch (location) {
                                case "Varrock East":
                                    bot.mineLocation = MineLocation.GENERIC;
                                    bot.mineArea = MineAreas.VARROCK_SOUTH_EAST_MINE;
                                    bot.bankArea = Areas.VARROCK_EAST_BANK_AREA;
                                    break;
                                case "Varrock West":
                                    bot.mineLocation = MineLocation.GENERIC;
                                    bot.mineArea = MineAreas.VARROCK_SOUTH_WEST_MINE;
                                    bot.bankArea = Areas.VARROCK_RS3_WEST_BANK_AREA;
                                    break;
                            }
                            break;
                        case "Tin rock":
                            bot.oreName = "Tin ore";
                            switch (location) {
                                case "Varrock West":
                                    bot.mineLocation = MineLocation.GENERIC;
                                    bot.mineArea = MineAreas.VARROCK_SOUTH_WEST_MINE;
                                    bot.bankArea = Areas.VARROCK_RS3_WEST_BANK_AREA;
                                    break;
                            }
                            break;
                        case "Iron rock":
                            bot.oreName = "Iron ore";
                            switch (location) {
                                case "Varrock West":
                                    bot.mineLocation = MineLocation.GENERIC;
                                    bot.mineArea = MineAreas.VARROCK_SOUTH_WEST_MINE;
                                    bot.bankArea = Areas.VARROCK_RS3_WEST_BANK_AREA;
                                    break;
                            }
                            break;
                        case "Coal rock":
                            bot.oreName = "Coal";
                            switch (location) {
                                case "Mining Guild":
                                    bot.mineLocation = MineLocation.MINING_GUILD;
                                    bot.mineArea = MineAreas.MINING_GUILD;
                                    bot.bankArea = Areas.FALA_E_BANK_AREA;
                                    break;
                            }
                            break;
                        case "Silver rock":
                            bot.oreName = "Silver ore";
                            switch (location) {
                                case "Al Kharid Mine":
                                    bot.mineLocation = MineLocation.GENERIC;
                                    bot.mineArea = MineAreas.AL_KHARID_SOUTH_MINE;
                                    bot.bankArea = Areas.ALKHARID_DUEL_ARENA_RS3_BANK_AREA;
                                    break;
                            }
                            break;
                        case "Mithril rock":
                            bot.oreName = "Mithril ore";
                            switch (location) {
                                case "Varrock East":
                                    bot.mineLocation = MineLocation.GENERIC;
                                    bot.mineArea = MineAreas.VARROCK_SOUTH_EAST_MINE;
                                    bot.bankArea = Areas.VARROCK_EAST_BANK_AREA;
                                    break;
                                case "Varrock West":
                                    bot.mineLocation = MineLocation.GENERIC;
                                    bot.mineArea = MineAreas.VARROCK_SOUTH_WEST_MINE;
                                    bot.bankArea = Areas.VARROCK_RS3_WEST_BANK_AREA;
                                    break;
                            }
                            break;
                        case "Adamantite rock":
                            bot.oreName = "Adamantite ore";
                            switch (location) {
                                case "Varrock East":
                                    bot.mineLocation = MineLocation.GENERIC;
                                    bot.mineArea = MineAreas.VARROCK_SOUTH_EAST_MINE;
                                    bot.bankArea = Areas.VARROCK_EAST_BANK_AREA;
                                    break;
                            }
                            break;
                        case "Luminite rock":
                            bot.oreName = "Luminite ore";
                            switch (location) {
                                case "Mining Guild Resource Dungeon (45dg)":
                                    bot.mineLocation = MineLocation.MINING_GUILD_RESOURCE;
                                    bot.mineArea = MineAreas.MINING_GUILD_RESOURCE_DUNGEON;
                                    bot.bankArea = Areas.FALA_E_BANK_AREA;
                                    break;
                            }
                            break;
                        case "Gold rock":
                            bot.oreName = "Gold ore";
                            switch (location) {
                                case "Al Kharid Mine":
                                    bot.mineLocation = MineLocation.GENERIC;
                                    bot.mineArea = MineAreas.AL_KHARID_SOUTH_MINE;
                                    bot.bankArea = Areas.ALKHARID_DUEL_ARENA_TENT_RS3_BANK_AREA;
                                    break;
                            }
                            break;
                        case "Runite rock":
                            bot.oreName = "Runite ore";
                            switch (location) {
                                case "Yanille":
                                    bot.mineArea = Areas.YANILLE_RS3_MINE_AREA;
                                    bot.bankArea = Areas.YANILLE_RS3_BANK_AREA;
                                    break;
                                case "Mining Guild":
                                    bot.mineLocation = MineLocation.MINING_GUILD;
                                    bot.mineArea = MineAreas.MINING_GUILD;
                                    bot.bankArea = Areas.FALA_E_BANK_AREA;
                                    break;
                            }
                            break;
                        case "Orichalcite rock":
                            bot.oreName = "Orichalcite ore";
                            switch(location){
                                case "Mining Guild":
                                    bot.mineLocation = MineLocation.MINING_GUILD;
                                    bot.mineArea = MineAreas.MINING_GUILD;
                                    bot.bankArea = Areas.FALA_E_BANK_AREA;
                                    break;
                            }
                            break;
                        case "Drakolith rock":
                            bot.oreName = "Drakolith";
                            switch (location) {
                                case "Mining Guild Resource Dungeon (45dg)":
                                    bot.mineLocation = MineLocation.MINING_GUILD_RESOURCE;
                                    bot.mineArea = MineAreas.MINING_GUILD_RESOURCE_DUNGEON;
                                    bot.bankArea = Areas.FALA_E_BANK_AREA;
                                    break;
                            }
                            break;
                        case "Phasmatite rock":
                            bot.oreName = "Phasmatite";
                            switch (location) {
                                case "Port Phasmatys":
                                    bot.mineLocation = MineLocation.GENERIC;
                                    bot.mineArea = MineAreas.PORT_PHASMATYS_PHASMATITE;
                                    bot.bankArea = Areas.CANIFIS_BANK_AREA;
                                    break;
                            }
                            break;
                        case "Banite rock":
                            bot.oreName = "Banite ore";
                            switch (location) {
                                case "North Fremennik":
                                    bot.mineLocation = MineLocation.FREM_BANE;
                                    bot.mineArea = MineAreas.FREM_BANE_ROCK_AREA;
                                    bot.bankArea = Areas.BURTHORPE_RS3_BANK_AREA;
                                    break;
                            }
                            break;
                        case "Dark animica rock":
                            bot.oreName = "Dark animica";
                            switch (location) {
                                case "Empty Throne Room":
                                    bot.mineLocation = MineLocation.EMPTY_THRONE_ROOM;
                                    bot.mineArea = MineAreas.EMPTY_THRONE_ROOM_DARK_ANIMICA_ROCK;
                                    bot.bankArea = Areas.VARROCK_EAST_BANK_AREA;
                                    break;
                            }
                            break;
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                super.getStart_BTEvent();

                bot.mineRockertunity = mineRockertunitiesCheckBox.isSelected();

                if (bot.usingPreset = (usePreset_CB.isSelected() && !usePreset_CB.isDisabled()))
                    if (one_Radio.isSelected())
                        bot.presetNumber = 1;
                    else
                        bot.presetNumber = 2;

                switch (playstyleComboBox.getSelectionModel().getSelectedItem().toString()){
                    case "Active (Four-tick)":
                        bot.playStyle = PlayStyle.ACTIVE;
                        break;
                    case "Normal":
                        bot.playStyle = PlayStyle.NORMAL;
                        break;
                    case "AFK":
                        bot.playStyle = PlayStyle.AFK;
                }

                startButton.textProperty().set("Resume");
                bot.guiWait = false;
            }
        };
    }

    private EventHandler<ActionEvent> getUsePreset_CBAction() {
        return event -> {

            one_Radio.setDisable(!usePreset_CB.isSelected());
            two_Radio.setDisable(!usePreset_CB.isSelected());

            powerDrop_CB.setSelected(false);
        };
    }


    private EventHandler<ActionEvent> powerDrop_CBAction() {
        return event -> {
            usePreset_CB.setSelected(false);
            one_Radio.setDisable(!usePreset_CB.isSelected());
            two_Radio.setDisable(!usePreset_CB.isSelected());
        };
    }
}
