package com.sudo.v3.spectre.bots.apexminer.branches;

import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexminer.ApexMiner;
import com.sudo.v3.spectre.statics.UpdateUI;

public class IsOreBoxFull extends SudoBranchTask {
    private ApexMiner bot;

    public IsOreBoxFull (ApexMiner bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        return bot.isBankOpen;
    }

    @Override
    public boolean validate() {
        validate = bot.oreBoxFull && Inventory.isFull();
        UpdateUI.debug("IsOreBotFullBranch: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        return bot.fillOreBoxLeaf;
    }
}
