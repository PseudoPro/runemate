package com.sudo.v3.spectre.bots.apexminer.branches;

import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexminer.ApexMiner;
import com.sudo.v3.spectre.statics.UpdateUI;

public class ShouldEquipPorter extends SudoBranchTask {
    private ApexMiner bot;

    public ShouldEquipPorter(ApexMiner bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        return bot.equipPorterLeaf;
    }

    @Override
    public boolean validate() {
        validate = Inventory.contains(bot.porterPattern) && Equipment.getItemIn(Equipment.Slot.POCKET) == null;
        UpdateUI.debug("ShouldEquipPorterBranch: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        bot.mineRockLeaf.setObject(bot.rockObj);
        return bot.mineRockLeaf;
    }
}
