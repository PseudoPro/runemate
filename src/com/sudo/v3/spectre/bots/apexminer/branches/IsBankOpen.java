package com.sudo.v3.spectre.bots.apexminer.branches;

import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexminer.ApexMiner;
import com.sudo.v3.spectre.statics.UpdateUI;

public class IsBankOpen extends SudoBranchTask {
    private ApexMiner bot;

    public IsBankOpen(ApexMiner bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        return bot.depositInventoryLeaf;
    }

    @Override
    public boolean validate() {
        validate = Bank.isOpen();
        UpdateUI.debug("IsBankOpenBranch: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        return bot.isInBankArea;
    }
}
