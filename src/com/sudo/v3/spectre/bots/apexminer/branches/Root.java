package com.sudo.v3.spectre.bots.apexminer.branches;

import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.RuneScape;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.local.Skill;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoRootTask;
import com.sudo.v3.spectre.api.util.SudoTimer;
import com.sudo.v3.spectre.bots.apexminer.ApexMiner;
import com.sudo.v3.spectre.bots.apexminer.enums.TraversalLocation;
import com.sudo.v3.spectre.common.leafs.InteractLeaf;
import com.sudo.v3.spectre.common.leafs.PowerDropLeaf;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.sudo.v3.ui.model.XPInfo;

public class Root extends SudoRootTask {
    private ApexMiner bot;

    public Root(ApexMiner bot) {
        super(bot);
        this.bot = bot;
    }

    private void resetStaminaTimer(){
        bot.staminaTimer.reset();
    }

    @Override
    public TreeTask rootTask() {

        if (bot.firstLogin) {
            if (RuneScape.isLoggedIn()) {
                // Add Skills to track to the HashMap
                bot.XPInfoMap.put(Skill.MINING, new XPInfo(Skill.MINING));

                bot.firstLogin = false;

                bot.updateSession(0.0);

                bot.powerDropLeaf = new PowerDropLeaf(bot) {
                    @Override
                    public void successAction() {
                        bot.inventoryCheckCount = 0;
                    }
                };

                bot.powerDropLeaf.setValues("ore", 6, 26);

                bot.checkJuJuTimer.reset();
            }
        }

        if(bot.guiResume){
            bot.staminaTimer = new SudoTimer(bot.playStyle.getMinTime(), bot.playStyle.getMaxTime());
            bot.staminaTimer.reset();

            UpdateUI.debug("Mining Level: " + Skill.MINING.getCurrentLevel());
            UpdateUI.debug("Location: " + bot.locationNameString);
            UpdateUI.debug("Rock: " + bot.rockName);
            UpdateUI.debug("PowerDropping: " + bot.isPowerDropping);
            UpdateUI.debug("UsingPreset: " + bot.usingPreset);
            UpdateUI.debug("Playstyle: " + bot.playStyle.name());
            UpdateUI.debug("Mining Rockertunity: " + bot.mineRockertunity);
            if (bot.usingPreset)
                UpdateUI.debug("Preset #: " + bot.presetNumber);

            bot.mineRockLeaf = new InteractLeaf<GameObject>(bot, bot.rockObj, bot.rockName, bot.mineAction){
                @Override
                public void successAction(){
                    resetStaminaTimer();
                    Execution.delay(2000, 4000);
                }
            };

            bot.guiResume = false;
        }

        if (bot.currentlyBreaking) {
            if (RuneScape.isLoggedIn()) {
                UpdateUI.currentTask("Attempting to log off.", bot);
                RuneScape.logout(true);
            }
            return bot.emptyLeaf;
        }

        if (!bot.isPremiumBot || bot.getMetaData().getHourlyPrice().doubleValue() > 0 || bot.isValidSession() || Environment.isSDK()) {
            if (bot.inventoryCheckCount == 0) {
                bot.inventoryCheckCount = Random.nextInt(8, 28);
            }

            if(bot.isBanking){
                UpdateUI.debug("Root: IsBanking: " + bot.isBanking);
                return bot.isBankOpen;
            }

            bot.oreBox = Inventory.newQuery().names(bot.oreBoxPattern).results().first();
            validate = bot.oreBox != null;
            UpdateUI.debug("Root: InventoryHasOreBox: " + validate);
            if (validate) {
                validate = bot.oreBoxFull ? Inventory.isFull() : (Inventory.getItems().size() >= bot.inventoryCheckCount && !Inventory.contains(bot.porterPattern));
                UpdateUI.debug("Root: IsInventoryNearFull: " + validate);
                if (validate) {
                    return bot.isOreBoxFull;
                } else {
                    return bot.isRockNearby;
                }
            } else {
                validate = Inventory.isFull();
                UpdateUI.debug("Root: IsInventoryFull: " + validate);
                if (validate) {
                    bot.isBanking = true;
                    bot.traversalLocation = TraversalLocation.BANK_AREA;
                    return bot.traversalLeaf;
                } else {
                    return bot.isRockNearby;
                }
            }
        } else {
            return bot.liteVersionExpireLeaf;
        }
    }
}
