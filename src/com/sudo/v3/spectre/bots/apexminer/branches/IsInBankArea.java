package com.sudo.v3.spectre.bots.apexminer.branches;

import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.apexminer.ApexMiner;
import com.sudo.v3.spectre.bots.apexminer.enums.TraversalLocation;
import com.sudo.v3.spectre.statics.UpdateUI;

public class IsInBankArea extends SudoBranchTask {
    private ApexMiner bot;

    public IsInBankArea(ApexMiner bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        return bot.openBankLeaf;
    }

    @Override
    public boolean validate() {
        validate = bot.bankArea.contains(bot.player) || bot.bankArea.distanceTo(bot.player) < 12;
        UpdateUI.debug("IsInBankArea: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        bot.isBanking = true;
        bot.traversalLocation = TraversalLocation.BANK_AREA;
        return bot.traversalLeaf;
    }
}
