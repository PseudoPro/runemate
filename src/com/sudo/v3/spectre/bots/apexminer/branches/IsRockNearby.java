package com.sudo.v3.spectre.bots.apexminer.branches;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.api.game.hud.SudoCamera;
import com.sudo.v3.spectre.bots.apexminer.ApexMiner;
import com.sudo.v3.spectre.bots.apexminer.enums.TraversalLocation;
import com.sudo.v3.spectre.statics.UpdateUI;

import java.util.Optional;

public class IsRockNearby extends SudoBranchTask {
    private ApexMiner bot;
    private Optional<GameObject> rockertunityRockObj;

    public IsRockNearby(ApexMiner bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        return bot.isPlayerMining;
    }

    @Override
    public boolean validate() {
        validate = false;
        // Check for glowing rock
        //UpdateUI.debug("RockName: " + bot.rockName);
        //UpdateUI.debug("MineAction: " + bot.mineAction);
        //UpdateUI.debug("mineArea: " + bot.mineArea);

        bot.rockQueryResults = GameObjects.newQuery().names(bot.rockName).actions(bot.mineAction).within(bot.mineArea).results();

        if(bot.mineRockertunity) {
            rockertunityRockObj = bot.rockQueryResults.stream().filter(i -> i.getSpotAnimationIds().contains(bot.rockertunityID)).findFirst();
            if (rockertunityRockObj != null && rockertunityRockObj.isPresent()) {
                bot.rockObj = rockertunityRockObj.get();
                validate = bot.rockObj != null && bot.rockObj.isValid();
                SudoCamera.ConcurrentlyTurnToWithYaw(bot.rockObj);
                UpdateUI.debug("IsRockertunityNearby: " + validate);
                if(validate)
                    UpdateUI.currentTask("Rockertunity discovered.", bot);
            }
        }
        if(!validate) {
            bot.rockObj = bot.rockQueryResults.nearest();
            validate = bot.mineArea.contains(bot.player) || bot.rockObj != null && bot.rockObj.isValid() && bot.rockObj.distanceTo(bot.player) < 12;
            UpdateUI.debug("IsRockNearby: " + validate);
        }

        return validate;
    }

    @Override
    public TreeTask failureTask() {
        bot.traversalLocation = TraversalLocation.MINE_AREA;
        return bot.traversalLeaf;
    }
}
