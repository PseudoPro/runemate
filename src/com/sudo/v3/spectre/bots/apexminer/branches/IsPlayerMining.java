package com.sudo.v3.spectre.bots.apexminer.branches;

import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v2.spectre.bots.sudoessence.assets.Animations;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.api.game.entities.ApexPlayer;
import com.sudo.v3.spectre.bots.apexminer.ApexMiner;
import com.sudo.v3.spectre.statics.UpdateUI;

public class IsPlayerMining extends SudoBranchTask {
    private ApexMiner bot;

    public IsPlayerMining(ApexMiner bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        return bot.isMiningLeaf;
    }

    @Override
    public boolean validate() {
        validate = ApexPlayer.isInAnimation(Animations.MINING_ANIM) && !bot.staminaTimer.hasExpired();
        UpdateUI.debug("IsPlayerMining: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        return bot.shouldEquipPorter;
    }
}
