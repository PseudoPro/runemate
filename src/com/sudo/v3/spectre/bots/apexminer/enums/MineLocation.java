package com.sudo.v3.spectre.bots.apexminer.enums;

public enum MineLocation {
    GENERIC,
    MINING_GUILD,
    MINING_GUILD_RESOURCE,
    PORT_PHASMATYS,
    FREM_BANE,
    EMPTY_THRONE_ROOM
}
