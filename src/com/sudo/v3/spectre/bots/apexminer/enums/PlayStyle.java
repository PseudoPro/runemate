package com.sudo.v3.spectre.bots.apexminer.enums;

public enum PlayStyle {
    ACTIVE,
    NORMAL,
    AFK;

    private int minTime, maxTime;

    static {
        ACTIVE.minTime = 800;
        ACTIVE.maxTime = 3000;

        NORMAL.minTime = 6000;
        NORMAL.maxTime = 37500;

        AFK.minTime = 8000;
        AFK.maxTime = 180000;
    }

    public int getMinTime(){
        return minTime;
    }

    public int getMaxTime(){
        return maxTime;
    }
}
