package com.sudo.v3.spectre.bots.apexminer.enums;

public enum TraversalLocation {
    BANK_AREA,
    MINE_AREA
}
