package com.sudo.v3.spectre.bots.apexminer;

import com.runemate.game.api.hybrid.RuneScape;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.definitions.ItemDefinition;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.DepositBox;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.queries.results.LocatableEntityQueryResults;
import com.runemate.game.api.hybrid.util.Regex;
import com.runemate.game.api.hybrid.util.calculations.CommonMath;
import com.runemate.game.api.script.framework.listeners.ChatboxListener;
import com.runemate.game.api.script.framework.listeners.events.ItemEvent;
import com.runemate.game.api.script.framework.listeners.events.MessageEvent;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBot;
import com.sudo.v3.spectre.api.util.SudoTimer;
import com.sudo.v3.spectre.bots.apexminer.branches.*;
import com.sudo.v3.spectre.bots.apexminer.enums.MineLocation;
import com.sudo.v3.spectre.bots.apexminer.enums.PlayStyle;
import com.sudo.v3.spectre.bots.apexminer.enums.TraversalLocation;
import com.sudo.v3.spectre.bots.apexminer.leafs.*;
import com.sudo.v3.spectre.bots.apexminer.ui.MineInfoUI;
import com.sudo.v3.spectre.bots.apexminer.ui.MinerFXGui;
import com.sudo.v3.spectre.common.leafs.InteractLeaf;
import com.sudo.v3.spectre.statics.Areas;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.sudo.v3.ui.Info;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class ApexMiner extends SudoBot implements ChatboxListener {

    private MineInfoUI infoUI;
    private MinerFXGui configUI;

    public Area bankArea = Areas.YANILLE_RS3_BANK_AREA, mineArea = Areas.YANILLE_RS3_MINE_AREA;

    public String mineAction = "Mine", locationNameString = "", rockName = "", oreName = "";
    public boolean usingPreset = false, isPowerDropping = false, oreBoxFull = false, isBanking = false, mineRockertunity = true;
    public int oreCount = 0, presetNumber = 1, inventoryCheckCount = 0, rockertunityID = 32534;

    public SudoTimer staminaTimer = new SudoTimer(5000, 90000),
        ignoreItemRemovalTimer = new SudoTimer(10000, 10000),
        checkJuJuTimer = new SudoTimer(300000, 300000);
    public TraversalLocation traversalLocation = TraversalLocation.MINE_AREA;
    public PlayStyle playStyle = PlayStyle.NORMAL;
    public MineLocation mineLocation = MineLocation.GENERIC;

    public Pattern oreBoxPattern = Regex.getPatternForContainsString("ore box"),
            geodePattern = Regex.getPatternForContainsString("geode"),
            porterPattern = Regex.getPatternForContainsString("of the porter"),
            stoneSpiriPattern = Regex.getPatternForContainsString("stone spirit"),
            oreBoxStoneSpiritPattern = Regex.getPatternContainingOneOf("ore box", "stone spirit");
    public SpriteItem oreBox;

    public LocatableEntityQueryResults<GameObject> rockQueryResults;
    public GameObject rockObj;

    // [1485, 55] for JuJu timer reading

    // Branches
    public IsOreBoxFull isOreBoxFull = new IsOreBoxFull(this);
    public IsPlayerMining isPlayerMining = new IsPlayerMining(this);
    public IsRockNearby isRockNearby = new IsRockNearby(this);
    public IsBankOpen isBankOpen = new IsBankOpen(this);
    public IsInBankArea isInBankArea = new IsInBankArea(this);
    public ShouldEquipPorter shouldEquipPorter = new ShouldEquipPorter(this);

    // Leafs
    public TraversalLeaf traversalLeaf = new TraversalLeaf(this);
    public FillOreBoxLeaf fillOreBoxLeaf = new FillOreBoxLeaf(this);
    public IsMiningLeaf isMiningLeaf = new IsMiningLeaf(this);
    public DepositInventoryLeaf depositInventoryLeaf = new DepositInventoryLeaf(this);
    public EquipPorterLeaf equipPorterLeaf = new EquipPorterLeaf(this);
    public InteractLeaf<GameObject> mineRockLeaf;

    // Default Constructor
    public ApexMiner() {
        // Give spectre client this class as the embeddableUI
        setEmbeddableUI(this);
    }

    @Override
    public void onStart(String... args) {
        super.onStart();
        getEventDispatcher().addListener(this);
        isPremiumBot = false;
    }

    @Override
    public String getBotName() {
        return "ApexMiner";
    }

    @Override
    public ObjectProperty<? extends Node> botInterfaceProperty() {
        if (botInterfaceProperty == null) {
            botInterfaceProperty = new SimpleObjectProperty<>(configUI = new MinerFXGui(this));
            guiList = new ArrayList<>();
            guiList.add(configUI);
            guiList.add(infoUI = new MineInfoUI(this));

        }
        return botInterfaceProperty;
    }

    @Override
    public TreeTask createRootTask() {
        return new Root(this);
    }


    @Override
    public void onItemAdded(ItemEvent event) {
        if (RuneScape.isLoggedIn() && player != null && !Bank.isOpen() && !bankArea.contains(player))
            super.onItemAdded(event);

        if (event != null) {
            ItemDefinition definition = event.getItem().getDefinition();

            if (definition != null && definition.getName().contains(oreName)) {
                oreCount += event.getQuantityChange();
            }
        }
    }

    @Override
    public void onItemRemoved(ItemEvent event) {
        if (RuneScape.isLoggedIn() && player != null && !Bank.isOpen() && !DepositBox.isOpen() && !bankArea.contains(player) && ignoreItemRemovalTimer.hasExpired())
            super.onItemRemoved(event);
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        String message = messageEvent.getMessage();

        if (message.toLowerCase().contains("not able to deposit anything in your backpack into your ore box")) {
            UpdateUI.debug("Ore Box 'Full' message received.");
            oreBoxFull = true;
        }

        if (message.toLowerCase().contains("add to your ore box:")) {
            oreBoxFull = false;
        }
    }

    @Override
    public void updateInfo() {
        // UpdateUI all current values in the XPInfo List
        XPInfoMap.values().forEach((xpInfo) -> xpInfo.update());

        displayInfoMap.put("Ores Mined: ", Integer.toString(oreCount) +
                " (" + Integer.toString((int) CommonMath.rate(TimeUnit.HOURS, STOPWATCH.getRuntime(), oreCount)) + "/Hour)");
        displayInfoMap.put("GP: ", Integer.toString(grossIncome) +
                " (" + Integer.toString((int) CommonMath.rate(TimeUnit.HOURS, STOPWATCH.getRuntime(), grossIncome)) + "/Hour)");

        try {
            info = new Info(XPInfoMap,           // XP Info
                    displayInfoMap,
                    STOPWATCH.getRuntimeAsString(),        // Total Runtime
                    currentTaskString,                     // Current Task
                    abTaskString);                         // antiban Task

        } catch (Exception e) {
            e.printStackTrace();
        }

        Platform.runLater(() -> infoUI.update());
    }
}
