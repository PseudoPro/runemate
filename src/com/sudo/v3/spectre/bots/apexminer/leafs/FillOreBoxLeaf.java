package com.sudo.v3.spectre.bots.apexminer.leafs;

import com.runemate.game.api.rs3.local.hud.interfaces.eoc.ActionBar;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexminer.ApexMiner;
import com.sudo.v3.spectre.statics.UpdateUI;

public class FillOreBoxLeaf extends LeafTask {
    private ApexMiner bot;
    private ActionBar.Slot oreBoxActionBar;

    public FillOreBoxLeaf(ApexMiner bot) {
        this.bot = bot;
    }

    @Override
    public void execute() {
        bot.ignoreItemRemovalTimer.reset();
        if ((oreBoxActionBar = ActionBar.newQuery().names(bot.oreBoxPattern).activatable(true).results().first()) != null) {
            UpdateUI.currentTask("Filling Ore Box from Action Bar.", bot);
            if(oreBoxActionBar.activate()){
                bot.inventoryCheckCount = 0;
                Execution.delay(500, 1000);
            }
        } else {
            UpdateUI.currentTask("Filling Ore Box from Inventory.", bot);
            if (bot.oreBox.interact("Fill")) {
                bot.inventoryCheckCount = 0;
                Execution.delay(500, 1000);
            }
        }
    }
}