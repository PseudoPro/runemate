package com.sudo.v3.spectre.bots.apexminer.leafs;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.rs3.local.hud.interfaces.Lodestone;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.api.game.hud.SudoCamera;
import com.sudo.v3.spectre.bots.apexminer.ApexMiner;
import com.sudo.v3.spectre.bots.apexminer.enums.MineLocation;
import com.sudo.v3.spectre.bots.apexminer.enums.TraversalLocation;
import com.sudo.v3.spectre.bots.apexminer.game.MineAreas;
import com.sudo.v3.spectre.common.navigation.Traverse;
import com.sudo.v3.spectre.statics.Areas;
import com.sudo.v3.spectre.statics.UpdateUI;

public class TraversalLeaf extends LeafTask {
    private ApexMiner bot;
    private GameObject obj;

    public TraversalLeaf(ApexMiner bot) {
        this.bot = bot;
    }

    @Override
    public void execute() {
        if (bot.traversalLocation == TraversalLocation.BANK_AREA) {
            UpdateUI.currentTask("Traversing to bank area.", bot);
            if (bot.mineLocation == MineLocation.GENERIC) {
                Traverse.webPath(bot.nav, bot.bankArea);
            } else if (bot.mineLocation == MineLocation.MINING_GUILD_RESOURCE || bot.mineLocation == MineLocation.MINING_GUILD) {
                if (MineAreas.MINING_GUILD_RESOURCE_DUNGEON.contains(bot.player)) {
                    // Interact with resource dungeon door
                    UpdateUI.debug("In Resource Dungeon, attempting to exit");
                    interactWith("Mysterious door", "Exit", MineAreas.MINING_GUILD_RESOURCE_DUNGEON);
                } else if (MineAreas.MINING_GUILD_LADDER_CLIMB_UP.contains(bot.player)) {
                    // Interact with stairs and climb up
                    UpdateUI.debug("In Mining Guild, attempting to exit");
                    interactWith("Ladder", "Climb-up", MineAreas.MINING_GUILD);
                } else if (MineAreas.MINING_GUILD.contains(bot.player)) {
                    UpdateUI.debug("In Mining Guild, running to exit");
                    Traverse.smartRegionPath(bot.nav, MineAreas.MINING_GUILD_LADDER_CLIMB_UP);
                } else {
                    Traverse.smartRegionPath(bot.nav, bot.bankArea);
                }
            } else if (bot.mineLocation == MineLocation.FREM_BANE){
                if (Areas.BURTHORPE_RS3_BANK_AREA.distanceTo(bot.player) > 100) {
                    Lodestone.BURTHORPE.teleport();
                } else {
                    Traverse.smartRegionPath(bot.nav, Areas.BURTHORPE_RS3_BANK_AREA);
                }
            } else if (bot.mineLocation == MineLocation.EMPTY_THRONE_ROOM){
                if(Areas.VARROCK_EAST_BANK_AREA.distanceTo(bot.player) > 100){
                    Lodestone.VARROCK.teleport();
                } else {
                    Traverse.smartRegionPath(bot.nav, Areas.VARROCK_EAST_BANK_AREA);
                }
            }
        } else if (bot.traversalLocation == TraversalLocation.MINE_AREA) {
            UpdateUI.currentTask("Traversing to mine area.", bot);
            if (bot.mineLocation == MineLocation.GENERIC) {
                Traverse.webPath(bot.nav, bot.mineArea);
            } else if (bot.mineLocation == MineLocation.MINING_GUILD || bot.mineLocation == MineLocation.MINING_GUILD_RESOURCE) {
                if (bot.mineLocation == MineLocation.MINING_GUILD_RESOURCE && MineAreas.MINING_GUILD.contains(bot.player)) {
                    interactWith("Mysterious entrance", "Enter", MineAreas.MINING_GUILD_RESOURCE_DUNGEON_ENTRANCE);
                } else if (MineAreas.MINING_GUILD_LADDER_CLIMB_DOWN.contains(bot.player)) {
                    interactWith("Ladder", "Climb-down", MineAreas.MINING_GUILD_LADDER_CLIMB_DOWN);
                } else if (MineAreas.MINING_GUILD_LADDER_CLIMB_UP.contains(bot.player)) {
                    Traverse.smartRegionPath(bot.nav, new Area.Circular(MineAreas.MINING_GUILD.getCenter(), 10));
                } else {
                    Traverse.smartRegionPath(bot.nav, MineAreas.MINING_GUILD_LADDER_CLIMB_DOWN);
                }
            } else if (bot.mineLocation == MineLocation.FREM_BANE){
                if(MineAreas.FREM_BANE_FULL_AREA.contains(bot.player)){
                    Traverse.smartRegionPath(bot.nav, MineAreas.FREM_BANE_ROCK_AREA);
                } else if (MineAreas.FREM_LADDER_CLIMB_UP.contains(bot.player)){
                    interactWith("Ladder", "Climb up", MineAreas.FREM_LADDER_CLIMB_UP);
                } else if (MineAreas.FREM_HUNT_SNOW_AREA.contains(bot.player)){
                    Traverse.smartRegionPath(bot.nav, MineAreas.FREM_LADDER_CLIMB_UP);
                } else if (MineAreas.FREM_STEPS_ASCEND.contains(bot.player)){
                    interactWith("Steps", "Ascend", MineAreas.FREM_STEPS_ASCEND);
                } else if (MineAreas.FREM_STEPS_ASCEND.distanceTo(bot.player) > 200) {
                    Lodestone.FREMENNIK_PROVINCE.teleport();
                } else {
                    Traverse.webPath(bot.nav, MineAreas.FREM_STEPS_ASCEND);
                }
            } else if (bot.mineLocation == MineLocation.EMPTY_THRONE_ROOM){
                if (MineAreas.EMPTY_THRONE_ROOM_AREA.contains(bot.player)){
                    Traverse.smartRegionPath(bot.nav, MineAreas.EMPTY_THRONE_ROOM_DARK_ANIMICA_ROCK);
                } else if (MineAreas.EMPTY_THRONE_ANCIENT_DOORS_ENTER.contains(bot.player)){
                    interactWith("Ancient doors", "Enter", MineAreas.EMPTY_THRONE_ANCIENT_DOORS_ENTER);
                } else {
                    Traverse.webPath(bot.nav, MineAreas.EMPTY_THRONE_ANCIENT_DOORS_ENTER);
                }
            }
        }
    }

    private void interactWith(String name, String action, Area area) {
        obj = GameObjects.newQuery().names(name).actions(action).within(area).results().nearest();
        Area objArea;
        Coordinate objCoord;

        if (obj != null && obj.isValid()) {
            if (obj.distanceTo(bot.player) > 4 || obj.getVisibility() < 0.8) {
                SudoCamera.ConcurrentlyTurnToWithYaw(obj);
            }

            if (obj.distanceTo(bot.player) > 10 && (objArea = obj.getArea()) != null && !objArea.getCoordinates().isEmpty() && (objCoord = objArea.getRandomCoordinate()) != null) {
                Traverse.smartRegionPath(bot.nav, new Area.Circular(objCoord, 5));
            } else {

                if (obj.interact(action, name)) {
                    Execution.delay(100, 2500);
                } else if (obj.interact(action)) {
                    Execution.delay(100, 2500);
                }
            }
        }
    }
}