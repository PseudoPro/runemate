package com.sudo.v3.spectre.bots.apexminer.leafs;

import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexminer.ApexMiner;
import com.sudo.v3.spectre.statics.UpdateUI;

public class IsMiningLeaf extends LeafTask {
    private ApexMiner bot;

    public IsMiningLeaf(ApexMiner bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        UpdateUI.currentTask("Currently mining. Breaking with " + bot.playStyle.name().toLowerCase() + " playstyle. (" + bot.staminaTimer.getSessionMaxTime() + "ms delay)", bot);
    }
}
