package com.sudo.v3.spectre.bots.apexminer.leafs;

import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexminer.ApexMiner;
import com.sudo.v3.spectre.statics.UpdateUI;


public class DepositInventoryLeaf extends LeafTask {
    private ApexMiner bot;

    public DepositInventoryLeaf(ApexMiner bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        if (bot.usingPreset) {
            UpdateUI.currentTask("Loading bank preset.", bot);
            if(Bank.loadPreset(bot.presetNumber))
                bot.isBanking = false;
        }else if(Inventory.contains(bot.oreName)){
            UpdateUI.currentTask("Depositing " + bot.oreName + " from inventory.", bot);
            Bank.deposit(bot.oreName, 0);
        } else if(Inventory.contains(bot.geodePattern)) {
            UpdateUI.currentTask("Depositing geodes from inventory.", bot);
            Bank.deposit(bot.geodePattern, 0);
        } else if(Inventory.containsAnyExcept(bot.oreBoxStoneSpiritPattern)){
            UpdateUI.currentTask("Depositing Misc Items from inventory.", bot);
            Bank.depositAllExcept(bot.oreBoxStoneSpiritPattern);
        } else if(Inventory.contains(bot.oreBoxPattern) && bot.oreBoxFull){
            UpdateUI.currentTask("Attempting to empty ore box.", bot);
            bot.oreBox = Inventory.newQuery().names(bot.oreBoxPattern).actions("Empty").results().first();
            if(bot.oreBox != null && bot.oreBox.isValid()){
                if(bot.oreBox.interact("Empty")){
                    bot.oreBoxFull = false;
                }
            }
        } else {
            bot.isBanking = false;
        }
    }
}
