package com.sudo.v3.spectre.bots.apexminer.leafs;

import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.rs3.local.hud.interfaces.eoc.ActionBar;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.apexminer.ApexMiner;
import com.sudo.v3.spectre.statics.UpdateUI;

public class EquipPorterLeaf extends LeafTask {
    private ApexMiner bot;
    private ActionBar.Slot porterActionBar;
    private SpriteItem porterItem;
    
    public EquipPorterLeaf (ApexMiner bot){
        this.bot = bot;
    }
    
    @Override
    public void execute() {
        bot.ignoreItemRemovalTimer.reset();
        if ((porterActionBar = ActionBar.newQuery().names(bot.porterPattern).activatable(true).results().first()) != null) {
            UpdateUI.currentTask("Equipping Porter from Action Bar.", bot);
            if(porterActionBar.activate()){
                Execution.delay(500, 1000);
            }
        } else {
            UpdateUI.currentTask("Equipping Porter from Inventory.", bot);
            porterItem = Inventory.getItems(bot.porterPattern).first();
            if (porterItem != null && porterItem.interact("Wear")) {
                Execution.delay(500, 1000);
            }
        }
    }
}
