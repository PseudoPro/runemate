package com.sudo.v3.spectre.bots.apexminer.game;

import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;

public class MineAreas {

    public static Area MINING_GUILD = new Area.Rectangular(new Coordinate(3066, 9756, 0), new Coordinate(2985, 9690, 0));
    public static Area MINING_GUILD_RESOURCE_DUNGEON = new Area.Rectangular(new Coordinate(1040, 4525, 0), new Coordinate(1065, 4500, 0));
    public static Area MINING_GUILD_RESOURCE_DUNGEON_ENTRANCE = new Area.Rectangular(new Coordinate(3020, 9740, 0), new Coordinate(3024, 9748, 0));
    public static Area MINING_GUILD_LADDER_CLIMB_UP = new Area.Rectangular(new Coordinate(3021, 9741, 0), new Coordinate(3016, 9736, 0));
    public static Area MINING_GUILD_LADDER_CLIMB_DOWN = new Area.Rectangular(new Coordinate(3016, 3342, 0), new Coordinate(3023, 3336, 0));

    public static Area PORT_PHASMATYS_PHASMATITE = new Area.Rectangular(new Coordinate(3685, 3402, 0), new Coordinate(3691, 3393, 0));

    public static Area FREM_BANE_ROCK_AREA = new Area.Rectangular(new Coordinate( 2721, 3869, 0), new Coordinate( 2711, 3879, 0));
    public static Area FREM_BANE_FULL_AREA = new Area.Rectangular(new Coordinate( 2710, 3885, 0), new Coordinate( 2743, 3839, 0));
    public static Area FREM_LADDER_CLIMB_UP = new Area.Rectangular(new Coordinate( 2737, 3839, 1), new Coordinate( 2742, 3831, 1));
    public static Area FREM_HUNT_SNOW_AREA = new Area.Rectangular(new Coordinate( 2697, 3839, 1), new Coordinate( 2746, 3801, 1));
    public static Area FREM_STEPS_ASCEND = new Area.Rectangular(new Coordinate(2724, 3794, 0), new Coordinate(2729, 3808, 0));

    public static Area EMPTY_THRONE_ROOM_DARK_ANIMICA_ROCK = new Area.Rectangular( new Coordinate( 2870, 12648, 2), new Coordinate( 2879, 12627, 2));
    public static Area EMPTY_THRONE_ROOM_AREA = new Area.Rectangular(new Coordinate( 2816, 12666, 2), new Coordinate( 2879, 12609, 2));
    public static Area EMPTY_THRONE_ANCIENT_DOORS_ENTER = new Area.Rectangular(new Coordinate( 3380, 3404, 0), new Coordinate( 3395, 3398, 0));

    public static Area VARROCK_SOUTH_WEST_MINE = new Area.Polygonal(
            new Coordinate(3180, 3381, 0),
            new Coordinate(3179, 3374, 0),
            new Coordinate(3171, 3366, 0),
            new Coordinate(3171, 3362, 0),
            new Coordinate(3179, 3362, 0),
            new Coordinate(3188, 3370, 0),
            new Coordinate(3187, 3381, 0)
    );

    public static Area VARROCK_SOUTH_EAST_MINE = new Area.Polygonal(
            new Coordinate(3279, 3372, 0),
            new Coordinate(3281, 3370, 0),
            new Coordinate(3281, 3366, 0),
            new Coordinate(3278, 3363, 0),
            new Coordinate(3278, 3360, 0),
            new Coordinate(3279, 3359, 0),
            new Coordinate(3283, 3359, 0),
            new Coordinate(3284, 3360, 0),
            new Coordinate(3287, 3360, 0),
            new Coordinate(3291, 3356, 0),
            new Coordinate(3293, 3356, 0),
            new Coordinate(3294, 3357, 0),
            new Coordinate(3295, 3357, 0),
            new Coordinate(3295, 3360, 0),
            new Coordinate(3293, 3362, 0),
            new Coordinate(3293, 3363, 0),
            new Coordinate(3292, 3364, 0),
            new Coordinate(3292, 3365, 0),
            new Coordinate(3291, 3366, 0),
            new Coordinate(3291, 3371, 0),
            new Coordinate(3292, 3372, 0),
            new Coordinate(3297, 3374, 0),
            new Coordinate(3288, 3376, 0),
            new Coordinate(3286, 3375, 0)
    );

    public static Area AL_KHARID_SOUTH_MINE = new Area.Polygonal(
            new Coordinate(3295, 3307, 0),
            new Coordinate(3304, 3307, 0),
            new Coordinate(3307, 3303, 0),
            new Coordinate(3304, 3298, 0),
            new Coordinate(3307, 3293, 0),
            new Coordinate(3303, 3287, 0),
            new Coordinate(3308, 3281, 0),
            new Coordinate(3315, 3271, 0),
            new Coordinate(3299, 3269, 0),
            new Coordinate(3289, 3274, 0),
            new Coordinate(3293, 3279, 0),
            new Coordinate(3291, 3284, 0),
            new Coordinate(3295, 3291, 0),
            new Coordinate(3290, 3300, 0)
    );

    public static Area AL_KHARID_NORTH_MINE = new Area.Polygonal(
            new Coordinate(3298, 3319, 0),
            new Coordinate(3293, 3310, 0),
            new Coordinate(3294, 3303, 0),
            new Coordinate(3307, 3303, 0),
            new Coordinate(3304, 3307, 0),
            new Coordinate(3305, 3312, 0),
            new Coordinate(3303, 3319, 0)
    );
}