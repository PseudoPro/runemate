package com.sudo.v3.spectre.bots.sudodivination.leafs;

import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudodivination.ApexDivination;
import com.sudo.v3.spectre.statics.UpdateUI;

public class ChooseConversionTypeLeaf extends LeafTask {
    private ApexDivination bot;

    public ChooseConversionTypeLeaf(ApexDivination bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        UpdateUI.currentTask("Selecting Conversion Type", bot);
        if(bot.configureComponent.interact("Select"))
            bot.configureRift = false;
    }
}
