package com.sudo.v3.spectre.bots.sudodivination.leafs;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.local.hud.interfaces.ChatDialog;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudodivination.ApexDivination;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 1/28/2017.
 */
public class EnterCacheLeaf extends LeafTask {
    private ApexDivination bot;
    private GameObject obj;

    public EnterCacheLeaf(ApexDivination bot) {
        this.bot = bot;
    }

    @Override
    public void execute() {
        obj = GameObjects.newQuery().actions("Enter cache").results().nearest();

        if (obj != null) {
            UpdateUI.currentTask("Entering Guthixian Cache", bot);

            String chatText;
            if ((chatText = ChatDialog.getText()) != null && !chatText.isEmpty() && (chatText.contains("You will receive XP for this activity.") || chatText.contains("You have already received the maximum"))) {
                ChatDialog.getContinue().select();
            } else if (ChatDialog.getOption(1) != null) {
                ChatDialog.getOption(1).select();
            } else {
                obj.interact("Enter cache");
            }
        }
    }
}