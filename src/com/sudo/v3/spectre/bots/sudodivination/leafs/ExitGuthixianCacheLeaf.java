package com.sudo.v3.spectre.bots.sudodivination.leafs;

import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v2.spectre.common.helpers.SudoTravel;
import com.sudo.v3.spectre.bots.sudodivination.ApexDivination;
import com.sudo.v3.spectre.api.game.hud.SudoCamera;
import com.sudo.v3.spectre.statics.UpdateUI;

public class ExitGuthixianCacheLeaf extends LeafTask {

    private ApexDivination bot;

    public ExitGuthixianCacheLeaf(ApexDivination bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        if(bot.unstableRift != null){
            if(bot.unstableRift.distanceTo(bot.player) > 10 && bot.unstableRift.getArea() != null){
                SudoTravel.bresenhamTowards(bot.unstableRift.getArea());
            }else{
                if(bot.unstableRift.getVisibility() < .8 || (bot.player != null && bot.unstableRift.distanceTo(bot.player) > 4))
                    SudoCamera.ConcurrentlyTurnToWithYaw(bot.unstableRift);

                UpdateUI.currentTask("Attempting to exit Guthixian Cache", bot);
                if(bot.unstableRift.interact("Exit", "Unstable rift")) {
                    Execution.delay(750, 1250);
                    Execution.delayWhile(bot.player::isMoving, 1000, 3000);
                }
            }
        }
    }
}
