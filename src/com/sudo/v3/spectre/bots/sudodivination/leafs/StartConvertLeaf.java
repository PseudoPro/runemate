package com.sudo.v3.spectre.bots.sudodivination.leafs;

import com.runemate.game.api.hybrid.local.hud.interfaces.Interfaces;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudodivination.ApexDivination;
import com.sudo.v3.spectre.api.game.hud.SudoCamera;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 11/29/2016.
 */
public class StartConvertLeaf extends LeafTask
{
    private ApexDivination bot;

    public StartConvertLeaf(ApexDivination bot){
        this.bot = bot;
    }

    @Override
    public void execute()
    {
        if(bot.riftObj != null)
        {
            if (!bot.riftObj.isVisible() || bot.riftObj.distanceTo(bot.player) > 6)
                SudoCamera.ConcurrentlyTurnToWithYaw(bot.riftObj);

            UpdateUI.currentTask("Attempting to interact with rift", bot);
            if(bot.riftObj.interact(bot.riftAction))
                Execution.delayWhile(bot.player::isMoving, 1000, 1500);
            else
            {
                bot.configureComponent = Interfaces.newQuery().containers(131).actions(bot.conversionType.getActionName()).results().first();

                if(bot.configureComponent != null && bot.configureComponent.isVisible() && bot.configureComponent.isValid())
                    bot.configureComponent.interact(bot.conversionType.getActionName());
            }
        }
    }
}
