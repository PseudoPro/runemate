package com.sudo.v3.spectre.bots.sudodivination.leafs;

import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudodivination.ApexDivination;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 11/29/2016.
 */
public class IsConvertingLeaf extends LeafTask
{
    private ApexDivination bot;

    public IsConvertingLeaf(ApexDivination bot)
    {
        this.bot = bot;
    }

    @Override
    public void execute()
    {
        UpdateUI.currentTask("Currently converting memories", bot);
    }
}