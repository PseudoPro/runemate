package com.sudo.v3.spectre.bots.sudodivination.leafs;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.definitions.ItemDefinition;
import com.runemate.game.api.hybrid.local.Skill;
import com.runemate.game.api.hybrid.local.hud.interfaces.Equipment;
import com.runemate.game.api.hybrid.local.hud.interfaces.InterfaceComponent;
import com.runemate.game.api.hybrid.local.hud.interfaces.Interfaces;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudodivination.ApexDivination;
import com.sudo.v3.spectre.api.game.hud.SudoCamera;
import com.sudo.v3.spectre.common.navigation.Traverse;
import com.sudo.v3.spectre.statics.UpdateUI;

import java.util.ArrayList;

/**
 * Created by SudoPro on 1/28/2017.
 */
public class GuthixianCacheLeaf extends LeafTask
{
    private ApexDivination bot;
    private GameObject unstableRift, memory;
    private boolean usingRetrieve = false;

    private ArrayList<String> memories = new ArrayList<String>(){
        {
            add("Tiny raw memory");
            add("Raw memory");
            add("Large memory");
            add("Tiny memory");
        }
    };

    public GuthixianCacheLeaf(ApexDivination bot){
        this.bot = bot;
    }

    @Override
    public void execute() {

        if (bot.player != null) {
            // Remove weapon (if equipped)
            SpriteItem item = Equipment.getItemIn(Equipment.Slot.WEAPON);
            ItemDefinition def = null;
            if (item != null) {
                if ((def = item.getDefinition()) != null) {
                    if (!memories.contains(def.getName())) {
                        UpdateUI.currentTask("Unequipping Weapon", bot);
                        item.interact("Remove");
                        Execution.delayWhile(() -> Equipment.getItemIn(Equipment.Slot.WEAPON) != null, 500);
                    }
                }
            } else if ((item = Equipment.getItemIn(Equipment.Slot.SHIELD)) != null) {
                // Remove shield/off-hand (if equipped)
                UpdateUI.currentTask("Unequipping Off-hand", bot);
                item.interact("Remove");
                Execution.delayWhile(() -> Equipment.getItemIn(Equipment.Slot.SHIELD) != null, 500);
            }

            // Find unstable Rift
            unstableRift = GameObjects.newQuery().names("Unstable rift").results().nearest();

            SpriteItem weapon = Equipment.getItemIn(Equipment.Slot.WEAPON);
            String weaponName;

            // If player is holding a memory
            if (weapon != null && weapon.getDefinition() != null && (weaponName = weapon.getDefinition().getName()) != null && memories.contains(weaponName)) {
                // Deposit memory
                if (unstableRift != null) {
                    InterfaceComponent comp = Interfaces.newQuery().containers(1545).textContains("1 charge").results().random();
                    if (comp != null) {
                        UpdateUI.currentTask("Triggering buff", bot);
                        comp.click();
                    } else {
                        if (unstableRift.getVisibility() < 0.8)
                            SudoCamera.ConcurrentlyTurnToWithYaw(unstableRift);

                        UpdateUI.currentTask("Depositing memory", bot);
                        if (unstableRift.distanceTo(bot.player) < 16) {
                            unstableRift.interact("Offer memory");
                        } else {
                            //SudoCamera.ConcurrentlyTurnToWithYaw(memory);
                            Traverse.smartRegionPath(bot.nav, new Area.Circular(unstableRift.getPosition(), 8));
                        }
                    }
                }

            } else {
                memory = findMemory();
                if (memory != null) {
                    if (memory.getVisibility() < 0.8)
                        SudoCamera.ConcurrentlyTurnToWithYaw(memory);

                    UpdateUI.currentTask("Grabbing memory", bot);
                    if (memory.distanceTo(bot.player) < 12) {
                        if (usingRetrieve)
                            memory.interact("Retrieve");
                        else
                            memory.interact("Take memory");
                    } else {
                        if(memory.getPosition() != null)
                            Traverse.smartRegionPath(bot.nav, new Area.Circular(memory.getPosition(), 6));
                    }

                }
            }
        }
    }

    private GameObject findMemory()
    {
        GameObject obj;
        int lvl = Skill.DIVINATION.getCurrentLevel();

        if (lvl < 45)
        {
            obj = GameObjects.newQuery().names(memories.get(0)).results().nearest();
            usingRetrieve = false;
        } else if (lvl >= 45 && lvl < 85)
        {
            obj = GameObjects.newQuery().names(memories.get(1)).results().nearest();
            usingRetrieve = false;
        } else
        {
            obj = GameObjects.newQuery().names(memories.get(2)).results().nearest();
            usingRetrieve = true;
        }

        return obj;
    }
}
