package com.sudo.v3.spectre.bots.sudodivination.leafs;

import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudodivination.ApexDivination;
import com.sudo.v3.spectre.api.game.hud.SudoCamera;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 11/29/2016.
 */
public class StartGatherLeaf extends LeafTask
{
    private ApexDivination bot;

    public StartGatherLeaf(ApexDivination bot)
    {
        this.bot = bot;
    }

    @Override
    public void execute()
    {
        try
        {
            if (!bot.wispNpc.isVisible())
                SudoCamera.ConcurrentlyTurnToWithYaw(bot.wispNpc);

            UpdateUI.currentTask("Attempting to gather from \"" + bot.wispNpc.getName() + "\"", bot);
            if(bot.wispNpc.interact(bot.wispAction))
                Execution.delayWhile(bot.player::isMoving, 1000, 1500);

        }
        catch(NullPointerException e){
            UpdateUI.currentTask("Interactable became null, retrying...", bot);
        }
    }
}
