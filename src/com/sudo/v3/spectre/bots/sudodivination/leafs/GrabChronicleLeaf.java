package com.sudo.v3.spectre.bots.sudodivination.leafs;

import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudodivination.ApexDivination;
import com.sudo.v3.spectre.api.game.hud.SudoCamera;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 11/29/2016.
 */
public class GrabChronicleLeaf extends LeafTask
{
    private ApexDivination bot;

    public GrabChronicleLeaf(ApexDivination bot){
        this.bot = bot;
    }

    @Override
    public void execute()
    {
        if(!bot.chronicleNpc.isVisible())
            SudoCamera.ConcurrentlyTurnToWithYaw(bot.chronicleNpc);

        UpdateUI.currentTask("Attempting to grab chronicle fragment", bot);
        bot.chronicleNpc.interact(bot.chronicleAction, bot.chronicle);
    }
}
