package com.sudo.v3.spectre.bots.sudodivination.leafs;

import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudodivination.ApexDivination;
import com.sudo.v3.spectre.api.game.hud.SudoCamera;
import com.sudo.v3.spectre.common.navigation.Traverse;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 1/29/2017.
 */
public class CatchButterflyLeaf extends LeafTask
{
    private ApexDivination bot;
    private Npc npc;

    public CatchButterflyLeaf(ApexDivination bot){
        this.bot = bot;
    }

    @Override
    public void execute()
    {
        npc = Npcs.newQuery().names("Guthixian butterfly").actions("Catch").results().nearest();
        if (npc != null)
        {
            UpdateUI.currentTask("Attempting to catch Guthixian Butterfly", bot);
            if (npc.distanceTo(Players.getLocal()) < 12)
            {
                if (!npc.isVisible())
                    SudoCamera.ConcurrentlyTurnToWithYaw(npc);

                if(npc.interact("Catch"))
                    Execution.delay(500, 1500);
            } else
                Traverse.smartRegionPath(bot.nav, new Area.Circular(npc.getPosition(), 6));
        }
    }
}
