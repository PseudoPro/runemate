package com.sudo.v3.spectre.bots.sudodivination.leafs;

import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.common.playersense.ApexPlayerSense;
import com.sudo.v3.spectre.bots.sudodivination.ApexDivination;
import com.sudo.v3.spectre.bots.sudodivination.enums.TraversalLocation;
import com.sudo.v3.spectre.common.navigation.Traverse;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 11/29/2016.
 */
public class TraversalLeaf extends LeafTask {
    private ApexDivination bot;

    public TraversalLeaf(ApexDivination bot) {
        this.bot = bot;
    }

    @Override
    public void execute() {
        if (bot.traversalLocation == TraversalLocation.rift) {
            UpdateUI.currentTask("Traveling to rift", bot);
            if(bot.player.distanceTo(bot.riftArea) > 15)
                Traverse.smartRegionPath(bot.nav, bot.riftArea);
            else
                Traverse.bresenhamPath(bot.riftArea);
        } else if (bot.traversalLocation == TraversalLocation.wisp) {
            UpdateUI.currentTask("Trying to find wisp/spring", bot);
            Traverse.bresenhamPath(new Area.Circular(bot.riftArea.getCenter(), 20));
        } else if (bot.traversalLocation == TraversalLocation.cache) {
            UpdateUI.currentTask("Traveling to Cache entrance", bot);
            Traverse.smartRegionPath(bot.nav, bot.riftArea);
        }

        // Delay after each attempt to traverse
        Execution.delay(500, 750);
        Execution.delayWhile(bot.player::isMoving, ApexPlayerSense.Key.TRAVERSAL_DELAY.getAsInteger());
    }
}