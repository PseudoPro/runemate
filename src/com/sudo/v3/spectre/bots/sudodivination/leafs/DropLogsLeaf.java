package com.sudo.v3.spectre.bots.sudodivination.leafs;

import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.local.hud.interfaces.SpriteItem;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudodivination.ApexDivination;
import com.sudo.v3.spectre.statics.UpdateUI;

import java.util.regex.Pattern;

/**
 * Created by SudoPro on 11/29/2016.
 */
public class DropLogsLeaf extends LeafTask
{
    private ApexDivination bot;
    private SpriteItem log;

    public DropLogsLeaf(ApexDivination bot)
    {
        this.bot = bot;
    }

    @Override
    public void execute()
    {
        log = Inventory.newQuery().names(Pattern.compile("(?i)^(.*logs|wooden knot)")).results().first();

        if(log != null)
        {
            UpdateUI.currentTask("Dropping log(s)", bot);
            log.interact("Drop");
        }
    }
}