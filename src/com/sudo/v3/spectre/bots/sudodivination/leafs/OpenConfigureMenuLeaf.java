package com.sudo.v3.spectre.bots.sudodivination.leafs;

import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.LeafTask;
import com.sudo.v3.spectre.bots.sudodivination.ApexDivination;
import com.sudo.v3.spectre.statics.UpdateUI;

public class OpenConfigureMenuLeaf extends LeafTask {
    private ApexDivination bot;

    public OpenConfigureMenuLeaf(ApexDivination bot){
        this.bot = bot;
    }

    @Override
    public void execute() {
        UpdateUI.currentTask("Attempting to open Configure Menu", bot);
        if(bot.riftObj.interact("Configure"))
            Execution.delay(500, 1500);
    }
}
