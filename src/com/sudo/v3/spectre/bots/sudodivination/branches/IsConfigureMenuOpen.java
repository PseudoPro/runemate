package com.sudo.v3.spectre.bots.sudodivination.branches;

import com.runemate.game.api.hybrid.local.hud.interfaces.InterfaceComponent;
import com.runemate.game.api.hybrid.local.hud.interfaces.Interfaces;
import com.runemate.game.api.hybrid.queries.results.InterfaceComponentQueryResults;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudodivination.ApexDivination;
import com.sudo.v3.spectre.statics.UpdateUI;

public class IsConfigureMenuOpen extends SudoBranchTask {
    private ApexDivination bot;
    private InterfaceComponentQueryResults results;

    public IsConfigureMenuOpen(ApexDivination bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        return bot.chooseConversionTypeLeaf;
    }

    @Override
    public boolean validate() {
        results = Interfaces.newQuery().containers(131).actions("Select").results();
        if(results != null && results.size() >= 3){
            UpdateUI.debug("configureComponent.getIndex: " + bot.conversionType.getIndex());
            bot.configureComponent = results.get(bot.conversionType.getIndex());
        }else{
            bot.configureComponent = null;
        }

        validate = bot.configureComponent != null && bot.configureComponent.isVisible() && bot.configureComponent.isValid();

        UpdateUI.debug(bot.player.getName() + " -> Branch IsConfigureMenuOpen: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask() {
        return bot.openConfigureMenuLeaf;
    }
}
