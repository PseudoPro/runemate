package com.sudo.v3.spectre.bots.sudodivination.branches;

import com.runemate.game.api.hybrid.local.hud.interfaces.ChatDialog;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudodivination.ApexDivination;
import com.sudo.v3.spectre.statics.Animations;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 11/29/2016.
 */
public class IsConverting extends SudoBranchTask{
    private ApexDivination bot;

    public IsConverting(ApexDivination bot) {
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        return bot.isConvertingLeaf;
    }

    @Override
    public boolean validate() {

            bot.riftObj = GameObjects.newQuery().names(bot.rift, bot.riftCache).results().nearest();
            if (bot.riftObj != null) {
                if (bot.player.distanceTo(bot.riftObj) > 4) {
                    UpdateUI.debug(bot.player.getName() + " -> Branch IsConverting: false");
                    return false;
                } else {
                    validate = Execution.delayUntil(() -> Animations.DIV_ANIM.contains(bot.player.getAnimationId()) && ChatDialog.getContinue() == null, 2000);
                    UpdateUI.debug(bot.player.getName() + " -> Branch IsConverting: " + validate);
                    return validate;
                }
            } else {
                UpdateUI.debug(bot.player.getName() + " -> Branch IsConverting: false (Rift is null)");
                return false;
            }

    }

    @Override
    public TreeTask failureTask() {
        return bot.startConvertLeaf;
    }
}