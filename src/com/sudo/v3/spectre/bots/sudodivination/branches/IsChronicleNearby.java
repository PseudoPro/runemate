package com.sudo.v3.spectre.bots.sudodivination.branches;

import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudodivination.ApexDivination;
import com.sudo.v3.spectre.bots.sudodivination.enums.WispType;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 11/29/2016.
 */
public class IsChronicleNearby extends SudoBranchTask
{
    private ApexDivination bot;

    public IsChronicleNearby(ApexDivination bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask()
    {
        return bot.grabChronicleLeaf;
    }

    @Override
    public boolean validate()
    {
        bot.chronicleNpc = Npcs.newQuery().names(bot.chronicle).results().nearest();
        validate = bot.grabChronicles && bot.chronicleNpc != null && !bot.chronicleFull;
        UpdateUI.debug(bot.player.getName() + " -> Branch IsChronicleNearby: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask()
    {
        bot.wispType = WispType.normal;
        return bot.isGathering;
    }
}
