package com.sudo.v3.spectre.bots.sudodivination.branches;

import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudodivination.ApexDivination;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 11/29/2016.
 */
public class IsInventoryNearFull extends SudoBranchTask
{
    private ApexDivination bot;

    public IsInventoryNearFull(ApexDivination bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask()
    {
        return bot.isRiftNearby;
    }

    @Override
    public boolean validate()
    {
        validate = Inventory.getItems().size() >= bot.invDepositNum;
        UpdateUI.debug(bot.player.getName() + " -> Branch IsInventoryNearFull: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask()
    {
        return bot.isChronicleNearby;
    }
}
