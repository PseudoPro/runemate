package com.sudo.v3.spectre.bots.sudodivination.branches;

import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudodivination.ApexDivination;
import com.sudo.v3.spectre.bots.sudodivination.enums.TraversalLocation;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 11/29/2016.
 */
public class IsRiftNearby extends SudoBranchTask
{
    private ApexDivination bot;

    public IsRiftNearby(ApexDivination bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask()
    {
        if(bot.configureRift){
            return bot.isConfigureMenuOpen;
        } else {
            return bot.isConverting;
        }
    }

    @Override
    public boolean validate()
    {
        bot.riftObj = GameObjects.newQuery().names(bot.rift, bot.riftCache).results().nearest();
        validate = bot.riftObj != null && bot.riftObj.distanceTo(bot.player) < 10;
        UpdateUI.debug(bot.player.getName() + " -> Branch IsRiftNearby: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask()
    {
        bot.traversalLocation = TraversalLocation.rift;
        return bot.traversalLeaf;
    }
}
