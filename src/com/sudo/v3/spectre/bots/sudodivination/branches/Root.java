package com.sudo.v3.spectre.bots.sudodivination.branches;

import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.RuneScape;
import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.local.Skill;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoRootTask;
import com.sudo.v3.spectre.bots.sudodivination.ApexDivination;
import com.sudo.v3.spectre.bots.sudodivination.enums.WispType;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.sudo.v3.ui.model.XPInfo;

import java.util.regex.Pattern;

/**
 * Created by SudoPro on 11/29/2016.
 */
public class Root extends SudoRootTask {
    private ApexDivination bot;
    private Npc enrichedWisp, guthixianButterfly;

    public Root(ApexDivination bot) {
        super(bot);
        this.bot = bot;
    }

    @Override
    public TreeTask rootTask() {

        if (bot.firstLogin) {
            if (RuneScape.isLoggedIn()) {
                if (Inventory.contains(bot.chronicle))
                    bot.chronicleCount = Inventory.getQuantity(bot.chronicle);
                else
                    bot.chronicleCount = 0;

                // Add Skills to track to the HashMap
                bot.XPInfoMap.put(Skill.DIVINATION, new XPInfo(Skill.DIVINATION));
                bot.XPInfoMap.put(Skill.HUNTER, new XPInfo(Skill.HUNTER));

                bot.startEnergyCount = Inventory.getQuantity(bot.energyID[bot.energyIndex]);
                bot.firstLogin = false;

                UpdateUI.debug("Divination Level: " + Skill.DIVINATION.getCurrentLevel());
                UpdateUI.debug("Energy Type: " + bot.energyID[bot.energyIndex]);
                UpdateUI.debug("Radius: " + bot.radius);
                UpdateUI.debug("Guthixian Cache: " + bot.playCache);
                UpdateUI.debug("Wisp/Spring: " + bot.wisp + " / " + bot.spring);
                UpdateUI.debug("Conversion Type: " + bot.conversionType.name());
                UpdateUI.debug("Inventory Deposit Count: " + bot.invDepositNum);
                UpdateUI.debug("Capture Chronicles: " + bot.grabChronicles);
                UpdateUI.debug("Min/Max Chronicle Timer: " + bot.chronicleMinTimer + " / " + bot.chronicleMaxTimer);
                UpdateUI.debug("Grab Butterflies: " + bot.catchButterflies);

                bot.updateSession(0.0);
            }
        }

        if (bot.getMetaData().getHourlyPrice().doubleValue() > 0 || bot.isValidSession() || Environment.isSDK()) {
            if (bot.configureRift){
                return bot.isRiftNearby;
            }

            if (bot.currentlyBreaking) {
                if (RuneScape.isLoggedIn()) {
                    UpdateUI.currentTask("Attempting to log off.", bot);
                    RuneScape.logout(true);
                }
                return bot.emptyLeaf;
            }

            // If user enables Guthixian Cache in GUI
            if (bot.playCache) {
                // If the Guthixian Cache opens up
                if (GameObjects.newQuery().names(bot.riftCache).results().nearest() != null)
                    return bot.isCacheNearby;
                else if ((bot.unstableRift = GameObjects.newQuery().names("Unstable rift").results().nearest()) != null) {
                    return bot.guthixianCacheLeaf;
                }
            }else{
                if ((bot.unstableRift = GameObjects.newQuery().names("Unstable rift").results().nearest()) != null) {
                    return bot.exitGuthixianCacheLeaf;
                }
            }

            enrichedWisp = Npcs.newQuery().names("Enriched " + bot.wisp.toLowerCase(), "Enriched " + bot.spring.toLowerCase()).results().nearest();
            if (bot.harvestEnriched && enrichedWisp != null && enrichedWisp.isValid() && !Inventory.isFull()) {
                bot.wispNpc = enrichedWisp;
                bot.wispType = WispType.enriched;
                return bot.isGathering;
            }

            if (bot.catchButterflies) {
                guthixianButterfly = Npcs.newQuery().names("Guthixian butterfly").actions("Catch").results().nearest();
                if (guthixianButterfly != null) {
                    return bot.catchButterflyLeaf;
                }
            }

            if (Inventory.newQuery().names(Pattern.compile("(?i)^(.*logs|wooden knot)")).results().size() > 0) {
                // If logs or wooden knots exist in the inventory, drop them
                return bot.dropLogsLeaf;
            } else
                return bot.isInventoryNearFull;
        } else {
            return bot.liteVersionExpireLeaf;
        }
    }
}