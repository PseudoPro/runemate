package com.sudo.v3.spectre.bots.sudodivination.branches;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudodivination.ApexDivination;
import com.sudo.v3.spectre.bots.sudodivination.enums.TraversalLocation;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 1/28/2017.
 */
public class IsCacheNearby extends SudoBranchTask
{
    private ApexDivination bot;
    private GameObject obj;

    public IsCacheNearby(ApexDivination bot){
        this.bot = bot;
    }

    @Override
    public TreeTask successTask()
    {
        return bot.enterCacheLeaf;
    }

    @Override
    public boolean validate()
    {
        obj = GameObjects.newQuery().actions("Enter cache").results().nearest();
        validate = obj != null && obj.getVisibility() > 0.8 && obj.distanceTo(bot.player) < 10;
        UpdateUI.debug(bot.player.getName() + " -> Branch IsCacheNearby: " + validate);

        return validate;
    }

    @Override
    public TreeTask failureTask()
    {
        bot.traversalLocation = TraversalLocation.cache;
        return bot.traversalLeaf;
    }
}
