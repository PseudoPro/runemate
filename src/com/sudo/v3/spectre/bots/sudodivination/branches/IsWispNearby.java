package com.sudo.v3.spectre.bots.sudodivination.branches;

import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.framework.tree.BranchTask;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudodivination.ApexDivination;
import com.sudo.v3.spectre.bots.sudodivination.enums.TraversalLocation;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 11/29/2016.
 */
public class IsWispNearby extends SudoBranchTask
{
    private ApexDivination bot;

    public IsWispNearby(ApexDivination bot)
    {
        this.bot = bot;
    }

    @Override
    public TreeTask successTask()
    {
        return bot.startGatherLeaf;
    }

    @Override
    public boolean validate()
    {
        if(bot.wispNpc == null || !bot.wispNpc.isValid())
        {
            Coordinate coord;
            if((coord = bot.player.getPosition()) != null) {
                Area aroundPlayer = new Area.Circular(coord, bot.radius);

                bot.wispNpc = Npcs.newQuery().names(bot.wisp).within(aroundPlayer).results().random();

                if (bot.wispNpc == null || !bot.wispNpc.isValid())
                    bot.wispNpc = Npcs.newQuery().names(bot.spring).within(aroundPlayer).results().nearest();

                if (bot.wispNpc == null || !bot.wispNpc.isValid())
                    bot.wispNpc = Npcs.newQuery().names(bot.wisp, bot.spring).results().nearest();

            }
        }

        validate = bot.wispNpc != null && bot.wispNpc.isValid();
        UpdateUI.debug(bot.player.getName() + " -> Branch IsWispNearby: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask()
    {
        bot.traversalLocation = TraversalLocation.wisp;
        return bot.traversalLeaf;
    }
}