package com.sudo.v3.spectre.bots.sudodivination.branches;

import com.runemate.game.api.hybrid.local.hud.interfaces.ChatDialog;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBranchTask;
import com.sudo.v3.spectre.bots.sudodivination.ApexDivination;
import com.sudo.v3.spectre.bots.sudodivination.enums.WispType;
import com.sudo.v3.spectre.api.game.entities.ApexPlayer;
import com.sudo.v3.spectre.statics.Animations;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 11/29/2016.
 */
public class IsGathering extends SudoBranchTask
{
    private ApexDivination bot;

    public IsGathering(ApexDivination bot)
    {
        this.bot = bot;
    }

    @Override
    public TreeTask successTask()
    {
        return bot.isGatheringLeaf;
    }

    @Override
    public boolean validate()
    {
        validate = Execution.delayUntil(() -> ApexPlayer.isInAnimation(Animations.DIV_ANIM) && ChatDialog.getContinue() == null, 1500);
        UpdateUI.debug(bot.player.getName() + " -> Branch IsGathering: " + validate);
        return validate;
    }

    @Override
    public TreeTask failureTask()
    {
        if (bot.wispType == WispType.enriched)
            return bot.startGatherLeaf;
        else if (bot.wispType == WispType.normal)
            return bot.isWispNearby;
        else
            return bot.emptyLeaf;
    }
}