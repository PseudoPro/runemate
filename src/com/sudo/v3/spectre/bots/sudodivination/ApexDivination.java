package com.sudo.v3.spectre.bots.sudodivination;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.entities.definitions.ItemDefinition;
import com.runemate.game.api.hybrid.local.hud.interfaces.InterfaceComponent;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.util.calculations.CommonMath;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.listeners.ChatboxListener;
import com.runemate.game.api.script.framework.listeners.InventoryListener;
import com.runemate.game.api.script.framework.listeners.events.ItemEvent;
import com.runemate.game.api.script.framework.listeners.events.MessageEvent;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.base.SudoBot;
import com.sudo.v3.spectre.bots.sudodivination.branches.*;
import com.sudo.v3.spectre.bots.sudodivination.enums.ConversionType;
import com.sudo.v3.spectre.bots.sudodivination.enums.TraversalLocation;
import com.sudo.v3.spectre.bots.sudodivination.enums.WispType;
import com.sudo.v3.spectre.bots.sudodivination.leafs.*;
import com.sudo.v3.spectre.bots.sudodivination.leafs.TraversalLeaf;
import com.sudo.v3.spectre.bots.sudodivination.ui.DivInfoUI;
import com.sudo.v3.spectre.bots.sudodivination.ui.DivinationFXGui;
import com.sudo.v3.spectre.api.util.SudoTimer;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.sudo.v3.ui.Info;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * ApexDivination Bot for RuneMate spectre
 */
public class ApexDivination extends SudoBot implements InventoryListener, ChatboxListener {
    private DivinationFXGui configUI;
    private DivInfoUI infoUI;

    public Area riftArea;

    public String chronicle = "Chronicle fragment", wisp = "", spring = "",
            rift = "Energy rift", riftCache = "Energy Rift", chronicleAction = "Capture", wispAction = "Harvest", riftAction = "Convert memories";

    public ConversionType conversionType = ConversionType.XP;
    public InterfaceComponent configureComponent;

    public int radius = 7, chronicleCount = 0, invDepositNum = 25, memoryCount = 0, chronicleMinTimer = 5000,
            chronicleMaxTimer = 300000, startEnergyCount = 0, energyCount = 0, energyIndex = 0;

    public boolean grabChronicles = true, chronicleFull = false, harvestEnriched = true, playCache = false, catchButterflies = false, configureRift = true;

    public TraversalLocation traversalLocation = TraversalLocation.wisp;
    public WispType wispType = WispType.normal;

    public Npc wispNpc, chronicleNpc;
    public GameObject riftObj, unstableRift;

    public int[] energyID = new int[]{
            29313,  // Pale Energy
            29314,  // Flickering Energy
            29315,  // Bright Energy
            29316,  // Glowing Energy
            29317,  // Sparkling Energy
            29318,  // Gleaming Energy
            29319,  // Vibrant Energy
            29320,  // Lustrous Energy
            31312,  // Elder Energy
            29321,  // Brilliant Energy
            29322,  // Radiant Energy
            29323,  // Luminous Energy
            29324   // Incandescent Energy
    };

    public SudoTimer chronicleTimer = new SudoTimer(chronicleMinTimer, chronicleMaxTimer);

    // Branches
    public IsCacheNearby isCacheNearby;
    public IsChronicleNearby isChronicleNearby;
    public IsConverting isConverting;
    public IsGathering isGathering;
    public IsInventoryNearFull isInventoryNearFull;
    public IsRiftNearby isRiftNearby;
    public IsWispNearby isWispNearby;
    public IsConfigureMenuOpen isConfigureMenuOpen;

    // Leafs
    public CatchButterflyLeaf catchButterflyLeaf;
    public DropLogsLeaf dropLogsLeaf;
    public EnterCacheLeaf enterCacheLeaf;
    public GrabChronicleLeaf grabChronicleLeaf;
    public GuthixianCacheLeaf guthixianCacheLeaf;
    public IsConvertingLeaf isConvertingLeaf;
    public IsGatheringLeaf isGatheringLeaf;
    public StartConvertLeaf startConvertLeaf;
    public StartGatherLeaf startGatherLeaf;
    public TraversalLeaf traversalLeaf;
    public ExitGuthixianCacheLeaf exitGuthixianCacheLeaf;
    public ChooseConversionTypeLeaf chooseConversionTypeLeaf;
    public OpenConfigureMenuLeaf openConfigureMenuLeaf;

    @Override
    public ObjectProperty<Node> botInterfaceProperty() {
        if (botInterfaceProperty == null) {
            botInterfaceProperty = new SimpleObjectProperty<>(configUI = new DivinationFXGui(this));
            guiList = new ArrayList<Node>();
            //guiList.add(_sbGUI = new SudoBuddyFXGui(this));
            guiList.add(configUI);
            guiList.add(infoUI = new DivInfoUI(this));
        }
        return botInterfaceProperty;
    }

    public ApexDivination() {
        super();
        setEmbeddableUI(this);

        // Branches
        isCacheNearby = new IsCacheNearby(this);
        isChronicleNearby = new IsChronicleNearby(this);
        isConverting = new IsConverting(this);
        isGathering = new IsGathering(this);
        isInventoryNearFull = new IsInventoryNearFull(this);
        isRiftNearby = new IsRiftNearby(this);
        isWispNearby = new IsWispNearby(this);
        isConfigureMenuOpen = new IsConfigureMenuOpen(this);

        // Leafs
        catchButterflyLeaf = new CatchButterflyLeaf(this);
        dropLogsLeaf = new DropLogsLeaf(this);
        enterCacheLeaf = new EnterCacheLeaf(this);
        grabChronicleLeaf = new GrabChronicleLeaf(this);
        guthixianCacheLeaf = new GuthixianCacheLeaf(this);
        isConvertingLeaf = new IsConvertingLeaf(this);
        isGatheringLeaf = new IsGatheringLeaf(this);
        startConvertLeaf = new StartConvertLeaf(this);
        startGatherLeaf = new StartGatherLeaf(this);
        traversalLeaf = new TraversalLeaf(this);
        exitGuthixianCacheLeaf = new ExitGuthixianCacheLeaf(this);
        chooseConversionTypeLeaf = new ChooseConversionTypeLeaf(this);
        openConfigureMenuLeaf = new OpenConfigureMenuLeaf(this);
    }

    @Override
    public TreeTask createRootTask() {
        return new Root(this);
    }

    @Override
    public void onStart(String... args) {
        super.onStart();

        getEventDispatcher().addListener(this);

        // Set bot to be ready to capture a chronicle as soon as bot is started
        chronicleTimer.setRemainingTime(0);
        chronicleTimer.start();

        try {
            setRiftArea();
        } catch (Exception e) {
            UpdateUI.debug("Unable to set Rift Area");
        }

    }

    @Override
    public String getBotName() {
        return "ApexDivination";
    }

    @Override
    public void onItemAdded(ItemEvent event) {
        super.onItemAdded(event);
        ItemDefinition definition = event.getItem().getDefinition();

        if (definition != null) {
            if (definition.getName().contains("memory")) {
                memoryCount++;
            }
            if (definition.getName().contains("energy")) {
                energyCount = Inventory.getQuantity(energyID[energyIndex]) - startEnergyCount;
            }
            if (definition.getName().contains("fragment") && Inventory.getQuantity(chronicle) >= 30)
                grabChronicles = false;

        }
    }

    @Override
    public void onMessageReceived(MessageEvent event) {
        String message = event.getMessage();

        // If user allows to grab chronicles
        if (grabChronicles && chronicleTimer.getRemainingTime() <= 0) {
            if (!chronicleFull && (message.contains("capture the chronicle fragment") || message.contains("fragment in Ironman mode"))) {
                chronicleCount = Inventory.getQuantity(chronicle);
                chronicleTimer.reset();
                UpdateUI.currentTask("New chronicle capture will be ready in " + (chronicleTimer.getRemainingTime() / 1000) + " seconds.", this);
            }

            if (!chronicleFull && message.contains("30 chronicle fragment")) {
                UpdateUI.currentTask("Chronicle count has reached 30. Disabling Chronicle Capturing.", this);
                chronicleFull = true;
            }
        }
    }

    @Override
    public void updateInfo() {

        // UpdateUI all current values in the XPInfo List
        XPInfoMap.values().forEach((xpInfo) -> xpInfo.update());

        displayInfoMap.put("Energy Count: ", Integer.toString(energyCount) +
                " (" + Integer.toString((int) CommonMath.rate(TimeUnit.HOURS, STOPWATCH.getRuntime(), energyCount)) + "/Hour)");
        displayInfoMap.put("GP: ", Integer.toString(grossIncome) +
                " (" + Integer.toString((int) CommonMath.rate(TimeUnit.HOURS, STOPWATCH.getRuntime(), grossIncome)) + "/Hour)");

        try {
            info = new Info(XPInfoMap,           // XP Info
                    displayInfoMap,
                    STOPWATCH.getRuntimeAsString(),        // Total Runtime
                    currentTaskString,                     // Current Task
                    abTaskString);                         // antiban Task

        } catch (Exception e) {
            e.printStackTrace();
        }

        Platform.runLater(() -> infoUI.update());
    }

    public void setRiftArea() {
        riftArea = new Area.Circular(GameObjects.newQuery().names(rift, riftCache).results().first().getPosition(), 5);
        //riftArea = new Area.Circular(new Coordinate(500, 500, 0), 4);
        Execution.delay(500);
    }
}