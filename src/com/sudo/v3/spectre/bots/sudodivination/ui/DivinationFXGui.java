package com.sudo.v3.spectre.bots.sudodivination.ui;

import com.runemate.game.api.hybrid.util.Resources;
import com.sudo.v3.spectre.bots.sudodivination.ApexDivination;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.GridPane;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Java FX Gui for configuring sudodivination bot settings
 */
public class DivinationFXGui extends GridPane implements Initializable {

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        setVisible(true);
    }

    public DivinationFXGui(ApexDivination bot) {
        // Load the fxml file using RuneMate's resources class.
        FXMLLoader loader = new FXMLLoader();

        Future<InputStream> stream = bot.getPlatform().invokeLater(() -> Resources.getAsStream("com/sudo/v3/spectre/bots/sudodivination/ui/DivinationGUI.fxml"));

        // Set this class as root of the fxml file, and SudoBuddyFXGui as the controller.
        loader.setController(new DivinationFXController(bot));
        loader.setRoot(this);

        try {
            loader.load(stream.get());
        } catch (IOException | InterruptedException | ExecutionException e) {
            System.err.println("Error loading GUI");
            e.printStackTrace();
        }

    }
}