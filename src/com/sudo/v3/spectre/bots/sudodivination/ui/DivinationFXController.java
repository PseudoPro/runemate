package com.sudo.v3.spectre.bots.sudodivination.ui;

import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.sudo.v3.spectre.bots.sudodivination.ApexDivination;
import com.sudo.v3.spectre.bots.sudodivination.enums.ConversionType;
import com.sudo.v3.ui.base.SudoFXController;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Java FX Controller for the DivinationFXGui class
 */
public class DivinationFXController extends SudoFXController implements Initializable {

    //private ObjectProperty<Node> property;

    private ApexDivination bot;

    //<editor-fold desc="Divination Tab">
    @FXML
    private CheckBox Enriched_CB, Chronicles_CB, Cache_CB, Butterflies_CB;

    @FXML
    private Button start_BT;

    @FXML
    private Slider Radius_Slider, Inventory_Slider;

    @FXML
    private ComboBox Wisp_ComboBox, Convert_ComboBox;

    @FXML
    private Text Radius_Text, InventoryCount_Text;

    @FXML
    private Label Prompt_L;
    //</editor-fold>


    public DivinationFXController(ApexDivination bot) {
        super(bot);
        this.bot = bot;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Initialize Super
        super.initialize(location, resources);

        // Add wisp options to Wisp_ComboBox
        //if(bot.getMetaData().getHourlyPrice().doubleValue() > 0 || bot.isPrivate() || Environment.isSDK()){
        Wisp_ComboBox.getItems().addAll("Pale wisp", "Flickering wisp", "Bright wisp", "Glowing wisp", "Sparkling wisp", "Gleaming wisp", "Vibrant wisp",
                "Lustrous wisp", "Elder wisp", "Brilliant wisp", "Radiant wisp", "Luminous wisp", "Incandescent wisp");
        //}else{
        //    Wisp_ComboBox.getItems().addAll("Pale wisp", "Flickering wisp", "Bright wisp", "Glowing wisp");
        //}

        // Add conversion methods to Convert_ComboBox
        Convert_ComboBox.getItems().addAll("Convert to energy", "Convert to experience", "Convert to enhanced experience");

        Wisp_ComboBox.setOnAction(getWispConvert_ComboBoxEvent());
        Convert_ComboBox.setOnAction(getWispConvert_ComboBoxEvent());

        start_BT.setOnAction(getStartButtonAction());

        // Handle Radius to scan wisps slider
        Radius_Slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            Radius_Text.setText(Integer.toString(newValue.intValue()));
            bot.radius = newValue.intValue();
        });

        // Handle Inventory deposit value slider
        Inventory_Slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            InventoryCount_Text.setText(Integer.toString(newValue.intValue()));
            bot.invDepositNum = newValue.intValue();
        });
    }

    // Enable or diable the start button based on if required settings are selected to start bot
    private EventHandler<ActionEvent> getWispConvert_ComboBoxEvent() {
        return event -> {
            if (Wisp_ComboBox.getSelectionModel().getSelectedItem() != null && Convert_ComboBox.getSelectionModel().getSelectedItem() != null)
                start_BT.setDisable(false);
            else
                start_BT.setDisable(true);
        };
    }

    public EventHandler<ActionEvent> getStartButtonAction() {
        return event -> {

            try {

                // Set Enriched condition
                bot.harvestEnriched = Enriched_CB.isSelected();

                // Set Chronicle condition
                bot.grabChronicles = Chronicles_CB.isSelected();

                // Set Guthixian Butterflies condition
                bot.catchButterflies = Butterflies_CB.isSelected();

                // Set Cache condition
                bot.playCache = Cache_CB.isSelected();

                // Set Wisps to capture
                bot.wisp = Wisp_ComboBox.getSelectionModel().getSelectedItem().toString();
                bot.energyIndex = Wisp_ComboBox.getSelectionModel().getSelectedIndex();
                bot.spring = bot.wisp.substring(0, bot.wisp.length() - 4) + "spring";

                // Set Convert method
                switch(Convert_ComboBox.getSelectionModel().getSelectedItem().toString()){
                    case "Convert to energy":
                        bot.conversionType = ConversionType.ENERGY;
                        break;
                    case "Convert to experience":
                        bot.conversionType = ConversionType.XP;
                        break;
                    case "Convert to enhanced experience":
                        bot.conversionType = ConversionType.ENHANCED_XP;
                        break;
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

            if (bot.wisp != null && !bot.wisp.equals("") && bot.riftAction != null && !bot.riftAction.equals("")) {
                try {
                    if (start_BT.getText().equals("Start"))
                        bot.getPlatform().invokeLater(() -> {
                            bot.setRiftArea();
                            bot.startEnergyCount = Inventory.getQuantity(bot.energyID[bot.energyIndex]);
                        });

                    if (bot.riftArea != null) {
                        start_BT.textProperty().set("Resume");
                        Prompt_L.textProperty().set(" ");
                        Wisp_ComboBox.setDisable(true);
                        super.getStart_BTEvent();
                    } else
                        Prompt_L.textProperty().set("Please start while standing next to the Energy Rift");

                } catch (Exception e) {
                    e.printStackTrace();

                }
            }
        };
    }
}