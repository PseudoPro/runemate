package com.sudo.v3.spectre.bots.sudodivination;


/**
 * ApexDivination Bot for RuneMate spectre
 */
public class ApexDivinationPrivate extends ApexDivination
{
    @Override
    public boolean isPrivate(){
        return true;
    }
}