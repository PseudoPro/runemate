package com.sudo.v3.spectre.bots.sudodivination.enums;

public enum ConversionType {

    ENERGY,
    XP,
    ENHANCED_XP;


    private String actionName;
    private int index;

    static {
        ENERGY.actionName = "Gain-energy";
        XP.actionName = "Gain-experience";
        ENHANCED_XP.actionName = "Gain-enhanced-experience";

        ENERGY.index = 0;
        XP.index = 1;
        ENHANCED_XP.index = 2;
    }

    public String getActionName() {
        return actionName;
    }

    public int getIndex(){ return index; }
}
