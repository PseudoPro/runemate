package com.sudo.v3.spectre.bots.sudodivination.enums;

/**
 * Created by SudoPro on 11/23/2016.
 */
public enum TraversalLocation
{
    rift,
    wisp,
    cache
}
