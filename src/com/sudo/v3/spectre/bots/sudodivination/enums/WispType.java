package com.sudo.v3.spectre.bots.sudodivination.enums;

/**
 * Created by SudoPro on 11/29/2016.
 */
public enum WispType
{
    enriched,
    normal
}
