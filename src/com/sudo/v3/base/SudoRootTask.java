package com.sudo.v3.base;

import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.GameEvents;
import com.runemate.game.api.hybrid.RuneScape;
import com.runemate.game.api.hybrid.entities.GroundItem;
import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.location.navigation.Traversal;
import com.runemate.game.api.hybrid.region.GroundItems;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.osrs.local.hud.interfaces.OptionsTab;
import com.runemate.game.api.script.Execution;
import com.runemate.game.api.script.framework.core.LoopingThread;
import com.runemate.game.api.script.framework.tree.TreeTask;
import com.sudo.v3.spectre.common.leafs.EmptyLeaf;
import com.sudo.v3.spectre.statics.UserList;
import com.sudo.v3.spectre.statics.UpdateUI;
import javafx.application.Platform;

import java.util.concurrent.TimeUnit;

/**
 * Created by SudoPro on 12/3/2016.
 */
public abstract class SudoRootTask extends SudoBranchTask {
    private GroundItem anagogicOrt;
    private Npc manifestedKnowledgeNpc, serenSpiritNpc;
    private SudoBot bot;
    private boolean checkBlackList = true, checkWhiteList = true;

    public SudoRootTask(SudoBot bot) {
        this.bot = bot;
    }

    @Override
    public TreeTask successTask() {
        if (bot.STOPWATCH.getRuntime() > 300000) {
            UpdateUI.currentTask("Ran out of time configuring GUI.", bot);
            bot.stop("Ran out of time configuring GUI.");
        }
        Execution.delay(1000);
        return new EmptyLeaf(bot);
    }

    @Override
    public boolean validate() {
        UpdateUI.debug("Root -> Waiting on GUI...  " + bot.guiWait);
        return bot.guiWait;
    }

    @Override
    public TreeTask failureTask() {
        // Update user statistics
        if(bot.updateUserStatisticsTimer.hasExpired()){
            if(bot.updateStatistics(0.1, false))
                UpdateUI.debug("Successfully updated user statistics.");

            bot.updateUserStatisticsTimer.reset();
        }

        if(bot.stopAfterMinutes > 0 && bot.STOPWATCH.getRuntime(TimeUnit.MINUTES) >= bot.stopAfterMinutes){
            UpdateUI.currentTask("Bot Stopping: Runtime has reached user-defined stop time.", bot);
            if(bot.readyForBreak()) {
                GameEvents.Universal.LOGIN_HANDLER.disable();
                GameEvents.Universal.LOBBY_HANDLER.disable();
                if(RuneScape.isLoggedIn())
                    RuneScape.logout();
                else
                    bot.stop("Bot Stopping: Runtime has reached user-defined stop time.");
            } else if(!RuneScape.isLoggedIn()) {
                bot.stop("Bot Stopping: Runtime has reached user-defined stop time.");
            }
        }

        if(bot.firstLogin){
            bot.updateStatistics(0, true);
            if (bot.getMetaData().getHourlyPrice().doubleValue() <= 0) {
                if (bot.isValidSession() && bot.isPremiumBot) {
                    //UpdateUI.currentTask("You have [" + UpdateUI.getDurationString(3600 - (bot.trialRunTime / 1000), DurationStringFormat.DESCRIPTION) + "] left in your daily trial", bot);
                    UpdateUI.currentTask("Enjoy your 15 minute trial!", bot);
                }
            }

            new LoopingThread(() ->
                    Platform.runLater(() -> bot.getPlatform().invokeLater(() -> bot.updateInfo())), 500).start();
        }

        if(bot.guiResume){
            UpdateUI.debug("Sessions Settings:");
            UpdateUI.debug("Game: " + (bot.isRS3 ? "RS3" : "OSRS"));
            UpdateUI.debug("Break Enabled: " + bot.breakEnabled);
            if(bot.breakEnabled) {
                UpdateUI.debug("Break Duration: " + bot.breakHandler.getMinBreakDuration() + " - " + bot.breakHandler.getMaxBreakDuration());
                UpdateUI.debug("Play Duration: " + bot.breakHandler.getMinDurationBetweenBreak() + " - " + bot.breakHandler.getMaxDurationBetweenBreak());
            }
            UpdateUI.debug("StopAfterMinutes: " + bot.stopAfterMinutes);
        }

        // Check if the user do me wrong
        if(checkBlackList){
            checkBlackList = false;
            if(UserList.blacklist.contains(Environment.getForumName())){
                UpdateUI.currentTask("You've been blacklisted from all Apex bots.", bot);
                UpdateUI.currentTask("If you believe this is an error, please contact Proxi on the forums.", bot);
                bot.stop("User blacklisted.");
            }

            if(UserList.trialAbusers.contains(Environment.getForumName()) && !bot.isPremiumBot){
                UpdateUI.currentTask("You've been flagged as an ApexBot free trial abuser. Stopping Bot.", bot);
                UpdateUI.currentTask("If you believe this is an error, please contact Proxi on the forums.", bot);
                bot.stop("User flagged as trial abuser.");
            }
        }

        if(checkWhiteList){
            checkWhiteList = false;
            if(UserList.whiteList.contains(Environment.getForumName())){
                UpdateUI.currentTask("You must be pretty special to get this kind of access...", bot);
            }
        }

        if(bot.breakEnabled)
            bot.breakHandler.execute();

        //bot.updateInfo(); // UpdateUI GUI Info

        if (!bot.currentlyBreaking){
            bot.player = Players.getLocal();

            if(bot.player != null) {
                //if(RuneScape.isLoggedIn() && !OptionsTab.areRoofsAlwaysHidden())
                //    OptionsTab.toggleAlwaysHideRoofs();

                if(bot.isDropping)
                    return bot.powerDropLeaf;

                // If user enabled antiban
                if (bot.useAntiBan && !bot.player.isMoving())
                    bot.abHandler.executeAntiBan(bot.antibanList);

                // Make sure playing is running
                if (bot.enableRun && !Traversal.isRunEnabled() && Traversal.getRunEnergy() > bot.runEnergy) {
                    Traversal.toggleRun();

                    // Change runEnergy threshold to a different number
                    bot.runEnergy = 20 + (int) (Math.random() * 30);
                }

                if (bot.isRS3) {
                    if (bot.anagogicOrtTimer.getRemainingTime() <= 0 && bot.checkAnagogic) {
                        anagogicOrt = GroundItems.newQuery().names("Anagogic ort").results().nearest();
                        if (anagogicOrt != null) {
                            UpdateUI.currentTask("Picking up Anagogic Ort and refreshing timer.", bot);
                            if (anagogicOrt.interact("Take", "Anagogic ort"))
                                bot.anagogicOrtTimer.reset();
                        }
                    }

                    if (bot.manifestedKnowledgeTimer.getRemainingTime() <= 0 && bot.checkManifestedKnowledge) {
                        manifestedKnowledgeNpc = Npcs.newQuery().names("Manifested knowledge").results().nearest();
                        if (manifestedKnowledgeNpc != null) {
                            UpdateUI.currentTask("Picking up Manifested knowledge and refreshing timer.", bot);
                            if (manifestedKnowledgeNpc.interact("Siphon", "Manifested knowledge"))
                                bot.manifestedKnowledgeTimer.reset();
                        }
                    }

                    if (bot.serenSpiritTimer.getRemainingTime() <= 0 && bot.checkSerenSpirit) {
                        serenSpiritNpc = Npcs.newQuery().names("Seren spirit").results().nearest();
                        if (serenSpiritNpc != null) {
                            UpdateUI.currentTask("Capturing Seren spirit and refreshing short timer.", bot);
                            if (serenSpiritNpc.interact("Capture", "Seren spirit"))
                                bot.serenSpiritTimer.reset();
                        }
                    }
                }
            }
        }
        return rootTask();
    }

    public abstract TreeTask rootTask();
}