package com.sudo.v3.interfaces;

import com.sudo.v3.base.SudoBot;

/**
 * antiban Interface
 */
public interface IAntiBan {
    void execute(SudoBot bot);
}