package com.sudo.v3.ui.base;

import com.sudo.v3.base.SudoBot;
import com.sudo.v3.ui.Info;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Abstract class to handle all of the constant settings in the Info GUI
 */
public abstract class InfoUIController extends VBox implements Initializable {

    private SudoBot bot;

    private TableColumn<Map.Entry<String, String>, String> infoColumn = new TableColumn<>("Info");
    private TableColumn<Map.Entry<String, String>, String> statsColumn = new TableColumn<>("Stats");

    private int tvSize;

    @FXML
    TableView<Map.Entry<String, String>> Stat_TV;

    @FXML
    ListView<String> CurrentTask_LV, AntiBan_LV;

    @FXML
    Button Settings_BT;//, SudoBuddy_BT;

    @FXML
    Label runtime_L;

    @FXML
    BorderPane tableview_H;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Settings_BT.setOnAction(getSettings_BTEvent());
        //SudoBuddy_BT.setOnAction((getSudoBuddy_BTEvent()));

        CurrentTask_LV.getItems().add("Starting bot...");
        AntiBan_LV.getItems().add("Starting bot...");

        setVisible(true);
    }

    public InfoUIController(SudoBot bot, int tvSize){
        this.bot = bot;
        this.tvSize = tvSize;
    }

    public void update() {
        try {

            Info i = bot.info;

            createStatTable(i);

            runtime_L.textProperty().set("" + i.runTime);

            // Handle the Current Task List View
            if(i.currentTask != null && !CurrentTask_LV.getItems().get(CurrentTask_LV.getItems().size()-1).equals(i.currentTask)) {
                if(CurrentTask_LV.getItems().size() >= 20)
                    CurrentTask_LV.getItems().remove(0);
                CurrentTask_LV.getItems().add(i.currentTask);
                CurrentTask_LV.scrollTo(CurrentTask_LV.getItems().size()-1);
            }

            // Handle the antiban List View
            if(i.abTask != null && !AntiBan_LV.getItems().get(AntiBan_LV.getItems().size()-1).equals(i.abTask)) {
                if(AntiBan_LV.getItems().size() >= 20)
                    AntiBan_LV.getItems().remove(0);
                AntiBan_LV.getItems().add(i.abTask);
                AntiBan_LV.scrollTo(AntiBan_LV.getItems().size()-1);
            }

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private EventHandler<ActionEvent> getSettings_BTEvent(){
        return event -> {
            bot.changeProperty(0); // Go to Settings GUI
        };
    }

//    private EventHandler<ActionEvent> getSudoBuddy_BTEvent(){
//        return event -> {
//            bot.changeProperty(0); // Go to SudoBuddy GUI
//        };
//    }

    private void createStatTable(Info info){

        int size = 24 + (tvSize * 25);

        Stat_TV.setMinHeight(size - 1);
        Stat_TV.setPrefHeight(size);

        tableview_H.setMinHeight(size - 1);
        tableview_H.setPrefHeight(size);

        if(!Stat_TV.getColumns().contains(infoColumn)) {
            Stat_TV.getColumns().addAll(infoColumn, statsColumn);
            Stat_TV.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
            infoColumn.setStyle("-fx-alignment: CENTER-RIGHT;");
            statsColumn.setStyle("-fx-alignment: CENTER-LEFT;");
            Stat_TV.setStyle("-fx-cell-size: 25px;");
        }

        Stat_TV.getItems().clear();
        infoColumn.setCellValueFactory(i -> Bindings.createStringBinding(() -> i.getValue().getKey()));
        statsColumn.setCellValueFactory(i -> Bindings.createStringBinding(() -> i.getValue().getValue()));

        Stat_TV.getItems().setAll(new LinkedHashSet<Map.Entry<String, String>>(){{
            info.xpInfoMap.values().forEach(i -> addAll(i.getMap(bot.STOPWATCH).entrySet()));
            addAll(info.displayInfoMap.entrySet());
        }});

        Stat_TV.getColumns().setAll(infoColumn, statsColumn);
    }
}
