package com.sudo.v3.ui.base;

import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.GameEvents;
import com.sudo.v3.antiban.*;
import com.sudo.v3.base.SudoBot;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.sudo.v3.spectre.statics.enums.DurationStringFormat;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Slider;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

/**
 * Java FX GUI Controller Abstract Class
 *
 * Other SudoBot's setting GUIs will extends this class
 */
public abstract class SudoFXController implements Initializable {
    private SudoBot bot;
    protected boolean rs3;

    //<editor-fold desc="antiban Tab">
    @FXML
    public CheckBox AntiBan_CB, AntiBan_HoverRndObj_CB, AntiBan_HoverRndNpc_CB, AntiBan_HoverRndPlyr_CB, AntiBan_HoverSkill_CB,
            AntiBan_RndDelay_CB, AntiBan_RightClickRndNpc_CB, AntiBan_RightClickRndPlyr_CB, AntiBan_CameraMiddleMouse_CB;

    @FXML
    public Slider AntiBanMin_Slider, AntiBanMax_Slider, StopAfterRuntime_Slider;

    @FXML
    public Text AntiBanSlider_Text;
    //</editor-fold>

    //<editor-fold desc="BreakHandler Tab">
    @FXML
    public CheckBox BreakHandler_CheckBox;

    @FXML
    public Slider MinBreakDuration_Slider, MaxBreakDuration_Slider, MinDurationBetweenBreak_Slider, MaxDurationBetweenBreak_Slider;

    @FXML
    public Text BreakDuration_Text, DurationBetweenBreak_Text, StopAfter_Text;
    //</editor-fold>

    //<editor-fold desc="Settings Tab">
    @FXML
    public CheckBox CameraMiddleMouse_CB, Anagogic_CB, run_CB, manifestedKnowledgeCheckbox, serenSpiritCheckbox;
    //</editor-fold>

    public SudoFXController(SudoBot bot){
        this.bot = bot;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        rs3 = Environment.isRS3();

        AntiBan_CB.setOnAction(getAntiBan_CBEvent());
        BreakHandler_CheckBox.setOnAction(getBreakHandler_CBEvent());

        // Handle antiban Minimum value slider
        AntiBanMin_Slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.doubleValue() > AntiBanMax_Slider.getValue()) {
                AntiBanMax_Slider.setValue(newValue.doubleValue());
                bot.abHandler.setMaxTime(newValue.intValue());
            }
            AntiBanSlider_Text.setText(Integer.toString(newValue.intValue()) + " - " + Integer.toString(bot.abHandler.getMaxTime()) + " seconds");
            bot.abHandler.setMinTime(newValue.intValue());
        });

        // Handle antiban Maximum value slider
        AntiBanMax_Slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.doubleValue() < AntiBanMin_Slider.getValue()) {
                AntiBanMin_Slider.setValue(newValue.doubleValue());
                bot.abHandler.setMinTime(newValue.intValue());
            }
            AntiBanSlider_Text.setText(Integer.toString(bot.abHandler.getMinTime()) + " - " + Integer.toString(newValue.intValue()) + " seconds");
            bot.abHandler.setMaxTime(newValue.intValue());
        });

        // Handle Break Duration Minimum value slider
        MinBreakDuration_Slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.doubleValue() > MaxBreakDuration_Slider.getValue()) {
                MaxBreakDuration_Slider.setValue(newValue.doubleValue());
                bot.breakHandler.setMaxBreakDuration(newValue.intValue() * 60000);
            }
            BreakDuration_Text.setText(Integer.toString(newValue.intValue()) + " - " + Integer.toString(bot.breakHandler.getMaxBreakDuration() / 60000) + " minutes");
            bot.breakHandler.setMinBreakDuration(newValue.intValue() * 60000);
        });

        // Handle Break Duration Maximum value slider
        MaxBreakDuration_Slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.doubleValue() < MinBreakDuration_Slider.getValue()) {
                MinBreakDuration_Slider.setValue(newValue.doubleValue());
                bot.breakHandler.setMinBreakDuration(newValue.intValue() * 60000);
            }
            BreakDuration_Text.setText(Integer.toString(bot.breakHandler.getMinBreakDuration() / 60000) + " - " + Integer.toString(newValue.intValue()) + " minutes");
            bot.breakHandler.setMaxBreakDuration(newValue.intValue() * 60000);
        });

        // Handle Minimum duration between break value slider
        MinDurationBetweenBreak_Slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.doubleValue() > MaxDurationBetweenBreak_Slider.getValue()) {
                MaxDurationBetweenBreak_Slider.setValue(newValue.doubleValue());
                bot.breakHandler.setMaxDurationBetweenBreak(newValue.intValue() * 60000);
            }
            DurationBetweenBreak_Text.setText(Integer.toString(newValue.intValue()) + " - " + Integer.toString(bot.breakHandler.getMaxDurationBetweenBreak() / 60000) + " minutes");
            bot.breakHandler.setMinDurationBetweenBreak(newValue.intValue() * 60000);
        });

        // Handle Maximum duration between break value slider
        MaxDurationBetweenBreak_Slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.doubleValue() < MinDurationBetweenBreak_Slider.getValue()) {
                MinDurationBetweenBreak_Slider.setValue(newValue.doubleValue());
                bot.breakHandler.setMinDurationBetweenBreak(newValue.intValue() * 60000);
            }
            DurationBetweenBreak_Text.setText(Integer.toString(bot.breakHandler.getMinDurationBetweenBreak() / 60000) + " - " + Integer.toString(newValue.intValue()) + " minutes");
            bot.breakHandler.setMaxDurationBetweenBreak(newValue.intValue() * 60000);
        });

        // Handle Stop After Runtime slider
        StopAfterRuntime_Slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            StopAfter_Text.setText(Integer.toString(newValue.intValue()) + " minutes");
            bot.stopAfterMinutes = newValue.intValue();
        });
    }

    public void getStart_BTEvent() {
        // Set Camera to use middle mouse or arrow keys
        bot.useMiddleMouseCamera = CameraMiddleMouse_CB.isSelected();
        UpdateUI.debug("UseMiddleMouse: " + bot.useMiddleMouseCamera);

        bot.checkAnagogic = Anagogic_CB.isSelected();
        bot.checkManifestedKnowledge = manifestedKnowledgeCheckbox.isSelected();
        bot.checkSerenSpirit = serenSpiritCheckbox.isSelected();

        bot.breakEnabled = BreakHandler_CheckBox.isSelected();
        if(bot.breakEnabled) {
            bot.currentlyBreaking = false;
            bot.breakHandler.startTimer();
            bot.getPlatform().invokeLater(() -> {
                UpdateUI.currentTask("Next break will occur once Runtime reaches: " + UpdateUI.getDurationString(bot.STOPWATCH.getRuntime(TimeUnit.SECONDS) + bot.breakHandler.getBotTimer().getRemainingTimeInSeconds(), DurationStringFormat.CLOCK), bot);
                GameEvents.Universal.LOGIN_HANDLER.enable();
            });
        }

        // Configure AntiBans
        if(AntiBan_CB.isSelected()) {
            bot.useAntiBan = true;
            bot.abHandler.setMinTime((int)AntiBanMin_Slider.getValue() * 1000);
            bot.abHandler.setMaxTime((int)AntiBanMax_Slider.getValue() * 1000);
            bot.abHandler.startTimer();
            addAntiBan();
        }
        else {
            bot.antibanList.clear();
            bot.useAntiBan = false;
        }

        AntiBanMin_Slider.setDisable(true);
        AntiBanMax_Slider.setDisable(true);

        UpdateUI.debug("antiban: " + bot.useAntiBan);

        bot.guiWait = false;
        bot.guiResume = true;
        UpdateUI.debug("GuiWait: " + bot.guiWait);

        bot.enableRun = run_CB.isSelected();
        UpdateUI.debug("Run Enabled: " + bot.enableRun);

        bot.changeProperty(1);
    }

    // Enable or disable in the BreakHandler tab based on the enabled checkbox
    private EventHandler<ActionEvent> getBreakHandler_CBEvent()
    {
        return event ->
        {
            MinBreakDuration_Slider.setDisable(!BreakHandler_CheckBox.isSelected());
            MaxBreakDuration_Slider.setDisable(!BreakHandler_CheckBox.isSelected());
            MinDurationBetweenBreak_Slider.setDisable(!BreakHandler_CheckBox.isSelected());
            MaxDurationBetweenBreak_Slider.setDisable(!BreakHandler_CheckBox.isSelected());
            BreakDuration_Text.setDisable(!BreakHandler_CheckBox.isSelected());
            DurationBetweenBreak_Text.setDisable(!BreakHandler_CheckBox.isSelected());
        };
    }

    // Enable or disable controls in the AntiBan tab based on the enabled checkbox
    private EventHandler<ActionEvent> getAntiBan_CBEvent()
    {
        return event ->
        {
            AntiBan_HoverRndObj_CB.setDisable(!AntiBan_CB.isSelected());
            AntiBan_HoverRndNpc_CB.setDisable(!AntiBan_CB.isSelected());
            AntiBan_HoverRndPlyr_CB.setDisable(!AntiBan_CB.isSelected());
            AntiBan_HoverSkill_CB.setDisable(!AntiBan_CB.isSelected());
            AntiBan_RndDelay_CB.setDisable(!AntiBan_CB.isSelected());
            AntiBan_RightClickRndNpc_CB.setDisable(!AntiBan_CB.isSelected());
            AntiBan_RightClickRndPlyr_CB.setDisable(!AntiBan_CB.isSelected());
            AntiBan_CameraMiddleMouse_CB.setDisable(!AntiBan_CB.isSelected());
            AntiBanMin_Slider.setDisable(!AntiBan_CB.isSelected());
            AntiBanMax_Slider.setDisable(!AntiBan_CB.isSelected());
        };
    }

    public void addAntiBan(){
        bot.antibanList.clear();
        if(AntiBan_HoverRndObj_CB.isSelected())
            bot.antibanList.add(new HoverRandomObj());

        if(AntiBan_HoverRndNpc_CB.isSelected())
            bot.antibanList.add(new HoverRandomNpc());

        if(AntiBan_HoverRndPlyr_CB.isSelected())
            bot.antibanList.add(new HoverRandomPlayer());

        if(AntiBan_HoverSkill_CB.isSelected())
            bot.antibanList.add(new HoverSkill());

        if(AntiBan_RndDelay_CB.isSelected())
            bot.antibanList.add(new RandomDelay());

        if(AntiBan_RightClickRndNpc_CB.isSelected())
            bot.antibanList.add(new RightClickRandomNpc());

        if(AntiBan_RightClickRndPlyr_CB.isSelected())
            bot.antibanList.add(new RightClickRandomPlayer());

        if(AntiBan_CameraMiddleMouse_CB.isSelected())
            bot.antibanList.add(new MoveCameraMiddleMouse());

        // Change the antiban interval from seconds to milliseconds
        bot.abHandler.setMinTime(bot.abHandler.getMinTime() * 1000);
        bot.abHandler.setMaxTime(bot.abHandler.getMaxTime() * 1000);

    }
}
