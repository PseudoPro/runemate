package com.sudo.v3.antiban;

import com.sudo.v3.base.SudoBot;
import com.sudo.v3.interfaces.IAntiBan;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.input.Mouse;
import com.runemate.game.api.hybrid.local.Camera;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.Players;

/**
 * Created by SudoPro on 3/4/2016.
 */
public class RightClickRandomPlayer implements IAntiBan
{
    @Override
    public void execute(SudoBot bot) {

        if(bot.player != null) {
            Area aroundPlayer = new Area.Circular(bot.player.getPosition(), 10);

            Player rndPlayer = Players.newQuery().within(aroundPlayer).filter(i -> !i.getName().equals(bot.player.getName())).results().random();
            if (rndPlayer != null) {
                UpdateUI.antiBanTask("Right-Clicking random player: " + rndPlayer.getName(), bot);
                if (rndPlayer.isVisible())
                    Mouse.click(rndPlayer, Mouse.Button.RIGHT);
                else {
                    Camera.concurrentlyTurnTo(rndPlayer);
                    Mouse.click(rndPlayer, Mouse.Button.RIGHT);
                }
            }
        }
    }
}
