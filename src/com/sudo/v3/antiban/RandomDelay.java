package com.sudo.v3.antiban;

import com.sudo.v3.base.SudoBot;
import com.sudo.v3.interfaces.IAntiBan;
import com.sudo.v3.spectre.api.util.SudoTimer;
import com.sudo.v3.spectre.statics.UpdateUI;

/**
 * Created by SudoPro on 3/5/2016.
 */
public class RandomDelay implements IAntiBan
{
    @Override
    public void execute(SudoBot bot) {
        if(bot.player != null) {
            SudoTimer timer = new SudoTimer(3000, 15000);

            UpdateUI.antiBanTask("Performing a random Delay for " + timer.getRemainingTime() / 1000 + " seconds.", bot);
            UpdateUI.currentTask("Performing a random Delay for " + timer.getRemainingTime() / 1000 + " seconds.", bot);
            timer.start();
            while(timer.getRemainingTime() > 0);


        }
    }
}
