package com.sudo.v3.antiban;

import com.sudo.v3.base.SudoBot;
import com.sudo.v3.interfaces.IAntiBan;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.runemate.game.api.hybrid.input.Mouse;
import com.runemate.game.api.hybrid.local.hud.interfaces.Interfaces;
import com.runemate.game.api.script.Execution;

/**
 * Created by SudoPro on 3/4/2016.
 *
 * Hover over the skill icon at the top of the screen
 */
public class HoverSkill implements IAntiBan
{
    @Override
    public void execute(SudoBot bot) {
        if(Interfaces.getAt(1213, 52) != null) {
            UpdateUI.antiBanTask("Hovering over Skill Interface", bot);
            Interfaces.getAt(1213, 52).hover();
            Execution.delayWhile(Mouse::isMoving, 400, 1500);
        }
    }
}
