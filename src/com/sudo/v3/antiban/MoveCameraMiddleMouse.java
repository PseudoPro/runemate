package com.sudo.v3.antiban;

import com.sudo.v3.base.SudoBot;
import com.sudo.v3.interfaces.IAntiBan;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.runemate.game.api.hybrid.input.Mouse;
import com.runemate.game.api.hybrid.local.hud.InteractablePoint;
import com.runemate.game.api.script.Execution;

/**
 * Created by SudoPro on 3/5/2016.
 */
public class MoveCameraMiddleMouse implements IAntiBan
{

    @Override
    public void execute(SudoBot bot) {
        if (bot.player != null) {
            // Set mouse in center
            UpdateUI.antiBanTask("Turning camera with Middle-Mouse Button", bot);
            bot.player.hover();

            // Move camera with mouse
            Mouse.press(Mouse.Button.WHEEL);
            Mouse.move(new InteractablePoint((int) (Mouse.getPosition().getX() - 400 + (Math.random() * 800)), (int) (Mouse.getPosition().getY() - 400 + (Math.random() * 800))));
            Execution.delayWhile(Mouse::isMoving, 200, 750);
            Mouse.release(Mouse.Button.WHEEL);
        }
    }
}
