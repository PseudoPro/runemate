package com.sudo.v3.antiban;

import com.sudo.v3.interfaces.IAntiBan;
import com.sudo.v3.base.SudoBot;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.runemate.game.api.hybrid.entities.Npc;
import com.runemate.game.api.hybrid.local.Camera;
import com.runemate.game.api.hybrid.local.hud.Menu;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.Npcs;
import com.runemate.game.api.script.Execution;

/**
 * Created by SudoPro on 3/4/2016.
 */
public class HoverRandomNpc implements IAntiBan {
    @Override
    public void execute(SudoBot bot) {
        if (bot.player != null && bot.player.getPosition() != null) {
            //Area aroundPlayer = DivMethods.getSurroundingSquare(player.getPosition(), 10);
            Area aroundPlayer = new Area.Circular(bot.player.getPosition(), 10);
            Npc randomNpc = Npcs.newQuery().within(aroundPlayer).results().random();
            if (randomNpc != null) {
                UpdateUI.antiBanTask("Hovering over an NPC: " + randomNpc.getName(), bot);
                if (randomNpc.isVisible()) {
                    randomNpc.hover();
                    Menu.open();
                } else {
                    Camera.concurrentlyTurnTo(randomNpc);
                    Execution.delay(200, 400);
                    randomNpc.hover();
                    Menu.open();
                }
            }
        }
    }
}