package com.sudo.v3.antiban;

import com.runemate.game.api.hybrid.entities.definitions.GameObjectDefinition;
import com.runemate.game.api.hybrid.entities.definitions.ItemDefinition;
import com.sudo.v3.base.SudoBot;
import com.sudo.v3.interfaces.IAntiBan;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.local.Camera;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.script.Execution;

/**
 * Created by SudoPro on 3/4/2016.
 */
public class HoverRandomObj implements IAntiBan
{

    @Override
    public void execute(SudoBot bot) {

        if(bot.player != null && bot.player.getPosition() != null) {
            Area aroundPlayer = new Area.Circular(bot.player.getPosition(), 12);

            GameObject randomObj = GameObjects.newQuery().within(aroundPlayer).results().random();
            GameObjectDefinition itemDef;
            if (randomObj != null) {
                if((itemDef = randomObj.getDefinition()) != null && itemDef.getName() != null) {
                    UpdateUI.antiBanTask("Hovering over random Game Object: " + randomObj.getDefinition().getName(), bot);
                    if (randomObj.isVisible())
                        randomObj.hover();
                    else {
                        Camera.concurrentlyTurnTo(randomObj);
                        Execution.delay(100);
                        randomObj.hover();
                    }
                }
            }
        }
    }
}
