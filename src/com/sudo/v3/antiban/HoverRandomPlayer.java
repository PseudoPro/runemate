package com.sudo.v3.antiban;

import com.sudo.v3.base.SudoBot;
import com.sudo.v3.interfaces.IAntiBan;
import com.sudo.v3.spectre.statics.UpdateUI;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.Camera;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.Execution;

/**
 * Created by SudoPro on 3/4/2016.
 */
public class HoverRandomPlayer implements IAntiBan
{
    @Override
    public void execute(SudoBot bot) {
        if(bot.player != null && bot.player.getPosition() != null) {
            Area aroundPlayer = new Area.Circular(bot.player.getPosition(), 10);

            Player rndPlayer = Players.newQuery().within(aroundPlayer).results().random();
            if (rndPlayer != null) {
                UpdateUI.antiBanTask("Hovering over random player", bot);
                if (rndPlayer.isVisible())
                    rndPlayer.hover();
                else {
                    Camera.concurrentlyTurnTo(rndPlayer);
                    Execution.delay(100);
                    rndPlayer.hover();
                }
            }
        }
    }
}
